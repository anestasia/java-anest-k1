package gui;

import bo.Manage;
import entity.Candidate;
import entity.Experience;
import entity.Fresher;
import entity.Intern;
import util.Validate;

import java.util.ArrayList;

/**
 * Created by Minh on 1/23/2017.
 */
public class Main {
    public static void main(String[] args) {
        Validate validate = new Validate();
        Manage manage = new Manage();
        Display display = new Display();
        ArrayList<Candidate> candidates = new ArrayList<>();
        Candidate candidate;
        int choice;
        do {
            display.showMenu();
            choice = validate.getInt("Enter your choice: ", "Invalid choice", 1, 5);
            switch (choice) {
                case 1:
                    do {
                        candidate = new Experience();
                        ((Experience)candidate).input();
                        candidates.add(candidate);
                    } while (validate.pressYN());
                    display.showAll(candidates);
                    break;
                case 2:
                    do {
                        candidate = new Fresher();
                        ((Fresher)candidate).input();
                        candidates.add(candidate);
                    } while (validate.pressYN());
                    display.showAll(candidates);
                    break;
                case 3:
                    do {
                        candidate = new Intern();
                        ((Intern)candidate).input();
                        candidates.add(candidate);
                    } while (validate.pressYN());
                    display.showAll(candidates);
                    break;
                case 4:
                    do {
                        display.showAll(candidates);
                        manage.searchCandidate(candidates);
                    } while (validate.pressYN());
                    break;
            }
        } while (choice != 5);
    }
}
