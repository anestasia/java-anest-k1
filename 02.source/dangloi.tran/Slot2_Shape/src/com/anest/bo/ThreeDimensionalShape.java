/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

/**
 *
 * @author Tran Dang Loi
 */
public abstract class ThreeDimensionalShape implements Shape{

    @Override
    public abstract double calPri();

    @Override
    public abstract double calArea();
    
}
