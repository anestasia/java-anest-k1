package bo;


import common.Common;
import java.util.Scanner;

public class Circle extends TwoDimensionalShape{

    private float radius;
    private float area;
    private float verimeter;

    public Circle() {
    }

    public Circle(float radius, float area, float verimeter) {
        this.radius = radius;
        this.area = area;
        this.verimeter = verimeter;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getVerimeter() {
        return verimeter;
    }

    public void setVerimeter(float verimeter) {
        this.verimeter = verimeter;
    }

    @Override
    public float calVerimeter() {
        return (float) (getRadius() * 2 * Math.PI);
    }

    @Override
    public float calArea() {
        return (float) (getRadius() * getRadius() * Math.PI);
    }

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Enter the radius for your circle: ");
            setRadius(in.nextFloat());
        } while (!Common.isNumber(in.nextLine())||Common.isPositveNumber(in.nextFloat()));
    }

    @Override
    public void output() {
        System.out.println("Your circle has verimeter is: "+ calVerimeter()+"\n");
        System.out.println("Your circle has area is:"+ calArea());
    }
   
   

}
