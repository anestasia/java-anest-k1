package bo;

import com.sun.org.apache.xpath.internal.operations.Number;

import java.util.Scanner;

/**
 * Created by W10-PRO on 23-Jan-17.
 */
public class Convert {
    Scanner sc = new Scanner(System.in);
    Validate val = new Validate();

    public void convBinary(int output) {
        String str;
        do {
            str = val.getString("Binary number: ");
        } while (!val.isBinary(str));
        int num = Integer.parseInt(str, 2);
        if (output == 2) {
            System.out.print("Decimal number: " + num);
        }
        if (output == 3) {
            System.out.print("Hexadecimal number: " + Integer.toHexString(num));
        }
    }

    public void convDecimal(int output) {
        String str;
        do {
            str = val.getString("Decimal number: ");
        } while (!val.isDecimal(str));
        int num = Integer.parseInt(str);
        if (output == 1) {
            System.out.print("Binary number: " + Integer.toBinaryString(num));
        }
        if (output == 3) {
            System.out.print("Hexadecimal number: " + Integer.toHexString(num));
        }
    }

    public void convHex(int output) {
        String str;
        do {
            str = val.getString("Hexadecimal number: ");
        } while (!val.isHex(str));
        if (output == 1) {
            System.out.print("Binary number: " + Integer.toBinaryString(Integer.parseInt(str, 16)));
        }
        if (output == 2) {
            System.out.print("Decimal number: " + Integer.parseInt(str, 16));
        }
    }
}
