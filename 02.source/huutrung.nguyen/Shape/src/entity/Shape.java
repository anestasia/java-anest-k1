/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 * @author admin A shape of universal
 */
public interface Shape {
	/**
	 * The function to calculate area of the shape
	 * 
	 * @return area of the shape
	 */
	public double calArea();

	/**
	 * The function to input properties of entity from the keyboard
	 */
	public void input();

	/**
	 * The function to show the area and other properties of shape.
	 */
	public void output();
}

