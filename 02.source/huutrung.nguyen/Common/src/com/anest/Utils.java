package com.anest;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Monkey D.Luffy
 */
public class Utils {

    static Scanner in = new Scanner(System.in);

    /**
     * This function for testing
     */
    public static void main(String[] args) {
        Utils a = new Utils();
    }

    /**
     * The function to get a double from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public double getDouble(String mess, String error, double min, double max) {
        String doubleNum;
        double num = 0;
        boolean check = true;
        while (check) {
            System.out.print(mess);
            doubleNum = in.nextLine();
            try {
                num = Double.parseDouble(doubleNum);
            } catch (NumberFormatException e) {
                System.out.println(error);
                continue;
            }
            if (num >= min && num <= max) {
                check = false;
            } else {
                System.out.println(error);
            }
        }
        return num;
    }

    /**
     * The function to get an integer from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public int getInt(String mess, String error, int min, int max) {
        String intNum;
        int num = 0;
        boolean check = true;
        while (check) {
            System.out.print(mess);
            intNum = in.nextLine();
            try {
                num = Integer.parseInt(intNum);
            } catch (NumberFormatException e) {
                System.out.println(error);
                continue;
            }
            if (num >= min && num <= max) {
                check = false;
            } else {
                System.out.println(error);
            }
        }
        return num;
    }

    /**
     * The function to get an number in a base under decimal from the keyboard
     * in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param base : base number you want get
     * @return an integer from keyboard
     */
    public int getNBaseUnderDec(String mess, String error, int base) {
        String nBase;
        while (true) {
            System.out.print(mess);
            nBase = in.nextLine();
            if (nBase.matches("[0-" + (base - 1) + "]+")) {
                break;
            }
            System.out.println(error);
        }
        return Integer.parseInt(nBase);
    }

    /**
     * The function to get an hexadecimal number from the keyboard in console
     * application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @return an integer from keyboard
     */
    public String getHexBase(String mess, String error) {
        String hex;
        while (true) {
            System.out.print(mess);
            hex = in.nextLine();
            if (hex.matches("[0-9a-fA-F]+")) {
                break;
            }
            System.out.println(error);
        }
        return hex;
    }

    /**
     * Press any key to continue
     *
     * @param mess : The <code>message</code>
     * @param yes : yes
     * @param no : no
     * @return <code>true</code> if user want to continue "yes",
     * <code>false</code>
     */
    public boolean OptionChoice(String mess, String yes, String no) {
        String choice;
        do {
            System.out.print(mess);
            choice = in.next();
        } while (!(choice.equalsIgnoreCase(yes) || choice.equalsIgnoreCase(no)));
        return choice.equalsIgnoreCase(yes);
    }

    public String getGraduateRank(String mess, String error) {   //get a graduated rank
        String graRank;
        while (true) {
            System.out.print(mess);
            graRank = in.nextLine();
            if (graRank.equalsIgnoreCase("excellence") || graRank.equalsIgnoreCase("good")
                    || graRank.equalsIgnoreCase("fair") || graRank.equalsIgnoreCase("poor")) {
                break;
            }
            System.out.println(error);
        }
        return graRank;
    }

    public String getPhone(String mess, String error, int n) {
        String phone;
        while (true) {
            System.out.print(mess);
            phone = in.nextLine();
            if (phone.matches("\\d+") || phone.length() >= n) {
                break;
            }
            System.out.println(error);
        }
        return phone;
    }

    public String getEmail(String mess, String error) {
        String email;
        while (true) {
            System.out.print(mess);
            email = in.nextLine();
            if (email.matches("^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$")) {
                break;
            }
            System.out.println(error);
        }
        return email;
    }
}
