/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BO.Validator;
import Entity.Matrix;

/**
 *
 * @author Cong Minh
 */
public class Main {
    public static void main(String[] args) {
        Matrix m = new Matrix();
        int start,end;
        int size = Validator.inputInt("Enter size of matrix: ");
        m.SetMatrix(size);
        m.SetN(size);
        m.RandomMatrix();
        m.ShowMatrix();
        while(true){
        do{
        start = Validator.inputInt("Enter start point: ");
        if(start<=0||start>=size) System.out.println("Invalid input!");
        }while(start<=0||start>=size);
        do{
        end = Validator.inputInt("Enter end point: ");
        if(end<=0||end>=size) System.out.println("Invalid input!");
        }while(end<=0||end>=size);
        if(m.CheckEdge(start, end)) System.out.println("This is an edge");
        else System.out.println("This is not an edge");
            System.out.println("");
    }
    }
}
