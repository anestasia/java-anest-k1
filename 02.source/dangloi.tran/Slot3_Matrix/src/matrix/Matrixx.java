/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author Tran Dang Loi
 */
public class Matrixx {

    private int[][] matrix;
    private int e;

    public Matrixx(int[][] matrix, int e) {
        this.matrix = matrix;
        this.e = e;
    }

    public Matrixx() {
    }

    public void inputM() {
        System.out.println("Enter e=");
        int e = new Scanner(System.in).nextInt();
        this.e = e;
        this.matrix = new int[e][e];
        for (int i = 0; i < e; i++) {
            for (int j = 0; j < e; j++) {
                matrix[i][j] = Math.abs(new Random().nextInt() % 2);
                System.out.println("matrix[" + i + "][" + j + "]=" + matrix[i][j]);

            }
        }

    }

    public boolean isEdge(int i, int j) {
        if (matrix[i][j] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void printGraph() {
        for (int i = 0; i < e; i++) {
            System.out.println("");
            for (int j = 0; j < e; j++) {
                System.out.print("  " + matrix[i][j]);
            }
        }
    }

}
