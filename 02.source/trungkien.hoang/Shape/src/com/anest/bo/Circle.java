/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Circle extends TwoDimensionalShape{
    
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public Circle() {
       
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }


    @Override
    public double calPri() {
        return 2*this.r*Math.PI;
    }

    @Override
    public double calArea() {
        return this.r*this.r*Math.PI;
    
    }
    
    @Override
    public void input(){
        System.out.print("Enter R: ");
        this.r = new Scanner(System.in).nextDouble();
                
    }
}
