/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Tran Dang Loi
 */
public class Triangle extends TwoDimensionalShape {

    private double e1;
    private double e2;
    private double e3;

    public Triangle() {
    }

    public Triangle(double e1, double e2, double e3) {
        this.e1 = e1;
        this.e2 = e2;
        this.e3 = e3;
    }

    public double getE1() {
        return e1;
    }

    public double getE2() {
        return e2;
    }

    public double getE3() {
        return e3;
    }

    public void setE1(double e1) {
        this.e1 = e1;
    }

    public void setE2(double e2) {
        this.e2 = e2;
    }

    public void setE3(double e3) {
        this.e3 = e3;
    }

    @Override
    public double calPri() {
        return this.e1 + this.e2 + this.e3;
    }

    @Override
    public double calArea() {
        return Math.sqrt((e1 + e2 + e3) / 2 * ((this.e1 + this.e2 + this.e3)/2 - e1) * ((this.e1 + this.e2 + this.e3)/2 - this.e2) * ((this.e1 + this.e2 + this.e3)/2 - this.e3));
    }

    @Override
    public void input() {
        System.out.println("Enter e1 = ");
        this.e1 = new Scanner(System.in).nextDouble();
        System.out.println("Enter e2 = ");
        this.e2 = new Scanner(System.in).nextDouble();
        System.out.println("Enter e3 = ");
        this.e3 = new Scanner(System.in).nextDouble();
    }

}
