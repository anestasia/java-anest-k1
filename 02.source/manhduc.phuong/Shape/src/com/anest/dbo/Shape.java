/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

/**
 *
 * @author Cpt
 */
public interface Shape {
    /**
     * the function to calculate perimeter
     * @return 
     */
    public double calArea();
    public double calPri();
    public void input();
}
