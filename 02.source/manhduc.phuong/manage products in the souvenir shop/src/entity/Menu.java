/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import bo.Validation;

/**
 *
 * @author Taka
 */
public class Menu {

    Validation vl = new Validation();
    Installization ini = new Installization();
    FileDBContent db = new FileDBContent();

    public void showMenu() {
        System.out.println("WELCOME TO PRODUCT MANAGEMENT\n"
                + "1. Create\n"
                + "2. Load from file\n"
                + "3. Sort (by price) and Find (by id)\n"
                + "4. Update / Delete\n"
                + "5. Report and save.\n"
                + "6. Exit and save\n");
    }

    public void chooseAnOption() {
        boolean checkContinue;
        String string = null;
        do {
            showMenu();
            int choice = vl.setInteger("Please choose an option: ", 1, 6);
            switch (choice) {
                case 1:
                    ini.create();
                    break;
                case 2:
                    db.loadingFile(ini.getList_product());
                    if (ini.getList_product().isEmpty()) {
                        System.out.println("The list is empty!");
                        ini.create();
                    } else {
                        System.out.println("Loading data fromn file sucessfully!");
                    }
                    break;
                case 3:
                    choice = vl.setInteger("Which option do you want to choose?"
                            + "\n1. Sort (by price)"
                            + "\n2. Find (by id)", 1, 2);
                    switch (choice) {
                        case 1:
                            ini.sortByPrice();
                            break;
                        case 2:
                            string = ini.getVl().setString("Input ID to find: ");
                            if (ini.findByID(string)) {
                                System.out.println("Found!");
                            } else {
                                System.out.println("Not Found!!");
                            }
                            break;
                    }
                    break;
                case 4:
                    string = ini.getVl().setString("Input item's ID to update or delete: ");
                    if (ini.findByID(string)) {
                        char choiceC = vl.checkUpdateOrDelete("Which option do you want to choose?"
                                + "\nU. Update  "
                                + "\nD. Delete  ");
                        switch (choiceC) {
                            case 'U':
                                ini.update(string);
                                break;
                            case 'D':
                                ini.delete(string);
                                break;
                        }
                        db.writeToFile(ini.getList_product());
                    }else{
                        System.out.println("ID's not existed");
                    }
                    break;
                case 5:
                    ini.reportAndSave();
                    break;
                case 6:
                    db.writeToFile(ini.getList_product());
                    System.exit(0);
            }
            checkContinue = vl.checkContinue("Do you want to continue? Y/N? "
                    + "\n(Y to return to main menu, N to exit the program) ");
        } while (checkContinue);
        if (!checkContinue) {
            System.out.println("EXIT_SUCCESSFUL");
            System.exit(0);
        }
    }
}
