/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.Display;
import bo.Input;
import entity.Product;
import java.util.ArrayList;
import static bo.Input.*;
import entity.Category;
import java.io.IOException;
/**
 *
 * @author Cong Minh
 */
public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<Product> product = new ArrayList();
        System.out.println("1. Creat");
        System.out.println("2. Load from file");
        System.out.println("3. Save to file");
        System.out.println("4. Exit");
        System.out.println("5. Display all");
        
        int choice;
        while(true){
            System.out.println("---------------");
        choice = inputInt("Enter your choice: ", 5, 1);
        switch(choice){
            case 1: 
                System.out.println("1. Book");
                System.out.println("2. Cloth");
                System.out.println("3. Disc");
                int choice1;
                choice1 = inputInt("Enter your choice: ", 3, 1);
                switch(choice1){
                    case 1: 
                        product.add(creat(Category.Book, product));
                        break;
                    case 2: 
                        product.add(creat(Category.Cloth, product));
                        break;
                    case 3: 
                        product.add(creat(Category.Disc, product));
                }
                break;
            case 2: 
                Input i = new Input();
                i.getAllItemsFromFile(product);
                break;
            case 3: 
                Input t = new Input();
                t.writeAllItemsToFile(product);
                break;
            case 4: 
                System.exit(0);
            case 5: 
                Display display = new Display();
                System.out.println("----------Book----------");
                display.displayAll(product, Category.Book);
                System.out.println("----------Cloth----------");
                display.displayAll(product, Category.Cloth);
                System.out.println("----------Disc----------");
                display.displayAll(product, Category.Disc);
        }
    }
}
}