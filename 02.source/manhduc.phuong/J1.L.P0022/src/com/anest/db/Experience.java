package com.anest.db;

import com.anest.Validator.ValidValue;
import java.util.ArrayList;


public class Experience extends Candidate{
    private int expInYear;
    private String proSkill;

    public Experience(int Candidatetype) {
        super(Candidatetype);
    }   
       
    public void setExperienceInfo(ArrayList<Candidate> list){
        ValidValue vl = new ValidValue(list);
        setCandidateInfo(list);
        System.out.println("Exp in year: ");
        this.expInYear = vl.setInt(100, 0);
        System.out.println("Pro skill: ");
        this.proSkill = vl.setString();
        
    }
    
    
}
