package bo;


import common.Common;
import java.util.Scanner;

public class Triangle extends TwoDimensionalShape{

    private float edge1, edge2, edge3;
    private float area;
    private float verimeter;
    private float p = (edge1+edge2+edge3)/2;

    public Triangle() {
    }

    public Triangle(float edge1, float edge2, float edge3, float area, float verimeter) {
        this.edge1 = edge1;
        this.edge2 = edge2;
        this.edge3 = edge3;
        this.area = area;
        this.verimeter = verimeter;
    }

    public float getEdge1() {
        return edge1;
    }

    public void setEdge1(float edge1) {
        this.edge1 = edge1;
    }

    public float getEdge2() {
        return edge2;
    }

    public void setEdge2(float edge2) {
        this.edge2 = edge2;
    }

    public float getEdge3() {
        return edge3;
    }

    public void setEdge3(float efde3) {
        this.edge3 = edge3;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getVerimeter() {
        return verimeter;
    }

    public void setVerimeter(float verimeter) {
        this.verimeter = verimeter;
    }

    @Override
    public float calArea() {
        float p = (getEdge1()*getEdge2()*getEdge3())/2;
        return (float) Math.sqrt(p*(p-getEdge1())*(p-getEdge2())*(p-getEdge3()));
    }

    @Override
    public float calVerimeter() {
    return (getEdge1()+getEdge2()+getEdge3());
    }

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        do {
            do {
                System.out.println("Enter the first edge: ");
                setEdge1(in.nextFloat());
            } while (!Common.isNumber(in.nextLine())||Common.isPositveNumber(in.nextFloat()));
            do {
                System.out.println("Enter the second edge: ");
                setEdge2(in.nextFloat());
            }while (!Common.isNumber(in.nextLine())||Common.isPositveNumber(in.nextFloat()));
            do {
                System.out.println("Enter the third edge: ");
                setEdge3(in.nextFloat());
            } while (!Common.isNumber(in.nextLine()));
        } while (checkTangle(getEdge1(), getEdge2(), getEdge3()));
    }

    public boolean checkTangle(float edge1, float edge2, float edge3) {
        if (edge1 + edge2 <= edge3 || edge1 + edge3 <= edge2 || edge2 + edge3 <= edge1) {
            return (false);
        } else {
            return (true);
        }
    }

    @Override
    public void output() {
        System.out.println("Your triangle has verimeter is: "+ calVerimeter()+"\n");
        System.out.println("Your triangle has area is:"+ calArea());
    }
    

}
