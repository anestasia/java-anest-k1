/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Taka
 */
public class FileDBContent {

    public void loadingFile(ArrayList<Product> list) {
        list.clear();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("souvenir shop.txt")));
            try {
                String line = reader.readLine();
                while (line != null) {
                    String[] str = line.trim().split("\\|");
                    if (str[0].trim().equals(Categories.Book.toString())) {
                        Book book = new Book(Categories.Book, Integer.parseInt(str[1].trim()), str[2].trim(), str[3].trim(), Double.parseDouble(str[4].trim()));
                        list.add(book);
                    } else if (str[0].trim().equals(Categories.Clothing.toString())) {
                        Clothing clothing = new Clothing(Categories.Clothing, Integer.parseInt(str[1].trim()), str[2].trim(), str[3].trim(), Double.parseDouble(str[4].trim()));
                        list.add(clothing);
                    } else if (str[0].trim().equals(Categories.Disc.toString())) {
                        Disc disc = new Disc(Categories.Disc, Integer.parseInt(str[1].trim()), str[2].trim(), str[3].trim(), Double.parseDouble(str[4].trim()));
                        list.add(disc);
                    }
                    line = reader.readLine();
                }
            } catch (IOException ex) {
                Logger.getLogger(FileDBContent.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileDBContent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeToFile(ArrayList<Product> list) {
        File file = new File("souvenir shop.txt");
        FileWriter fw;
        try {
            fw = new FileWriter(file.getAbsoluteFile(), false);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("Total: " + list.size());
            for (int i = 0; i < list.size(); i++) {
                bw.newLine();
                bw.write(list.get(i).toString());
            }
            bw.close();
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileDBContent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeAtTheEndOfFile(Product p) {
        File file = new File("souvenir shop.txt");
        FileWriter fw;
        try {
            fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.newLine();
            bw.write(p.toString());
            bw.close();
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileDBContent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
