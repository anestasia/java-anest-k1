/**
 *
 */
package com.anest.k1.product.ui;

import com.anest.k1.product.bo.ListProduct;
import com.anest.k1.product.bo.Valid;
import com.anest.k1.product.entity.Book;
import com.anest.k1.product.entity.Clothing;
import com.anest.k1.product.entity.DISC;
import com.anest.k1.product.entity.ECat;
import com.anest.k1.product.entity.Product;
import java.util.Collections;

/**
 * @author dell
 *
 */
public class Application {

    public static void main(String[] args) {
        Valid valid = new Valid();
        ListProduct products = new ListProduct();
        do {
            System.out.println("WELCOME TO PRODUCT MANAGEMENT \n\t 1. Create\n\t 2. Load from file\n\t 3. Sort(by price)\n\t 4. Exit and save\n "
                    + "(Please choose 1 to Create, 2 to Load from the file, 3 to Find and Sort, 4 to Exit");
            int choice = valid.getIntNumber("", 4, 1);
            if (choice == 4) {
                break;
            }
            Product pro = null;
            switch (choice) {
                case 1:
                    System.out.println("\t 1. Book\n\t 2. Clothing\n\t 3. Disc ");
                    int choice1 = valid.getIntNumber("Enter your option: ", 3, 1);
                    switch (choice1) {
                        case 1:
                            pro = new Book();
                            break;
                        case 2:
                            pro = new Clothing();
                            break;
                        case 3:
                            pro = new DISC();
                            break;
                    }
                    pro.input();
                    products.addProduct(pro);
                    products.writeFile("new_product.data");
                    products.show();
                    break;
                case 2:
                    products.loadFromFile("new_product.data");
                    products.show();
                    break;
                case 3:
                    Collections.sort(products.getProducts());
                    products.show();
                break;
                default:
                    break;
            }
        } while (true);
    }

}
