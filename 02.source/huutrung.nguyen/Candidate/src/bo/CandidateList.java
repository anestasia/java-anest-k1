/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Monkey D.Luffy
 */
public class CandidateList {

    List<Candidate> candidateList;

    public CandidateList() {
        candidateList = new ArrayList<>();
    }

    public void showList() {    //Display list
        sortByExp();
        sortByGradTime();
        sortBySemester();
        System.out.println("========== Experience Candidate =========");
        candidateList.forEach((a) -> {  //Display experiences in list
            if (a instanceof Experience) {
                System.out.println(a.getFirstName() + " " + a.getLastName());
            }
        });
        System.out.println("========== Fresher Candidate ==========");
        candidateList.forEach((a) -> {  //Display freshers in list
            if (a instanceof Fresher) {
                System.out.println(a.getFirstName() + " " + a.getLastName());
            }
        });
        System.out.println("========== Intern Candidate ==========");
        candidateList.forEach((a) -> {  //Display interns in list
            if (a instanceof Intern) {
                System.out.println(a.getFirstName() + " " + a.getLastName());
            }
        });
    }

    public List<String> search(String str) {    //Search by name
        String s = str.toLowerCase();
        ArrayList<String> resultList = new ArrayList();
        candidateList.stream().filter((a) -> (a.getFirstName().toLowerCase().contains(s)
                || a.getLastName().toLowerCase().contains(s))).forEachOrdered((a) -> {
            resultList.add(a.toString());
        });
        if (!resultList.isEmpty()) {
            return resultList;
        }
        return null;
    }

    public void sortByExp() {   //If experience, sort by years of experience
        Collections.sort(candidateList, (Candidate o1, Candidate o2)
                -> ((Experience) o1).getYearOfExp() - ((Experience) o2).getYearOfExp());
    }

    public void sortByGradTime() {  //If fresher, sort by graduated time
        Collections.sort(candidateList, (Candidate o1, Candidate o2)
                -> ((Fresher) o1).getGradTime() - ((Fresher) o2).getGradTime());
    }

    public void sortBySemester() {  //If semester, sort by semester
        Collections.sort(candidateList, (Candidate o1, Candidate o2)
                -> ((Intern) o1).getSemester().compareToIgnoreCase(((Intern) o2).getSemester()));
    }
    
    public void add(Candidate can){
        candidateList.add(can);
    }
    
    public int size(){
        return candidateList.size();
    }
}
