/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author Monkey D.Luffy
 */

import bo.ShapeList;
import bo.Utils;
import entity.Shape;
import entity.threeDimensional.Cube;
import entity.threeDimensional.Sphere;
import entity.threeDimensional.Tetrahedron;
import entity.twoDimensional.Circle;
import entity.twoDimensional.Square;
import entity.twoDimensional.Triangle;

public class Application {

    public static void main(String[] args) {
        ShapeList shapeList = new ShapeList();
        do {
            // random from 1 to 7
            System.out.print("*Enter your choice : \n\t 1. Add a circle \n\t 2. Add a triangle"
                    + " \n\t 3. Add a square \n\t 4. Add a cube \n\t 5. Add a sphere \n\t "
                    + "6. Add a tetrahedron \n\t 7. Cancel \n Your choice: ");
            int choice = Utils.getIntNumber("", 7, 1);
            if (choice == 7) {
                break;
            }
            Shape shape = null;
            switch (choice) {
                case 1:
                    shape = new Circle();
                    break;
                case 2:
                    shape = new Triangle();
                    break;
                case 3:
                    shape = new Square();
                    break;
                case 4:
                    shape = new Cube();
                    break;
                case 5:
                    shape = new Sphere();
                    break;
                case 6:
                    shape = new Tetrahedron();
                    break;
                default:
                    break;
            }
            shape.input();
            shapeList.add(shape);
        } while (true);
        System.out.println(" Show list : ");
        shapeList.showList();

    }

}
