package com.anest.db;

import com.anest.Validator.ValidValue;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cpt
 */
public class Intern extends Candidate{
    private String majors;
    private String semester;
    private String universityName;

    public Intern(int candidatetype) {
        super(candidatetype);
    }
    
    public void setInternInfo(ArrayList<Candidate> list){
        setCandidateInfo(list);
        ValidValue vl = new ValidValue(list);
        System.out.println("Majors: ");
        this.majors = vl.setString();
        System.out.println("Semester: ");
        this.semester = vl.setString();
        System.out.println("University: ");
        this.universityName = vl.setString();
    }
    
}
