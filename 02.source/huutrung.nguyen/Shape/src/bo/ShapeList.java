/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

/**
 *
 * @author Monkey D.Luffy
 */
import java.util.ArrayList;
import java.util.List;

import entity.Shape;
import entity.threeDimensional.Cube;
import entity.threeDimensional.Sphere;
import entity.threeDimensional.Tetrahedron;
import entity.twoDimensional.Circle;
import entity.twoDimensional.Square;
import entity.twoDimensional.Triangle;

public class ShapeList {

    private List<Shape> shapes;

    /**
     * The constructor without parameters
     */
    public ShapeList() {
        this.shapes = new ArrayList<>();
    }

    /**
     * @param shapes
     */
    public ShapeList(List<Shape> shapes) {
        super();
        this.shapes = shapes;
    }

    /**
     * this function to appends the specified element to the end of this list
     *
     * @param shape
     */
    public void add(Shape shape) {
        this.shapes.add(shape);
    }

    /**
     * this function to removes the element at the specified position in this
     * list
     *
     * @param index
     */
    public void removeAt(int index) {
        this.shapes.remove(index);
    }

    /**
     * @param area
     * @return
     */
    public Shape searchArea(double area) {
        for (int i = 0; i < this.shapes.size() - 1; ++i) {
            if (this.shapes.get(i).calArea() == area) {
                return this.shapes.get(i);
            }
        }
        return null;
    }

    /**
     * this function to show all shapes
     */
    public void showList() {
        if (this.shapes.isEmpty()) {
            System.err.println(" empty! ");
        }
        for (Shape shape : this.shapes) {
            shape.output();
        }
    }

    /**
     * this function to get all circle of list shapes
     *
     * @return
     */
    public List<Circle> getCircles() {
        List<Circle> circles = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape instanceof Circle) {
                circles.add((Circle) shape);
            }
        }
        return circles;
    }

    /**
     * this function to get all triangle of list shapes
     *
     * @return
     */
    public List<Triangle> getTriangles() {
        List<Triangle> triangles = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape instanceof Triangle) {
                triangles.add((Triangle) shape);
            }
        }
        return triangles;
    }

    /**
     * this function to get all square of list shapes
     *
     * @return
     */
    public List<Square> getSquares() {
        List<Square> squares = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape instanceof Square) {
                squares.add((Square) shape);
            }
        }
        return squares;
    }

    /**
     * this function to get all cube of list shapes
     *
     * @return
     */
    public List<Cube> getCubes() {
        List<Cube> cubes = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape instanceof Cube) {
                cubes.add((Cube) shape);
            }
        }
        return cubes;
    }

    /**
     * this function to get all sphere of list shapes
     *
     * @return
     */
    public List<Sphere> getSpheres() {
        List<Sphere> spheres = new ArrayList<>();
        for (Shape shape : shapes) {
            if (shape instanceof Sphere) {
                spheres.add((Sphere) shape);
            }
        }
        return spheres;
    }

    /**
     * this function to get all tetrahedron of list shapes
     *
     * @return
     */
    public List<Tetrahedron> getTetrahedrons() {
        List<Tetrahedron> tetrahedrons = new ArrayList<>();
        for (Shape shape : shapes) {
            if (tetrahedrons instanceof Tetrahedron) {
                tetrahedrons.add((Tetrahedron) shape);
            }
        }
        return tetrahedrons;
    }

}
