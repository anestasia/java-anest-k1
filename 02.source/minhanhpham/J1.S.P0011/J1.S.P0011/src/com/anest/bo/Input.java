/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class Input {

    public static String inputBin() {
        System.out.print("Enter a Binary: ");
        Scanner in = new Scanner(System.in);
        String bin;
        while (true) {
            try {
                bin = in.nextLine();
                int dec = Integer.parseInt(bin, 2);
                System.out.println("---" + dec);
                return bin;
            } catch (Exception e) {
                System.out.print("Enter a Binary:");
            }
        }

    }

    public static String inputHex() {
        System.out.print("Enter a Hexadecimal: ");
        Scanner in = new Scanner(System.in);
        String hex;
        while (true) {
            try {
                hex = in.nextLine();
                int dec = Integer.parseInt(hex, 16);
                return hex;
            } catch (Exception e) {
                System.out.print("Enter a Hexadecimal:");
            }
        }

    }

    public static int inputDec() {
        System.out.print("Enter an Integer > 0: ");
        Scanner in = new Scanner(System.in);
        int n;
        while (true) {
            try {
                n = Integer.parseInt(in.nextLine());
                if (n < 0) {
                    System.out.print("Enter an Integer >0 :");
                } else {
                    return n;
                }
            } catch (Exception e) {
                System.out.print("Enter an Integer > 0");
            }
        }
    }
}
