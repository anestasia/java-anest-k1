/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.until;

import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Input {

    public double getNumber(String mes, String err, double min, double max) {
        Validate validate = new Validate();
        Scanner in = new Scanner(System.in);
        String s;
        double number;
        while (true) {
            System.out.print(mes);
            s = in.nextLine();
            s = s.trim();
            if (validate.isNumber(s)) {
                number = Double.parseDouble(s);
                if (number < min || number > max)
                    System.out.println(err);
                else
                    return number;
            } else
                System.out.println(err);
        }
    }

    public boolean pressYN() {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String s;
        System.out.print("Do you want to continue?(press y to continue)");
        s = in.nextLine().trim();
        if(s.equalsIgnoreCase("y")) 
            return true;
        else
            return false;
        }
}