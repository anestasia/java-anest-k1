/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.bo;

import candidate.until.Input;
import candidate.until.Validate;
import com.sun.xml.internal.stream.Entity;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Deverloper {

    private double id;
    private String firstName;
    private String lastName;
    private int birthDate;
    private String address;
    private String phone;
    private String email;
    private int candidateTpye;

    public void input() {
        Input input = new Input();
        Validate validate = new Validate();
        Scanner in = new Scanner(System.in);
        System.out.println("\nInput database\n");
        this.id = input.getNumber("Enter ID: ", "Invalid ID number", 0, Double.MAX_VALUE);
        System.out.print("Enter first name: ");
        this.firstName = in.nextLine().trim();
        System.out.print("Enter last name: ");
        this.lastName = in.nextLine().trim();
        do {
            this.birthDate = (int) input.getNumber("Enter birth date: ", "Invalid year", 0, Double.MAX_VALUE);
        } while (!validate.isYear(1990, this.birthDate));
        System.out.print("Enter adress: ");
        this.address = in.nextLine().trim();
        do {
            System.out.print("Enter phone number: ");
            this.phone = in.nextLine().trim();
        } while (!validate.isPhone(this.phone));
        do {
            System.out.print("Enter emall: ");
            this.email = in.nextLine().trim();
        } while (!validate.isEmail(this.email));
    }

    public void showName() {
        System.out.println(this.firstName + this.lastName);
    }

    public void showInfor() {
        System.out.println(this.firstName + this.lastName + " | " + this.birthDate + " | " + this.address + " | " + this.phone + " | " + this.email + " | " + this.candidateTpye);
    }

    public Deverloper(Double id, String firstName, String lasteName, int birthDate, String address, String phone, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lasteName;
        this.birthDate = birthDate;
        this.address = address;
        this.phone = phone;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Deverloper{" + "id=" + id + ", firstName=" + firstName + ", lasteName=" + lastName + ", birthDate=" + birthDate + ", address=" + address + ", phone=" + phone + ", email=" + email + '}';
    }

    public Deverloper() {
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCandidateTpye() {
        return candidateTpye;
    }

    public void setCandidateTpye(int candidateTpye) {
        this.candidateTpye = candidateTpye;
    }

}
