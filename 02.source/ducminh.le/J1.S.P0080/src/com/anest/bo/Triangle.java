package com.anest.bo;

import com.anest.util.Input;

/**
 * Created by Minh on 1/13/2017.
 */
public class Triangle extends TwoDimensionalShape {
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle() {
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public boolean isTriangle(double sideA, double sideB, double sideC) {
        if((sideA + sideB) > sideC && (sideB + sideC) > sideA && (sideA + sideC) > sideB) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void input() {
        Input input = new Input();
        do {
            this.sideA = input.getDouble("Enter side A: ", "Invalid number", 0, Double.MAX_VALUE);
            this.sideB = input.getDouble("Enter side B: ", "Invalid number", 0, Double.MAX_VALUE);
            this.sideC = input.getDouble("Enter side C: ", "Invalid number", 0, Double.MAX_VALUE);
        } while(!isTriangle(sideA, sideB, sideC));
    }

    @Override
    public void printResult() {
        System.out.println("Perimeter: " + getPerimeter());
        System.out.println("Area: " + getArea());
    }

    @Override
    public double getPerimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public double getArea() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * halfPerimeter - sideC);
    }

    @Override
    public double getVolume() {
        return 0;
    }
}
