/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Book;
import entity.Category;
import entity.Clothe;
import entity.Disc;
import entity.Product;
import java.util.ArrayList;
import entity.Category;

/**
 *
 * @author Cong Minh
 */
public class Display {

    public void displayAll(ArrayList<Product> lst, Category category) {
        String current;
        for (int i = 0; i < lst.size(); i++) {
            if (lst.get(i).getCategory().equalsIgnoreCase(category.toString())) {

                if (lst.get(i).getCategory().equalsIgnoreCase(Category.Book.toString())) {
                    Book book = (Book) lst.get(i);
                    current = book.getCategory() + "|" + book.getId() + "|"
                            + book.getTitle() + "|" + book.getAuthor() + "|"
                            + book.getPrice();
                    System.out.println(current);
                } else if (lst.get(i).getCategory().equalsIgnoreCase(Category.Cloth.toString())) {
                    Clothe clothe = (Clothe) lst.get(i);
                    current = clothe.getCategory() + "|" + clothe.getId() + "|"
                            + clothe.getDepartment() + "|" + clothe.getBrand() +"|"+
                            + clothe.getPrice();
                    System.out.println(current);
                } else {
                    Disc disc = (Disc) lst.get(i);
                    current = disc.getCategory() + "|" + disc.getId() + "|" + disc.getGenre() + "|"
                            + disc.getFormat() + "|" + disc.getPrice();
                    System.out.println(current);
                }
            }
        }
    }

}

