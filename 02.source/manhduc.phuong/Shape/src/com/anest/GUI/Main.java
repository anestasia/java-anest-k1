/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.GUI;

import com.anest.dbo.Circle;
import com.anest.dbo.Cube;
import com.anest.dbo.Shape;
import com.anest.dbo.Sphere;
import com.anest.dbo.Square;
import com.anest.dbo.Tetrahedron;
import com.anest.dbo.Triangle;
import com.anest.validator.ValidContinue;
import com.anest.validator.ValidInt;

/**
 *
 * @author Cpt
 */
public class Main {

    public static void main(String[] args) {
        boolean checkContinue = true;
        while (checkContinue) {
            System.out.println("Enter 0 to create a list of Two demensional Shape");
            System.out.println("Enter 1 to create a list of Three Demensional Shape");
            int choice = ValidInt.getIntNumber("", 1, 0);
            Shape[] shapes = new Shape[3];
            switch (choice) {
                case 0:
                    for (int i = 0; i < 3; i++) {
                        if (i == 0) {
                            shapes[0] = new Circle();                         
                        } else if (i == 1) {
                            shapes[1] = new Square();
                        } else if (i == 2) {
                            shapes[2] = new Triangle();
                        }
                        shapes[i].input();
                        System.out.println(shapes[i]);
                    }                        
                    break;
                case 1:
                    for (int i = 0; i < 3; i++) {
                        if (i == 0) {
                            shapes[i] = new Sphere();
                        } else if (i == 1) {
                            shapes[i] = new Cube();
                        } else if (i == 2) {
                            shapes[i] = new Tetrahedron();
                        }
                        shapes[i].input();
                        System.out.println(shapes[i]);
                    }
                    break;
            }
            checkContinue = ValidContinue.checkContinue();
        }
    }
}
