package Shape;

public abstract class Shape {

    public Shape() {
    }

    public abstract void calPer();
    
    public abstract void calArea();

    public abstract void calV();
}
