/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Cong Minh
 */
public class Disc extends Product{
    private String genre;
    private String format;

    public Disc() {
    }

    public Disc(String genre, String format, String id, Category category, 
            double price) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
    
}
