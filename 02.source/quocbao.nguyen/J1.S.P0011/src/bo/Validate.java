package bo;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Validate {

    public boolean isBinary(String str) {
        char[] arr = str.toCharArray();
        for (char checkBinary : arr) {
            if (!(checkBinary == '1' || checkBinary == '0')) {
                return false;
            }
        }
        return true;
    }

    public String getString(String mes) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mes);
        return sc.nextLine();
    }

    public boolean isDecimal(String str) {
        for (int i = 1; i < str.length() - 1; i++) {
            if (!(str.charAt(i) >= '0' && str.charAt(i) <= '9')) return false;
        }
        return true;
    }

    public boolean isHex(String str) {
        boolean check;
        try {
            // try to parse the string to an integer, using 16 as radix
            int t = Integer.parseInt(str, 16);
            // parsing succeeded, string is valid hex number
            check = true;
        } catch (NumberFormatException e) {
            // parsing failed, string is not a valid hex number
            check = false;
        }
        return (check);
    }

    public int getInt(String mes, String error, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        while (true) {
            try {
                System.out.print(mes);
                number = sc.nextInt();
                if (number < min || number > max) {
                    System.out.println(error);
                    sc.nextLine();
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }

    public boolean YNquestion() {
        Scanner sc = new Scanner(System.in);
        String str;
        do {
            System.out.println("");
            System.out.println("================");
            System.out.print("Do you want to continue?  ");
            str = sc.nextLine();
            if (str.equalsIgnoreCase("y")) {
                break;
            } else if (str.equalsIgnoreCase("n")) return false;
            else System.out.println("Error! Please enter Y/N!");
        } while (true);
        return true;
    }


    public boolean isRank(String str) {
        if (str.equalsIgnoreCase("good")) return true;
        else if (str.equalsIgnoreCase("excellence")) return true;
        else if (str.equalsIgnoreCase("fair")) return true;
        else if (str.equalsIgnoreCase("poor")) return true;
        else System.out.println("Error!!!");
        return false;
    }
}
