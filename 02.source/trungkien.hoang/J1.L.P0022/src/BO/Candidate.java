/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import util.Input;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public  class Candidate {
   
    public int ID;
    public int DateOfBirth;
    public CanType CandidateType;
    public String FirstName;
    public String LastName;
    public String Address;
    public String Email;
    public String Phone;

    public Candidate(int ID, int DateOfBirth, CanType CandidateType, String FirstName, String LastName, String Address, String Email, String Phone) {
        this.ID = ID;
        this.DateOfBirth = DateOfBirth;
        this.CandidateType = CandidateType;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Address = Address;
        this.Email = Email;
        this.Phone = Phone;
    }

    public Candidate() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(int DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public CanType getCandidateType() {
        return CandidateType;
    }

    public void setCandidateType(CanType CandidateType) {
        this.CandidateType = CandidateType;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }
    

    public void input(){
        
        Validator validate = new Validator();
        Input input = new Input();
        int x;
        Scanner scan = new Scanner(System.in);
        this.ID = (int)input.getNumber("Enter the Candidate's Id", "Invalid value! Please enter number only", 0, Integer.MAX_VALUE);
        System.out.println(" The Candidate type : ");
        System.out.println("0. Experience");
        System.out.println("1. Fresher");
        System.out.println("2. Intern"); 
        x = (int)input.getNumber("Please choose the type(0-2): ", "Invalid Input! Please enter number only", 0, 2);
        switch(x)
        {
            case 0: 
                this.CandidateType = CanType.Experience;
                break;
            case 1: 
                this.CandidateType = CanType.Fresher;
                break;
            case 2: 
                this.CandidateType = CanType.Intern;
                break;            
                
        }
        System.out.print("Input the Cadidate's First Name : ");
        this.FirstName = scan.nextLine().trim();
        System.out.print("Input the Candidate's Lat Name: ");
        this.LastName = scan.nextLine().trim();
        this.DateOfBirth = (int)input.getNumber("Input the Candidate's Birthday", "Invalid value! Please try again", 1900, Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        do
        {
            System.out.print("Input the Candidate's phone number : ");
            this.Phone = scan.nextLine().trim();
        }
        while(!validate.checkPhone(this.Phone));
        do
        {
            System.out.println("Input the Candidate's E-mail : ");
            this.Email = scan.nextLine().trim();
        }
        while(!validate.checkMail(this.Email));

    }
     public void showName()
    {
        System.out.println(this.FirstName+""+this.LastName);
    }
}
