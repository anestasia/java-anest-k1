package com.anest.entity;

import com.anest.bo.CandidateList;
import java.util.List;
import java.util.Scanner;

public class Candidate {

    Scanner in = new Scanner(System.in);
    private String Candidate_ID;
    private String First_Name;
    private String Last_Name;
    private int BirthDate;
    private String Address;
    private String Phone;
    private String email;
    protected int type;
    Condition valid = new Condition();

    public Candidate(String Candidate_ID, String First_name, String Last_Name, int BirthDate, String Address, String Phone, String email) {
        this.Candidate_ID = Candidate_ID;
        this.First_Name = First_name;
        this.Last_Name = Last_Name;
        this.BirthDate = BirthDate;
        this.Address = Address;
        this.Phone = Phone;
        this.email = email;
    }

    public Candidate() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCandidate_ID() {
        return Candidate_ID;
    }

    public void setCandidate_ID(String Candidate_ID) {
        this.Candidate_ID = Candidate_ID;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String First_Name) {
        this.First_Name = First_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String Last_Name) {
        this.Last_Name = Last_Name;
    }

    public int getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(int BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    @Override
    public String toString() {
        return Candidate_ID + " | " + First_Name + " | " + Last_Name + " | " + BirthDate + " | " + Address + " | " + Phone + " | " + email;
    }

    public void Create(List<Candidate> can) {
        do {
            System.out.println("Enter your ID:");
            Candidate_ID = in.nextLine();
        } while (valid.idChecker(Candidate_ID, can) == false);
        First_Name = valid.CheckName("Enter your first name:");
        Last_Name = valid.CheckName("Enter your last name:");
        BirthDate = valid.CheckBirthDate("Enter your birthdate:");
        System.out.println("Enter your address:");
        Address = in.nextLine();
        Phone = valid.CheckPhone("Enter your phone:");
        do {
            System.out.println("Enter your email:");
            email = in.nextLine();
        } while (valid.mailChecker(email) == false);

    }

}
