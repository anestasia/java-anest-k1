/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;


/**
 *
 * @author Cpt
 */
public class Circle extends TwoDimensionalShape{
    
    private double r;

    public Circle() {
    }

    public Circle(double r) {
        this.r = r;
    }
    
    
    

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    
    /**
     * Caculate Circle's Area
     * @return 
     */
    @Override
    public double calArea() {
        return this.r*this.r*Math.PI;
    }

    @Override
    public double calPri() {
        return 2*this.r*Math.PI;
    }

    @Override
    public void input() {
        this.r = ValidDouble.getDoubleNumber("Enter circle r = ",Double.MAX_VALUE,0);
    }

    @Override
    public String toString() {
        return "Circle's pri " + calPri() + " Circle's Area: " + calArea();
    }
    
    
}
