package com.anest.bo;

import com.anest.util.Input;

/**
 * Created by Minh on 1/14/2017.
 */
public class Sphere extends ThreeDimensionalShape {
    private double radius;

    public Sphere() {
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void input() {
        Input input = new Input();
        this.radius = input.getDouble("Enter radius: ", "Invalid number", 0, Double.MAX_VALUE);
    }

    @Override
    public void printResult() {
        System.out.println("Area: " + getArea());
        System.out.println("Volume: " + getVolume());
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return 4 * Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getVolume() {
        return 4 / 3 * Math.PI * Math.pow(radius, 3);
    }
}
