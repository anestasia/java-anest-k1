/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Experience extends Candidate {

    private int expInYear;
    private String proSkill;
    Valid valid = new Valid();

    public Experience() {
    }

    public Experience(String id, String firtsName, String lastName, int birthDate, String address, int phone, String email, int expInYear, String proSkill) {
        super(id, firtsName, lastName, birthDate, address, phone, email);
        this.expInYear = expInYear;
        this.proSkill = proSkill;
        super.type = 0;
    }

    public int getExpInYear() {
        return expInYear;
    }

    public void setExpInYear(int expInYear) {
        this.expInYear = expInYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + expInYear + " | " + proSkill + " | " + type;
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        this.id = valid.setIdTest("Enter ID: ");
        System.out.println("Enter First Name: ");
        this.firtsName = s.nextLine();
        System.out.println("Enter Last Name: ");
        this.lastName = s.nextLine();
        this.birthDate = valid.setBirthDate("Enter birthdate: ");
        System.out.println("Enter address : ");
        this.address = s.nextLine();
        this.phone = valid.setPhoneTest("Enter your phone: ");
        this.email = valid.setEmail();
        this.expInYear = valid.setExpInYear("Enter year of experience: ");
        System.out.println("Enter proSkill : ");
        this.proSkill = s.nextLine();
    }

    @Override
    public Object callDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callCreat() {
        return new Experience(this.id, this.firtsName, this.lastName, this.birthDate, this.address, this.phone, this.email, this.expInYear, this.proSkill);
    }

}
