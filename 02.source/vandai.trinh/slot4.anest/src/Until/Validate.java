/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Until;

import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Validate {

    private String Hex = "1234567890ABCDEF";
    private Scanner in = new Scanner(System.in);

    public boolean isBinary(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '1' && s.charAt(i) != '0') {
                System.out.println("Not a binary!!");
                return false;
            }
        }
        return true;
    }

    public boolean isDec(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                System.out.println("Not a decimal!!");
                return false;
            }
        }
        return true;
    }

    public boolean isHex(String s) {
        for (int i = 0; i < s.length(); i++) {
            String temp = "";
            temp += s.charAt(i);
            if (!Hex.contains(temp)) {
                System.out.println("Not a hex!!");
                return false;
            }
        }
        return true;
    }

    public boolean isNumber(String s) {
        while (true) {
            try {
                double number = Double.parseDouble(s);
                return true;
            } catch (NumberFormatException n) {
                System.out.println("Not a number");
                return false;
            }
        }
    }

    public double getNumber(String mes, String err, double min, double max) {
        String s;
        double number;
        Validate validate = new Validate();
        while (true) {
            System.out.print(mes);
            s = in.nextLine();
            s = s.trim();
            if (validate.isNumber(s)) {
                number = Double.parseDouble(s);
                if (number < min || number > max) {
                    System.out.println(err);
                } else {
                    return number;
                }
            } else {
                System.out.println(err);
            }
        }
    }

    public boolean pressYN() {
        in.useDelimiter("\n");
        String s;
        System.out.print("Do you want to continue?(press y to continue): ");
        s = in.nextLine().trim();
        if (s.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }

    public String nomalizeString(String string) {
        string.trim();
        String[] arr = string.split(" ");
        String trueString = "";
        for (String s : arr) {
            if (!s.isEmpty()) {
                trueString += s + " ";
            }
        }
        trueString.trim();
        return trueString;
    }

    public static void main(String[] args) {
        Validate validate = new Validate();
        if (validate.isHex("AF")) {
            System.out.println("fsdfs");
        }
    }
}
