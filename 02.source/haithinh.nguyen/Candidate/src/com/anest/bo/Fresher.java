/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Fresher extends Candidate {

    private int graDate;
    private String gradRank;
    private String education;
    Valid valid = new Valid();

    public Fresher(String id, String firtsName, String lastName, int birthDate, String address, int phone, String email, int GraDate, String GradRank) {
        super(id, firtsName, lastName, birthDate, address, phone, email);
        this.graDate = GraDate;
        this.gradRank = GradRank;
        super.type = 1;
    }

    public Fresher() {
    }

    public int getGraDate() {
        return graDate;
    }

    public void setGraDate(int graDate) {
        this.graDate = graDate;
    }

    public String getGradRank() {
        return gradRank;
    }

    public void setGradRank(String gradRank) {
        this.gradRank = gradRank;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + graDate + " | " + gradRank + " | " + education + " | " + type;
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        this.id = valid.setIdTest("Enter ID: ");
        System.out.println("Enter First Name: ");
        this.firtsName = s.nextLine();
        System.out.println("Enter Last Name: ");
        this.lastName = s.nextLine();
        this.birthDate = valid.setBirthDate("Enter birthdate: ");
        System.out.println("Enter address : ");
        this.address = s.nextLine();
        this.phone = valid.setPhoneTest("Enter your phone: ");
        this.email = valid.setEmail();
        this.graDate = valid.setGraDate();
        this.gradRank = valid.setRank();
        System.out.println("Enter education : ");
        this.education = s.nextLine();
    }

    @Override
    public Object callDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callCreat() {
        return new Fresher(this.id, this.firtsName, this.lastName, this.birthDate, this.address, this.phone, this.email, this.graDate, this.gradRank);
    }

}
