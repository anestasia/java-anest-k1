package com.anest.db;

import com.anest.Validator.ValidValue;
import java.util.ArrayList;

public class Candidate {

    
    private String candidateId;
    private int phone;
    private int birthDate;
    private int candidatetype;
    private String firstName;
    private String lastName;
    private String address;
    private String email;


    public Candidate() {
        
    }

    public Candidate(int candidatetype) {
        this.candidatetype = candidatetype;
    }

    public int getCandidatetype() {
        return candidatetype;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCandidateId() {
        return candidateId;
    }

    public String getEmail() {
        return email;
    }

    public int getPhone() {
        return phone;
    }

    
    
    public void setCandidateInfo(ArrayList<Candidate> list) {
        ValidValue vl = new ValidValue(list);
        this.candidateId = vl.setCandidateID("Enter Candidate ID: ");
        this.phone = vl.validPhoneNumber("Phone: ", phone);
        System.out.println("Last Name: ");
        this.lastName = vl.setString();
        System.out.println("First Name: ");
        this.firstName = vl.setString();
        System.out.println("BirthDate: ");
        this.birthDate = vl.setYear();
        System.out.println("Address: ");
        this.address = vl.setString();
        this.email = vl.setEmail();
    }

    @Override
    public String toString() {
        return (lastName + "|" + firstName + "|" + birthDate + "|" + address + "|" + phone + "|" + email + "|" + candidatetype);
    }

}
