/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import util.Input;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Intern extends Candidate{

    String Major;
    String University;
    int Semester;
    
    public Intern(String Major, String University, int Semester) {
        this.Major = Major;
        this.University = University;
        this.Semester = Semester;
    }

    public Intern() {
    }
    
   

    public String getMajor() {
        return Major;
    }

    public String getUniversity() {
        return University;
    }

    public int getSemester() {
        return Semester;
    }

    public void setMajor(String Major) {
        this.Major = Major;
    }

    public void setUniversity(String University) {
        this.University = University;
    }

    public void setSemester(int Semester) {
        this.Semester = Semester;
    }
    
    @Override
   public void input()
   {
       super.input();
       Input input = new Input();
       Scanner scan =  new Scanner(System.in);
       
       System.out.print("Input the Candidate's university: ");
       this.University = scan.nextLine().trim();
       System.out.print("Input the Candidate's Major: ");
       this.Major = scan.nextLine().trim();
       this.Semester = (int)input.getNumber("Input the Candidate's semester: ", "Invalid Value! Please try again", 1, 50);
       
   }
      
}
