/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Book extends Product {

    private String author;
    private String title;

    public Book() {
    }

    public Book(String id, ECat category, String author, String title, int price) {
        super(id, category, price);
        this.author = author;
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return super.getId() + " | " + super.getCategory() + " | " + 
                title + " | " + author + " | " + super.getPrice(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void input() {
        super.input(); //To change body of generated methods, choose Tools | Templates.
        super.setCategory(ECat.BOOK);
        System.out.print("Title: ");
        title = new Scanner(System.in).nextLine();
        System.out.print("Author: ");
        author= new Scanner(System.in).nextLine();
    }
    
    
}
