/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;
import com.anest.validator.ValidInt;
import javax.xml.validation.Validator;

/**
 *
 * @author Cpt
 */
public class Tetrahedron extends ThreeDimensionalShape {

    private double a;

    public Tetrahedron() {
    }

    public Tetrahedron(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public double calPri() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double calArea() {
        return Math.sqrt(3) * this.a * this.a;
    }


    @Override
    public double volume() {
        return Math.pow(this.a, 3)/(6*Math.sqrt(2));
    }

    @Override
    public void input() {
        this.a = ValidDouble.getDoubleNumber("Enter tetrahedron a: ",Double.MAX_VALUE,0);
    }

    @Override
    public String toString() {
        return "Tetrahedron's volume: " + volume() + " Tetrahedron's Area: " + calArea() + " Tetrahendron doesn't has Pri ";
    }

    
}
