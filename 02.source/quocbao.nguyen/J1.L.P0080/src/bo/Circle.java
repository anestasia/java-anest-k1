package bo;

import utir.Validate;

import java.util.Scanner;

/**
 * Created by W10-PRO on 13-Jan-17.
 */
public class Circle extends TwoDim {
    private double radius;
    Scanner sc = new Scanner(System.in);

    public Circle() {
        radius = 0;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getVolumn() {
        return 0;
    }

    @Override
    public void input() {
        Validate validate = new Validate();
        System.out.println("CIRCLE");
        setRadius(validate.getDouble("Validate radius: ", "Please enter a float number!!!"));
    }
}
