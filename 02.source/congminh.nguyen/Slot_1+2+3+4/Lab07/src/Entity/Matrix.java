/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import BO.Validator;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Cong Minh
 */
public class Matrix {
    public int n;
    int[][] matrix ;

    public Matrix() {
    }

    public Matrix(int n) {
        this.n = n;
    }

    public void SetN(int n) {
        this.n = n;
    }

    public void SetMatrix(int n) {
        matrix = new int[n][n];
    }
    
    
    
    public void RandomMatrix(){
//        this.n =n;
        Random r = new Random();
        for(int i=0;i<n;i++){
            for( int j=0;j<n;j++){
                matrix[i][j] = r.nextInt(2);
            }
        }
                
    }
    public void ShowMatrix(){
        System.out.println("The matrix is: ");
        for(int i=0;i<n;i++){
            for( int j=0;j<n;j++){
                System.out.print(matrix[i][j]+"  ");
            }
            System.out.println("");
        }
    }
    
    public boolean CheckEdge(int a, int b){
        if(matrix[a][b]==1) return true;
        return false;
    }
}
