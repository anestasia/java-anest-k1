/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author OPF_Lep
 */
public class TenToOthers {

    static int n;
    static int base;
    static String base36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static StringBuffer s = new StringBuffer("");

    public TenToOthers(int n, int base) {
        this.n = n;
        this.base = base;
    }

    public static StringBuffer convert(int n, int base) {
        TenToOthers.n = n;
        TenToOthers.base = base;
        do {
            s.append(base36.charAt(n % base));
            n /= base;
        } while (n != 0);
        return s.reverse();
    }

}
