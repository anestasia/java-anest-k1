package bo;

import java.util.Calendar;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {

    Calendar ca = Calendar.getInstance();
    int currenYear = ca.get(Calendar.YEAR);

    public boolean isBirthDate(String s) {
        if (s.length() > 4 || !isNumber(s)) {
            return (false);
        } else {
            int temp = Integer.parseInt(s);
            if (temp < 1900 || temp > currenYear) {
                return (false);
            } else {
                return (true);
            }
        }
    }

    public boolean isNumber(String str) {
        try {
            long d = Long.parseLong(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean isDouble(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public boolean isPhone(String s) {
        if (isNumber(s) && s.length() >= 10) {
            return (true);
        } else {
            return (false);
        }
    }

    public int getInt(String msg, String error, int min, int max) {
        String s;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println(msg);
            s = sc.nextLine();
            if (!isNumber(s) || (Integer.parseInt(s) < min || Integer.parseInt(s) > max)) {
                System.out.println(error);
            }
        } while (!isNumber(s) || (Integer.parseInt(s) < min || Integer.parseInt(s) > max));
        return (Integer.parseInt(s));
    }

    public boolean getYN(String msg) {
        String temp = new String();
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println(msg);
            temp = scanner.nextLine();
            if (!temp.equalsIgnoreCase("n") && !temp.equalsIgnoreCase("y")) {
                System.out.println("Please enter yes or no!");
            }
        } while (!temp.equalsIgnoreCase("n") && !temp.equalsIgnoreCase("y"));
        return (temp.equalsIgnoreCase("y"));
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,6}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

}
