/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoIt;

import Validate.Validate;

/**
 *
 * @author Nguyen Phong
 */
public class Experience extends Candidate{
    
    private int explnYear;
    private String proSkill;

    @Override
    public void setCandidate() {
        super.setCandidate();
        explnYear = Validate.validYearExp();
        System.out.print("Enter Professionall Skill : ");
        proSkill = in.nextLine();

    }

    @Override
    public void setCandidateType(int candidateType) {
        super.setCandidateType(0);
    }

    public int getExplnYear() {
        return explnYear;
    }

    public void setExplnYear(int explnYear) {
        this.explnYear = explnYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }

}
