/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.twoDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import bo.Utils;

public class Square extends TwoDimensionalShape {

    /**
     * One edge of the square
     */
    private double edge;

    /**
     * The constructor without parameters
     */
    public Square() {
        super();
        this.edge = 0;
    }

    public Square(double edge) {
        this.edge = edge;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.Shape#output()
     */
    @Override
    public void output() {
        // TODO Auto-generated method stub
        System.out.println(" Square : \t One edge : " + this.edge);
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Pri  :" + this.calPri());
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#calArea()
     */
    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        return this.edge * this.edge;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#calPri()
     */
    @Override
    public double calPri() {
        // TODO Auto-generated method stub
        return 4 * this.edge;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#input()
     */
    @Override
    public void input() {
        // TODO Auto-generated method stub
        this.edge = Utils.getDoubleNumber("Enter square : ", Double.MAX_VALUE, 0);
    }

}
