/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;

/**
 *
 * @author Taka
 */
public class Disc extends Product {

    private String genre;
    private String format;

    //Dùng khi đọc dữ liệu từ file để tạo mới phần tử
    public Disc(Categories category,int id, String genre, String format,  double price) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    //Dùng khi khởi tạo sản phẩm trong menu
    public Disc(Categories categories) {
        super(categories);
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setGenre() {
        String genre1 = vl.setString("Please input disc's genre: ");
        this.genre = genre1;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat() {
        String format1 = vl.setString("Please input disc's format: ");
        this.format = format1;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void input(ArrayList<Product> list) {
        super.input(list); //To change body of generated methods, choose Tools | Templates.
        setGenre();
        setFormat();
    }

    @Override
    public String toString() {
        return this.getCategory() + " | " + this.getId() + " | " + genre + " | " + format + " | " + this.getPrice();
    }

}
