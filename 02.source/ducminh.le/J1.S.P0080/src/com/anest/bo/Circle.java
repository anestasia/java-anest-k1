package com.anest.bo;

import com.anest.util.Input;

/**
 * Created by Minh on 1/13/2017.
 */
public class Circle extends TwoDimensionalShape {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void input() {
        Input input = new Input();
        this.radius = input.getDouble("Enter radius: ", "Invalid number", 0, Double.MAX_VALUE);
    }

    @Override
    public void printResult() {
        System.out.println("Perimeter: " + getPerimeter());
        System.out.println("Area: " + getArea());
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getVolume() {
        return 0;
    }
}