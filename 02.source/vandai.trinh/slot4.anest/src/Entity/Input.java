/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import Until.Validate;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Input {
    private String inputString;
    Validate validate = new Validate();
    
    public void inputBinary() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Enter base binary : ");
            this.inputString = in.nextLine().trim();
        }while(!validate.isBinary(inputString));
    }
    
    public void inputDec() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Enter base decimal : ");
            this.inputString = in.nextLine().trim();
        }while(!validate.isDec(inputString));
    }
    
    public void inputHex() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Enter base hex : ");
            this.inputString = in.nextLine().trim();
        }while(!validate.isHex(inputString));
    }

    public String getInputString() {
        return inputString;
    }

    public void setInputString(String inputString) {
        this.inputString = inputString;
    }

    public Validate getValidate() {
        return validate;
    }

    public void setValidate(Validate validate) {
        this.validate = validate;
    }
    
    
}
