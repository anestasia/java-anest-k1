/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Product;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Validate implements Serializable {

    private static final long serialVersionUID = 1L;
//validate String

    public boolean stringChecker(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isAlphabetic(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
//validate integer

    public int readInt(String mes, int min, int max) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n;
        System.out.println(mes);
        while (true) {
            try {
                n = Integer.parseInt(br.readLine());
                if (n < min || n > max) {
                    System.out.println("RE - ENTER !");
                } else {
                    return n;
                }
            } catch (Exception e) {
                System.out.println("INVALID ! RE - ENTER !");
            }
        }
    }

    public double readDoub(String mes, double min, double max) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double n;
        System.out.println(mes);
        while (true) {
            try {
                n = Double.parseDouble(br.readLine());
                if (n < min || n > max) {
                    System.out.println("RE - ENTER");
                } else {
                    return n;
                }
            } catch (Exception e) {
                System.out.println("INVALID ! RE - ENTER !");
            }
        }
    }
//check continue

    public boolean isContinue(String mes) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s;
        System.out.print(mes);
        while (true) {
            try {
                s = br.readLine().trim().toLowerCase();
                if (s.length() != 1 || (s.charAt(0) != 'y' && s.charAt(0) != 'n')) {
                    System.out.print("RE-ENTER: ");
                } else if (s.charAt(0) == 'y') {
                    return true;
                } else if (s.charAt(0) == 'n') {
                    return false;
                }
            } catch (Exception e) {
            }
        }
    }
    
// check update or remove 
        public String isOption(String mes) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s;
        System.out.print(mes);
        while (true) {
            try {
                s = br.readLine().trim().toLowerCase();
                if (s.length() != 1 || (s.charAt(0) != 'u' && s.charAt(0) != 'r')) {
                    System.out.print("RE-ENTER: ");
                } else if (s.charAt(0) == 'u') {
                    return "u";
                } else if (s.charAt(0) == 'r') {
                    return "r";
                }
            } catch (Exception e) {
            }
        }
    }
//Check duplicate

    public boolean isChecker(int id, List<Product> lst) {
        for (int i = 0; i < lst.size(); i++) {
            return false;
        }
        return true;
    }
}
