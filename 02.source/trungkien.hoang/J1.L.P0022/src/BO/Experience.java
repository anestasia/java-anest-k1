/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import util.Input;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Experience extends Candidate{
    
    int ExpInYear;
    String ProSkill;

    public Experience(int ID, int DateOfBirth, CanType CandidateType, String FirstName, String LastName, String Address, String Email, String Phone) {
        super(ID, DateOfBirth, CandidateType, FirstName, LastName, Address, Email, Phone);
    }

  
    public Experience() {
    }

    public int getExpInYear() {
        return ExpInYear;
    }

    public String getProSkill() {
        return ProSkill;
    }

    public void setExpInYear(int ExpInYear) {
        this.ExpInYear = ExpInYear;
    }

    public void setProSkill(String ProSkill) {
        this.ProSkill = ProSkill;
    }
    
    @Override
    public void input()
    {
        super.input();
        Validator validate = new Validator();
        Input input = new Input();
        Scanner scan = new Scanner(System.in);
        this.ExpInYear = (int)input.getNumber("Input the experience year: ", "Invalid Value! Please Try Again", 0, 100);
        System.out.println("Input the Professional Skills: ");
        this.ProSkill = scan.nextLine();
    }
}
