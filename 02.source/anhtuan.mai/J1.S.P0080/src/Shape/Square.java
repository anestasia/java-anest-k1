package Shape;

public class Square extends Shape {

    private double edge;

    public Square() {
    }

    public Square(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public void calV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void calPer() {
        System.out.println("Tinh chu vi" + edge * 4);
    }

    @Override
    public void calArea() {
        System.out.println("Tinh dien tich" + edge * edge);
    }

}
