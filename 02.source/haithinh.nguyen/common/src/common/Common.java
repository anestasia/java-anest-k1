/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import static com.edu.fpt.tring.StringUtility.nomarLine;
import static com.edu.fpt.validate.Validator.validateDouble;
import static com.edu.fpt.validate.Validator.validateInt;

/*
 *
 * @author Thinh
 */
public class Common {
    public static void main(String[] args) {
        double a= validateDouble(0.0,10.0,"so thuc a [0,10] :");
        System.out.println("so thuc a= "+a);
        String st = nomarLine();
        System.out.println("chuoi la: "+st);
        int b= validateInt(0,20,"so nguyen b [0,20]:");
        System.out.println("so nguyen b= "+b);
    } 
    
}
