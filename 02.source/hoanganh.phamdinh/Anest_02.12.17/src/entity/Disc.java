/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Disc extends Product {

    private String genre;
    private String format;
    private static final long serialVersionUID = 1L;

    public Disc() {
    }

    public Disc(int id, Catagory category, double price, String genre, String format) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void input(ArrayList<Product> list) {
        super.input(list);
        Scanner sc = new Scanner(System.in);
        this.setCategory(Catagory.Disc);
        System.out.print(" Genre = ");
        this.genre = new Scanner(System.in).nextLine().trim();
        System.out.print(" Format = ");
        this.format = new Scanner(System.in).nextLine().trim();
    }

    @Override
    public String toString() {
        return " | " + getId() + " | " + getCategory() + " | " + genre + " | " + format + " | " + getPrice();
    }

}
