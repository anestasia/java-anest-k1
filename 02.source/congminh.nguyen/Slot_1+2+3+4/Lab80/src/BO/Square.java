/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cong Minh
 */
public class Square extends TwoDimensionalShape {

    private double edge;

    public Square() {
    }

    public Square(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    /**
     * this function calculate area
     *
     * @return
     */
    @Override
    public double calAre() {
        return this.edge * this.edge;
    }

    /**
     * this function calculate perimeter
     *
     * @return
     */
    @Override
    public double calPri() {
        return this.edge * 4;
    }
 
    @Override
    public void input() {
        this.edge = Validator.inputDouble("Enter edge: ", 0);
    }


    @Override
    public double volume() {
       return 0;
    }
}
