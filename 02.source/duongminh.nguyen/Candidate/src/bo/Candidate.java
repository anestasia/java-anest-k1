package bo;

import java.util.ArrayList;
import java.util.Scanner;

public class Candidate {

    private String candidateID;
    private String firstName;
    private String lastName;
    private String birthdate;
    private String address;
    private String phone;
    private String email;
    private int candidateType;

    public String getCandidateID() {
        return candidateID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public int getCandidateType() {
        return candidateType;
    }

    public Candidate() {
    }

    public Candidate(String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        this.candidateID = candidateID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.candidateType = candidateType;
    }
    

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public Candidate input(Candidate Candidate) {
        return Candidate;
    }
}
