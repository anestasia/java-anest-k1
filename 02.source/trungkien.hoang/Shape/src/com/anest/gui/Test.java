/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.gui;

import com.anest.bo.Circle;
import com.anest.bo.Shape;
import com.anest.bo.Square;
import com.anest.bo.Triangle;

/**
 *
 * @author KienKeng
 */
public class Test {
    public static void main(String[] args) {
        
        Shape[] shape = new Shape[10]; 
        
        for(int i =0 ;i< shape.length;i++)
        {
            System.out.println("Index : "+i);
            if(i%2 == 0 )
            {
                shape[i] = new Circle();
            }
            else if(i%3 ==0)
            {
                shape[i] = new Square();
            }
            else
            {
                shape[i] = new Triangle();
            }
            
            shape[i].input();
            System.out.println("Dien Tich : " +shape[i].calArea() );
            System.out.println("CHu vi: " +shape[i].calPri());
            
        }
    }
    
}
