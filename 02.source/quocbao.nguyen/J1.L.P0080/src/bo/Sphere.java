package bo;

import utir.Validate;

import java.util.Scanner;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Sphere extends ThreeDim {
    private double radius;
    Scanner sc = new Scanner(System.in);

    public Sphere() {
        radius = 0;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return 4 * Math.PI * radius * radius;
    }

    @Override
    public double getVolumn() {
        return 4 * Math.PI * radius * radius * radius / 3;
    }

    @Override
    public void input() {
        Validate validate = new Validate();
        setRadius(validate.getDouble("Enter the radius: ", "Please enter a float number!!!"));
    }
}
