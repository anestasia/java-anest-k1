/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.ConversionSystem;
import com.anest.Utils;
import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Application {

    static final String ERROR = "Invalid input, please re-enter!";

    public static void main(String[] args) {
        Application app = new Application();
        ConversionSystem convert = new ConversionSystem();
        Scanner in = new Scanner(System.in);
        Utils v = new Utils();
        do {
            int choice = app.SupMenu();
            switch (choice) {
                case 1: {
                    System.out.println("======= Converting from binary to decimal number =======");
                    int bin = v.getNBaseUnderDec("Enter binary number: ", ERROR, 2);
                    int dec = convert.ConvertNBaseUnderDecToDec(bin, 2);
                    System.out.println("We got: " + dec);
                    break;
                }
                case 2: {
                    System.out.println("======= Converting from binary to hexa number =======");
                    int bin = v.getNBaseUnderDec("Enter binary number: ", ERROR, 2);
                    int dec = convert.ConvertNBaseUnderDecToDec(bin, 2);
                    String hex = convert.ConvertDecToHex(dec);
                    System.out.println("We got: " + hex);
                    break;
                }
                case 3: {
                    System.out.println("======= Converting from decimal to binary number =======");
                    int dec = v.getInt("Enter decimal number: ", ERROR, 0, Integer.MAX_VALUE);
                    String bin = convert.ConvertDecToNBaseUnderDec(dec, 2);
                    System.out.println("We got: " + bin);
                    break;
                }
                case 4: {
                    System.out.println("======= Converting from decimal to hexa number =======");
                    int dec = v.getInt("Enter decimal number: ", ERROR, 0, Integer.MAX_VALUE);
                    String hex = convert.ConvertDecToHex(dec);
                    System.out.println("We got: " + hex);
                    break;
                }
                case 5: {
                    System.out.println("======= Converting from hexa to binary number =======");
                    String hex = v.getHexBase("Enter hexa number: ", ERROR);
                    int dec = convert.ConvertHexaToDec(hex);
                    String bin = convert.ConvertDecToNBaseUnderDec(dec, 2);
                    System.out.println("We got: " + bin);
                    break;
                }
                case 6: {
                    System.out.println("======= Converting from hexa to decimal number =======");
                    String hex = v.getHexBase("Enter hexa number: ", ERROR);
                    int dec = convert.ConvertHexaToDec(hex);
                    System.out.println("We got: " + dec);
                    break;
                }
            }
        } while (v.OptionChoice("Do you want to continue?(Y-Yes N-No): ", "Y", "N"));
    }

    int SupMenu() {
        Utils v = new Utils();
        int choice1, choice2;
        while (true) {
            choice1 = v.getInt("Choose base number input (1-Bin 2-Dec 3-Hex): ", ERROR, 1, 3);
            choice2 = v.getInt("Choose base number output (1-Bin 2-Dec 3-Hex): ", ERROR, 1, 3);
            if (choice1 != choice2) {
                break;
            }
            System.out.println(ERROR);
        }
        if (choice1 == 1 && choice2 == 2) {
            return 1;
        } else if (choice1 == 1 && choice2 == 3) {
            return 2;
        } else if (choice1 == 2 && choice2 == 1) {
            return 3;
        } else if (choice1 == 2 && choice2 == 3) {
            return 4;
        } else if (choice1 == 3 && choice2 == 1) {
            return 5;
        }
        return 6;
    }
}
