/**
 * 
 */
package com.anest.k1.product.bo;

import java.util.ArrayList;
import java.util.List;

import com.anest.k1.product.entity.Book;
import com.anest.k1.product.entity.Clothing;
import com.anest.k1.product.entity.DISC;
import com.anest.k1.product.entity.ECat;
import com.anest.k1.product.entity.Product;

/**
 * @author dell
 *
 */
public class ListProduct {
	private int quantity;
	private List<Product> products;
	/**
	 * 
	 */
	public ListProduct() {
		super();
		this.quantity = 0;
		this.products = new ArrayList<>();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param quantity
	 * @param products
	 */
	public ListProduct(int quantity, List<Product> products) {
		super();
		this.quantity = quantity;
		this.products = products;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}
	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	/**
	 * 
	 * @param product
	 */
	public void addProduct(Product product) {
		if(products.contains(product)){
			System.out.println("Duplicate!");
		} else {
			this.products.add(product);
			this.quantity++;
		}
		
	}
	
	/**
	 * 
	 * @param fileName
	 */
	public void loadFromFile(String fileName) {
		String[] lines = UTF8FileUtility.getLines(fileName);
		try {
			this.quantity = Integer.parseInt(lines[0].trim());
			for(int i = 1 ; i < this.quantity + 1; ++i) {
				this.products.add(getProductFromLine(lines[i]));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Có lỗi xảy ra, vui lòng xem lại danh sách!!!");
		}
		
	}
	
	/**
	 * 
	 * @param line
	 * @return
	 */
	public Product getProductFromLine(String line) {
		String[] tokens = line.split("\\|");
		String category = tokens[0].trim();
		//System.err.println(tokens[1].trim());
		int id = Integer.parseInt(tokens[1].trim());
		int price = Integer.parseInt(tokens[4].trim());
		String p1 = tokens[2].trim();
		String p2 = tokens[3].trim();
		
		if(category.equalsIgnoreCase("BOOK")) {
			return new Book(id, ECat.BOOK, price, p1, p2);
		} else if(category.equalsIgnoreCase("CLOTHING")) {
			return new Clothing(id, ECat.CLOTHING, price, p1, p2);
		}
		return new DISC(id, ECat.CD, price, p1, p2);
	}
	
	/**
	 * 
	 * @param fileName
	 */
	public void writeFile(String fileName) {
		UTF8FileUtility.createWriter(fileName);
		UTF8FileUtility.writeln(this.quantity + "");
		for(Product product : this.products ) {
			UTF8FileUtility.writeln(product.toString());
		}
		
		UTF8FileUtility.closeWriter();
	}
	/**
	 * 
	 */
	public void show() {
		for(Product product : this.products) {
			System.out.println(product.toString());
		}
	}
}
