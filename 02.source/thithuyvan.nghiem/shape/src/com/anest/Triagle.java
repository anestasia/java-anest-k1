package com.anest;

import java.util.Scanner;

public class Triagle extends TwoDimensionalShape {

    private double a2;
    private double a1;
    private double a3;
    private double p;

    @Override
    public double calArea() {
        p = (a1 + a2 + a3) / 2;

        return Math.sqrt(p * (p - a1) * (p - a2) * (p - a3));
    }

    @Override
    public double calPri() {
        return a1 + a2 + a3;
    }

    @Override
    public void input() {
        System.out.print("Enter a1= ");
        a1 = new Scanner(System.in).nextDouble();
        System.out.print("Enter a2= ");
        a2 = new Scanner(System.in).nextDouble();
        System.out.print("Enter a3= ");
        a3 = new Scanner(System.in).nextDouble();
    }

}
