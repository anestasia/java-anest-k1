package com.anest.db;

import com.anest.Validator.ValidValue;
import java.util.ArrayList;


public class Fresher extends Candidate {
  
    private String  graduation_date;
    private String education;
    private String graduation_rank;;

    public Fresher(int candidatetype) {
        super(candidatetype);
    }

    
    public void setFreasherInfo(ArrayList<Candidate> list){
        setCandidateInfo(list);
        ValidValue vl = new ValidValue(list);
        this.graduation_rank = vl.validRank("Enter number corresponding to Graduation rank as below: Excellence: 0, Good: 1, Fair: 2, Poor: 3");
        System.out.println("Graduation Date: ");
        this.graduation_date = vl.setString();
        System.out.println("Education: ");
        this.education = vl.setString();
    }
 

}
