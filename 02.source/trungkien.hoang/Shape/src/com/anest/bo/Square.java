/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Square extends TwoDimensionalShape{
    
    private double edge;

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    

    @Override
    public double calArea() {
      return  4 * this.edge;
    }

    @Override
    public double calPri() {
       return this.edge*this.edge;
    }
    
    @Override
    public void input(){
        System.out.print("Enter edge: ");
        this.edge = new Scanner(System.in).nextDouble();
                
    }


}
