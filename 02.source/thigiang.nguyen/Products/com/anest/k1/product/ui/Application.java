/**
 * 
 */
package com.anest.k1.product.ui;

import com.anest.k1.product.bo.ListProduct;
import com.anest.k1.product.entity.Clothing;
import com.anest.k1.product.entity.DISC;
import com.anest.k1.product.entity.ECat;
import com.anest.k1.product.entity.Product;

/**
 * @author dell
 *
 */
public class Application {
	public static void main(String[] args) {
		ListProduct products = new ListProduct();
		products.loadFromFile("new_product.txt");
		products.show();
		
		 //duplicate product : id = 3
		products.addProduct(new DISC(3, ECat.CD, 100000, "Trữ tình", "mp3"));
//		
		Product product = new Clothing();
		product.input();
		
		System.out.println(product);
		
		products.addProduct(product);
		
		products.writeFile("new_product.txt");
	}
	
}
