/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

/**
 *
 * @author OPF_Lep
 */
public class Candidate {

    protected String ID;
    protected String firstName;
    protected String lastName;
    protected int birthDate;
    protected String Address;
    protected String phone;
    protected String eMail;

    public Candidate(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.Address = Address;
        this.phone = phone;
        this.eMail = eMail;
    }

    public Candidate(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail, int type) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.Address = Address;
        this.phone = phone;
        this.eMail = eMail;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Candidate{" + "ID=" + ID + ", firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate + ", Address=" + Address + ", phone=" + phone + ", eMail=" + eMail + ", type=" + type + '}';
    }

    public Candidate() {
    }
    protected int type;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
