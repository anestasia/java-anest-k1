/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest;

import java.util.Scanner;

/**
 *
 * @author Van SO CUTE
 */
public class Circle extends TwoDimensionalShape {

    private double r;

    public Circle() {
    }

    public Circle(double r) {
        this.r = r;
    }
    

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        return r * r * Math.PI;
    }

    @Override
    public double calPri() {
        return r * 2 * Math.PI;
    }

    @Override
    public void input() {
        System.out.print("Enter r= ");
         double r =new Scanner(System.in).nextDouble();
    }

}
