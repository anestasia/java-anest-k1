/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Cube extends ThreeDimensionalShape{
    private double edge;
    
    public Cube() {
    }
    
    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    @Override
    public double calArea() {
        return 6*this.edge*this.edge;
    }

    @Override
    public double calPri() {
       return 0;
    }

    @Override
    public double calV() {
        return Math.pow(edge,3);
    }

    @Override
    public void inPut() {
        System.out.println(" Cube:");
        System.out.println("enter edge= ");
        this.edge = new Scanner(System.in).nextDouble(); 
    
    }
    
}
