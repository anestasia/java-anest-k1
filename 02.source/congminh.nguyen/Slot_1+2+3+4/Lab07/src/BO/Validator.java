/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Scanner;

/**
 *
 * @author Cong Minh
 */
public class Validator {
   public static int inputInt(String mes){
       Scanner sc = new Scanner(System.in);
       int input;
        while(true){
            try{
                System.out.print(mes);
                input = sc.nextInt();
                return input;
            }catch(Exception e){
                System.out.println("Invalid input.");
                sc.next();
            }
        }
    }
    
}
