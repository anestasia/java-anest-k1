/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.gui;

import candidate.bo.Deverloper;
import candidate.bo.Experience;
import candidate.bo.Fresher;
import candidate.bo.Internship;
import candidate.until.Input;
import java.awt.Menu;
import java.util.ArrayList;

/**
 *
 * @author Trinh Dai
 */
public class Main {

    public static void main(String[] args) {
        ShowMenu menu = new ShowMenu();
        Input input = new Input();
        Deverloper deverloper = new Deverloper();
        ArrayList<Deverloper> Candidates = new ArrayList();
        Funtions fun = new Funtions();
        int choice;
        do {
            menu.showMenuStart();
            choice = (int) input.getNumber("Enter your choice: ", "Invalid choice!!", 1, 5);
            switch (choice) {
                case 1:
                    do {
                        int choice2;
                        fun.showAllExp(Candidates);
                        menu.showMenuDevelop();
                        choice2 = (int) input.getNumber("Enter your choice: ", "Invalid choice!!", 1, 4);
                        switch (choice2) {
                            case 1:
                                do {
                                    fun.createExp(Candidates);
                                } while (input.pressYN());
                                break;
                            case 2:
                                do {
                                    fun.updateExp(Candidates);
                                } while (input.pressYN());
                                break;
                            case 3:
                                do {
                                    fun.deleteExp(Candidates);
                                } while (input.pressYN());
                                break;
                        }
                        while (choice != 4);
                    } while (input.pressYN());
                    break;
                case 2:
                     do {
                        int choice2;
                        fun.showAllFre(Candidates);
                        menu.showMenuDevelop();
                        choice2 = (int) input.getNumber("Enter your choice: ", "Invalid choice!!", 1, 4);
                        switch (choice2) {
                            case 1:
                                do {
                                    fun.createFre(Candidates);
                                } while (input.pressYN());
                                break;
                            case 2:
                                do {
                                    fun.updateFre(Candidates);
                                } while (input.pressYN());
                                break;
                            case 3:
                                do {
                                    fun.deleteFre(Candidates);
                                } while (input.pressYN());
                                break;
                        }
                        while (choice != 4);
                    } while (input.pressYN());
                    break;
                case 3:
                    do {
                        int choice2;
                        fun.showAllInter(Candidates);
                        menu.showMenuDevelop();
                        choice2 = (int) input.getNumber("Enter your choice: ", "Invalid choice!!", 1, 4);
                        switch (choice2) {
                            case 1:
                                do {
                                    fun.createIntern(Candidates);
                                } while (input.pressYN());
                                break;
                            case 2:
                                do {
                                    fun.updateIntern(Candidates);
                                } while (input.pressYN());
                                break;
                            case 3:
                                do {
                                    fun.deleteIntern(Candidates);
                                } while (input.pressYN());
                                break;
                        }
                        while (choice != 4);
                    } while (input.pressYN());
                    break;
                case 4:
                    do {
                        menu.showNameDeverlop(Candidates);
                        menu.searchingDeverLop(Candidates);
                    } while (input.pressYN());
            }
        
            }
            while (choice != 5);
        }

    }
