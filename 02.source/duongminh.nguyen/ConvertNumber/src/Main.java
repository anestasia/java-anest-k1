import util.Input;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Input input = new Input();
        do {
            System.out.println("CONVERT BASE 2, 10, 16 SYSTEM");
            int baseInput = input.getNumberBase("Enter base number input: ", "Enter base 2, 10 or 16");
            int baseOutput = input.getNumberBase("Enter base number output: ", "Enter base 2, 10 or 16");
            while (true) {
                try {
                    System.out.print("Enter number: ");
                    String number = in.nextLine().trim();
                    System.out.println("Output: " + Integer.toString(Integer.parseInt(number, baseInput), baseOutput));
                    break;
                } catch (Exception e) {
                    System.out.println("Invalid input");
                }
            }
        } while (input.pressYN());
    }
}

