/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Clothing extends Product {
    private String department;
    private String brand;
    private static final long serialVersionUID = 1L;
    
    public Clothing() {
    }
    
    public Clothing(int id, Catagory category, double price, String department, String brand) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     *
     */
    @Override
        public void input(ArrayList<Product> list) {
        super.input(list);
            Scanner sc = new Scanner(System.in);
            this.setCategory(Catagory.Clothing);
		System.out.print(" Department = ");
		this.department = new Scanner(System.in).nextLine().trim();
		System.out.print(" Brand = ");
		this.brand = new Scanner(System.in).nextLine().trim();
    }

    @Override
    public String toString() {
        return " | " + getId() + " | " + getCategory() + " | " + department + " | " + brand + " | " + getPrice();
        
    } 
}
