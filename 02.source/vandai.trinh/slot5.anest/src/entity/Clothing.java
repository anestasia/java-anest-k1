/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Clothing extends Product implements Serializable{
    private String department;
    private String brand;

    public Clothing() {
    }

    public Clothing( String category, int id, String price, String department, String brand) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return super.toString()+" | " +department+ " | " +brand;
    }

    @Override
    public void input() {
        super.input();
        setCategory("Clothing");
        System.out.print("Enter the department: ");
        this.department= new Scanner(System.in).nextLine().trim();
        System.out.print("Enter the brand: ");
        this.brand = new Scanner(System.in).nextLine().trim();
    }
    
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
}
