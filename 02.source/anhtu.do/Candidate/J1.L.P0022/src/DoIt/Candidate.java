/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoIt;
import Validate.Validate;
import java.util.Scanner;
/**
 *
 * @author Nguyen Phong
 */
public class Candidate {

    private String candidateId;
    private String firstName;
    private String lastName;
    private int birthDate;
    private String address;
    private String phone;
    private String email;
    private int candidateType;
    Scanner in = new Scanner(System.in);

    public void setCandidate() {
        System.out.print("Enter Candidate Id: ");
        candidateId = in.nextLine();
        System.out.print("Enter First Name : ");
        firstName = in.nextLine();
        System.out.print("Enter Last Name : ");
        lastName = in.nextLine();
        birthDate = Validate.validDateBirth();
        System.out.print("Enter Address: ");
        address = in.nextLine();
        phone = Validate.validPhone();
        email = Validate.validEmail();
    }

    public void setCandidateType(int candidateType) {
        this.candidateType = candidateType;
    }

    public void outputCandidateName() {
        System.out.println(firstName + " " + lastName);
    }

    public int getCandidateType() {
        return candidateType;
    }

    public void output() {
        System.out.println(firstName + lastName + "|" + birthDate + "|" + address
                + "|" + phone + "|" + email + "|" + candidateType);
    }

    public String getCandidateId() {
        return candidateId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

}
