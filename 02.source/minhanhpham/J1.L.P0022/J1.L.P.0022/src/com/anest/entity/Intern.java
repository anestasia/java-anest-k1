/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.entity;

import java.util.List;

/**
 *
 * @author MyPC
 */
public class Intern extends Candidate{
    private String majors;
    private String semester;
    private String universityName;
    Condition valid = new Condition();

    public Intern(String Candidate_ID, String First_Name, String Last_Name, int BirthDate, String Address, String Phone, String email, String majors, String semester, String universityName) {
        super(Candidate_ID, First_Name, Last_Name, BirthDate, Address, Phone, email);
        this.majors = majors;
        this.semester = semester;
        this.universityName = universityName;
        super.type = 3;
    }

    public Intern() {
        super.type = 3;
    }

//    public Intern() {
//    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public Condition getValid() {
        return valid;
    }

    public void setValid(Condition valid) {
        this.valid = valid;
    }
    @Override
    public String toString(){
        return super.toString() + " | " + majors + " | " + semester + " | " + universityName + " | " + type;
    }
    @Override
    public void Create(List<Candidate> can){
        super.Create(can);
        System.out.println("Enter majors : ");
        majors = in.nextLine();
        System.out.println("Enter semester : ");
        semester = in.nextLine();
        System.out.println("Enter universityName : ");
        universityName = in.nextLine();
    }
    
}
