/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;

/**
 *
 * @author Taka
 */
public class Clothing extends Product {

    private String department;
    private String brand;

    //Dùng khi đọc dữ liệu từ file để tạo mới phần tử
    public Clothing(Categories category, int id, String department, String brand, double price) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
    }

    //Dùng khi khởi tạo sản phẩm trong menu
    public Clothing(Categories categories) {
        super(categories);
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setDepartment() {
        String department1 = vl.setString("Please input clothing's department: ");
        this.department = department1;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setBrand() {
        String brand1 = vl.setString("Please input clothing's brand: ");
        this.brand = brand1;
    }

    @Override
    public void input(ArrayList<Product> list) {
        super.input(list);
        setDepartment();
        setBrand();
    }

    @Override
    public String toString() {
        return this.getCategory() + " | " + this.getId() + " | " + department + " | " + brand + " | " + this.getPrice();
    }

}
