/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;

/**
 *
 * @author Taka
 */
public class Book extends Product {

    private String title;
    private String author;

    //Dùng khi đọc dữ liệu từ file để tạo mới phần tử
    public Book(Categories category, int id, String title, String author, double price) {
        super(id, category, price);
        this.title = title;
        this.author = author;
    }

    //Dùng khi khởi tạo sản phẩm trong menu
    public Book(Categories categories) {
        super(categories);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTitle() {
        String title1 = vl.setString("Enter book's title: ");
        this.title = title1;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAuthor() {
        String author1 = vl.setString("Enter book's author: ");
        this.author = author1;
    }

    @Override
    public void input(ArrayList<Product> list) {
        super.input(list);
        setTitle();
        setAuthor();
    }

    @Override
    public String toString() {
        return this.getCategory() + " | " + this.getId() + " | " + title + " | " + author + " | " + this.getPrice();
    }

}
