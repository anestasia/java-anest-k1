package bo;


import common.Common;
import java.util.Scanner;

public class Square extends TwoDimensionalShape{

    private float edge;
    private float area;
    private float verimeter;

    public Square() {
    }

    public Square(float edge, float area, float verimeter) {
        this.edge = edge;
        this.area = area;
        this.verimeter = verimeter;
    }

    public float getEdge() {
        return edge;
    }

    public void setEdge(float edge) {
        this.edge = edge;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getVerimeter() {
        return verimeter;
    }

    public void setVerimeter(float verimeter) {
        this.verimeter = verimeter;
    }

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Enter the edge for your square: ");
            setEdge(in.nextFloat());
        } while (!Common.isNumber(in.nextLine())||Common.isPositveNumber(in.nextFloat()));
    }

    @Override
    public float calArea() {
        return (getEdge() * getEdge());
    }

    @Override
    public float calVerimeter() {
        return (getEdge() * 4);
    }

    @Override
    public void output() {
        System.out.println("Your square has verimeter is: "+ calVerimeter()+"\n");
        System.out.println("Your square has area is:"+ calArea());
    }
    

}
