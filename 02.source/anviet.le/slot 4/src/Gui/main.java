/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import convert.newpackage.bo.BO;
import convert.newpackage.bo.Utils;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class main {

    public static void main(String[] args) {

        BO test = new BO();

        Scanner sc = new Scanner(System.in);
        System.out.println("1.Binary\n2.Decimal\n3.Hex ");
        do {
            int base = Utils.getIntNumber("Enter base: ", "Please enter 1-3!!!", 1, 3);
            int target;

            switch (base) {
                case 1://binary
                    System.out.println("1.Binary\n2.Decimal\n3.Hex ");
                    target = Utils.getIntNumber("Enter base you want to convert: ", "Please enter 2 or 3!!!", 2, 3);
                    test.conBinary(target);
                    break;
                case 2://decimal
                    System.out.println("1.Binary\n2.Decimal\n3.Hex ");
                    target = Utils.getIntNumber("Enter base you want to convert: ", "Please enter 1 or 3!!!", 1, 3);
                    test.conDecimal(target);
                    break;
                case 3://hex
                    System.out.println("1.Binary\n2.Decimal\n3.Hex ");
                    target = Utils.getIntNumber("Enter base you want to convert: ", "Please enter 1 or 2!!!", 1, 2);
                    test.conHex(target);
                    break;
            }
        } while (Utils.checkContinue());
    }
}
