package com.anest;

import java.util.Scanner;

public class Square extends TwoDimensionalShape {

    private double edge;

    
    public Square(){
        
    }

    public Square(double edge) {
        this.edge = edge;
    }
    

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public double calArea() {
        return edge * edge;
    }

    @Override
    public double calPri() {
        return 4 * edge;
    }

    @Override
    public void input() {
        System.out.print("Enter r= ");
        double r =new Scanner(System.in).nextDouble();
    }

}
