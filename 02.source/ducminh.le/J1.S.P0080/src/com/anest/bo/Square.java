package com.anest.bo;

import com.anest.util.Input;

/**
 * Created by Minh on 1/14/2017.
 */
public class Square extends TwoDimensionalShape {
    private double edge;

    public Square() {
    }

    public Square(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public void input() {
        Input input = new Input();
        this.edge = input.getDouble("Enter edge: ", "Invalid number", 0, Double.MAX_VALUE);
    }

    @Override
    public void printResult() {
        System.out.println("Perimeter: " + getPerimeter());
        System.out.println("Area: " + getArea());
    }

    @Override
    public double getPerimeter() {
        return edge * 4;
    }

    @Override
    public double getArea() {
        return Math.pow(edge, 2);
    }

    @Override
    public double getVolume() {
        return 0;
    }
}