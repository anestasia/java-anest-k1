/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Scanner;

/**
 *
 * @author Hoang Trung Duc
 */
public class Rectangle extends TwoDimensons {

    private int a, b;

    public Rectangle() {
    }

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public void getArea() {
        System.out.println("DIen tich la: " + a * b);
    }

    @Override
    public void getParameter() {
        System.out.println("Chu vi la: " + 2 * (a + b));
    }

    @Override
    public void input() {
        common.Input in = new common.Input();
        String s;
        do {
            do {
                System.out.println("Nhap canh:");
                Scanner sc = new Scanner(System.in);
                s = sc.nextLine();
            } while (!in.isNumber(s));
            setA(Integer.parseInt(s));
        } while (a <= 0);
        do {
            do {
                System.out.println("Nhap canh:");
                Scanner sc = new Scanner(System.in);
                s = sc.nextLine();
            } while (!in.isNumber(s));
            setB(Integer.parseInt(s));
        } while (b <= 0);
    }

    @Override
    public void getV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
