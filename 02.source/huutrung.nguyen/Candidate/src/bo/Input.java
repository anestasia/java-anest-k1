/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import com.anest.Utils;
import java.util.Calendar;
import java.util.Scanner;

public class Input {

    static final String ERROR = "Invalid input, please re-enter!";
    static Scanner in = new Scanner(System.in);
    static Utils val = new Utils();
    
    public static void main(String[] args) { // for testing
    }

    public void inputCandidate(Candidate c) { //Input candidate
        in.useDelimiter("\n");
        System.out.print("Enter first name: ");
        c.setFirstName(in.next());
        System.out.print("Enter last name: ");
        c.setLastName(in.next());
        int birthDate = val.getInt("Birthdate: ", ERROR, 1900, Calendar.getInstance().get(Calendar.YEAR));
        c.setBirthDate(birthDate);
        System.out.print("Address: ");
        c.setAddress(in.next());
        String phone = val.getPhone("Phone: ", ERROR, 10);
        c.setPhone(phone);
        String email = val.getEmail("Email: ", ERROR);
        c.setEmail(email);
    }

    public void inputExperience(Experience e) {
        int exp = val.getInt("Year of experience: ", ERROR, 0, 100);
        e.setYearOfExp(exp);
        System.out.print("Proskill: ");
        e.setProSkill(in.next());
    }

    public void inputFresher(Fresher f) {
        int gradTime = val.getInt("Graduate time: ", ERROR, f.getBirthDate(),
                Calendar.getInstance().get(Calendar.YEAR));
        f.setGradTime(gradTime);
        String gradRank = val.getGraduateRank("Graduate rank: ", ERROR);
        f.setGradRank(gradRank);
        System.out.print("Education: ");
        f.setEducation(in.next());
    }

    public void inputIntern(Intern i) {
        System.out.print("Major: ");
        i.setMajor(in.next());
        System.out.print("Semester: ");
        i.setSemester(in.next());
        System.out.print("University: ");
        i.setUniversity(in.next());
    }
}
