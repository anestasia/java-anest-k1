/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Triangle extends TwoDimensionalShape {

    private double a, b, c;

    public Triangle() {
        super();
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double callPri() {
        return (a + b + c);
    }

    @Override
    public double callArea() {
        return Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - a) * (((a + b + c) / 2) - b) * (((a + b + c) / 2) - c));
    }

    @Override
    public void input() {
        System.out.println("Enter a = ");
        this.a = new Scanner(System.in).nextDouble();
        System.out.println("Enter b = ");
        this.b = new Scanner(System.in).nextDouble();
        System.out.println("Enter c = ");
        this.c = new Scanner(System.in).nextDouble();
    }

}
