/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.entity;

import java.util.List;

/**
 *
 * @author MyPC
 */
public class Fresher extends Candidate {
    private String graDate;
    private String gradRank;
    Condition valid = new Condition();

    public Fresher(String Candidate_ID, String First_Name, String Last_Name, int BirthDate, String Address, String Phone, String email, String graDate, String gradRank) {
        super(Candidate_ID, First_Name, Last_Name, BirthDate, Address, Phone, email);
        this.graDate = graDate;
        this.gradRank = gradRank;
        super.type = 2;
    }

    public Fresher() {
        super.type = 2;
    }
    
    

    public String getGraDate() {
        return graDate;
    }

    public void setGraDate(String graDate) {
        this.graDate = graDate;
    }

    public String getGradRank() {
        return gradRank;
    }

    public void setGradRank(String gradRank) {
        this.gradRank = gradRank;
    }
     @Override
    public String toString() {
        return super.toString() + " | " + graDate + " | " + gradRank + " | " + " | " + type;
    }
    @Override
    public void Create(List<Candidate> can){
        super.Create(can);
        System.out.println("Enter your graduation Date:");
        graDate = in.nextLine();
        gradRank = valid.gradRank("Enter your graduation rank: ");
    }
    
}
