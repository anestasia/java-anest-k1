/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BO.CanType;
import BO.Candidate;
import java.util.ArrayList;

/**
 *
 * @author KienKeng
 */
public class Display {
    
    public void Menu()
    {
        System.out.println("CANDIDATE MANAGEMENT SYSTEM");
        System.out.println("1. Experience");
        System.out.println("2. Fresher");
        System.out.println("3. Internship");
        System.out.println("4. Searching");
        System.out.println("5. Exit");
        
    }

    public void displayAll(ArrayList<Candidate> a)
    {
        System.out.println("===========EXPERIENCE CANDIDATE============");
        for (Candidate candidate : a) {
            if(candidate.getCandidateType().equals(CanType.Experience)){
                candidate.showName();
            }
        }
        
        System.out.println("==========FRESHER CANDIDATE==============");
        for (Candidate candidate : a) {
            if(candidate.getCandidateType().equals(CanType.Fresher)){
                candidate.showName();
            }
        }
        
        System.out.println("===========INTERN CANDIDATE==============");
        for (Candidate candidate : a) {
            if(candidate.getCandidateType().equals(CanType.Intern)){
                candidate.showName();
            }
        }
    }
}
