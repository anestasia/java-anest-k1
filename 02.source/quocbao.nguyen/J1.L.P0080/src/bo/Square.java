package bo;


import utir.Validate;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Square extends TwoDim {
    private double edge;

    public Square() {
        edge = 0;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return 4 * edge;
    }

    @Override
    public void input() {
        Validate validate = new Validate();
        setEdge(validate.getDouble("Validate edge of your square: ", "Please enter a float number!!!"));
    }

    @Override
    public double getVolumn() {
        return 0;
    }

    public double getPerimeter() {
        return edge * edge;
    }
}
