/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BO.Circle;
import BO.Cube;
import BO.Shape;
import BO.Sphrere;
import BO.Square;
import BO.Tetrahedron;
import BO.Triangle;
import BO.Validator;
import java.util.Scanner;

/**
 *
 * @author Cong Minh
 */
public class Main {

    public static void main(String[] args) {
        int choice = 0;
        Shape shape = null;
        while (true) {
            System.out.println("Choose type of shape: ");
            System.out.println("1- Two Demensional");
            System.out.println("2- Three Demensional");
            System.out.println("-------------");
            choice = Validator.inputInt("Enter your choice: ");
            switch (choice) {
                case 1:
                    System.out.println("Choose shape you want to calculate: ");
                    System.out.println("1- Circle.");
                    System.out.println("2- Square.");
                    System.out.println("3- Triangle.");
                    int choice1 = Validator.inputInt("Enter your choice: ");
                    switch (choice1) {
                        case 1:
                            shape = new Circle();
                            break;
                        case 2:
                            shape = new Square();
                            break;
                        case 3:
                            shape = new Triangle();
                            break;
                        default:
                            System.out.println("Invalid input. Try again!");
                            break;
                    }
                    break;
                case 2:
                    System.out.println("Choose shape you want to calculate: ");
                    System.out.println("1- Sphrere.");
                    System.out.println("2- Cube.");
                    System.out.println("3- Tetrahedron.");
                    int choice2 = Validator.inputInt("Enter your choice: ");
                    switch (choice2) {
                        case 1:
                            shape = new Sphrere();
                            break;
                        case 2:
                            shape = new Cube();
                            break;
                        case 3:
                            shape = new Tetrahedron();
                            break;
                        default:
                            System.out.println("Invalid input. Try again!");
                            break;
                    }
                    break;
                default:
                    System.out.println("Invalid input. Try again!");
                    break;
            }
            if (shape == null) {
                continue;
            }
            shape.input();
            System.out.println("Dien tich la: " + shape.calAre());
            System.out.println("Chu vi la   : " + shape.calPri());
            System.out.println("The tich la : " + shape.volume());
            System.out.println("-------------------------");
        }
    }
}
