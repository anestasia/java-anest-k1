/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package covertbase;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class CovertBase {

    public static void menu() {
        System.out.println("1.	Required user choose the base number input ");
        System.out.println("2.	Required user choose the base number out ");
        System.out.println("3.	Required user enter the input value ");

    }

    public static int getChoice(int i, int j, String mess) {
        Scanner sc = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.print(mess);
            try {
                choice = Integer.parseInt(sc.nextLine());
                if (choice > i && choice < j) {
                    return choice;
                }
            } catch (Exception e) {
                System.out.println("error");
            }
        }
    }

    public static void run() {
        int baseint, baseout;
        int choice = 0;
        int num10 = 0;
        String num;
        menu();

        baseint = getChoice(0, 37, "Input base (1-36):");

        baseout = getChoice(0, 37, "output base (1-36):");

        num = Input.inputNumber(baseint);
        System.out.println("Output " + TenToOthers.convert(OthersToTen.convert(num, baseint), baseout));
    }

    public static void main(String[] args) {
        run();
    }

}
