package gui;

import entity.Candidate;
import entity.Experience;
import entity.Fresher;
import entity.Intern;

import java.util.ArrayList;

/**
 * Created by Minh on 2/16/2017.
 */
public class Display {
    public void showAll(ArrayList<Candidate> candidates) {
        System.out.println();
        System.out.println("List of candidate");
        System.out.println("==========EXPERIENCE CANDIDATE============");
        for (Candidate candidate : candidates)
            if (candidate instanceof Experience)
                System.out.println(candidate.toString());
        System.out.println("==========FRESHER CANDIDATE============");
        for (Candidate candidate : candidates)
            if (candidate instanceof Fresher)
                System.out.println(candidate.toString());
        System.out.println("==========INTERN CANDIDATE============");
        for (Candidate candidate : candidates)
            if (candidate instanceof Intern)
                System.out.println(candidate.toString());
    }

    public void showMenu() {
        System.out.println("CANDIDATE MANAGEMENT SYSTEM");
        System.out.println("1. Experience");
        System.out.println("2. Fresher");
        System.out.println("3. Internship");
        System.out.println("4. Searching");
        System.out.println("5. Exit");
    }
}
