
package shape.bo;


import com.edu.Validate.Validator;
import java.util.Scanner;


public class Cube extends ThreeDimensionalShapes{
    private double e;
   

    public Cube() {
    }

    public Cube(double e) {
        this.e = e;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    @Override
    public double calArea() {
     return 6*Math.pow(this.e, 2);   
    }

    @Override
    public double calPri() {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    private double areaSide(){
        
        return 4*this.e*this.e  ;
    }
    @Override
    public void input() {
        System.out.print("Enter e=");
        this.e = Validator.validateDouble();
    }

    @Override
    public double volume() {
        return Math.pow(e, 3);
    }
    
    
}
