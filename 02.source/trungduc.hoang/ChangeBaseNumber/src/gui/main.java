/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.Validate;
import java.util.Scanner;

/**
 *
 * @author Hoang Trung Duc
 */
public class main {

    static Validate validate = new Validate();

    public static int selectBase(String msg) {
        // Menu mini
        int choice = validate.getInt("\n"
                + "1 is binary\n"
                + "2 is decimal\n"
                + "3 is hexadecimal\n" + msg, "Invalid choice", 1, 3);

        // Switch case:
        switch (choice) {
            case 1:
                return (2);
            case 2:
                return (10);
            case 3:
                return (16);
            default:
                return (0);
        }
    }

    public static void main(String[] args) {
        int inputBase, outputBase;
        String number;
        int tempInDecimal;
        do {

            // Select input output base: 
            inputBase = selectBase("Choose the base number input: ");
            outputBase = selectBase("Choose the base number output: ");

            // Enter data:
            System.out.println("Enter your number you want to convert: ");
            number = (new Scanner(System.in)).nextLine();

            try {
                // Convert all to Decimal
                tempInDecimal = Integer.parseInt(number, inputBase);

                // Process 2 10 16
                switch (outputBase) {
                    case 2:
                        System.out.println("Binary: " + Integer.toBinaryString(tempInDecimal));
                        break;
                    case 10:
                        System.out.println("Decimal: " + tempInDecimal);
                        break;
                    case 16:
                        System.out.println("Hexa: " + Integer.toHexString(tempInDecimal));
                        break;
                    default:
                        break;
                }
            } catch (NumberFormatException e) {
                // If can't convert to 10 ->
                System.out.println("Invalid Number Input");
            }
        } while (validate.getYN("Do you want to try more convert?"));
    }

}
