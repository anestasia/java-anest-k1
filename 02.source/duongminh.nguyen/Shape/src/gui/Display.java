package gui;

import java.util.Scanner;

public class Display {

    public static void menu() {
        System.out.println("Please choose your shape to calculate:\n"
                + "1 Circle\n"
                + "2 Square\n"
                + "3 Triangl\n"
                + "4 Cube\n"
                + "5 Tetrahedron\n"
                + "6 Sphere\n"
                + "Press any key to exist");
    }

    public static boolean pressYN() {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String s;
        while (true) {
            System.out.print("Do you want to continue? (Y-yes, N-no)");
            s = in.nextLine();
            s = s.trim();
            if (s.equalsIgnoreCase("y")) {
                return true;
            }
            if (s.equalsIgnoreCase("n")) {
                return false;
            }
            System.out.println("Invalid choice");
        }
    }

}
