/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Book;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import entity.Category;
import entity.Clothe;
import entity.Disc;
import entity.Product;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Cong Minh
 */
public class Input {

    public static int inputInt(String mes, int max, int min) {
        Scanner sc = new Scanner(System.in);
        int input;
        while (true) {
            try {
                System.out.print(mes);
                input = sc.nextInt();
//                System.out.println(input);
                if ((input > max || input < min)) {
//                    sc.next();    
                    System.out.println("Invalid input.");
                    input = inputInt(mes, max, min);
                }
                return input;
            } catch (Exception e) {
                System.out.println("Invalid input.");
                sc.next();
            }
        }
    }

    public static String inputString(String mes) throws IOException {
        String input;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(mes);
        while (true) {
            input = br.readLine();
            if (input.isEmpty()) {
                System.out.print("Re-Enter: ");
            } else {
                return input;
            }
        }
    }

    public static double inputDouble(String mes, double min) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(mes);
        double input = 0;
        boolean flag = false;
        do {
            try {
                input = Double.parseDouble(br.readLine());
                if (input < min) {
                    System.out.print("Re-Enter: ");
                    flag = false;
                } else {
                    flag = true;
                }
            } catch (Exception e) {
                System.out.print("Re-Enter: ");
            }
        } while (flag == false);
        return input;
    }

    public static Product creat(Category category, ArrayList<Product> lst) throws IOException {
        String id;
        String title;
        String author;
        double price;
        String department;
        String brand;
        String genre;
        String format;
        Validator t = new Validator();
        do{
            id = inputString("Enter product's ID: ").trim();
            if(!t.idChecker(id, lst)) 
                System.out.println("ID existed.");
        }while(!t.idChecker(id, lst));
        
        price = inputDouble("Enter product price: ", 0);
        switch (category) {
            case Book:
                title = inputString("Enter product's title: ").trim();
                do {
                    author = inputString("Enter product's author: ").trim();
                    if (!t.stringChecker(author)) {
                        System.out.println("Invalid input");
                    }
                } while (!t.stringChecker(author));

                return (new Book(id, category, price, title, author));

            case Cloth:
                do {
                    department = inputString("Enter product's department: ").trim();
                    if (!t.stringChecker(department)) {
                        System.out.println("Invalid input");
                    }
                } while (!t.stringChecker(department));

                brand = inputString("Enter product's brand: ").trim();
                return (new Clothe(department, brand, id, category, price));
            default:
                do {
                    genre = inputString("Enter product's genre: ").trim();
                    if (!t.stringChecker(genre)) {
                        System.out.println("Invalid input");
                    }
                } while (!t.stringChecker(genre));
                do {
                    format = inputString("Enter product's genre: ").trim();
                    if (!t.stringChecker(format)) {
                        System.out.println("Invalid input");
                    }
                } while (!t.stringChecker(format));
                return (new Disc(genre, format, id, category, price));
        }
    }

    public void getAllItemsFromFile(ArrayList<Product> lst) throws IOException {
        String fileName;
        fileName = inputString("Enter file name: ").trim();
        RandomAccessFile f;
        StringTokenizer sk;
        String s = "";
        String id;
        String title;
        String author;
        double price;
        String department;
        String brand;
        String genre;
        String format;
        Category category;
        String category1;
        Validator t = new Validator();
        try {
            f = new RandomAccessFile(fileName, "r");
            while ((s = f.readLine()) != null) {
                sk = new StringTokenizer(s, "|");
                category1 = sk.nextToken();
                category = Category.valueOf(category1);
                switch (category) {
                    case Book:
                        id = sk.nextToken();
                        if(!t.idChecker(id, lst)) break;
                        title = sk.nextToken();
                        author = sk.nextToken();
                        price = Double.parseDouble(sk.nextToken());
                        lst.add(new Book(id, category, price, title, author));
                        break;
                    case Cloth:
                        id = sk.nextToken();
                        if(!t.idChecker(id, lst)) break;
                        department = sk.nextToken();
                        brand = sk.nextToken();
                        price = Double.parseDouble(sk.nextToken());
                        lst.add(new Clothe(department, brand, id, category, price));
                        break;
                    case Disc:
                        id = sk.nextToken();
                        if(!t.idChecker(id, lst)) break;
                        genre = sk.nextToken();
                        format = sk.nextToken();
                        price = Double.parseDouble(sk.nextToken());
                        lst.add(new Disc(genre, format, id, category, price));

                }
            }
            f.close();
            System.out.println("Successfully!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            //System.out.println("Error !");
        }
    }

    public void writeAllItemsToFile(ArrayList<Product> lst) throws IOException {
        File file = new File("output.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        String current;
        for(int i=0;i<lst.size();i++){
            if(lst.get(i).getCategory().equalsIgnoreCase(Category.Book.toString())){
                Book book = (Book) lst.get(i);
                current = book.getCategory()+ "|" + book.getId()+"|"+
                        book.getTitle() + "|" + book.getAuthor() +"|"+
                        book.getPrice();
                bw.write(current);
                bw.write("\r\n");
            }
            else if(lst.get(i).getCategory().equalsIgnoreCase(Category.Cloth.toString())){
                Clothe clothe = (Clothe) lst.get(i);
                current = clothe.getCategory()+"|"+clothe.getId()+"|"
                        + clothe.getDepartment()+"|"+clothe.getBrand()+"|"+
                        + clothe.getPrice();
                bw.write(current);
                bw.write("\r\n");
            }
            else {
                Disc disc = (Disc) lst.get(i);
                current = disc.getCategory()+"|"+disc.getId()+"|"+disc.getGenre()+"|"+
                        disc.getFormat()+"|"+disc.getPrice();
                bw.write(current);
                bw.write("\r\n");
            }
        }
        bw.close();
        fw.close();
         System.out.println("Successfully!");
    }
}
