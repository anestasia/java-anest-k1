/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import bo.Validate;
import java.io.Serializable;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Product implements Serializable {

    private int id;
    private Catagory category;
    private double price;
    Validate va = new Validate();
    private static final long serialVersionUID = 1L;

    public Product() {
    }

    public Product(int id, Catagory category, double price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(ArrayList<Product> list) {
        int id1;
        if (list.isEmpty()){
            id1 = 1;
        }
        else {
            id1 = list.size() + 1;
        }
        this.id = id1;
    }

    public Catagory getCategory() {
        return category;
    }

    public void setCategory(Catagory category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return id + "          " + category;
    }

    public void input(ArrayList<Product> list) {
        System.out.print(" Id = ");
        setId(list);
        //System.err.println(this.id);
        System.out.print(" Price = ");
        this.price = va.readDoub("", Double.MIN_VALUE, Double.MAX_VALUE);
    }

}
