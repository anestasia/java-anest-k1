package BO;

import common.Input;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hoang Trung Duc
 */
public interface Shape {

    public abstract void getArea();

    public abstract void getParameter();

    public abstract void getV();

    public abstract void input();

}
