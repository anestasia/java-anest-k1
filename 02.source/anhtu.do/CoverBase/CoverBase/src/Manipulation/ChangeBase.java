/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manipulation;

import java.util.Scanner;
import Manipulation.Validator;

/**
 *
 * @author Nguyen Phong
 */
public class ChangeBase {

    public static void baseBinDecHex(int inBase, int outBase) {
        if (inBase == 1) {
            if (outBase == 2) {
                bintoDec();
            }
            if (outBase == 3) {
                bintoHex();
            }

        }
        if (inBase == 2) {
            if (outBase == 1) {
                dectoBin();
            }
            if (outBase == 3) {
                dectoHex();
            }
        }
        if (inBase == 3) {
            if (outBase == 2) {
                hextoDec();
            }
            if (outBase == 1) {
                hextoBin();
            }
        }
    }

    public static int inputDec() {
        System.out.println("Enter integer > 0 :");
        Scanner in = new Scanner(System.in);
        int n;
        while (true) {
            try {
                n = Integer.parseUnsignedInt(in.nextLine());
                return n ;
            } catch (Exception e) {
                System.out.println("Enter integer > 0 :");
            }
        }
    }

    public static void dectoHex() {
        int dec = inputDec();
        System.out.println("Convert: " + dec + "(DEC) = " + Integer.toHexString(dec) + "(HEX)");
    }

    public static void dectoBin() {
        int dec = inputDec();
        System.out.println("Convert: " + dec + "(DEC) = " + Integer.toBinaryString(dec) + "(BIN)");
    }

    public static String inputHex() {
        System.out.println("Enter number in Hexadecimal mode: ");
        String hex = "";
        while (true) {
            try {
                hex = Validator.valiHex();
                return hex;
            } catch (Exception e) {
                System.out.println("Enter number in Hexadecimal mode: ");
            }
        }
    }

    public static void hextoBin() {
        String hex = inputHex();
        int dec = Integer.parseInt(hex, 16);
        System.out.println("Convert: " + hex + "(HEX)" + Integer.toBinaryString(dec) + "(BIN)");
    }

    public static void hextoDec() {
        String hex = inputHex();
        System.out.println("Convert: " + hex + "(HEX)" + Integer.parseInt(hex, 16) + "(DEC)");
    }

    public static String inputBin() {
        String bin = "";
        bin = Validator.valiBin();
        return bin;
    }

    public static void bintoDec() {
        String bin = inputBin();
        int dec = Integer.parseInt(bin, 2);
        System.out.println("Convert: " + bin + "(BIN)" + Integer.toHexString(dec) + "(DEC)");
    }

    public static void bintoHex() {
        String bin = inputBin();
        int dec = Integer.parseInt(bin, 2);
        System.out.println("Convert: " + bin + "(BIN)" + Integer.toHexString(dec) + "(HEX)");
    }
}
