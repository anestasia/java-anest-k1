/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoIt;

import Validate.Validate;

/**
 *
 * @author Nguyen Phong
 */
public class Fresher extends Candidate{
    
    private int graduationDate;
    private String graduationRank;
    private String education;

    @Override
    public void setCandidate() {
        super.setCandidate();
        System.out.print("Enter Graduated Time : ");
        graduationDate = Integer.parseInt(in.nextLine());
        System.out.println("Enter your Graduation Rank: ");
        graduationRank = Validate.validRankGra();
        System.out.print("Enter Where Student Graduated : ");
        education = in.nextLine();
    }

    @Override
    public void setCandidateType(int candidateType) {
        super.setCandidateType(1);
    }
}
