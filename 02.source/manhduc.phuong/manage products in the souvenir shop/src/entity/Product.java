/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import bo.Validation;
import java.util.ArrayList;

/**
 *
 * @author Taka
 */
public abstract class Product{

    Validation vl = new Validation();
    private int id;
    private Categories category;
    private double price;

    public Product(int id, Categories category, double price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public Product(Categories category) {
        super();
        this.category = category;
    }

    public Product() {
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(ArrayList<Product> list) {
        int id1;
        if (list.isEmpty()) {
            id1 = 1;
        } else {
            id1 = list.size() + 1;
        }
        this.id = id1;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setPrice() {
        double price1 = vl.setDouble("Enter " + getCategory() + "'s price: ", 0, Double.MAX_VALUE);
        this.price = price1;
    }

    public void input(ArrayList<Product> list) {
        setId(list);
        setPrice();
    }


}
