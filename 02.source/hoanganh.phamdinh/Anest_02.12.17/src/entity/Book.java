/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Book extends Product {

    private String title;
    private String author;
    private static final long serialVersionUID = 1L;

    public Book() {
    }

    public Book(int id, Catagory category, double price, String title, String author) {
        super(id, category, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     */
    @Override
    public void input(ArrayList<Product> list) {
        super.input(list);
        Scanner sc = new Scanner(System.in);
        this.setCategory(Catagory.Book);
        System.out.print(" Title = ");
        this.title = new Scanner(System.in).nextLine().trim();
        System.out.print(" Author = ");
        this.author = new Scanner(System.in).nextLine().trim();
    }

    @Override
    public String toString() {
        return " | " + getId() + " | " + getCategory() + " | " + title + " | " + author + " | " + getPrice();
    }
}
