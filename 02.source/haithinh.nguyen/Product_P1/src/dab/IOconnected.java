/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dab;

import bo.Book;
import bo.Clothing;
import bo.Disc;
import bo.Product;
import java.io.BufferedWriter;
import java.util.List;
import com.edu.fpt.file.UTF8FileUtility;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Thinh
 */
public class IOconnected implements Serializable {

    private static final long seri = 1L;

//    public static boolean luuFile(ArrayList<Product> list, String path) {
//        try {
//            FileOutputStream fileOutputStream = new FileOutputStream(path);
//            OutputStreamWriter outputStreamWrite = new OutputStreamWriter(fileOutputStream, "UTF-8");
//            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWrite);
//            int size = list.size();
//            String s = String.valueOf(size);
//            bufferedWriter.write(s);
//            bufferedWriter.newLine();
//            for (Product product : list) {
//                System.out.println(product.toString());
//                bufferedWriter.write(product.toString());
//                bufferedWriter.newLine();
//            }
//            bufferedWriter.close();
//            outputStreamWrite.close();
//            fileOutputStream.close();
//            return true;
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return false;
//    }
//
//    public static ArrayList<Product> docFile(String path) {
//        ArrayList<Product> list = new ArrayList<Product>();
//        try {
//            FileInputStream fileInputStream = new FileInputStream(path);
//            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
//            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//            int size = Integer.parseInt(bufferedReader.readLine());
//            System.out.println(size);
//            Product product = new Product();
//            for (int i = 0; i < size; i++) {
//                String line = bufferedReader.readLine();
//                String[] arr = line.split("|");
//                if (arr[1].trim().compareToIgnoreCase("Book") == 0) {
//                    product = new Book(Integer.parseInt(arr[1].trim()), arr[0].trim(), arr[2].trim(), arr[3].trim(), Integer.parseInt(arr[4].trim()));
//                }
//                if (arr[1].trim().compareToIgnoreCase("Clothing") == 0) {
//                    product = new Clothing(Integer.parseInt(arr[1].trim()), arr[0].trim(), arr[2].trim(), arr[3].trim(), Integer.parseInt(arr[4].trim()));
//                }
//                if (arr[1].trim().compareToIgnoreCase("Disc") == 0) {
//                    product = new Disc(Integer.parseInt(arr[1].trim()), arr[0].trim(), arr[2].trim(), arr[3].trim(), Integer.parseInt(arr[4].trim()));
//                }
//            }
//            list.add(product);
//            bufferedReader.close();
//            inputStreamReader.close();
//            fileInputStream.close();
//        } catch (Exception ex) {
//            System.err.println("File not exit");;
//        }
//        return list;
//    }

    public static ArrayList<Product> read(String path) {
        ArrayList<Product> list = new ArrayList<Product>();
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object data = objectInputStream.readObject();
            list = (ArrayList<Product>) data;
            objectInputStream.close();
            fileInputStream.close();
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public static boolean write(ArrayList<Product> list, String path) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

}
