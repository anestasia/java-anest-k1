package com.anest.Validator;

import com.anest.db.Candidate;
import com.anest.db.Rank;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidValue {

    Scanner sc = new Scanner(System.in);
    ArrayList<Candidate> list;

    public ValidValue(ArrayList<Candidate> list) {
        this.list = list;
    }

    public boolean checkContinue() {
        while (true) {
            String c;
            System.out.println("Do you want to continue? Y/N?: ");
            c = sc.nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                return true;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                return false;
            } else {
                System.out.println("Invalid input!!! Enter again: ");
            }
        }
    }

    public int setInt(int max, int min) {
        boolean check = true;
        int n = 0;
        while (check) {
            try {
                n = Integer.parseInt(sc.nextLine());
                if (n > max || n < min) {
                    System.out.println("Please choose number from " + min + " to " + max + ": ");
                    continue;
                }
                check = false;
            } catch (Exception e) {
                System.out.println("Input number please...");
            }
        }
        return (n);
    }

    public int setYear() {
        Calendar cal = Calendar.getInstance();
        boolean check = true;
        int year = setInt(cal.get(Calendar.YEAR), 1900);
        return year;
    }

    public String setString() {
        String str = sc.nextLine();
        String st = "";
        StringTokenizer tok = new StringTokenizer(str, " ");
        while (tok.hasMoreTokens()) {
            st += convert(tok.nextToken()) + " ";
        }
        return st;
    }

    public String convert(String str) {
        String tmp = "";
        tmp += String.valueOf(str.charAt(0)).toUpperCase();
        tmp += str.substring(1);
        return tmp;
    }

    public String validRank(String s) {
        String rog = null;
        System.out.println(s);
        int rank = setInt(3, 0);
        switch (rank) {
            case 0:
                rog = Rank.Excellence.toString();
                break;
            case 1:
                rog = Rank.Good.toString();
                break;
            case 2:
                rog = Rank.Fair.toString();
                break;

            case 3:
                rog = Rank.Poor.toString();
                break;

            default:
                break;
        }
        return rog;
    }

    public int validPhoneNumber(String s, int num) {
        System.out.println(s);
        while (true) {
            String phoneNumber = sc.nextLine();
            if (phoneNumber.length() == 10) {
                try {
                    num = Integer.parseInt(phoneNumber);
                    if(checkExistNumberPhone(num)){
                        continue;
                    }
                    return num;
                } catch (NumberFormatException e) {
                    System.out.println("Input number please...");
                }
            } else {
                System.out.println("Invalid input! Phone is number with minimum 10 characters");
            }
        }
    }

    boolean checkExistNumberPhone(int num) {
        for (int i = 0; i < list.size(); i++) {
            if (num == list.get(i).getPhone()) {
                System.out.println("Phone's already created! Enter another Phone!");
                return true;
            }
        }
        return false;
    }

    public boolean isValidEmailAddress(String email) {
        String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    boolean checkExistEmail(String s) {
        for (int i = 0; i < list.size(); i++) {
            if (s.equals(list.get(i).getEmail())) {
                System.out.println("Email's already created! Enter another Email!");
                return false;
            }
        }
        return true;
    }

    public String setEmail() {
        while (true) {
            System.out.println("Enter Candidate's Email");
            String s = sc.nextLine();
            if (isValidEmailAddress(s)==true && checkExistEmail(s)==true) {
                return s;
            } else {
                System.out.println("Invalid Email!!! Enter again!");
            }
        }
    }

    public String setCandidateID(String mess) {
        while (true) {
            boolean check = false;
            System.out.println(mess);
            String str = sc.nextLine();
            str = str.trim();
            if (str.contains(" ")) {
                System.out.println("Invalid ID! Enter again: ");
            } else {
                str = convert(str);
                for (int i = 0; i < list.size(); i++) {
                    if (str.equals(list.get(i).getCandidateId())) {
                        System.out.println("The ID's already created!");
                        check = true;
                    }
                }
                if (check == false) {
                    return str;
                }
            }
        }
    }

}
