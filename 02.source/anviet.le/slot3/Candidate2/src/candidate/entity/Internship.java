/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.entity;

/**
 *
 * @author lean1
 */
public class Internship extends Candidate{
    private String majors;
    private String semester;
    private String uniName;

    public Internship() {
    }

    public Internship(String id, String ltName, String ftName, int birthday, String address, String phone, String email, int type, String majors, String semester, String uniName) {
        super(id, ltName, ftName, birthday, address, phone, email, type);
        this.majors = majors;
        this.semester = semester;
        this.uniName = uniName;
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }
}

