package com.anest.entity;

import com.anest.bo.CandidateList;
import com.fptu.validator.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.anest.bo.Valid;

/**
 *
 * @author MC
 */
public class Condition {

    Scanner in = new Scanner(System.in);
    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public boolean mailChecker(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public String convert(String s) {
        String s0 = "";
        s0 += s.charAt(0);
        if (s.length() > 1) {
            s0 += s.substring(1);
        }
        return s0;
    }

    public boolean idChecker(String id, List<Candidate> candidate) {
        for (int i = 0; i < candidate.size(); i++) {
            if (id.equals(candidate.get(i).getCandidate_ID())) {
                return false;
            }
        }
        return true;
    }

    public String CheckName(String enter) {
        Valid v = new Valid();
        System.out.println(enter);
        String name = in.nextLine();
        name = v.normalizeString(name);
        return name;
    }

    public int CheckBirthDate(String enter) {
        int b;
        while (true) {
            b = Validator.validateInt("Enter your birth date:");
            if (b < 1900 || b > 2017) {
                System.out.println("Invalid Birth date!");
            } else {
                return b;
            }
        }
    }

    public String CheckPhone(String enter) {
        String phone;
        System.out.println(enter);
        while (true) {
            phone = in.nextLine();
            if (phone.length() < 10) {
                System.out.println("Invalid phone number! Enter again:");
            } else {
                for (int i = 0; i < phone.length(); i++) {
                    if (!Character.isDigit(phone.charAt(i))) {
                        System.out.println("Invalid phone number! Enter again:");
                        break;
                    }
                    if (i == phone.length() - 1) {
                        return phone;
                    }
                }
            }
        }
    }


    public int ExpInYear(int birthDate) {
        int ExpInYear;
        while (true) {
            ExpInYear = Validator.validateInt("Enter your experience in year:");
            if (ExpInYear < 0 || ExpInYear > 2017 - birthDate) {
                System.out.println("Invalid input! Enter again:");
            } else {
                return ExpInYear;
            }
        }
    }

    public String gradRank(String enter) {
        System.out.println(enter);
        String gradRank;
        while (true) {
            gradRank = in.nextLine();
            if (gradRank.equalsIgnoreCase("Excellence") == false && gradRank.equalsIgnoreCase("Good") == false && gradRank.equalsIgnoreCase("Fair") == false && gradRank.equalsIgnoreCase("Poor") == false) {
                System.out.println("Graducation rank includes Excellence or Good or Fair or Poor");
            } else {
                return gradRank;
            }
        }
    }
}
