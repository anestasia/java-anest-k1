/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Clothing extends Product {

    private String department;
    private String brand;

    public Clothing() {
    }

    public Clothing(String department, String brand) {
        this.department = department;
        this.brand = brand;
    }

    public Clothing(int id, String category, String department, String brand, int price) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public void setInput() {
        Scanner s = new Scanner(System.in);
        super.setInput();
        System.out.print("Category : Clothing");
        super.setCategory("Clothing");
        System.out.print("Enter department : ");
        this.department = s.nextLine();
        System.out.print("Enter brand : ");
        this.brand = s.nextLine();
        System.out.print("Enter price : ");
        super.setPrice(s.nextInt());
    }

    @Override
    public String toString() {
        return super.toString() + " | " + department + " | " + brand + " | " + super.getPrice();
    }

}
