/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Product;
import java.util.ArrayList;

/**
 *
 * @author Cong Minh
 */
public class Validator {
    public boolean stringChecker(String name) {
        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    public boolean idChecker(String id, ArrayList<Product> lst){
        for(int i=0;i<lst.size();i++){
            if(id.equals(lst.get(i).getId())) return false;
        }
        return true;
    }
    
    
}
