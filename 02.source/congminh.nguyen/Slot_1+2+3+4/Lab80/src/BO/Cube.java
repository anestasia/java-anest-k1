/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cong Minh
 */
public class Cube extends ThreeDimensionalShape{

    private double edge;

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    @Override
    public double calAre() {
        return 6*this.edge*this.edge;
    }

    @Override
    public double calPri() {
       return 0;
    }

    @Override
    public double volume() {
        return Math.pow(edge,3);
    }

    @Override
    public void input() {
        this.edge = Validator.inputDouble("Enter edge: ",  0);
    }
   
}
