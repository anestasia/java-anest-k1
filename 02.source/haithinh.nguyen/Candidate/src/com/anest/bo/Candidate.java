/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

/**
 *
 * @author Thinh
 */
public class Candidate {

    String id;
    String firtsName;
    String lastName;
    int birthDate;
    String address;
    int phone;
    String email;
    protected int type;

    public Candidate() {
    }

    public Candidate(String id, String firtsName, String lastName, int birthDate, String address, int phone, String email) {
        this.id = id;
        this.firtsName = firtsName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
        this.phone = phone;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirtsName() {
        return firtsName;
    }

    public void setFirtsName(String firtsName) {
        this.firtsName = firtsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return id + " | " + firtsName + " " + lastName + " | " + birthDate + " | " + address + " | " + phone + " | " + email;
    }

    public Object callDelete() {
        return null;
    }

    public Object callUpdate() {
        return null;
    }

    public Object callCreat() {
        return null;
    }

    public void input() {
    }
}
