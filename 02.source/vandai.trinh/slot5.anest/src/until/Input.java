/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package until;

import entity.Product;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Input {
    private Scanner in = new Scanner(System.in);
   // check id if id had exist ask cliet reinput
    public static boolean checkID(int id, ArrayList<Product> list) {
        return !list.contains(new Product(id, null, ""));
    }

    public boolean pressYN() {
        in.useDelimiter("\n");
        String s;
        System.out.print("Do you want to continue?(press y to continue): ");
        s = in.nextLine().trim();
        if(s.equalsIgnoreCase("y")) 
            return true;
        else
            return false;
        }
}