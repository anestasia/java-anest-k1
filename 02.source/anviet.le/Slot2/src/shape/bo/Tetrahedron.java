/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.bo;

import com.edu.Validate.Validator;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class Tetrahedron extends ThreeDimensionalShapes{
    private double a;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public Tetrahedron(double a) {
        this.a = a;
    }

    public Tetrahedron() {
    }
    @Override
    public double calArea() {
        return Math.sqrt(3)*this.a*this.a;
        
    }

    @Override
    public double calPri() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void input() {
          System.out.print("Enter a=");
        this.a = Validator.validateDouble();
    }

    @Override
    public double volume() {
        return (Math.pow(this.a, 3))/(6*Math.sqrt(2));
    }
    
}
