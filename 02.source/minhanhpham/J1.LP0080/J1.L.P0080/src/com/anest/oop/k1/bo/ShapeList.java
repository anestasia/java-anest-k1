/**
 * 
 */
package com.anest.oop.k1.bo;

import java.util.ArrayList;
import java.util.List;

import com.anest.oop.k1.entity.Shape;
import com.anest.oop.k1.entity.twoDimensional.Circle;
import com.anest.oop.k1.entity.twoDimensional.Square;
import com.anest.oop.k1.entity.twoDimensional.Triangle;

/**
 * @author admin
 *
 */
public class ShapeList {

	private List<Shape> shapes;

	/**
	 *  The constructor without parameters
	 */
	public ShapeList() {
		this.shapes = new ArrayList<>();
	}

	/**
	 * @param shapes
	 */
	public ShapeList(List<Shape> shapes) {
		super();
		this.shapes = shapes;
	}
	
	/**
	 * this function to appends the specified element to the end of this list 
	 * @param shape
	 */
	public void add(Shape shape) {
		this.shapes.add(shape);
	}
	
	/**
	 * this function to removes the element at the specified position in this list 
	 * @param index
	 */
	public void removeAt(int index) {
		this.shapes.remove(index);
	}
	
	/**
	 * @param area
	 * @return
	 */
	public Shape searchArea(double area) {
		for (int i = 0; i < this.shapes.size() - 1; ++i) {
			if (this.shapes.get(i).calArea() == area) {
				return this.shapes.get(i);
			}
		}
		return null;
	}
	
	/**
	 * this function to show all shapes
	 */
	public void showList() {
		if(this.shapes.isEmpty()) 
			System.err.println(" empty! ");
		for (Shape shape : this.shapes) {
			shape.output();
		}
	}

	/**
	 * this function to get all circle of list shapes
	 * 
	 * @return
	 */
	public List<Circle> getCircles() {
		List<Circle> circles = new ArrayList<>();
		for (Shape shape : shapes) {
			if (shape instanceof Circle) {
				circles.add((Circle) shape);
			}
		}
		return circles;
	}

	/**
	 * this function to get all triangle of list shapes
	 * 
	 * @return
	 */
	public List<Triangle> getTriangles() {
		List<Triangle> triangles = new ArrayList<>();
		for (Shape shape : shapes) {
			if (shape instanceof Triangle) {
				triangles.add((Triangle) shape);
			}
		}
		return triangles;
	}

	/**
	 * this function to get all square of list shapes
	 * 
	 * @return
	 */
	public List<Square> getSquares() {
		List<Square> squares = new ArrayList<>();
		for (Shape shape : shapes) {
			if (shape instanceof Square) {
				squares.add((Square) shape);
			}
		}
		return squares;
	}

}
