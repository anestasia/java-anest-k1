/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.bo;


import com.edu.Validate.Validator;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class Triangle extends TwodementionalShape {

    double a;
    double b;
    double c;
    double p;

    public Triangle() {
    }

    public Triangle(double a, double b, double c, double p) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.p = p;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    @Override
    public double calPri() {
        
        return this.a + this.b + this.c;
    }

    @Override
    public double calArea() {
        return (Double)Math.sqrt((calPri()/ 2) * ((calPri() / 2) - a) * ((calPri() / 2) - b) * ((calPri() / 2) - c));
    }

    @Override
    public void input() {
        System.out.print("Enter a=");
        this.a = Validator.validateDouble();
        System.out.print("Enter b=");
        this.b = Validator.validateDouble();
        System.out.print("Enter c=");
        this.c = Validator.validateDouble();
    }

    public double checkTriangle(){
        if(this.a < 0|| this.b<0||this.c<0||(this.a+this.b)<this.c||(this.b+this.c)<this.a||(this.a+this.c)<this.b){
            return 0;
        }     
        else return 1;
    }
}