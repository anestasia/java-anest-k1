package util;

import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Minh on 1/14/2017.
 */
public class Validate {
    public int getInt(String mes, String err, int min, int max) {
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                System.out.print(mes);
                int num = in.nextInt();
                if (num < min || num > max)
                    System.out.println(err);
                else
                    return num;
            } catch (InputMismatchException e) {
                System.out.println(err);
            }
        }
    }

    public boolean pressYN() {
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print("Do you want to continue?(Y - Yes, N - No) ");
            String s = in.nextLine();
            if (s.equalsIgnoreCase("Y"))
                return true;
            if (s.equalsIgnoreCase("N"))
                return false;
            System.out.println("Invalid choice");
        }
    }

    public boolean isYear(int year, int num) {
        int now = Calendar.getInstance().get(Calendar.YEAR);
        if (num < year && num > now)
            return false;
        return true;
    }

    public boolean isPhone(String phone) {
        if (phone.length() < 10) {
            System.out.println("Invalid phone number");
            return false;
        }
        for (int i = 0; i < phone.length(); i++) {
            if (phone.charAt(i) < 48 || phone.charAt(i) > 57) {
                System.out.println("Invalid phone number");
                return false;
            }
        }
        return true;
    }

    public boolean isEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,6}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        if (!m.matches()) {
            System.out.println("Invalid email");
            return false;
        }
        return true;
    }

    public boolean isRankOfGraduation(String rank) {
        if (rank.equalsIgnoreCase("Excellence") || rank.equalsIgnoreCase("Good")
                || rank.equalsIgnoreCase("Fair") || rank.equalsIgnoreCase("Poor"))
            return true;
        System.out.println("Invalid Rank of graduation");
        return false;
    }
}