/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

/**
 *
 * @author OPF_Lep
 */
public class Internship extends Candidate {

    String majors;
    String semester;
    String universityName;

    public String getMajors() {
        return majors;
    }

    public Internship(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail, int type) {
        super(ID, firstName, lastName, birthDate, Address, phone, eMail, type);
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Internship(String majors, String semester, String universityName, String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail) {
        super(ID, firstName, lastName, birthDate, Address, phone, eMail);
        this.majors = majors;
        this.semester = semester;
        this.universityName = universityName;
    }

    public Internship() {
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
