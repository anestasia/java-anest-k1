/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;

import java.util.Scanner;
import shape.bo.ThreeDimensionShape;

/**
 *
 * @author Eo Mo
 */
public class Cube extends ThreeDimensionShape {

    private double canh;

    public double getCanh() {
        return canh;
    }

    public void setCanh(double canh) {
        this.canh = canh;
    }

    @Override
    public double calArea() {
        return this.canh * this.canh * this.canh;
    }

    @Override
    public double calPri() {
        return this.canh * 4;
    }

    @Override
    public void input() {
        System.out.println("nhap vao canh: " + this.canh);
        this.canh = new Scanner(System.in).nextDouble();
    }

}
