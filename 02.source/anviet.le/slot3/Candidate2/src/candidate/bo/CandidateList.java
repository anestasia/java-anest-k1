/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.bo;
import candidate.entity.Candidate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import candidate.bo.Utils;
import candidate.entity.Expericene;
import candidate.entity.Fresher;
import candidate.entity.Internship;

public class CandidateList {
      ArrayList<Candidate> candi = new ArrayList<>();
   
    Scanner sc = new Scanner(System.in);
    Utils ut=new Utils();
    public void getExperience() {
        do {
            Expericene ex = new Expericene();
            System.out.print("Enter ID: ");
            ex.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            ex.setFirstName(sc.nextLine());
            System.out.print("Enter Last name: ");
            ex.setLastName(sc.nextLine());
            ex.setBirthday(ut.getIntNumber("Enter Birth year: ", "Please enter a year : ", 1900, 2017));
            System.out.print("Enter Address: ");
            ex.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (ut.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers:");
                System.out.println("");
            } while (true);
            ex.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (ut.isEmail(mail)) 
                    break;
                System.out.println("Please enter an email!!!");
            } while (true);
            ex.setType(0);
            ex.setExpInYear(ut.getIntNumber("Enter Year of experience: ", "Error, enter 0-100:", 0, 100));
            System.out.print("Rank of Graduation: ");
            ex.setProSkill(sc.nextLine());
            candi.add(ex);
        } while (ut.checkContinue());
    }

    public void getFresher() {
        do {
            Fresher tp = new Fresher();
            System.out.print("Enter ID: ");
            tp.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            tp.setFirstName(sc.nextLine());
            System.out.print("Enter Last name: ");
            tp.setLastName(sc.nextLine());
            tp.setBirthday(ut.getIntNumber("Enter Birth year: ", "Please enter a year: ", 1900, Integer.MAX_VALUE));
            System.out.print("Enter Address: ");
            tp.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (ut.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers:");
                System.out.println("");
            } while (true);
            tp.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (ut.isEmail(mail)) break;
                System.out.println("Please enter an email!!!");
            } while (true);
            tp.setType(1);
            System.out.print("Enter Graduation date: ");
            tp.setGraduation_date(sc.nextLine());
            String rankOfGraduation;
            do {
                System.out.print("Enter Rank of Graduation: ");
                rankOfGraduation = sc.nextLine();
            } while (!ut.isRank(rankOfGraduation));
            tp.setGraduation_rank(rankOfGraduation);
            System.out.print("Enter Education: ");
            tp.setEducation(sc.nextLine());
            candi.add(tp);
        } while (ut.checkContinue());
    }

    public void getIntern() {
        do {
            Internship tp = new Internship();
            System.out.print("Enter ID: ");
            tp.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            tp.setFirstName(sc.nextLine());
            System.out.print("Enter Last name: ");
            tp.setLastName(sc.nextLine());
            tp.setBirthday(ut.getIntNumber("Enter Birth year: ", "Please enter a year : ", 1900, 2017));
            System.out.print("Enter Address: ");
            tp.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (ut.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers:");
                System.out.println("");
            } while (true);
            tp.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (ut.isEmail(mail)) break;
                System.out.println("Please enter an email:");
            } while (true);
            tp.setType(2);
            System.out.print("Enter Major: ");
            tp.setMajors(sc.nextLine());
            System.out.print("Enter Semester: ");
            tp.setSemester(sc.nextLine());
            System.out.print("Enter University: ");
            tp.setUniName(sc.nextLine());
            candi.add(tp);
        } while (ut.checkContinue());
    }

    public void searching() {
        System.out.print("Enter First or Last Name: ");
        String searchingName = sc.nextLine();
        int type = ut.getIntNumber("Enter Type: ", "Error! Please enter 1-2:", 0, 2);
        for (int i = 0; i < candi.size();i++){
            if(candi.get(i).getType()==type&&(candi.get(i).getFirstName().contains(searchingName)||candi.get(i).getLastName().contains(searchingName)))
                System.out.println("the information to print:"+ candi.get(i).getFirstName() + "|| " + candi.get(i).getLastName()+"||"+ candi.get(i).getAddress()+"||"+candi.get(i).getPhone());
        }
    }
    
}



