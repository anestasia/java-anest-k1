/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dab;

import bo.Book;
import bo.Clothing;
import bo.Disc;
import bo.Product;
import com.edu.fpt.validate.Validator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Valid {

    public static void show(ArrayList<Product> productList) {
        for (Product product : productList) {
            System.out.println(product);
        }
    }

    public static void sortAndFind(ArrayList<Product> list) {
        System.out.println("1. sort(by price)");
        System.out.println("2. find(by id)");
        int choice = Validator.validateInt(1, 2, "Enter your choice: ");
        switch (choice) {
            case 1:
                Collections.sort(list);
                show(list);
                break;
            case 2:
                boolean test = false;
                System.out.println("Enter the id of product: ");
                int id = new Scanner(System.in).nextInt();
                for (Product pr : list) {
                    if (id == pr.getId()) {
                        System.out.println(pr);
                        test = true;
                    }
                }
                if (!test) {
                    System.out.println("No found!!!");
                }
        }
    }

    public void creatPro(ArrayList<Product> product) {
        System.out.println("you want to creat: ");
        System.out.println("1. Book");
        System.out.println("2. Clothing");
        System.out.println("3. Disc");
        System.out.println("0. exits");
        Product pro = new Product();
        int choice1 = Validator.validateInt(0, 3, "Enter your choice :");
        switch (choice1) {
            case 1:
                pro = new Book();
                break;
            case 2:
                pro = new Clothing();
                break;
            case 3:
                pro = new Disc();
                break;
            case 0:
                System.out.println("goodbye :) ");
                return;
        }
        pro.setInput();
        product.add(pro);
    }
}
