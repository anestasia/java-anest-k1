/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.threeDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import bo.Utils;

public class Tetrahedron extends ThreeDimensionalShape {

    private double edge;

    public Tetrahedron() {
        super();
        this.edge = 0;
    }

    public Tetrahedron(double edge) {
        this.edge = edge;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#calArea()
     */
    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        return Math.sqrt(3) * Math.pow(this.edge, 2);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#input()
     */
    @Override
    public void input() {
        // TODO Auto-generated method stub
        this.edge = Utils.getDoubleNumber("Enter square : ", Double.MAX_VALUE, 0);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#output()
     */
    @Override
    public void output() {
        // TODO Auto-generated method stub
        System.out.println(" Sphere : \t edge : " + this.edge);
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Vol  :" + this.calVol());
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#volume()
     */
    @Override
    public double calVol() {
        // TODO Auto-generated method stub
        return Math.sqrt(2) / 12 * Math.pow(this.edge, 3);
    }
}
