/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.*;
import bo.Validation;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Taka
 */
public class Installization {

    private Validation vl;
    //Danh sách chứa những sản phẩm đã có trong cửa hàng
    private ArrayList<Product> list_product;
    private FileDBContent db;

    public Installization() {
        this.list_product = new ArrayList<>();
        this.vl = new Validation();
        this.db = new FileDBContent();
    }

    public ArrayList<Product> getList_product() {
        return list_product;
    }

    public void setList_product(ArrayList<Product> list_product) {
        this.list_product = list_product;
    }

    public Validation getVl() {
        return vl;
    }

    public void setVl(Validation vl) {
        this.vl = vl;
    }

    public void create() {
        boolean checkContinue;
        //Vong lặp để bắt tạo tối đa 10 sản phẩm
        do {
            int count = 1;
            while (count <= 10) {
                boolean check = true;
                int choice = vl.setInteger("Which product do you want to create? \n"
                        + "1.Book\n"
                        + "2.Clothing\n"
                        + "3.Disc\n", 1, 3);
                switch (choice) {
                    case 1:
                        //tạo sản phẩm book, xác định giá trị cho category luôn
                        Product book = new Book(Categories.Book);
                        //truyền danh sách sản phẩm có trong cửa hàng
                        book.input(list_product);
                        //kiểm tra sản phẩm đã tồn tại hay chưa
                        check = vl.checkDuplicateItem(list_product, book);
                        //chưa tồn tại thì thêm vào
                        if (check) {
                            list_product.add(book);
                            db.writeAtTheEndOfFile(book);
                        }
                        break;
                    case 2:
                        Product clothes = new Clothing(Categories.Clothing);
                        clothes.input(list_product);
                        check = vl.checkDuplicateItem(list_product, clothes);
                        if (check) {
                            list_product.add(clothes);
                            db.writeAtTheEndOfFile(clothes);
                        }
                        break;
                    case 3:
                        Product disc = new Disc(Categories.Disc);
                        disc.input(list_product);
                        check = vl.checkDuplicateItem(list_product, disc);
                        if (check) {
                            list_product.add(disc);
                            db.writeAtTheEndOfFile(disc);
                        }
                        break;
                }
                //Nếu sản phẩm được thêm mới thì sẽ thông báo cho người dùng
                //Ghi lại thông tin các sản phẩm trong file txt
                if (check) {
                    System.out.println("You just create " + count + " product!" + "You must create more " + (10 - count) + " product!");
                    count++;
                }
            }
            checkContinue = vl.checkContinue("Do you want to continue (Y/N)? Choose Y to continue, N to return main screen.");
        } while (checkContinue);
    }

    public void reportAndSave() {
        if (list_product.isEmpty()) {
            final JDialog dialog = new JDialog();
            dialog.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(dialog,
                    "If you have loaded data from file already and this message still exist, please create new product!"
                    + "\nOtherwise please load data from file.");
        } else {
            System.out.println("SHOP SOUVENIR");
            for (int i = 0; i < list_product.size(); i++) {
                System.out.println(list_product.get(i));
            }
        }
    }

    public void sortByPrice() {
        //tao list moi chua phan tu cua list ban dau
        ArrayList<Product> list_new = new ArrayList<>();
        //copy cac phan tu cua list cu qua clone
        for (int i = 0; i < list_product.size(); i++) {
            list_new.add(list_product.get(i));
        }
        //xoa list cu
        list_product.clear();
        //tim kiem toi khi nao list moi bi xoa sach
        while (!list_new.isEmpty()) {
            //vi tri cua so lon nhat
            int index = 0;
            //gia tri max cua san pham:
            double min = list_new.get(0).getPrice();
            //duyet cac phan tu trong clone de tim so lon nhat
            for (int i = 0; i < list_new.size(); i++) {
                //so sanh phan tu thu i voi max
                //neu price cua ima lon hon max thi gan index = vi tri cua i
                if (min > list_new.get(i).getPrice()) {
                    index = i;
                    min = list_new.get(i).getPrice();
                }
            }
            //search trong mang moi khi nao index trung nhau thi them phan tu do vao mang ban dau
            for (int i = 0; i < list_new.size(); i++) {
                if (index == i) {
                    list_product.add(list_new.get(index));
                }
            }
            //xoa phan tu tai vi tri index cua mang moi
            list_new.remove(index);
        }
//        //copy mang hien tai qua mang moi
//        for (int i = 0; i < list_product.size(); i++) {
//            list_new.add(list_product.get(i));
//        }
//        //xoa mang cu vi luu phan tu tu lon toi be
//        list_product.clear();
//        //copy nguoc mang moi vao mang cu
//        for (int i = list_new.size() - 1; i >= 0; i--) {
//            list_product.add(list_new.get(i));
//        }
////        xoa mang moi
//        list_new.clear();
        //ghi list vua thu duoc vao file
        db.writeToFile(list_product);
    }

    public boolean findByID(String ID) {
        for (int i = 0; i < list_product.size(); i++) {
            if (ID.equals(Integer.toString(list_product.get(i).getId()))) {
                System.out.println(list_product.get(i));
                return true;
            }
        }
        return false;
    }

    void update(String string) {
        for (int i = 0; i < list_product.size(); i++) {
            if (string.equals(Integer.toString(list_product.get(i).getId()))) {
                switch (list_product.get(i).getCategory()) {
                    case Book:
//                        title, author, price
                        String title = vl.setString("Please input book's title: ");
                        String author = vl.setString("Please input book's author: ");
                        Double priceB = vl.setDouble("Please input new price: ", 0, Double.MAX_VALUE);
                        Product book = new Book(Categories.Book, list_product.get(i).getId(), title, author, priceB);
                        list_product.set(i, book);
                        break;
                    case Clothing:
//                        , department, brand, price
                        String department = vl.setString("Please input clothing's department: ");
                        String brand = vl.setString("Please input clothing's author: ");
                        Double priceC = vl.setDouble("Please input new price: ", 0, Double.MAX_VALUE);
                        Product clothing = new Book(Categories.Book, list_product.get(i).getId(), department, brand, priceC);
                        list_product.set(i, clothing);
                        break;
                    case Disc:
//, genre, format, price.
                        String genre = vl.setString("Please input disc's genre: ");
                        String format = vl.setString("Please input disc's format: ");
                        Double priceD = vl.setDouble("Please input new price: ", 0, Double.MAX_VALUE);
                        Product disc = new Book(Categories.Book, list_product.get(i).getId(), genre, format, priceD);
                        list_product.set(i, disc);
                        break;
                }
                System.out.println("UPDATE SUCCESSFULLY!!!");
                break;
            }
        }
    }

    void delete(String string) {
        for (int i = 0; i < list_product.size(); i++) {
            if (string.equals(Integer.toString(list_product.get(i).getId()))) {
                System.out.println("DELETE COMPLETED!!!");
                list_product.remove(i);
                break;
            }
        }
    }

}
