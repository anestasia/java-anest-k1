package bo;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public abstract class TwoDim implements Shape {
    public abstract double getPerimeter();

    public abstract double getArea();

    public abstract void input();

}
