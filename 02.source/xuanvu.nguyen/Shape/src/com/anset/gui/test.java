/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anset.gui;

import com.anest.bo.Circle;
import com.anest.bo.Cube;
import com.anest.bo.Shape;
import com.anest.bo.Sphere;
import com.anest.bo.Square;
import com.anest.bo.Tetrahedron;
import com.anest.bo.ThreeDimensionalShape;
import com.anest.bo.Triagle;
import com.edu.fpt.validate.Validator;

/**
 *
 * @author Thinh
 */
public class test {

    public static void main(String[] args) {
        int choice = 0;
        Shape shape = null;
        ThreeDimensionalShape three = null;
        while (true) {
            System.out.println("Choose type of shape: ");
            System.out.println("1- Two Demensional");
            System.out.println("2- Three Demensional");
            System.out.println("0- Exits");
            System.out.println("-------------");
            choice = Validator.validateInt("Enter your choice :");
            switch (choice) {
                case 1:
                    System.out.println("Choose shape you want to calculate: ");
                    System.out.println("1- Circle.");
                    System.out.println("2- Square.");
                    System.out.println("3- Triangle.");
                    int choice1 = Validator.validateInt("Enter your choice : ");
                    switch (choice1) {
                        case 1:
                            shape = new Circle();
                            break;
                        case 2:
                            shape = new Square();
                            break;
                        case 3:
                            shape = new Triagle();
                            break;
                        default:
                            System.out.println("Invalid input. Try again!");
                            break;
                    }
                    break;
                case 2:
                    System.out.println("Choose shape you want to calculate: ");
                    System.out.println("1- Sphrere.");
                    System.out.println("2- Cube.");
                    System.out.println("3- Tetrahedron.");
                    int choice2 = Validator.validateInt("Enter your choice : ");
                    switch (choice2) {
                        case 1:
                            shape = new Sphere();
                            three = new Sphere();
                            break;
                        case 2:
                            shape = new Cube();
                            three = new Cube();
                            break;
                        case 3:
                            shape = new Tetrahedron();
                            three = new Tetrahedron();
                            break;
                        default:
                            System.out.println("Invalid input. Try again!");
                            break;
                    }
                    break;
                case 0:
                    System.out.println("goodbye :) ");
                    return;
                default:
                    System.out.println("Invalid input. Try again!");
                    break;
            }
            if (shape == null) {
                continue;
            }
            shape.inPut();
            if (shape.calArea() != 0) {
                System.out.println("Dien tich la: " + shape.calArea());
            }
            if (shape.calPri() != 0) {
                System.out.println("Chu vi la   : " + shape.calPri());
            }
            if (three.calV() != 0) {
                System.out.println("The tich la : " + three.calV());
            }
            System.out.println("-------------------------");
        }
    }
}
