/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

/**
 *
 * @author OPF_Lep
 */
public class Experience extends Candidate {

    public Experience(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail, int type) {
        super(ID, firstName, lastName, birthDate, Address, phone, eMail, type);
    }

    private int ExpInYear;
    private String ProSkill;

    public int getExpInYear() {
        return ExpInYear;
    }

    public void setExpInYear(int ExpInYear) {
        this.ExpInYear = ExpInYear;
    }

    public String getProSkill() {
        return ProSkill;
    }

    public void setProSkill(String ProSkill) {
        this.ProSkill = ProSkill;
    }

    public Experience() {
    }

    public Experience(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail) {
        super(ID, firstName, lastName, birthDate, Address, phone, eMail);
    }

 

    public Experience(int ExpInYear, String ProSkill, String CandidateId, String FirstName, String LastName, int BirthDate, String Address, String Phone, String Email) {
        super(CandidateId, FirstName, LastName, BirthDate, Address, Phone, Email);
        this.ExpInYear = ExpInYear;
        this.ProSkill = ProSkill;
        super.type = 0;
    }

}
