/**
 * Created by W10-PRO on 28-Jan-17.
 */
public class Main {
    public static void main(String[] args) {
        Editor edit = new Editor();
        Inoutput in = new Inoutput();
        String str = in.input();
        if(str != ""){
            System.out.println("Before: " + str);
            str = edit.trimSpace(str);
            str = str.toLowerCase();
            str = edit.upperCaseAfterDot(str);
            System.out.println("After: " + str);
            in.output(str);
        }
        else System.out.println("Nothing in input file!");

    }
}
