/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.gui;

import candidate.bo.Deverloper;
import candidate.bo.Experience;
import candidate.bo.Fresher;
import candidate.bo.Internship;
import candidate.until.Input;
import java.util.ArrayList;
import java.util.Scanner;
import javax.print.attribute.standard.PrinterStateReason;

/**
 *
 * @author Trinh Dai
 */
public class Funtions {

    public void showAllExp(ArrayList<Deverloper> Candidates) {
        System.out.println("===========EXPERIENCE CANDIDATE============");

        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 0) {
                list.showInfor();
            }
        }
    }

    public void showAllFre(ArrayList<Deverloper> Candidates) {
        System.out.println("===========FRESHER CANDIDATE============");

        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 1) {
                list.showInfor();
            }
        }
    }

    public void showAllInter(ArrayList<Deverloper> Candidates) {
        System.out.println("===========INTERSHIP CANDIDATE============");

        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 2) {
                list.showInfor();
            }
        }
    }

//    find candidate to update
    public Deverloper searchdeverlop(ArrayList<Deverloper> Candidates, int type) {
        Input input = new Input();
        Scanner in = new Scanner(System.in);
        Double inputId;
        do {
            System.out.println("Input Candidate ID : ");
            inputId = in.nextDouble();
            for (Deverloper list : Candidates) {
                if (list.getCandidateTpye() == type && inputId == list.getId()) {
                    return list;
                }
            }
            System.out.println("Not found");
        } while (input.pressYN());
        return null;
    }

    public void createExp(ArrayList<Deverloper> Candidates) {
        Deverloper exp = new Experience();
        exp.input();
        Candidates.add(exp);
    }
    
    public void createFre(ArrayList<Deverloper> Candidates) {
        Deverloper fre = new Fresher();
        fre.input();
        Candidates.add(fre);
    }
    
    public void createIntern(ArrayList<Deverloper> Candidates) {
        Deverloper intern = new Internship();
        intern.input();
        Candidates.add(intern);
    }
    
    
    public void updateExp(ArrayList<Deverloper> Candidates) {
        Deverloper exp = new Deverloper();
        exp = searchdeverlop(Candidates, 0);
        exp.input();
    }

    public void updateFre(ArrayList<Deverloper> Candidates) {
        Deverloper fre = new Deverloper();
        fre = searchdeverlop(Candidates, 1);
        fre.input();
    }

    public void updateIntern(ArrayList<Deverloper> Candidates) {
        Deverloper intern = new Deverloper();
        intern = searchdeverlop(Candidates, 2);
        intern.input();
    }

    public void deleteExp(ArrayList<Deverloper> Candidates) {
        for (Deverloper list : Candidates) {
            if (list.getId() == searchdeverlop(Candidates, 0).getId()) {
                Candidates.remove(list);
                return;
            }
        }
    }

    public void deleteFre(ArrayList<Deverloper> Candidates) {
        for (Deverloper list : Candidates) {
            if (list.getId() == searchdeverlop(Candidates, 1).getId()) {
                Candidates.remove(list);
                return;
            }
        }
    }

    public void deleteIntern(ArrayList<Deverloper> Candidates) {
        for (Deverloper list : Candidates) {
            if (list.getId() == searchdeverlop(Candidates, 2).getId()) {
                Candidates.remove(list);
                return;
            }
        }
    }

}
