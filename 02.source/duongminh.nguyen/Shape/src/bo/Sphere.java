package bo;

import common.Common;
import java.util.Scanner;

public class Sphere extends ThreeDimensionalShape {

    private float radius;
    private float area;
    private float volume;

    public Sphere(float radius, float area, float volume) {
        this.radius = radius;
        this.area = area;
        this.volume = volume;
    }

    public Sphere() {
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Enter the radius for your sphere: ");
            setRadius(in.nextFloat());
        } while (!Common.isNumber(in.nextLine())||Common.isPositveNumber(in.nextFloat()));
    }

    @Override
    public float calArea() {
        return (float) (4*Math.PI*getRadius()*getRadius());
    }

    @Override
    public float calVolume() {
    return (float) ((4*getRadius()*getRadius()*getRadius()*Math.PI)/3);
    }

    @Override
    public void output() {
      System.out.println("Your sphere has volume is: " + calVolume() + "\n");
        System.out.println("Your sphere has area is:" + calArea());}
    

}
