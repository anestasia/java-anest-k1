//
package shape.bo;


import java.util.*;


public class Square extends TwodementionalShape{
    double e;

    public Square(double e) {
        this.e = e;
    }
    
    public Square() {
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    @Override
    public double calArea() {
      return  this.e*this.e;
    }

    @Override
    public double calPri() {
        return 4*this.e;
    }

    @Override
    public void input() {
        System.out.print("Enter e=");
        this.e=new Scanner(System.in).nextDouble();
    }
    
    
}
