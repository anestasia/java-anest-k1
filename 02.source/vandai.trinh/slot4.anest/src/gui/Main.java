/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Entity.Convert;
import Entity.Input;
import Until.Validate;

/**
 *
 * @author Trinh Dai
 */
public class Main {
    public static void main(String[] args) {
        Convert convert = new Convert();
        Input in = new Input();
        Menu menu = new Menu();
        Validate validate = new Validate();
        int choice;
        do {
            menu.headMenu();
            choice = (int)validate.getNumber("Enter your choice: ", "Invalid choice!!", 0, 6);
            switch(choice) {
                case 1:
                    do {
                        in.inputBinary();
                        System.out.println("Binary to Decimal is :" + convert.binToDec(in.getInputString()));
                    }while(validate.pressYN());
                    break;
                case 2:
                    do {
                        in.inputBinary();
                        System.out.println("Binary to Hex is :" + convert.binaryToHex(in.getInputString()));
                    }while(validate.pressYN());
                    break;
                case 3:
                    do {
                        in.inputDec();
                        System.out.println("Binary to Decimal is :" + convert.decToBin(in.getInputString()));
                    }while(validate.pressYN());
                    break;
                case 4:
                    do {
                        in.inputDec();
                        System.out.println("Binary to Hex is :" + convert.decToHex(in.getInputString()));
                    }while(validate.pressYN());
                    break;
                case 5:
                    do {
                        in.inputHex();
                        System.out.println("Binary to Decimal is :" + convert.hexToBinary(in.getInputString()));
                    }while(validate.pressYN());
                    break;
                case 6:
                    do {
                        in.inputHex();
                        System.out.println("Binary to Hex is :" + convert.hexToDec(in.getInputString()));
                    }while(validate.pressYN());
                    break;
            }
        }while(choice != 0);
    }
}
