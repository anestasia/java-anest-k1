/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;

import java.util.Scanner;
import shape.bo.Shape;

/**
 *
 * @author Eo Mo
 */
public class Test {

    public static void main(String[] args) {
        Shape shape;
        Shape[] test = new Shape[100];

        for (int i = 0; i < 10; i++) {
            String choice;

            System.out.println("hay dien cac lua chon sau (1-Circle, 2- Square, 3-Triangle, 4-Sphere, 5-Cube, 0-Exit");
            choice = new Scanner(System.in).nextLine();
            for (int j = 0; j < 10; j++) {
                if (choice.equals("1")) {
                    test[i] = new Circle();
                }
                if (choice.equals("2")) {
                    test[i] = new Square();

                }
                if (choice.equals("3")) {
                    test[i] = new Triangle();
                }
                if (choice.equals("4")) {
                    test[i] = new Sphere();

                }
                if (choice.equals("5")) {
                    test[i] = new Cube();
                }
                if (choice.equals("0")) {
                    System.out.println("Exit");
                }
            }

            test[i].input();
            System.out.print("Diện tích ( Thể tích )= " + test[i].calArea());
            System.out.println("Chu vi= " + test[i].calPri());

        }

    }

}
