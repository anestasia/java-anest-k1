package com.anest.bo;

import static com.anest.bo.Input.inputBin;
import static com.anest.bo.Input.inputDec;
import static com.anest.bo.Input.inputHex;

/**
 *
 * @author MyPC
 */
public class ChangBase {

    public static void base(int inBase, int outBase) {
        if (inBase == 1) {
            if (outBase == 2) {
                binToDec();
            }

            if (outBase == 3) {
                binToHex();
            }
        }
        if (inBase == 2) {
            if (outBase == 1) {
                decToBin();
            }
            if (outBase == 3) {
                decToHex();
            }
        }
        if (inBase == 3) {
            if (outBase == 1) {
                hexToBin();
            }
            if (outBase == 2) {
                hexToDec();
            }
        }

    }

    public static void decToHex() {
        int dec = inputDec();
        System.out.println("Convert " + dec + "(Dec) = " + Integer.toHexString(dec) + "(Hex)");
    }

    public static void decToBin() {
        int dec = inputDec();
        System.out.println("Convert: " + dec + "(Dec) = " + Integer.toBinaryString(dec) + "(Bin)");
    }

    public static void hexToBin() {

        String hex = inputHex();
        int dec = Integer.parseInt(hex, 16);
        System.out.println("Convert:  " + hex + "(Hex) = " + Integer.toBinaryString(dec) + "(Bin)");
    }

    public static void hexToDec() {
        String hex = inputHex();
        System.out.println("Convert:  " + hex + "(Hex) = " + Integer.parseInt(hex, 16) + "(Dec)");
    }

    public static void binToHex() {

        String bin = inputBin();
        int dec = Integer.parseInt(bin, 2);
        System.out.println("Convert:  " + bin + "(Bin) = " + Integer.toHexString(dec) + "(Hex)");
    }

    public static void binToDec() {
        String bin = inputBin();
        System.out.println("Convert:  " + bin + "(Bin) = " + Integer.parseInt(bin, 2) + "(Dec)");
    }

}
