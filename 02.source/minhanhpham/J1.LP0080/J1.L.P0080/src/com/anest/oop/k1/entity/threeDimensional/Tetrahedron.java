/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.oop.k1.entity.threeDimensional;

import com.anest.oop.k1.bo.Utils;

/**
 *
 * @author admin
 */
public class Tetrahedron extends ThreeDimensionalShape {

    private double a;
    private double b;
    private double c;
    private double d;

    @Override
    public double calArea() {
        double p1 = (a + b + c) / 2;
        double p2 = (a + b + d) / 2;
        double p3 = (a + d + c) / 2;
        double p4 = (d + b + c) / 2;
        return Math.sqrt(p1 * (p1 - a) * (p1 - b) * (p1 - c)) + Math.sqrt(p2 * (p2 - a) * (p2 - b) * (p2 - d)) + Math.sqrt(p3 * (p3 - a) * (p3 - d) * (p3 - c)) + Math.sqrt(p4 * (p4 - d) * (p4 - b) * (p4 - c));
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#calPri()
     */
    @Override
    public double calPri() {
        return this.a + this.b + this.c + this.d;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#input()
     */
    @Override
    public void input() {
        this.a = Utils.getDoubleNumber("Enter Triangle a: ", Double.MAX_VALUE, 0);
        this.b = Utils.getDoubleNumber("Enter Triangle b: ", Double.MAX_VALUE, 0);
        this.c = Utils.getDoubleNumber("Enter Triangle c: ", Double.MAX_VALUE, 0);
        this.d = Utils.getDoubleNumber("Enter Triangle d: ", Double.MAX_VALUE, 0);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#output()
     */
    @Override
    public void output() {
        System.out.println(" Tetrahedron : \t Four sides : (" + this.a + ", " + this.b + ", " + this.c + ", " + this.d + ")");
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Pri  :" + this.calPri());
        System.out.println("\t+ Volume: " + this.volume());
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#volume()
     */

    @Override
    public double volume() {
        return 0;
    }

}
