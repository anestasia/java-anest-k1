/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Square extends TwoDimensionalShape{
    private double edge;
    public Square() {
    }
 
    public double getEdge() {
        return edge;
    }

    public void getEdge(double edge) {
        this.edge = edge;
    }
    
    @Override
    public double calArea() {
        return this.edge * this.edge;
       }

    @Override
    public double calPri() {
        return 4 * this.edge;
    }

    @Override
    public void inPut() {
        System.out.println(" Square:");
        System.out.println("enter e= ");
        this.edge= new Scanner(System.in).nextDouble();
    }
  
}
