/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Cube extends ThreeDimensionalShape {

    private double edge;

    public Cube() {
        super();
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public double callPri() {
        return 0;
    }

    @Override
    public double callArea() {
        return 6 * edge * edge;
    }

    @Override
    public double Volume() {
        return this.edge * this.edge * this.edge;
    }

    @Override
    public void input() {
        System.out.println("Enter edge = ");
        this.edge = new Scanner(System.in).nextDouble();
    }

}
