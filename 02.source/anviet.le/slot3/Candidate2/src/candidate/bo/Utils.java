/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.bo;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author thienbd@gmail.com
 * <p>
 * </p>
 */
public class Utils {
	
	private static Scanner scanner = new Scanner(System.in);
	
	/**
	 * Press anykey to continue
	 * 
	 * @return <code>true</code> if user want to continue "Y/y", <code>false</code> 
	 */
    public static boolean checkContinue() {
        boolean check;
        while (true) {
            System.out.print("Do you want to continue? Y/N?: ");
            String c = scanner.nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                check = true;
                return check;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                System.out.println("EXIT_SUCCESS");
                check = false;
                return check;
            } else {
                System.out.println("Invalid input!!! Enter again: ");
            }
        }
    }
    
    /**
     * The function to get an integer from the keyboard in console application
     * 
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public int getIntNumber(String mes, String error, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        while (true) {
            try {
                System.out.print(mes);
                number = sc.nextInt();
                if (number < min || number > max) {
                    System.out.println(error);
                    sc.nextLine();
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }
    
    /**
     * The function to get a double from the keyboard in console application
     * 
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return a double from keyboard
     */
    public static double getDoubleNumber(String mess,double max,double min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                double result = Double.parseDouble(str);
                if(result>max || result <min){
                    System.out.println("Please enter number from " +min + " to " + max +": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
        public boolean isRank(String str) {
        if (str.equalsIgnoreCase("good")) return true;
        else if (str.equalsIgnoreCase("excellence")) return true;
        else if (str.equalsIgnoreCase("fair")) return true;
        else if (str.equalsIgnoreCase("poor")) return true;
        else System.out.println("Error!!!");
        return false;
    }
        
    public boolean isEmail(String str) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = str.matches(EMAIL_REGEX);
        return b;
    }
    public boolean isNumberic(String str) {
        int count = 0;
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
            count++;
        }
        if (count >= 10) return true;
        else return false;
    }
    
}
