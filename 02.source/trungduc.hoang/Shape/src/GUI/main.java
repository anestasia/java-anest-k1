/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import common.Input;
import java.util.Scanner;
import BO.Circle;
import BO.Cube;
import BO.Rectangle;
import BO.Shape;
import BO.Sphere;
import BO.Square;
import BO.Tetraheron;
import java.util.ArrayList;

/**
 *
 * @author Hoang Trung Duc
 */
public class main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            System.out.println("1. Circle");
            System.out.println("2. Sphere");
            System.out.println("3. Tetraheron");
            System.out.println("4. Cube");
            System.out.println("5. Square");
            System.out.println("6. Rectangle");
            System.out.println("7. Exit");
            String st;

            common.Input in = new common.Input();
            do {
                st = sc.nextLine();
                if (!in.isNumber(st)) {
                    System.out.println("Invalid Choice");
                }
            } while (!in.isNumber(st));
            choice = Integer.parseInt(st);
            switch (choice) {
                case 1:
                    Shape a = new Circle();
                    a.input();
                    a.getArea();
                    a.getParameter();
                    break;
                case 2:
                    Shape sphere = new Sphere();
                    sphere.input();
                    sphere.getV();
                    sphere.getParameter();
                    break;
                case 3:
                    Shape t = new Tetraheron();
                    t.input();
                    t.getParameter();
                    t.getV();
                    break;
                case 4:
                    Shape cu = new Cube();
                    cu.input();
                    cu.getParameter();
                    cu.getV();
                    break;
                case 5:
                    Shape square = new Square();
                    square.input();
                    square.getArea();
                    square.getParameter();
                    break;
                case 6:
                    Shape rec = new Rectangle();
                    rec.input();
                    rec.getArea();
                    rec.getParameter();
                    break;
                default:
                    break;
            }
        } while (choice != 7);
    }
}
