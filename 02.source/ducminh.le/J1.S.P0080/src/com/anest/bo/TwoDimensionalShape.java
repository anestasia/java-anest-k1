package com.anest.bo;

/**
 * Created by Minh on 1/14/2017.
 */
public abstract class TwoDimensionalShape implements IntShape {
    @Override
    public void input() {
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public void printResult() {
    }
}
