/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cong Minh
 */
public class Triangle extends TwoDimensionalShape {

    private double[] edge = new double[3];

    public Triangle() {
    }

    public Triangle(double a[]) {
        for (int i = 0; i < 3; i++) {
            this.edge[i] = a[i];
        }
    }

    @Override
    public double calAre() {
        double p = calPri()/2;
        return Math.sqrt(p * (p - edge[0]) * (p - edge[1] * (p - edge[2])));
//        return edge[1];
    }

    @Override
    public double calPri() {
        return edge[0] + edge[1] + edge[2];
    }

    @Override
    public void input() {
        do {
            for (int i = 0; i < 3; i++) {
                this.edge[i] = Validator.inputDouble("Enter edge " +(i+1) +" : " , 0);
                
            }
            if (checkTriangle() == false) {
                    System.out.println("This is not a triangle. Please re-input");
                }
        } while (checkTriangle() == false);
//        double p=calPri();
//        System.out.println(Math.sqrt(p * (p - edge[0]) * (p - edge[1] * (p - edge[2]))));
    }

    private boolean checkTriangle() {
        double a = this.edge[0];
        double b = this.edge[1];
        double c = this.edge[2];
        if ((a + b > c) && (a + c > b) && (b + c > a) && (a > 0)
                && (b > 0) && (c > 0)) {
            return true;
        } else {
            return false;
        }
    }

    
    @Override
    public double volume() {
        return 0;
    }

}
