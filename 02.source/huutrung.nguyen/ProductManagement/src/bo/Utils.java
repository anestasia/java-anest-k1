package bo;

import java.util.Scanner;

public class Utils {

    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) { // For testing
        Utils a = new Utils();
    }

    /**
     * The function to get a double from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public double getDouble(String mess, String error, double min, double max) {
        String doubleNum;
        double num = 0;
        boolean check = true;
        while (check) {
            System.out.print(mess);
            doubleNum = in.nextLine();
            try {
                num = Double.parseDouble(doubleNum);
            } catch (NumberFormatException e) {
                System.out.println(error);
                continue;
            }
            if (num >= min && num <= max) {
                check = false;
            } else {
                System.out.println(error);
            }
        }
        return num;
    }

    /**
     * The function to get an integer from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public int getInt(String mess, String error, int min, int max) {
        String intNum;
        int num = 0;
        boolean check = true;
        while (check) {
            System.out.print(mess);
            intNum = in.nextLine();
            try {
                num = Integer.parseInt(intNum);
            } catch (NumberFormatException e) {
                System.out.println(error);
                continue;
            }
            if (num >= min && num <= max) {
                check = false;
            } else {
                System.out.println(error);
            }
        }
        return num;
    }

    /**
     * The function to get an number in a base under decimal from the keyboard
     * in console application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @param base : base number you want get
     * @return an integer from keyboard
     */
    public int getNBaseUnderDec(String mess, String error, int base) {
        String nBase;
        while (true) {
            System.out.print(mess);
            nBase = in.nextLine();
            if (nBase.matches("[0-" + (base - 1) + "]+")) {
                break;
            }
            System.out.println(error);
        }
        return Integer.parseInt(nBase);
    }

    /**
     * The function to get an hexadecimal number from the keyboard in console
     * application
     *
     * @param mess : The <code>message</code>
     * @param error: error name displayed
     * @return an integer from keyboard
     */
    public String getHexBase(String mess, String error) {
        String hex;
        while (true) {
            System.out.print(mess);
            hex = in.nextLine();
            if (hex.matches("[0-9a-fA-F]+")) {
                break;
            }
            System.out.println(error);
        }
        return hex;
    }

    public boolean isID(String id) {
        return id.matches("[S|s][V|v][0-9]{4}");
    }

    /**
     * Press any key to continue
     *
     * @param mess : The <code>message</code>
     * @param yes : yes
     * @param no : no
     * @return <code>true</code> if user want to continue "yes",
     * <code>false</code>
     */
    public boolean OptionChoice(String mess, String yes, String no) {
        String choice;
        do {
            System.out.print(mess);
            choice = in.next();
        } while (!(choice.equalsIgnoreCase(yes) || choice.equalsIgnoreCase(no)));
        return choice.equalsIgnoreCase(yes);
    }
}
