package bo;

import utir.Validate;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Tetrahedron extends ThreeDim {
    private double edge;

    public Tetrahedron() {
        edge = 0;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return Math.sqrt(3) / 4 * edge * edge;
    }

    @Override
    public void input() {
        Validate validate = new Validate();
        setEdge(validate.getDouble("Enter the edge: ", "Please enter a float number!!!"));
    }

    @Override
    public double getVolumn() {
        return Math.sqrt(2) / 12 * edge * edge * edge;
    }

    public double getPerimeter() {
        return 0;
    }
}
