/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Product implements Comparable<Product>, Serializable {

    private int id;
    private String category;
    private String price;

    public Product(int id, String category, String price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public Product() {
    }

    public void input() {

        System.out.print("Enter id: ");
        this.id = new Scanner(System.in).nextInt();
        System.out.print("Enter the price: ");
        this.price = new Scanner(System.in).nextLine().trim();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return category + " | " + id + " | " + price;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Product o) {
        return this.price.compareToIgnoreCase(o.price);
    }

}
