/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Book extends Product implements Serializable{
    private String title;
    private String author;

    public Book( String category, int id, String price, String title, String author) {
        super(id, category, price);
        this.title = this.title;
        this.author = author;
    }

    @Override
    public void input() {
        super.input();
        setCategory("Book");
        System.out.print("Enter the title: ");
        this.title = new Scanner(System.in).nextLine().trim();
        System.out.print("Enter the author: ");
        this.author = new Scanner(System.in).nextLine().trim();
    }

    @Override
    public String toString() {
        return super.toString() + " | "+title+" | "+author;
    }
   

    public Book() {
    }
    

    public String getTilte() {
        return title;
    }

    public void setTilte(String tilte) {
        this.title = tilte;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
}
