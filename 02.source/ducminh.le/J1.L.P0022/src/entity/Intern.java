package entity;

import util.Validate;

import java.util.Scanner;

/**
 * Created by Minh on 1/14/2017.
 */
public class Intern extends Candidate {
    private int semester;
    private String majors, universityName;

    public Intern() {
    }

    @Override
    public void input() {
        super.input();
        Scanner in = new Scanner(System.in);
        Validate validate = new Validate();
        setCandidateType(2);
        System.out.print("Enter Majors: ");
        this.majors = in.nextLine();
        this.semester = validate.getInt("Enter Semester: ", "Invalid semester", 0, 100);
        System.out.print("Enter University name: ");
        this.universityName = in.nextLine();
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
