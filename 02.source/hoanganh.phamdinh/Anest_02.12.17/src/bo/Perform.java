/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Book;
import entity.Catagory;
import entity.Clothing;
import entity.Disc;
import entity.Product;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Perform implements Serializable {

    Validate va = new Validate();
    private static final long serialVersionUID = 1L;

    public List<Product> loadFile(List<Product> list) {
        try { // dat try cacth de tranh ngoai le khi tao va doc File
            //        // tao file
//        File f = new File("souvenir-shop.txt");
//        //clear list de cho khach hang khong load file lien tuc
//        list.clear();
//        try {
//            //doc file
//            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
//            String line = "";
//            try {
//                line = br.readLine();
//                while (line != null) {
//                    //cat file theo dau |
//                    String[] s = line.trim().split("\\|");;
//                    //nhan dang catagory: book
//                    if (s[0].trim().equals(Catagory.Book.toString())) {
//                        //tao new object de add vao list
//                        Book b = new Book(Integer.parseInt(s[1].trim()), Catagory.Book, Integer.parseInt(s[2].trim()), s[3].trim(), s[4].trim());
//                        list.add(b);
//                        //nhan dang catagory: clothing
//                    } else if (s[0].trim().equals(Catagory.Clothing.toString())) {
//                        //tao new object de add vao list
//                        Clothing c = new Clothing(Integer.parseInt(s[1].trim()), Catagory.Clothing, Integer.parseInt(s[2].trim()), s[3].trim(), s[4].trim());
//                        list.add(c);
//                        //nhan dang catagory: disc
//                    } else if (s[0].trim().equals(Catagory.Disc.toString())) {
//                        //tao new object de add vao list
//                        Disc d = new Disc(Integer.parseInt(s[1].trim()), Catagory.Disc, Integer.parseInt(s[2].trim()), s[3].trim(), s[4].trim());
//                        list.add(d);
//                    }
//                    // tiep tuc doc dong tiep theo khi vong loop while van con hoat dong
//                    line = br.readLine();
//                }
//                br.close();
//            } catch (IOException ex) {
//                Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
//        }
            FileInputStream f = new FileInputStream("Savedraft.txt");// tao file f tro den student.dat
            try {
                ObjectInputStream inStream = new ObjectInputStream(f);// dung de doc theo Object vao file f
                // dung inStream doc theo Object, ep kieu tra ve la ArrayList
                ArrayList<Product> newf = new ArrayList<>();
                newf = (ArrayList<Product>) inStream.readObject();
                for (int i = 0; i < newf.size(); i++) {
                    if (va.isChecker(newf.get(i).getId(), list)) {
                        list.add(newf.get(i));
                    }
                }
                f.close();
            } catch (IOException ex) {
                Logger.getLogger(Perform.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Perform.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Perform.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (ArrayList<Product>) list;
    }

    public void writeFile(List<Product> list) {
        try {   // dat try cacth de tranh ngoai le khi tao va ghi File
            FileOutputStream f = new FileOutputStream("Savedraft.txt", false);   // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(list);   // ghi student theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error Write file");
        }
    }

    public void sort(ArrayList<Product> list) {
        ArrayList<Product> arr = new ArrayList<>();//temp arraylist (list moi)
        for (int i = 0; i < list.size(); i++) {
            arr.add(list.get(i));//dua toan bo du lieu list cu vao list moi
        }
        list.clear();//clear list cu
        //array moi con phan tu thi con tim phan tu nho nhat de add vao list cu
        while (!arr.isEmpty()) {
            int pos = 0;
            double min = arr.get(0).getPrice();
            for (int i = 0; i < arr.size(); i++) {
                if (min > arr.get(i).getPrice()) {
                    pos = i;//luu vi tri cua phan tu nho nhat   
                    min = arr.get(i).getPrice();//tim gia tri nho nhat
                }
            }//add phan tu nho nhat o list moi ve list cu   
            list.add(arr.get(pos));
            arr.remove(pos);// xoa phan tu min o list moi   
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    public int search(ArrayList<Product> list) {
        int n, pos = -1;
        //search with key n
        n = va.readInt("input ID to searching: ", 0, Integer.MAX_VALUE);
        //cho vong for chay trong list, neu tim thay phan tu yeu cau break luon.
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == n) {
                pos = i;
                System.out.println(list.get(i));
                break;
            }
            
        }
        return pos;
    }
//ghi file theo line 

    public void save(ArrayList<Product> list) {
        File f = new File("Savedraft.txt");
        try {
            FileWriter fw = new FileWriter(f.getAbsoluteFile(), false);//tao file writer luồng, false de ghi lai tu dau 
            BufferedWriter bw = new BufferedWriter(fw);//ghi tat ca luong vao file
            for (int i = 0; i < list.size(); i++) {
                bw.write(list.get(i).toString());// ghi lien tuc next line
                bw.newLine();
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Perform.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void remove(ArrayList<Product> list) {
        int pos = -1;
            pos = search(list);
        //sau khi search thi xoa phan tu do ra khoi list
        list.remove(list.get(pos));
        //sau khi xoa thi ghi lai vao file
        save(list);
    }

    public void update(ArrayList<Product> list) {
        int pos = -1;
        String choice = null;
        double price = 0;
        Scanner sc = new Scanner(System.in);
        pos = search(list);
        switch (list.get(pos).getCategory()) {
            case Book:
                System.out.print("Update title: ");
                String title = sc.next();
                System.out.print("Update author: ");
                String author = sc.next();
                price = va.readDoub("Price: ", Double.MIN_VALUE, Double.MAX_VALUE);
                Book b = new Book(list.get(pos).getId(), Catagory.Book, price, title, author);
                list.set(pos, b);
                save(list);
                break;
            case Clothing:
                System.out.print("Update Department: ");
                String Department = sc.next();
                System.out.print("Update Brand: ");
                String Brand = sc.next();
                price = va.readDoub("Price: ", Double.MIN_VALUE, Double.MAX_VALUE);
                Clothing c = new Clothing(list.get(pos).getId(), Catagory.Book, price, Department, Brand);
                list.set(pos, c);
                save(list);
                break;
            case Disc:
                System.out.print("Update genre: ");
                String Genre = sc.next();
                System.out.print("Update Format: ");
                String Format = sc.next();
                price = va.readDoub("Price: ", Double.MIN_VALUE, Double.MAX_VALUE);
                Disc d = new Disc(list.get(pos).getId(), Catagory.Book, price, Genre, Format);
                list.set(pos, d);
                save(list);
                break;
        }
    }
}
