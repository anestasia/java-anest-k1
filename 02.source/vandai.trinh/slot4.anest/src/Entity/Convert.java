/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import javax.print.attribute.standard.Finishings;

/**
 *
 * @author Trinh Dai
 */
public class Convert {
    /**
     * convert number and ABCDEF to char
     * example A = 10 , B = 11...
     * 
     */
    public int charToInt(char c) {
        int i;
        for(i = 65; i < 87; i++) {
            if(c == i)
                return i-55;
        }
        for(i = 48; i <=57; i++) {
            if(c == i)
                return i-48;
        }
        return 0;
    }
    
    public String binToDec(String binary) {
        Convert s = new Convert();
        double result = 0;
        for(int i=0; i < binary.length(); i++) {
            int temp = s.charToInt(binary.charAt(i));
            result += temp*Math.pow(2, (binary.length() - i-1) );
        }
        return Integer.toString((int)result);
    }
    
    public String hexToDec(String hex) {
        Convert s = new Convert();        
        double result = 0;
        for(int i=0; i < hex.length(); i++) {
            int temp = s.charToInt(hex.charAt(i));
            result += temp*Math.pow(16, (hex.length() - i-1) );
        }
        return Integer.toString((int)result);
    }
    
    public String decToBin(String dec) {
        String result = "";
        int temp = Integer.parseInt(dec);
        System.out.println(temp);
        while(temp > 0) {
            result += (char)(temp % 2 + '0');
            temp /=2;
        }
        String reverse = new StringBuffer(result).reverse().toString();
        return reverse;
    }
    
    public String decToHex(String dec) {
        String result = "";
        int temp = Integer.parseInt(dec);
        while(temp > 0) {
            if(temp % 16 > 9) {
                result += (char)(temp % 16 + 55);
            }else
                result += (char)(temp % 16 + '0');
            temp /=16;
        }
        String reverse = new StringBuffer(result).reverse().toString();
        return reverse;        
    }
    
    public String binaryToHex(String binary) {
        Convert convert = new Convert();
        String result = convert.binToDec(binary);
        return convert.decToHex(result);
    }
    
    public String hexToBinary(String hex) {
        Convert convert = new Convert();
        String result = convert.hexToDec(hex);
        return convert.decToBin(result);
    }

    public static void main(String[] args) {
        Convert convert = new Convert();
        System.out.println(convert.binaryToHex("1001"));
    }
}
