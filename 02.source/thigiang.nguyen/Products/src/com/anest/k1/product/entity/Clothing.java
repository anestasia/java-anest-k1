/**
 *
 */
package com.anest.k1.product.entity;

import java.util.Scanner;

public class Clothing extends Product {

    private String department;
    private String brand;

    public Clothing() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Clothing(int id, ECat category, int price, String department, String brand) {
        super(id, category, price);
        this.brand = brand;
        this.department = department;
        // TODO Auto-generated constructor stub
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     *
     */
    @Override
    public void input() {
        super.input();
        Scanner sc = new Scanner(System.in);
        this.setCategory(ECat.CLOTHING);
        System.out.println(" Department = ");
        this.department = sc.nextLine().trim();
        System.out.println(" Brand = ");
        this.brand = sc.nextLine().trim();
    }

    @Override
    public String toString() {
        return this.getCategory() + "|" + this.getId() + "|" + this.department + "|" + this.brand + "|" + this.getPrice();
    }
}
