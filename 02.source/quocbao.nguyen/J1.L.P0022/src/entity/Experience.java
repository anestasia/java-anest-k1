package entity;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class Experience extends Candidate {
    private int ExpInYear;
    private String ProSkill;

    public Experience() {
    }

    public Experience(String id, String ltName, String ftName, int birthday, String address, String phone, String email, int type, int expInYear, String proSkill) {
        super(id, ltName, ftName, birthday, address, phone, email, type);
        ExpInYear = expInYear;
        ProSkill = proSkill;
    }

    public int getExpInYear() {
        return ExpInYear;
    }

    public void setExpInYear(int expInYear) {
        ExpInYear = expInYear;
    }

    public String getProSkill() {
        return ProSkill;
    }

    public void setProSkill(String proSkill) {
        ProSkill = proSkill;
    }

}
