/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k1.product.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class Valid {
    private static Scanner scanner = new Scanner(System.in);

    public static String InputString(String mes) throws IOException {
        String input;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(mes);
        while (true) {
            input = br.readLine();
            if (input.isEmpty()) {
                System.out.print("Enter again: ");
            } else {
                return input;
            }
        }
    }
    public static int getIntNumber(String mess, int max, int min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                int result = Integer.parseInt(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
}
