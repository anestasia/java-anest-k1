/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cong Minh
 */
public class Circle extends TwoDimensionalShape{
    
    private double r;

    public Circle() {
    }

    public Circle(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    /**
     * this function calculate area
     * @return 
     */
    
    @Override
    public double calAre() {
        return this.r*this.r*Math.PI;
    }

    /**
     * this function calculate perimeter
     * @return 
     */
    @Override
    public double calPri() {
        return 2*this.r*Math.PI;
    }

    @Override
    public double volume() {
       return 0;
    }
        
    @Override
    public void input() {
        this.r = Validator.inputDouble("Enter r: ", 0);
    }
   
}
