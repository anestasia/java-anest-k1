/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author OPF_Lep
 */
public class Menu {

    ArrayList<String> menu = new ArrayList();
    ArrayList<Candidate> list = new ArrayList<>();
    ArrayList<Candidate> temp = new ArrayList<>();

    public void addOption() {
        menu.add("1. Experience");
        menu.add("2. Fresher");
        menu.add("3. Internship");
        menu.add("4. Searching");
        menu.add("5. Exit");
    }

    public void showMenu() {

        System.out.println("==== CANDIDATE MANEGEMENT SYSTEM ===");
        for (int i = 0; i < 5; i++) {
            System.out.println(menu.get(i));
        }
    }

    public int getChoice(int i, int j, String input, String error) {
        Scanner sc = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.print(input);
            try {
                choice = Integer.parseInt(sc.nextLine());
                if (choice > i && choice < j) {
                    return choice;
                }
            } catch (Exception e) {
                System.out.println(error);
            }
        }
    }

    public Candidate createNewCandidate() {
        Candidate c = new Candidate();
        System.out.print("Input ID:");
        c.setID(new Scanner(System.in).nextLine());
        System.out.print("Input first name:");
        c.setFirstName(new Scanner(System.in).nextLine());
        System.out.print("Input last name:");
        c.setLastName(new Scanner(System.in).nextLine());
        c.setBirthDate(Validation.inputBirthDate());
        System.out.print("Input address: ");
        c.setAddress(new Scanner(System.in).nextLine());
        c.setPhone(Validation.inputPhone());
        c.seteMail(Validation.inputEmail());
        System.out.println("ok");
        return c;
    }

    public Experience createNewExp() {
        Experience e;
        Candidate c;
        c = createNewCandidate();
        e = new Experience(c.getID(), c.getFirstName(), c.getFirstName(), c.getBirthDate(), c.getAddress(), c.getPhone(), c.geteMail(), 0);
        e.setExpInYear(Validation.inputExpYear());
        System.out.print("Pro Skill: ");
        e.setProSkill(new Scanner(System.in).nextLine());
        return e;
    }

    public Fresher createNewFrs() {
        Fresher f;
        Candidate c;
        c = createNewCandidate();
        f = new Fresher(c.getID(), c.getFirstName(), c.getFirstName(), c.getBirthDate(), c.getAddress(), c.getPhone(), c.geteMail(), 1);
        System.out.print("Graduation date: ");
        f.setGraduation_date(Validation.inputGra_Date());
        System.out.print("Rank of graduation: ");
        f.setGraduation_rank(Validation.inputRank());
        return f;
    }

    public Internship createNewIntern() {
        Internship i;
        Candidate c;
        c = createNewCandidate();
        i = new Internship(c.getID(), c.getFirstName(), c.getFirstName(), c.getBirthDate(), c.getAddress(), c.getPhone(), c.geteMail(), 2);
        System.out.print("Majors: ");
        i.setMajors(new Scanner(System.in).nextLine());
        System.out.print("Semester: ");
        i.setSemester(new Scanner(System.in).nextLine());
        System.out.print("University Name: ");
        i.setUniversityName(new Scanner(System.in).nextLine());
        return i;
    }

    public void searching() {
        String name;
        int type;
        System.out.print("Input name to search: ");
        name = new Scanner(System.in).nextLine();
        type = getChoice(-1, 4, "Input type: ", "Error");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getFirstName().contains(name) || list.get(i).getLastName().contains(name) & list.get(i).getType() == type) {
                temp.add(list.get(i));
            }
        }
        if (temp.size() == 0) {
            System.out.println("There is no candidate like this");
        } else {
            for (int i = 0; i < temp.size(); i++) {
                System.out.println(temp.get(i).toString());
            }
        }
    }

    public void run() {
        int k = 0;
        addOption();
        do {
            k = 1;
            showMenu();
            switch (getChoice(0, 6, "Input your choice: ", "Error")) {
                case 1:
                    list.add(createNewExp());
                    break;
                case 2:
                    list.add(createNewFrs());
                    break;
                case 3:
                    list.add(createNewIntern());
                    break;
                case 4:
                    searching();
                    break;
                case 5:
                    System.out.println("Good bye");
                    k = 0;
            }
        } while (k == 1);
    }
}
