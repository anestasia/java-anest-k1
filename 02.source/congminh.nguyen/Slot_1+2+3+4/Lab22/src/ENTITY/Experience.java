/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;
import ENTITY.Type;
/**
 *
 * @author Cong Minh
 */
public class Experience extends Candidate{
    private int year;
    
    
    public Experience(String id, String firstName,String lastName,
            int birthDate,String address, String phone, String email,
            int year, Type type){
       super(id, firstName, lastName, birthDate, address, phone, email, type);
        this.year = year;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    
    
    
}
