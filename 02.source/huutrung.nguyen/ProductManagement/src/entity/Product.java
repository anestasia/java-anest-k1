/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import bo.Utils;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Product implements Comparable<Product> {

    Utils val = new Utils();
    private String id;
    private ECat category;
    private int price;

    public Product() {
    }

    public Product(String id, ECat category, int price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ECat getCategory() {
        return category;
    }

    public void setCategory(ECat category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int compareTo(Product o) {
        return price - o.price;
    }

    public void input() {
        while (true) {
            System.out.print("Id: ");
            id = new Scanner(System.in).nextLine();
            if (!val.isID(id)) {
                System.out.println("This's not an id");
            } else {
                break;
            }
        }
        price = val.getInt("Price: ", "Invalid input", 0, Integer.MAX_VALUE);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Product p = (Product) obj;
        if (p.id.equalsIgnoreCase(id)) {
            return true;
        }
        return false;
    }
}
