/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BO.CanType;
import BO.Candidate;
import BO.Experience;
import BO.Fresher;
import BO.Intern;
import BO.Rank;
import BO.Validator;
import BO.getCandidateType;
import java.util.ArrayList;
import util.Input;
import java.util.Scanner;
import util.Searching;

/**
 *
 * @author KienKeng
 */
public class Test {
    
    public static void main(String[] args) {
        
        ArrayList<Candidate> li = new ArrayList<>();
        Candidate candidate = new Candidate();
        Display display = new Display();
        Searching search = new Searching();
        Scanner scan = new Scanner(System.in);
        
        String s;
        int choice;
        Input input = new Input();
     
        do{
            display.Menu();
            choice = (int)input.getNumber("Enter your choice :", "Invalid input! please re-enter!", 0, 5);
            switch(choice){ 
            case 1:
                do{
                    candidate = new Experience();
                    ((Experience)candidate).input();
                    li.add(candidate);
                }
                while(input.pressYN());
                display.displayAll(li);
                break;
            case 2:
                do{
                    candidate = new Fresher();
                    ((Fresher)candidate).input();
                    li.add(candidate);
                }
                while(input.pressYN());
                display.displayAll(li);
                break;
            case 3:
                 do{
                    candidate = new Intern();
                    ((Intern)candidate).input();
                    li.add(candidate);
                }
                while(input.pressYN());
                display.displayAll(li);
                break;
            case 4:
                do{
                    search.SearchingCandidate(li);
                }
                while(input.pressYN());
                break;
            case 5:
                break;
            }
          
        }while(choice !=5);
            li.clear();
    }    
      
}
