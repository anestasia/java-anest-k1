/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author Trinh Dai
 */
public class Menu {
    public void headMenu() {
        System.out.println("=================   CONVERT BASE  ====================");
        System.out.println("==  1. Binary(base 2)     to Decimal(base 10)       ==");
        System.out.println("==  2. Binary(base 2)     to Hex    (base 16)       ==");
        System.out.println("==  3. Decimal(base 10)   to Binary (base 2)        =="); 
        System.out.println("==  4. Decimal(base 10)   to Hex    (base 16)       ==");
        System.out.println("==  5. Hex(base 16)       to Binary (base 2)        ==");
        System.out.println("==  6. Hex(base 16)       to Decimal(base 10)       ==");
        System.out.println("==  0. Exit                                         ==");
        System.out.println("======================================================");
    }
}
