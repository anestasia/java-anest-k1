/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.oop.k1.entity.twoDimensional;

import com.anest.oop.k1.entity.Shape;

/**
 *
 * @author Cpt
 */
public abstract  class TwoDimensionalShape implements Shape{

    @Override
    public abstract double calArea();
    

    @Override
    public abstract double calPri();

    @Override
    public abstract void input();
    
}
