/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import entity.Category;
/**
 *
 * @author Cong Minh
 */
public class Book extends Product{
    private String title;
    private String author;

    public Book() {
    }

    public Book(String id, Category category, double price,
            String title, String author) {
        super(id, category, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
}
