package com.anest.bo;

import com.anest.util.Input;

/**
 * Created by Minh on 1/14/2017.
 */
public class Tetrahedron extends ThreeDimensionalShape{
    private double edge;

    public Tetrahedron() {
    }

    public Tetrahedron(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public void input() {
        Input input = new Input();
        this.edge = input.getDouble("Enter edge: ", "Invalid number", 0, Double.MAX_VALUE);
    }

    @Override
    public void printResult() {
        System.out.println("Area: " + getArea());
        System.out.println("Volume: " + getVolume());
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public double getArea() {
        return Math.sqrt(3) * Math.pow(edge, 2);
    }

    @Override
    public double getVolume() {
        return Math.sqrt(2) / 12 * Math.pow(edge, 3);
    }
}
