
package bo;
public abstract class ThreeDimensionalShape implements Shape{
    public abstract float calArea();
    public abstract float calVolume();

    @Override
    public abstract void input();

    @Override
    public abstract void output();

    
    
    
}
