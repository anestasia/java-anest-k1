/**
 * 
 */
package com.anest.k1.product.entity;

import java.util.Scanner;

/**
 * @author dell
 *
 */
public class DISC extends Product {
	private String genre;
	private String format;
	/**
	 * 
	 */
	public DISC() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param id
	 * @param category
	 * @param price
	 */
	public DISC(int id, ECat category, int price, String genre, String format) {
		super(id, category, price);
		this.genre = genre;
		this.format = format;
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}
	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}
	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	
	@Override
	public void input() {
		super.input();
		this.setCategory(ECat.CD);
		System.out.println(" Genre = ");
		this.genre = new Scanner(System.in).nextLine().trim();
		System.out.println(" Format = ");
		this.format = new Scanner(System.in).nextLine().trim();
	}
	
	@Override
	public String toString() {
		return this.getCategory() + "|" + this.getId() + "|" + this.genre + "|" + this.format + "|" + this.getPrice();
	}
}
