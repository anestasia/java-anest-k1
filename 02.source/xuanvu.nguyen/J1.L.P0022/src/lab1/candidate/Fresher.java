/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

/**
 *
 * @author OPF_Lep
 */
public class Fresher extends Candidate {

    int Graduation_date;
    String Graduation_rank;
    String Education;

    public Fresher(int Graduation_date, String Graduation_rank, String Education, String CandidateId, String FirstName, String LastName, int BirthDate, String Address, String Phone, String Email) {
        super(CandidateId, FirstName, LastName, BirthDate, Address, Phone, Email);
        this.Graduation_date = Graduation_date;
        this.Graduation_rank = Graduation_rank;
        this.Education = Education;
        super.type = 0;
    }

    public Fresher() {
    }

    public Fresher(String ID, String firstName, String lastName, int birthDate, String Address, String phone, String eMail, int type) {
        super(ID, firstName, lastName, birthDate, Address, phone, eMail, type);
    }

    public int getGraduation_date() {
        return Graduation_date;
    }

    public void setGraduation_date(int Graduation_date) {
        this.Graduation_date = Graduation_date;
    }

    public String getGraduation_rank() {
        return Graduation_rank;
    }

    public void setGraduation_rank(String Graduation_rank) {
        this.Graduation_rank = Graduation_rank;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String Education) {
        this.Education = Education;
    }

}
