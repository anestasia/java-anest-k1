/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Scanner;

/**
 *
 * @author Hoang Trung Duc
 */
public class Sphere extends ThreeDimensons {

    private int a;

    public Sphere() {
    }

    public Sphere(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public void getV() {
        System.out.println("V = " + (4 / 3) * a * a * a * Math.PI);
    }

    @Override
    public void getParameter() {
        System.out.println("S = " + 4 * a * a * Math.PI);
    }

    @Override
    public void input() {
        common.Input in = new common.Input();
        String s;
        do {
            do {
                System.out.println("Nhap ban kinh:");
                Scanner sc = new Scanner(System.in);
                s = sc.nextLine();
            } while (!in.isNumber(s));
            setA(Integer.parseInt(s));
        } while (a <= 0);
    }
}
