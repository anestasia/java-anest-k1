package bo;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Validate {

    public double getDouble(String mes, String error) {
        Scanner sc = new Scanner(System.in);
        double number = 0;
        while (true) {
            try {
                System.out.println(mes);
                number = sc.nextDouble();
                if (number <= 0) {
                    sc.nextLine();
                    System.out.println(error);
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }

    public int getInt(String mes, String error, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        while (true) {
            try {
                System.out.print(mes);
                number = sc.nextInt();
                if (number < min || number > max) {
                    System.out.println(error);
                    sc.nextLine();
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }

    public boolean YNquestion() {
        Scanner sc = new Scanner(System.in);
        String str;
        do {
            System.out.print("Do you want to continue?  ");
            str = sc.nextLine();
            if (str.equalsIgnoreCase("y")) {
                break;
            } else if (str.equalsIgnoreCase("n")) return false;
            else System.out.println("Error! Please enter Y/N!");
        } while (true);
        return true;
    }

    public boolean isNumberic(String str) {
        int count = 0;
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
            count++;
        }
        if (count >= 10) return true;
        else return false;
    }

    public boolean isRank(String str) {
        if (str.equalsIgnoreCase("good")) return true;
        else if (str.equalsIgnoreCase("excellence")) return true;
        else if (str.equalsIgnoreCase("fair")) return true;
        else if (str.equalsIgnoreCase("poor")) return true;
        else System.out.println("Error!!!");
        return false;
    }

    public boolean isEmail(String str) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = str.matches(EMAIL_REGEX);
        return b;
    }
}
