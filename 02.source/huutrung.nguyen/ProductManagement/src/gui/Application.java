/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.ProductList;
import bo.Utils;
import entity.Book;
import entity.Clothes;
import entity.Disc;
import entity.Product;
import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Application {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        Utils val = new Utils();
        Application a = new Application();
        ProductList pl = new ProductList();
        int choice_1;
        do {
            choice_1 = a.menu();
            switch (choice_1) {
                case 1: {
                    int choice_2, count = 0;
                    while (true) {
                        choice_2 = a.subMenu();
                        Product product = null;
                        switch (choice_2) {
                            case 1: {
                                product = new Book();
                                break;
                            }
                            case 2: {
                                product = new Clothes();
                                break;
                            }
                            case 3: {
                                product = new Disc();
                                break;
                            }
                        }
                        product.input();
                        pl.add(product);
                        count++;
                        if (count >= 10) {
                            if (!val.OptionChoice("Do you want to continue? Y-Yes N-No: ", "Y", "N")) {
                                break;
                            }
                        }
                    }
                    break;
                }
                case 2: {
                    System.out.print("Enter file name: ");
                    String fn = in.nextLine();
                    pl.loadFromFile(fn);
                    break;
                }
                case 3: {
                    System.out.println("Id: ");
                    String id = in.next();
                    Product p = pl.search(id);
                    if (p != null) {
                        System.out.println(p);
                    } else {
                        System.out.println("Not found!");
                    }
                    pl.sort();
                    System.out.println("Sort successful!");
                    break;
                }
                case 4: {
                    System.out.println("Id: ");
                    String id = in.next();
                    Product p = pl.search(id);
                    if (val.OptionChoice("Do you want to update (U) or delete (D) product: ", "U", "D")) {
                        pl.update(id);
                        System.out.println("Update successful!");
                    } else {
                        pl.delete(id);
                        System.out.println("Delete successful!");
                    }
                    break;
                }
                case 5: {
                    pl.showList();
                    break;
                }
                case 6: {
                    System.out.print("Enter file to save: ");
                    String fn = in.nextLine();
                    pl.saveToFile(fn);
                    break;
                }
            }
        } while (choice_1 != 6);
    }

    public int menu() {
        Utils val = new Utils();
        System.out.println("=============================");
        System.out.println("WELCOME TO PRODUCT MANAGEMENT");
        System.out.println("1. Create");
        System.out.println("2. Load from file");
        System.out.println("3. Sort (by price) and Find (by id)");
        System.out.println("4. Update / Delete");
        System.out.println("5. Show list");
        System.out.println("6. Exit and save to file");
        return val.getInt("Enter your choice: ", "Invalid input", 1, 6);
    }

    public int subMenu() {
        Utils val = new Utils();
        System.out.println("__________________");
        System.out.println("1. Create book");
        System.out.println("2. Create clothes");
        System.out.println("3. Create disc");
        return val.getInt("Enter your choice: ", "Invalid input", 1, 3);
    }
}
