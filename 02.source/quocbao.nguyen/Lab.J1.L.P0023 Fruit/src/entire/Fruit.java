package entire;

/**
 * Created by W10-PRO on 27-Jan-17.
 */
public class Fruit {
    private String fruitID;
    private String fruitName;
    private double fruitPrice;
    private String origin;

    public String getFruitID() {
        return fruitID;
    }

    public void setFruitID(String fruitID) {
        this.fruitID = fruitID;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public double getFruitPrice() {
        return fruitPrice;
    }

    public void setFruitPrice(double fruitPrice) {
        this.fruitPrice = fruitPrice;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }


    public Fruit() {

    }

    public Fruit(String fruitID, String fruitName, double fruitPrice, String origin) {
        this.fruitID = fruitID;
        this.fruitName = fruitName;
        this.fruitPrice = fruitPrice;
        this.origin = origin;
    }
    public void getAllFruit(String fruitID, String fruitName, double fruitPrice, String origin) {
        this.fruitID = fruitID;
        this.fruitName = fruitName;
        this.fruitPrice = fruitPrice;
        this.origin = origin;
    }
}
