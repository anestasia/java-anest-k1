package gui;

import bo.Bo;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class Main {
    public static void main(String[] args) {
        Bo bo = new Bo();
        Menu menu = new Menu(4);
        menu.add("Create Fruit");
        menu.add("View orders");
        menu.add("Shopping");
        menu.add("Exit");
        int choice;
        do {
            System.out.println("\nFRUIT SHOP SYSTEM");
            choice = menu.getChoice();
            switch (choice) {
                case 1:
                    bo.getFruit();
                    break;
                case 2:
                    bo.getListOrder();
                    break;
                case 3:
                    bo.getOrder();
                    break;
                case 4:
                    System.out.println("Exit!!!");
                    break;
            }
        } while (choice != 4);
    }
}
