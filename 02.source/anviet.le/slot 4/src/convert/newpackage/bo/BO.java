/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert.newpackage.bo;

import java.util.Scanner;

public class BO {

    Scanner sc = new Scanner(System.in);
 
    //binary to decimal and hex
    public void conBinary(int choice) {
        String binary;
        do{
        System.out.println("Enter number:");
        binary=sc.nextLine();
        }while(!Utils.isBinary(binary));
        int output = Integer.parseInt(binary, 2);
        if (choice == 2) {
            System.out.print("Decimal number: " + output);
        }
        if (choice == 3) {
            System.out.print("Hexadecimal number: " + Integer.toHexString(output));
        }

    }
//decimal to binary and hex
    public void conDecimal(int choice) {
        String dec;
        do{
        System.out.println("Enter number:");
        dec=sc.nextLine();
        }while(!Utils.isDecimal(dec));
        int output = Utils.getIntNumber("Wrong!", "Input number 0-9", 0, 9);
        if (choice == 1) {
            System.out.print("Binary number: " + Integer.toBinaryString(output));
        }
        if (choice == 3) {
            System.out.print("Hexadecimal number: " + Integer.toHexString(output));
        }

    }
//hex to binary and decimal
    public void conHex(int choice) {
        String hex;
        do {
            System.out.println("Input hex number");
            hex = sc.nextLine();
        } while (!Utils.isHex(hex));
        if (choice == 1) {
            System.out.print("Binary number: " + Integer.toBinaryString(Integer.parseInt(hex, 16)));
        }
        if (choice == 2) {
            System.out.print("Decimal number: " + Integer.parseInt(hex, 16));
        }
    }
}
