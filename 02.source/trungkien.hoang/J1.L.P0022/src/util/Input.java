/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import BO.Candidate;
import BO.Validator;
import BO.getCandidateType;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Input {
    
    Validator validate = new Validator();
    Scanner scan = new Scanner(System.in);
    
    public double getNumber(String mess, String err, int min, int max)
    {
        String s;
        double number;
        while(true)
        {
            System.out.println(mess);
            s = scan.nextLine().trim();
            if(validate.checkNumber(s))
            {
                number = Double.parseDouble(s);
                if(number < min || number > max)
                {
                    System.out.println(err);
                }
                else    
                    return number;
            }
            else
            {
                System.out.println(err);
            }
        }
    }
    
     public boolean pressYN() {     
        String s;
        
        while (true) {
            System.out.print("Do you want to continue? (Y-yes, N-no)");
            s = scan.nextLine();
            s = s.trim();
            if (s.equalsIgnoreCase("y")) return true;
            if (s.equalsIgnoreCase("n")) return false;
            System.out.println("Invalid choice");
        }
    }
    

    
}
