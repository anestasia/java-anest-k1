/**
 *
 */
package com.anest.k1.product.ui;

import com.anest.k1.product.bo.ListProduct;
import com.anest.k1.product.entity.Book;
import com.anest.k1.product.entity.Clothing;
import com.anest.k1.product.entity.DISC;
import com.anest.k1.product.entity.Product;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        ListProduct p = new ListProduct();
        Product product;
        Scanner scanner = new Scanner(System.in);
        int choice = 0;
        int temp = 0;
        while (true) {
            System.out.println("\nWELCOME TO PRODUCT MANAGEMENT\n");
            System.out.println("1. Create\n"
                    + "2. Load from file\n"
                    + "3. Sort (by price) and Find (by id)\n"
                    + "4. Update / Delete\n"
                    + "5. Report and save\n"
                    + "6. Exit and save\n");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    while (temp < 4) {
                        System.out.println("Choose type you want to add (1- Book; 2- Clothes; 3-Disc)");
                        temp = scanner.nextInt();

                        switch (temp) {
                            case 1:
                                product = new Book();
                                product.input();
                                p.addProduct(product);
                                // p.writeFile("products.txt");
                                p.writeFile("products.dat");
                                break;
                            case 2:
                                product = new Clothing();
                                product.input();
                                p.addProduct(product);
                                // p.writeFile("products.txt");
                                p.writeFile("products.dat");
                                break;
                            case 3:
                                product = new DISC();
                                product.input();
                                p.addProduct(product);
//                                p.writeFile("products.txt");
                                p.writeFile("products.dat");
                                break;
                            default:
                                System.out.println("Error! Please try again: ");
                                temp = scanner.nextInt();
                                break;
                        }
                        break;
                    }
                    break;
                case 2:
                    //p.loadFromFile("products.txt");
                    p.loadFromFile("products.dat");
                    break;
                case 3:
                    int check = 0;
                    while (check > 2 || check < 1) {
                        System.out.println("Choose type you want (1 - Sort (by Price); 2 - Find (by Id))");
                        check = scanner.nextInt();
                    }

                    switch (check) {
                        case 1:
                            p.sortByPrice();
                            System.out.println("After sort: ");
                            p.show();
                            break;
                        case 2:
                            Product newP;
                            try {
                                System.out.println("Enter product id to search: ");
                                scanner.nextLine();
                                int did = scanner.nextInt();
                                int index = p.findID(did);
                                if (index == -1) {
                                    System.out.println("Không tìm thấy phần tử nào ở vị trí " + did);
                                    continue;
                                }
                                Product pt = p.getProducts().get(index);
                                if (pt instanceof Book) { // Nó có phải là book ko .
                                    newP = new Book();
                                } else if (pt instanceof Clothing) {
                                    // Có phải là clothing ko 
                                    newP = new Clothing();
                                } else {
                                    newP = new DISC();
                                }
                                p.find(did, newP);
                            } catch (Exception e) {
                                System.out.println(e);
                                scanner.nextInt();
                                continue;
                            }

                            break;
                    }

                    break;
                case 4:
                    int tmp = 0;
                    while (tmp > 2 || tmp < 1) {
                        System.out.println("Choose type you want (1 - Update Products; 2 - Delete Products)");
                        tmp = scanner.nextInt();
                    }
                    switch (tmp) {
                        case 1:
                            Product newP;
                            System.out.println("Enter product id to update: ");
                            scanner.nextLine(); // Xóa bộ nhớ đệm
                            int id = Integer.parseInt(scanner.nextLine());
                            scanner.nextLine();// Xóa bộ nhớ đệm
                            Product pt = p.getProducts().get(p.findID(id));
                            if (pt instanceof Book) { // Nó có phải là book ko .
                                newP = new Book();
                            } else if (pt instanceof Clothing) {
                                // Có phải là clothing ko 
                                newP = new Clothing();
                            } else {
                                newP = new DISC();
                            }
                            newP.input(); // Đến đây newP là gì thì đã xác định rồi nên cứ gọi input là nó tự nhảy vào input của loại đó
                            newP.setId(pt.getId()); // Giữ nguyên id, ko đc thay đổi
                            p.updateProduct(id, newP);
                            p.writeFile("products.dat");

                            break;
                        case 2:
                            System.out.println("Enter product id to delete: ");
                            scanner.nextLine();
                            int did = Integer.parseInt(scanner.nextLine());
                            scanner.nextLine();
                            p.deleteProduct(did);
                            p.writeFile("products.dat");
                            break;

                    }
                    break;
                case 5:
                    p.show();
                    break;
                case 6:
                    // Save !?
                    p.writeFile("products.dat");
//                    p.writeFile("products.txt");
                    System.exit(0);
            }
        }
    }

}
