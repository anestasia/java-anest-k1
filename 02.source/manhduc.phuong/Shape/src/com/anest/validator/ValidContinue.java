/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.validator;

import java.util.Scanner;

/**
 *
 * @author Cpt
 */
public class ValidContinue {

    public static boolean checkContinue() {
        boolean check;
        while (true) {
            System.out.println("Do you want to continue? Y/N?: ");
            String c = new Scanner(System.in).nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                check = true;
                return check;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                System.out.println("EXIT_SUCCESS");
                check = false;
                return check;
            } else {
                System.out.println("Invalid input!!! Enter again: ");
            }
        }
    }
}
