package bo;


public abstract interface Shape {
    public abstract void input();
    public abstract void output();
}
