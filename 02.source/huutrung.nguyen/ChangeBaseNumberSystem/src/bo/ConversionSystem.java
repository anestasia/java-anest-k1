/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

/**
 *
 * @author Monkey D.Luffy
 */
public class ConversionSystem {

    public int ConvertNBaseUnderDecToDec(int num, int base) {
        boolean check = true;
        int dec = 0, p = 0, n;
        do {
            n = num % 10;
            dec += n * Math.pow(base, p++);
            num /= 10;
            if (dec > Integer.MAX_VALUE) {
                check = false;
            }
        } while (num != 0);
        if (!check || base < 0 || base > 9) {
            return -1;
        }
        return dec;
    }

    public int ConvertHexaToDec(String hex) {
        hex = hex.toLowerCase();
        boolean check = true;
        int dec = 0;
        for (int i = 0; i < hex.length(); i++) {
            if (hex.charAt(i) >= 'a') {
                dec += ((int) hex.charAt(i) - 87) * Math.pow(16, (hex.length() - i - 1));
            } else {
                dec += ((int) hex.charAt(i) - 48) * Math.pow(16, (hex.length() - i - 1));
            }
            if (dec > Integer.MAX_VALUE) {
                check = false;
            }
        }
        if (!check) {
            return -1;
        }
        return dec;
    }
    
    public String ConvertDecToNBaseUnderDec(int dec, int base){
        String str = "";
        int n;
        do{
            n = dec%base;
            str = n + str;
            dec /= base;
        }while(dec != 0);
        return str;
    }
    
    public String ConvertDecToHex(int dec){
        if (dec > Integer.MAX_VALUE)
            return null;
        String str = "";
        int n;
        do{
            n = dec%16;
            if (n > 9)
                n = (char)(n+55);
            str = n + str;
            dec /= 16;
        }while(dec != 0);
        return str;
    }
}

