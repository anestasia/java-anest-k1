/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Book;
import entity.Catagory;
import entity.Clothing;
import entity.Disc;
import entity.Product;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pham Dinh Hoang Anh
 */
public class Menu {

    Scanner sc = new Scanner(System.in);
    Validate va = new Validate();
    Perform p = new Perform();
    ArrayList<Product> productList = new ArrayList<>();

    ;

    public void menu() {

        int choice = 0;
        System.out.println("1. Create");
        System.out.println("2. Load from file");
        System.out.println("3. Sort (by price) and Find (by id)");
        System.out.println("4. Update / Delete");
        System.out.println("5. Report and save");
        System.out.println("6. Exit");
        //validate choice 
        while (true) {
            choice = va.readInt("Enter your choice: ", 0, 6);
            switch (choice) {
                case 1:
                    menuChoice("Choice product \n" + "1. Book\n" + "2. Clothing\n" + "3. Disc", 1, 3);
                    break;
                case 2:
                    try {
                        p.loadFile(productList);
                        for (int i = 0; i < productList.size(); i++) {
                            System.out.println(productList.get(i));
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case 3:
                    p.sort(productList);
                    p.search(productList);
                    break;
                case 4:
                    option("Chose !");
                    break;
                case 5:
                    //   p.save(productList);
                    p.writeFile(productList);
                    break;
                case 6:
                    System.exit(0);
            }
        }
    }

    public void menuChoice(String mes, int min, int max) {
        int n, count = 0;
        System.out.println(mes);
        //list de chua cac san pham
        do {
            // khach hang lua chon 1 trong 3 loai san pham
            //book
            //clothes
            //disc
            n = va.readInt("Input choice: ", 0, 4);
            switch (n) {
                case 1:
                    Book b = new Book();
                    b.input(productList);
                    productList.add(b);
                    count += 1;
                    break;
                case 2:
                    Clothing c = new Clothing();
                    c.input(productList);
                    productList.add(c);
                    count += 1;
                    break;
                case 3:
                    Disc d = new Disc();
                    d.input(productList);
                    productList.add(d);
                    count += 1;
                    break;
                case 4:
                    System.exit(0);
            }
        } while (va.isContinue("Do you want to continue ? (y/n)"));
        for (int i = 0; i < productList.size(); i++) {
            System.out.println(productList.get(i));
        }
    }

    public void option(String mes) {
        String s;
        System.out.println(mes);
        do {
            s = va.isOption("Your choice:\n" + "U. Update\n" + "R. Remove\n");
            if (s.equals("u")) {
                p.update(productList);
            } else if (s.equals("r")) {
                p.remove(productList);
            }
        } while (va.isContinue("Do you want to continue ? (y/n)"));
    }
}
