/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manipulation;

import java.util.Scanner;

/**
 *
 * @author spd09
 */
public class Validator {

    public static String valiBin() {
        Scanner in = new Scanner(System.in);
        String bin = "";
        int sign = 0;
        while (true) {
            try {
                while (sign == 0) {
                    sign = 1;
                    System.out.println("Enter a number in Binary mode:");
                    bin = in.nextLine();
                    int len = bin.length();
                    for (int i = 0; i < len; i++) {
                        if (!(bin.charAt(i) == '0' || bin.charAt(i) == '1')) {
                            sign = 0;
                        }
                    }
                    if (sign == 1) {
                        return bin;
                    }
                }              
            } catch (Exception e) {
                System.out.println("Enter a number in Binary mode: ");
            }
        }
    }

    public static String valiHex() {
        Scanner in = new Scanner(System.in);
        String hex = "";
        int sign = 0;
        while (true) {
            try {
                while (sign == 0) {
                    sign = 1;
                    System.out.println("Enter a number in Binary mode: ");
                    hex = in.nextLine();
                    int len = hex.length();
                    for (int i = 0; i < len; i++) {
                        if ((hex.charAt(i) >= 'A' && hex.charAt(i) <= 'F')
                                || (hex.charAt(i) >= '0' && hex.charAt(i) <= '9')) {
                            sign = 1;
                        }
                    }
                    if (sign == 1) {
                        return hex;
                    }
                }
            } catch (Exception e) {
                System.out.println("Enter a number in Binary mode: ");
            }
        }
    }
}
