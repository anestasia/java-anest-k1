/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.gui;

import com.anest.src.Candidate;
import com.anest.src.Experience;
import com.anest.src.Fresher;
import com.anest.src.Intern;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Van SO CUTE
 */
public class Test {

    public static void main(String[] args) {
        ArrayList<Candidate> e;
        e = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        String continues = new String();
        String foundName= new String();
        int option = 0;
        int count = -1;
        do {
            System.out.println("CANDIDATE MANAGEMENT SYSTEM\n"
                    + "1.	Experience\n"
                    + "2.	Fresher\n"
                    + "3.	Internship\n"
                    + "4.	Searching\n"
                    + "5.	Exit");
            option = Integer.parseInt(in.nextLine());

            if (option == 1 || option == 2 || option == 3) {
                do {
                    count++;
                    if (option == 1) {
                        e.add(new Experience());

                    }
                    if (option == 2) {
                        e.add(new Fresher());
                    }
                    if (option == 3) {
                        e.add(new Intern());
                    }
                    e.get(count).setCandidate();
                    e.get(count).setCandidateType(8);
                    System.out.println("Do you want to continue (Y/N)");
                    continues = in.nextLine();
                } while (continues.equalsIgnoreCase("y"));
            }
            if (option == 4) {
                System.out.println("List of Candidate: ");
                System.out.println("==========EXPERIENCE CANDIDATE============= ");
                for (int i = 0; i < e.size(); i++) {
                    if (e.get(i).getCandidateType() == 0) {
                        e.get(i).outputCandidateName();
                    }
                }
                System.out.println("==========FRESHER CANDIDATE============= ");
                for (int i = 0; i < e.size(); i++) {
                    if (e.get(i).getCandidateType() == 1) {
                        e.get(i).outputCandidateName();
                    }
                }
                System.out.println("==========INTERN CANDIDATE============= ");
                for (int i = 0; i < e.size(); i++) {
                    if (e.get(i).getCandidateType() == 2) {
                        e.get(i).outputCandidateName();
                    }
                }
                System.out.println("Input Candidate Name: ");
                foundName = in.nextLine();
                for (int i = 0; i < e.size(); i++) {
                    if(e.get(i).getFirstName().contains(foundName)) 
                        e.get(i).output();
                    else{
                        if(e.get(i).getLastName().contains(foundName)) 
                        e.get(i).output();
                    }
                }
            }
        } while (option != 5);

    }

}
