/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.entity;

/**
 *
 * @author lean1
 */
public class Expericene  extends Candidate {
    private int ExpInYear;
    private String ProSkill;

    public Expericene() {
    }

    public Expericene(String id, String ltName, String ftName, int birthday, String address, String phone, String email, int type, int expInYear, String proSkill) {
        super(id, ltName, ftName, birthday, address, phone, email, type);
        ExpInYear = expInYear;
        ProSkill = proSkill;
    }

    public int getExpInYear() {
        return ExpInYear;
    }

    public void setExpInYear(int expInYear) {
        ExpInYear = expInYear;
    }

    public String getProSkill() {
        return ProSkill;
    }

    public void setProSkill(String proSkill) {
        ProSkill = proSkill;
    }

}

    

