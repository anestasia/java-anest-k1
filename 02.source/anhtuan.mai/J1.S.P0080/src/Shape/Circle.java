package Shape;

public class Circle extends Shape {

    private double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius; //gan tham so truyen vao current object
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void calPer() {
        System.out.println("Tinh chu vi"+ 2*Math.PI*radius);
    }

    @Override
    public void calArea() {
        System.out.println("Tinh dien tich"+ radius*radius*Math.PI);
    }

    @Override
    public void calV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
