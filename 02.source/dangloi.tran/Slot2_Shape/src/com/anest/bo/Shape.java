/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

/**
 *
 * @author Tran Dang Loi
 */
public interface Shape {
    /**
     * the function to calculate Area
     * @return 
     */
    public double calArea();
    /**
     * the function to calculate Parimeter
     * @return 
     */
    public double calPri();
    public void input();
    
}
