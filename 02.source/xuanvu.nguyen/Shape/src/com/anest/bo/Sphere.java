/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Sphere extends ThreeDimensionalShape {
    private double r;
    
    public Sphere() {
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    @Override
    public double calArea() {
       return 4*Math.PI*this.r*this.r;
    }

    @Override
    public double calPri() {
       return 0;
    }

    @Override
    public double calV() {
       return 4/3*Math.PI*Math.pow(r, 3);
    }


    @Override
    public void inPut() {
        System.out.println(" Sphere:");
        System.out.println("enter r= ");
        this.r = new Scanner(System.in).nextDouble(); 
    }


}
