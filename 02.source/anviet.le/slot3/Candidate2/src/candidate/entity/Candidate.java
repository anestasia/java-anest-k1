/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.entity;

/**
 *
 * @author lean1
 */
public class Candidate {
/**
 * Created by W10-PRO on 22-Jan-17.
 */
    private String id;
    private String lastName;
    private String fisrtName;
    private int birthday;
    private String address;
    private String phone;
    private String email;
    private int type;

    public Candidate() {
    }

    public Candidate(String id, String lastName, String firstName, int birthday, String address, String phone, String email, int type) {
        this.id = id;
        this.lastName = lastName;
        this.fisrtName = firstName;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String ltName) {
        this.lastName = ltName;
    }

    public String getFirstName() {
        return fisrtName;
    }

    public void setFirstName(String ftName) {
        this.fisrtName = ftName;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "ID:'" + id + '\'' +
                ", Last name='" + lastName + '\'' +
                ", First Name: '" + fisrtName + '\'' +
                ", Birthday: " + birthday +
                ", Address: '" + address + '\'' +
                ", Phone: '" + phone + '\'' +
                ", Email: '" + email + '\'' +
                '}';
    }
}

    

