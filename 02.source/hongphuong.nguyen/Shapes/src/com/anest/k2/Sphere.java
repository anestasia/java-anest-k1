/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k2;

import java.util.Scanner;

/**
 *
 * @author Hong Phuong
 */
public class Sphere extends ThreeDimensionalShape {

    private double banKinh;
    
    public Sphere() {
        
    }
    
    public Sphere(double bk) {
        banKinh = bk;
    }
    
    @Override
    public double chuVi() {
        return banKinh * 3.14159 * 2;
    }
    
    

    @Override
    public double dienTich() {
        return 4.0/3 * 3.14159 * Math.pow(banKinh,2);
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter the Redius: ");
        this.banKinh = s.nextDouble();
    }

    @Override
    public double v() {
        return 4.0/3 * 3.14159 * Math.pow(banKinh,3); 
    }
    
}
