/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest;

import java.util.Scanner;

/**
 *
 * @author Van SO CUTE
 */
public class Cube extends ThreeDimensionalShape {
private double e;
    @Override
    public double calArea(){
        return 6*e*e;
    }

    @Override
    public double calPri() {
        return -1;
    }

    @Override
    public double volume() {
        return e*e*e;
    }

    @Override
    public void input() {
        System.out.print("Enter e= ");
        e =new Scanner(System.in).nextDouble();
    }
    
}
