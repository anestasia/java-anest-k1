/**
 *
 */
package com.anest.k1.product.bo;

import java.util.ArrayList;
import java.util.List;

import com.anest.k1.product.entity.Product;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ListProduct implements Serializable {

    private int quantity;
    private List<Product> products;

    public ListProduct() {
        super();
        this.quantity = 0;
        this.products = new ArrayList<>();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param quantity
     * @param products
     */
    public ListProduct(int quantity, List<Product> products) {
        super();
        this.quantity = quantity;
        this.products = products;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     *
     * @param product
     */
    public void addProduct(Product product) {
        if (products.contains(product)) {
            System.out.println("Duplicate!");
        } else {
            this.products.add(product);
            this.quantity++;
        }

    }

    public boolean deleteProduct(int id) {
        try {
            int index = findID(id); // Lấy vị trí của phần tử cần sửa có id 
            products.remove(index);
            System.out.println("Delete products successfully!!!");
            if (index == -1) { // không tìm thấy
                System.err.println("Không tìm thấy phần tử nào có id là : " + id);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void updateProduct(int id, Product p) {
        try {
            int index = findID(id); // Lấy vị trí của phần tử cần sửa có id 
            if (index == -1) { // không tìm thấy
                System.err.println("Không tìm thấy phần tử nào có id là : " + id);
                return;
            }

            Product product = products.get(index);
            System.out.println("Phần tử tìm thấy : " + product.toString());
            // thay thế product bằng p 
            products.set(index, p);
            System.out.println("Update products successfully!!!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void find(int id, Product p) {
        int index = findID(id); // Lấy vị trí của phần tử cần sửa có id 
        if (index == -1) { // không tìm thấy
            System.err.println("Không tìm thấy phần tử nào có id là : " + id);
            return;
        } else {
            Product product = products.get(index);
            System.out.println("Phần tử tìm thấy : " + product.toString());
        }
    }

    public int findID(int id) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void sortByPrice() {
        for (int i = 0; i < products.size() - 1; i++) {
            for (int j = i + 1; j < products.size(); j++) {
                if (products.get(i).getPrice() > (products.get(j).getPrice())) {
                    Product temp = products.get(i);
                    products.set(i, products.get(j));
                    products.set(j, temp);
                }
            }
        }

    }

    /**
     *
     * @param fileName
     */
    public void loadFromFile(String fileName) {
//        String[] lines = UTF8FileUtility.getLines(fileName);
//        try {
//            this.quantity = Integer.parseInt(lines[0].trim());
//            for (int i = 1; i < this.quantity + 1; ++i) {
//                this.products.add(getProductFromLine(lines[i]));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.err.println("Có lỗi xảy ra, vui lòng xem lại danh sách!!!");
//        }
        try {
            ListProduct listProduct;
            FileInputStream f = new FileInputStream(fileName);	// tao file f tro den student.dat
            ObjectInputStream inStream = new ObjectInputStream(f);	// dung de doc theo Object vao file f
            // dung inStream doc theo Object, ep kieu tra ve la Student
            listProduct = (ListProduct) inStream.readObject();
            this.quantity = listProduct.quantity;
            this.products = listProduct.products;

            inStream.close();

        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error Read file");
        }
    }

    /**
     *
     * @param line
     * @return
     */
//    public void getProductFromLine(String line) {
//        try {	// dat try cacth de tranh ngoai le khi tao va doc File
//            ListProduct listProduct;
//            FileInputStream f = new FileInputStream("product.dat");	// tao file f tro den student.dat
//            ObjectInputStream inStream = new ObjectInputStream(f);	// dung de doc theo Object vao file f
//            // dung inStream doc theo Object, ep kieu tra ve la Student
//            listProduct = (ListProduct) inStream.readObject();
//            this.products = listProduct.products;
//            this.quantity = listProduct.quantity;
//            inStream.close();
//        } catch (ClassNotFoundException e) {
//            System.out.println("Class not found");
//        } catch (IOException e) {
//            System.out.println("Error Read file");
//        }
////        String[] tokens = line.split("\\|");
////        String category = tokens[0].trim();
////        int id = Integer.parseInt(tokens[1].trim());
////        int price = Integer.parseInt(tokens[4].trim());
////        String p1 = tokens[2].trim();
////        String p2 = tokens[3].trim();
////
////        if (category.equalsIgnoreCase("BOOK")) {
////            return new Book(id, ECat.BOOK, price, p1, p2);
////        } else if (category.equalsIgnoreCase("CLOTHING")) {
////            return new Clothing(id, ECat.CLOTHING, price, p1, p2);
////        }
////        return new DISC(id, ECat.CD, price, p1, p2);
//    }
    /**
     *
     * @param fileName
     */
    public void writeFile(String fileName) {
//        UTF8FileUtility.createWriter(fileName);
//        UTF8FileUtility.writeln(this.quantity + "");
//        for (Product product : this.products) {
//            UTF8FileUtility.writeln(product.toString());
//        }
//
//        UTF8FileUtility.closeWriter();
        try {	// dat try cacth de tranh ngoai le khi tao va ghi File

            FileOutputStream f = new FileOutputStream(fileName);	// tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f);	// dung de ghi theo Object vao file f
            oStream.writeObject(this);	// ghi student theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error Write file");
        }

    }

    /**
     *
     */
    public void show() {
        for (Product product : this.products) {
            System.out.println(product.toString());
        }
    }
}
