package com.edu.van.validate;


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Van SO CUTE
 */
public class Input {

    public static int validDateBirth() {
        Scanner in = new Scanner(System.in);
        int birth = 0;
        while (true) {
            try {
                do {
                    System.out.println("Enter Date of Birth (1900-->2016): ");
                    birth = Integer.parseInt(in.nextLine());
                } while (birth > 2016 || birth < 1900);
                return birth;
            } catch (NumberFormatException e) {
                System.out.println("Enter Date of Birth (1900-->2016): ");
            }

        }
    }

    public static String validPhone() {
        String phone = new String();
        int phone2 = 0;
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                do {
                    System.out.println("Enter Phone (with min 10 characters:) ");
                    phone = in.nextLine();
                    phone2 = Integer.parseInt(phone);
                } while (phone.length() < 10);
                return phone;
            } catch (Exception e) {
                System.out.println("Enter Phone (with min 10 characters:) ");
            }
        }
    }

    public static String validEmail() {
        Scanner in = new Scanner(System.in);
        String email = new String();
        do {
            System.out.println("Enter Email: format <account name>@<domain>. ");
            email = in.nextLine();
        } while (!(email.endsWith("@fpt.edu.vn") || email.endsWith("@gmail.com")));
        return email;
    }

    public static int validYearExp() {
        int year = 0;
        Scanner in = new Scanner(System.in);
        while (true) {
            try {
                do {
                    System.out.println("Enter Year of Experience(0 to 100) :");
                    year = Integer.parseInt(in.nextLine());
                } while (year < 0 || year > 100);
                return year;
            } catch (Exception e) {
                System.out.println("Enter Year of Experience(0 to 100) :");
            }
        }
    }

    public static String validRankGra() {
        String rank = new String();
        Scanner in = new Scanner(System.in);
        do {
            System.out.println(" Enter Rank of Graduation(Excellence, Good, Fair, Poor):");
            rank = in.nextLine();
        } while (!(rank.equalsIgnoreCase("Excellence") || rank.equalsIgnoreCase("Good") || rank.equalsIgnoreCase("Fair") || rank.equalsIgnoreCase("Poor")));
        return rank;
    }

}


