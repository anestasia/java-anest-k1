/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Book extends Product {

    private String title;
    private String author;

    public Book() {
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public Book(int id, String category, String title, String author, int price) {
        super(id, category, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public void setInput() {
        Scanner s = new Scanner(System.in);
        super.setInput();
        System.out.print("Category : Book");
        super.setCategory("Book");
        System.out.print("Enter title : ");
        this.title = s.nextLine();
        System.out.print("Enter author : ");
        this.author = s.nextLine();
        System.out.print("Enter price : ");
        super.setPrice(s.nextInt());
    }

    @Override
    public String toString() {
        return super.toString()+ " | " + title + " | " + author + " | " + super.getPrice();
    }
    
}
