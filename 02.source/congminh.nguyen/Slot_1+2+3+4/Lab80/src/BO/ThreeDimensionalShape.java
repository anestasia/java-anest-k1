/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author Cong Minh
 */
public abstract class ThreeDimensionalShape implements Shape{

    @Override
    public abstract double calAre();
    //To change body of generated methods, choose Tools | Templates.

    @Override
    public abstract double calPri();
    //To change body of generated methods, choose Tools | Templates.

    public abstract double volume();
}
