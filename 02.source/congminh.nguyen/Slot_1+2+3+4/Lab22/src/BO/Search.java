/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import static BO.Input.*;
import ENTITY.Candidate;
import ENTITY.Experience;
import ENTITY.Fresher;
import ENTITY.Intern;
import java.io.IOException;
import java.util.ArrayList;
import ENTITY.Type;
/**
 *
 * @author Cong Minh
 */
public class Search {

    public void displayAll(ArrayList<Candidate> lst, Type type) {
        for (int i = 0; i < lst.size(); i++) {
            if (lst.get(i).getType() == type.toString()) {
                System.out.println(lst.get(i).getFirstName() + " "
                        + lst.get(i).getLastName());
            }
        }
    }

    public ArrayList<Candidate> search(ArrayList<Candidate> lst, String str) {

        ArrayList<Candidate> result = new ArrayList();
        str = str.toLowerCase();
        for (int i = 0; i < lst.size(); i++) {
            if (lst.get(i).getLastName().toLowerCase().contains(str)) {
                result.add(lst.get(i));
            } else if (lst.get(i).getFirstName().toLowerCase().contains(str)) {
                result.add(lst.get(i));
            }
        }
        return result;
    }

    public void display(ArrayList<Candidate> lst) {
        for (int i = 0; i < lst.size(); i++) {

            System.out.println(lst.get(i).getFirstName() + " " + lst.get(i).getLastName()
                    + " | " + lst.get(i).getBirthDate() + " | " + lst.get(i).getAddress()
                    + " | " + lst.get(i).getPhone() + " | " + lst.get(i).getEmail()
                    + " | " + lst.get(i).getId()
            );
        }
    }

    
    public void searchCandidate() throws IOException {
        Validator t = new Validator();
        String name;
        int type;
        do {
            name = t.normalizeString(InputString("Enter name to search: "));
            if (t.stringChecker(name) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.stringChecker(name) == false);

        type = inputInt("Enter type: ", 2, 0);

    }
}
