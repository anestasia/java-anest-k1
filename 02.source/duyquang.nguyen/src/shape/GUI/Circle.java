/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;

import java.util.Scanner;
import shape.bo.TwoDimensionShape;

/**
 *
 * @author Eo Mo
 */
public class Circle extends TwoDimensionShape {
    private double r;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    
    
    @Override
    public double calArea() {
        return this.r * this.r * Math.PI;
    }

    @Override
    public double calPri() {
        return this.r * 2 * Math.PI;
    }

    @Override
    public void input() {
        System.out.print("Nhap vao r= "+this.r);
        this.r=new Scanner(System.in).nextDouble();
    }
}

 

