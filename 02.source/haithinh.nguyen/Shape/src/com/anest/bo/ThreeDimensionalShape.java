/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

/**
 *
 * @author Thinh
 */
public abstract class ThreeDimensionalShape implements Shape {

    /**
     *
     * @return
     */
    
    public abstract double calV();

    @Override
    public abstract void inPut();

    @Override
    public abstract double calPri();

    @Override
    public abstract double calArea();
    
}
