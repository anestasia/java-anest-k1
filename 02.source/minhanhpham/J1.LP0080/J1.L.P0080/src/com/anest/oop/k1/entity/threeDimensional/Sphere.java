/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.oop.k1.entity.threeDimensional;

import com.anest.oop.k1.bo.Utils;

/**
 *
 * @author Cpt
 */
public class Sphere extends ThreeDimensionalShape {

    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    @Override
    public double calArea() {
        return 4 * Math.PI * this.radius * this.radius;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#calPri()
     */
    @Override
    public double calPri() {
        return 2 * this.radius * Math.PI;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#input()
     */
    @Override
    public void input() {
        radius = Utils.getDoubleNumber("Enter radius of sphere:", Double.MAX_VALUE, 0);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#output()
     */
    @Override
    public void output() {
        // TODO Auto-generated method stub
        System.out.println(" Sphere : \t Radius : (" + this.radius + ")");
        System.out.println("\t+ Area : " + this.calArea());
        System.out.println("\t+ Pri  : " + this.calPri());
        System.out.println("\t+ Volume: " + this.volume());
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#volume()
     */
    @Override
    public double volume() {
        return 4 / 3 * Math.PI * this.radius * this.radius * this.radius;
    }
}
