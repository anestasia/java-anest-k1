/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.DB;

import com.anest.Validator.ValidateValue;

/**
 *
 * @author Taka
 */
public class Conversion {

    public int count = 0;
    public int num = 0;

    //khởi tạo đối tượng đồng thời khởi tạo các giá trị của ini
    public Conversion() {

    }

    public void convert() {
        ValidateValue vl = new ValidateValue();
        System.out.println("I can help you converting number from any base to any base.Let's try: ");
        vl.inputBaseInputNumber();
        vl.inputBaseOutputNumber();
        vl.inputNumberToBeConverted();
        vl.createArrayToValidNumberOutput();
        //chuyển qua cơ số 10
        for (int i = vl.getIni().getNumber().length() - 1; i >= 0; i--) {//xét từ chữ số cuối cùng tới chữ số đầu tiên của số bị chuyển
            try {
                //Chuyển giá trị từ String của chữ số thứ i sang integer rồi nhân với base input mũ thứ tự
                num += Integer.parseInt(String.valueOf(vl.getIni().getNumber().charAt(i))) * Math.pow(vl.getIni().getBaseInput(), count);

            } catch (NumberFormatException e) {
                for (int j = 10; j < vl.array.length; j++) {
                    if (String.valueOf(vl.getIni().getNumber().charAt(i)).equals(vl.array[j])) {
                        num += j * Math.pow(vl.getIni().getBaseInput(), count);
                    }
                }
            }
            count++;
        }

        String st = "";
        while (true) {
            String str;

//            System.out.println("----- "+vl.getIni().getBaseOutput());
            if (num % vl.getIni().getBaseOutput() < 10) {
                str = Integer.toString(num % vl.getIni().getBaseOutput());//tại cái này

            } else {
                for (int i = 10;; i++) {
                    if (num % vl.getIni().getBaseOutput() == i) {
                        str = vl.array[i];
                        break;
                    }
                }
            }
            num = num / vl.getIni().getBaseOutput();    //gán lại số sum     
            st += str;
            if (num == 0) {
                vl.getIni().setResult(st);
                break;
            }
        }
        String newStr = new StringBuilder(st).reverse().toString();
        vl.getIni().setResult(newStr);
        System.out.println(vl.getIni());
    }
}
