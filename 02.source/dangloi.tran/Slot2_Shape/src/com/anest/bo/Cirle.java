/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import com.edu.fpt.validate.Validator;
import java.util.Scanner;

/**
 *
 * @author Tran Dang Loi
 */
public class Cirle extends TwoDimensionalShape{
    private double r;

    public Cirle() {
    }

    public Cirle(double r) {
        this.r = r;
    }
    
    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public double calArea() {
        return this.r * this.r * Math.PI;
        }
/**
 * 
 * @return 
 */
    @Override
    public double calPri() {
        return 2* this.r * Math.PI;
    }

    @Override
    public void input() {
        System.out.println("Enter r = ");
        this.r= Validator.validateDouble();
        
    }
    
}
