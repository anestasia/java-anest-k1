package com.anest.k1.product.entity;

import java.util.Scanner;

public class Book extends Product {
	private String title;
	private String author;
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(int id, ECat category, int price, String title, String author) {
		super(id, category, price);
		this.author = author;
		this.title = title;
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param title
	 * @param author
	 */
	public Book(String title, String author) {
		super();
		this.title = title;
		this.author = author;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	@Override
	public void input() {
		super.input();
		this.setCategory(ECat.BOOK);
		System.out.print(" Title = ");
		this.title = new Scanner(System.in).nextLine().trim();
		System.out.print(" Author = ");
		this.title = new Scanner(System.in).nextLine().trim();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getCategory() + "|" + this.getId() + "|" + this.title + "|" + this.author + "|" + this.getPrice();
	}
	
	
}
