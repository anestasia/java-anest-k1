/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.gui;

import candidate.bo.Deverloper;
import candidate.until.Input;
import candidate.until.Validate;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class ShowMenu {

    public ShowMenu() {
    }

    public void showMenuStart() {
        System.out.println("==========CANDIDATE MANAGEMENT SYSTEM===========");
        System.out.println("==      1. Experience                         ==");
        System.out.println("==      2. Fresher                            ==");
        System.out.println("==      3. Internship                         ==");
        System.out.println("==      4. Searching                          ==");
        System.out.println("==      5. Exit                               ==");
    }
    
    public void showMenuDevelop() {
       System.out.println("==========CANDIDATE MANAGEMENT SYSTEM===========");
       System.out.println("==       1. Create                            ==");
       System.out.println("==       2. Update                            ==");
       System.out.println("==       3. Delete                            ==");
       System.out.println("==       4. Exit                              ==");
    }

    public void showDeverlop(ArrayList<Deverloper> Candidates) {
        if (Candidates.isEmpty()) {
            System.out.println("No deverloper found!!");
        } else {
            for (Deverloper list : Candidates) {
                list.showInfor();
            }
        }
    }

    public void showNameDeverlop(ArrayList<Deverloper> Candidates) {
        System.out.println("List of candidate:");
        System.out.println("===========EXPERIENCE CANDIDATE============");

        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 0) {
                System.out.println(list.getFirstName() + " " + list.getLastName());
            }
        }

        System.out.println("===========FRESHER CANDIDATE============");
        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 1) {
                System.out.println(list.getFirstName() + " " + list.getLastName());
            }
        }

        System.out.println("===========INTERSHIP CANDIDATE============");
        for (Deverloper list : Candidates) {
            if (list.getCandidateTpye() == 2) {
                System.out.println(list.getFirstName() + " " + list.getLastName());
            }
        }
    }

    public void searchingDeverLop(ArrayList<Deverloper> Candidates) {
        Input input = new Input();
        Scanner in = new Scanner(System.in);
        String inputStr;
        int inputTypeCandidates;
        System.out.println("Input Candidate name (First name or Last name): ");
        inputStr = in.nextLine().trim();
        inputTypeCandidates = (int) input.getNumber("Input type of candidate: ", "Invalid type of candidate", 0, 2);
        for (Deverloper list : Candidates) {
            if ((list.getFirstName().contains(inputStr) || list.getLastName().contains(inputStr)) && list.getCandidateTpye() == inputTypeCandidates) {
                list.showInfor();
            }
        }
    }

    public void isDeverloper(ArrayList<Deverloper> Candidates) {
        Input input = new Input();
        Scanner in = new Scanner(System.in);
        String inputStr;

    }

    public void updateDeverlop(ArrayList<Deverloper> Candidates) {

    }

}
