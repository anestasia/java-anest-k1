/**
 *
 */
package com.anest.oop.k1.ui;

import java.util.Random;

import com.anest.oop.k1.bo.ShapeList;
import com.anest.oop.k1.bo.Utils;
import com.anest.oop.k1.entity.Shape;
import com.anest.oop.k1.entity.threeDimensional.Cube;
import com.anest.oop.k1.entity.threeDimensional.Sphere;
import com.anest.oop.k1.entity.threeDimensional.Tetrahedron;
import com.anest.oop.k1.entity.twoDimensional.Circle;
import com.anest.oop.k1.entity.twoDimensional.Square;
import com.anest.oop.k1.entity.twoDimensional.Triangle;

/**
 * @author admin
 *
 */
public class Application {

    public static void main(String[] args) {
        ShapeList shapeList = new ShapeList();
        do {
            System.out.print("*Enter your choice : \n\t 1. Two Dimentional Shape \n\t 2. Three Dimentional Shape \n\t 3. Cancel \n Your choice: ");
            int choice = Utils.getIntNumber("", 3, 1);
            if (choice == 3) {
                break;
            }
            Shape shape = null;
            switch (choice) {
                case 1:
                    System.out.print("*Enter your choice : \n\t 1. Circle \n\t 2. Triangle \n\t 3. Square \n Your choice: ");
                    int choice1 = Utils.getIntNumber("", 3, 1);
                    switch (choice1) {
                        case 1:
                            shape = new Circle();
                            break;
                        case 2:
                            shape = new Triangle();
                            break;
                        case 3:
                            shape = new Square();
                            break;
                    }
                    break;
                case 2:
                    System.out.print("*Enter your choice : \n\t 1. Cube \n\t 2. Sphere \n\t 3. Tetrahedron \n Your choice: ");
                    int choice2 = Utils.getIntNumber("", 3, 1);
                    switch (choice2) {
                        case 1:
                            shape = new Cube();
                            break;
                        case 2:
                            shape = new Sphere();
                            break;
                        case 3:
                            shape = new Tetrahedron();
                            break;
                    }
                    break;
            }
            shape.input();
            shapeList.add(shape);
        } while (true);
        for (Circle circle : shapeList.getCircles()) {
            circle.output();
        }
        System.out.println(" Show list : ");
        shapeList.showList();

    }

}
