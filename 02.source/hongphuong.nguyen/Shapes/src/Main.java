
import com.anest.k2.Circle;
import com.anest.k2.Shape;
import com.anest.k2.Sphere;
import com.anest.k2.Square;
import com.anest.k2.ThreeDimensionalShape;
import com.anest.k2.Triangle;
import com.anest.k2.TwoDimensionalShape;


public class Main {
    
    public static void main(String[] args) {
//        TwoDimensionalShape tds ; 
//        tds = new Circle();
//        tds.input();
//        double dienTich = tds.dienTich();
//        double chuVi = tds.chuVi();
//        System.out.println("Dien tich la: " + dienTich);
//        System.out.println("Chu vi la: " + chuVi);
//        
//        tds = new Square(6);
//        dienTich = tds.dienTich();
//        chuVi = tds.chuVi();
//        System.out.println("Dien tich la: " + dienTich);
//        System.out.println("Chu vi la: " + chuVi);
//        
        Shape[] arr  = new Shape[5];
        
        for(int i = 0; i < arr.length; i++) {
            if(i%2 == 0){
                arr[i] = new Sphere();
                arr[i].input();
                
                System.out.println("v = " + ((ThreeDimensionalShape)arr[i]).v());
            }
            if(i%2 != 0) {
                arr[i] = new Square();
                arr[i].input();
            }
            System.out.println("Dien tich la: " + arr[i].dienTich());
            System.out.println("Chu vi la: " + arr[i].chuVi());
        }
        
        
    }
    
}
