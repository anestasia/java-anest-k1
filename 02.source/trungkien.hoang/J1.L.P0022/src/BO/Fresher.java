/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import util.Input;
import java.util.Calendar;
import java.util.Scanner;


/**
 *
 * @author KienKeng
 */
public class Fresher extends Candidate{

    
    Rank GraduationRank;
    int GraduationTime;

    public Fresher(int ID, int DateOfBirth, CanType CandidateType, String FirstName, String LastName, String Address, String Email, String Phone) {
        super(ID, DateOfBirth, CandidateType, FirstName, LastName, Address, Email, Phone);
    }

    public Fresher() {
    }

    public Fresher(Rank GraduationRank, int GraduationTime) {
        this.GraduationRank = GraduationRank;
        this.GraduationTime = GraduationTime;
    }

    public Rank getGraduationRank() {
        return GraduationRank;
    }

    public void setGraduationRank(Rank GraduationRank) {
        this.GraduationRank = GraduationRank;
    }

    public int getGraduationTime() {
        return GraduationTime;
    }

    public void setGraduationTime(int GraduationTime) {
        this.GraduationTime = GraduationTime;
    }

    
    
    @Override
    public void input()
    {
        super.input();
        int x;
        Input input = new Input();
        Scanner scan =  new Scanner(System.in);
        Validator validate = new Validator();
        System.out.println(" Graduation Rank : ");
        System.out.println("0. Excellence");
        System.out.println("1. Good");
        System.out.println("2. Fair"); 
        System.out.println("3. Poor");
        x = (int)input.getNumber("Enter the rank of Graduation: ", "Invalid Input! Please enter number only", 0, 3);
        switch(x)
        {
            case 0: 
                this.GraduationRank = Rank.excellence;
                break;
            case 1: 
               this.GraduationRank = Rank.good;
                break;
            case 2:
                this.GraduationRank = Rank.fair;
                break;
            case 3:
                this.GraduationRank = Rank.poor;
                break;

        }
    }
    
    
    
    
}
