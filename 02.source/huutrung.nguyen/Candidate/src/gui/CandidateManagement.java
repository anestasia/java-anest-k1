package gui;

import bo.CandidateList;
import bo.Fresher;
import bo.Experience;
import bo.Input;
import bo.Intern;
import com.anest.Utils;
import java.util.List;
import java.util.Scanner;

public class CandidateManagement {

    static final String ERROR = "Invalid input, please re-enter!";

    public static void main(String[] args) {
        CandidateManagement can = new CandidateManagement();
        CandidateList candidateList = new CandidateList();
        Utils val = new Utils();
        Input input = new Input();
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        int choice;
        do {
            choice = can.menu();
            switch (choice) {
                case 1: {   //Create an experience
                    do {
                        Experience experienceObject = new Experience();
                        input.inputCandidate(experienceObject);
                        input.inputExperience(experienceObject);
                        experienceObject.setId(candidateList.size() + 1);
                        candidateList.add(experienceObject);
                    } while (val.OptionChoice("Do you want to add more experiences?(Y/N): ", "Y", "N"));
                    break;
                }
                case 2: {   //Create a fresher
                    do {
                        Fresher fresherObject = new Fresher();
                        input.inputCandidate(fresherObject);
                        input.inputFresher(fresherObject);
                        fresherObject.setId(candidateList.size() + 1);
                        candidateList.add(fresherObject);
                    } while (val.OptionChoice("Do you want to add more freshers?(Y/N): ", "Y", "N"));
                    break;
                }
                case 3: {   //Create an intern
                    do {
                        Intern internObject = new Intern();
                        input.inputCandidate(internObject);
                        input.inputIntern(internObject);
                        internObject.setId(candidateList.size() + 1);
                        candidateList.add(internObject);
                    } while (val.OptionChoice("Do you want to add more interns?(Y/N): ", "Y", "N"));
                    break;
                }

                case 4: {   //Search by name
                    System.out.print("Enter name: ");
                    String str = in.next();
                    List<String> resultList = candidateList.search(str);
                    if (resultList.isEmpty()) {
                        System.out.println("Can not find this name!");
                    } else {
                        resultList.forEach((a) -> {
                            System.out.println(a);
                        });
                    }
                    break;
                }
                case 5: {   //Display list
                    candidateList.showList();
                    break;
                }
            }
        } while (choice != 6);
    }

    public int menu() {  //Menu management
        Utils val = new Utils();
        System.out.println("CANDIDATE MANAGERMENT SYSTEM");
        System.out.println("1.Experience");
        System.out.println("2.Fresher");
        System.out.println("3.Intern");
        System.out.println("4.Searching");
        System.out.println("5.Show list");
        System.out.println("6.Exit");
        int choice = val.getInt("Enter your choice: ", "Invalid input, please re-enter!", 1, 6);
        return choice;
    }
}
