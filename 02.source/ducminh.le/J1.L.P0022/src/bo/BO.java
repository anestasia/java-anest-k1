package bo;

import entity.Candidate;
import entity.Experience;
import entity.Fresher;
import entity.Intern;
import util.Validate;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Minh on 1/23/2017.
 */
public class BO {
    Scanner in = new Scanner(System.in);
    Validate validate = new Validate();
    ArrayList<Candidate> candidates = new ArrayList<>();

    public void getExperience() {
        do {
            Experience experience = new Experience();
            System.out.println();
            System.out.print("Enter Candidate ID: ");
            experience.setCandidateId(in.nextLine());
            System.out.print("Enter First name: ");
            experience.setFirstName(in.nextLine());
            System.out.print("Enter Last name: ");
            experience.setLastName(in.nextLine());
            do {
                experience.setBirthDate(validate.getInt("Enter Birth date: ", "Invalid year", 0, Integer.MAX_VALUE));
            } while (!validate.isYear(1900, experience.getBirthDate()));
            System.out.print("Enter Address: ");
            experience.setAddress(in.nextLine());
            do {
                System.out.print("Enter Phone: ");
                experience.setPhone(in.nextLine());
            } while (!validate.isPhone(experience.getPhone()));
            do {
                System.out.print("Enter Email: ");
                experience.setEmail(in.nextLine());
            } while (!validate.isEmail(experience.getEmail()));
            experience.setCandidateType(0);
            experience.setExpInYear(validate.getInt("Enter Year of experience: ", "Invalid year", 1, 100));
            System.out.print("Enter Professional kill: ");
            experience.setProSkill(in.nextLine());
            candidates.add(experience);
        } while (validate.pressYN());
        System.out.println();
    }

    public void getFresher() {
        do {
            Fresher fresher = new Fresher();
            System.out.println();
            System.out.print("Enter Candidate ID: ");
            fresher.setCandidateId(in.nextLine());
            System.out.print("Enter First name: ");
            fresher.setFirstName(in.nextLine());
            System.out.print("Enter Last name: ");
            fresher.setLastName(in.nextLine());
            do {
                fresher.setBirthDate(validate.getInt("Enter Birth date: ", "Invalid year", 0, Integer.MAX_VALUE));
            } while (!validate.isYear(1900, fresher.getBirthDate()));
            System.out.print("Enter Address: ");
            fresher.setAddress(in.nextLine());
            do {
                System.out.print("Enter Phone: ");
                fresher.setPhone(in.nextLine());
            } while (!validate.isPhone(fresher.getPhone()));
            do {
                System.out.print("Enter Email: ");
                fresher.setEmail(in.nextLine());
            } while (!validate.isEmail(fresher.getEmail()));
            fresher.setCandidateType(1);
            do {
                fresher.setGraduationDate(validate.getInt("Enter Graduation date: ", "Invalid year", 0, Integer.MAX_VALUE));
            } while (!validate.isYear(1900, fresher.getGraduationDate()));
            do {
                System.out.print("Enter Rank of graduation: ");
                fresher.setGraduationRank(in.nextLine());
            } while (!validate.isRankOfGraduation(fresher.getGraduationRank()));
            System.out.print("Enter University name: ");
            fresher.setEducation(in.nextLine());
            candidates.add(fresher);
        } while (validate.pressYN());
        System.out.println();
    }

    public void getIntern() {
        do {
            Intern intern = new Intern();
            System.out.println();
            System.out.print("Enter Candidate ID: ");
            intern.setCandidateId(in.nextLine());
            System.out.print("Enter First name: ");
            intern.setFirstName(in.nextLine());
            System.out.print("Enter Last name: ");
            intern.setLastName(in.nextLine());
            do {
                intern.setBirthDate(validate.getInt("Enter Birth date: ", "Invalid year", 0, Integer.MAX_VALUE));
            } while (!validate.isYear(1900, intern.getBirthDate()));
            System.out.print("Enter Address: ");
            intern.setAddress(in.nextLine());
            do {
                System.out.print("Enter Phone: ");
                intern.setPhone(in.nextLine());
            } while (!validate.isPhone(intern.getPhone()));
            do {
                System.out.print("Enter Email: ");
                intern.setEmail(in.nextLine());
            } while (!validate.isEmail(intern.getEmail()));
            intern.setCandidateType(2);
            System.out.print("Enter Majors: ");
            intern.setMajors(in.nextLine());
            intern.setSemester(validate.getInt("Enter Semester: ", "Invalid semester", 0, 100));
            System.out.print("Enter University name: ");
            intern.setUniversityName(in.nextLine());
            candidates.add(intern);
        } while (validate.pressYN());
        System.out.println();
    }

    public void searchCandidate() {
        System.out.println();
        System.out.println("List of candidate");
        System.out.println("==========EXPERIENCE CANDIDATE============");
        for (Candidate candidate : candidates) {
            if (candidate instanceof Experience)
                System.out.println(candidate.getFirstName() + " " + candidate.getLastName());
        }
        System.out.println("==========FRESHER CANDIDATE============");
        for (Candidate candidate : candidates) {
            if (candidate instanceof Fresher)
                System.out.println(candidate.getFirstName() + " " + candidate.getLastName());
        }
        System.out.println("==========INTERN CANDIDATE============");
        for (Candidate candidate : candidates) {
            if (candidate instanceof Intern)
                System.out.println(candidate.getFirstName() + " " + candidate.getLastName());
        }
        System.out.println();
        System.out.print("Enter First or Last name: ");
        String name = in.nextLine();
        int type = validate.getInt("Enter Candidate type: ", "Invalid type", 0, 2);
        for (Candidate candidate : candidates) {
            if ((candidate.getFirstName().contains(name) || candidate.getLastName().contains(name))
                    && candidate.getCandidateType() == type)
                System.out.println(
                        candidate.getFirstName() + " " + candidate.getLastName()
                                + " | " + candidate.getBirthDate() + " | " + candidate.getAddress()
                                + " | " + candidate.getPhone() + " | " + candidate.getEmail()
                                + " | " + candidate.getCandidateType());
        }
        System.out.println();
    }
}
