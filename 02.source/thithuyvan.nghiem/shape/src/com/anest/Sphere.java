/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest;

import java.util.Scanner;

/**
 *
 * @author Van SO CUTE
 */
public class Sphere extends ThreeDimensionalShape {
    private double ra;

    public double getRa() {
        return ra;
    }

    public void setRa(double ra) {
        this.ra = ra;
    }
    
    @Override
    public double calArea() {
        return 4*Math.PI*ra*ra;
    }

    @Override
    public double calPri(){
        return -1;
    }

    @Override
    public double volume() {
        return 4/3*ra*ra*ra*Math.PI;
    }

    @Override
    public void input() {
        System.out.print("Enter ra= ");
        ra =new Scanner(System.in).nextDouble();
    }
    
}
