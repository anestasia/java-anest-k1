/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.validator;

import java.util.Scanner;

/**
 *
 * @author Cpt
 */
public class ValidInt {
    public static int getIntNumber(String mess, int max, int min) {
        while (true) {
            try {
                System.out.println(mess);
                String str = new Scanner(System.in).nextLine();
                int result = Integer.parseInt(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
}
