package bo;

import common.Common;
import java.util.Scanner;

public class Fresher extends Candidate {

    private String Graduation_date;
    private String Graduation_rank;
    private String Graduation;

    public String getGraduation_date() {
        return Graduation_date;
    }

    public void setGraduation_date(String Graduation_date) {
        this.Graduation_date = Graduation_date;
    }

    public String getGraduation_rank() {
        return Graduation_rank;
    }

    public void setGraduation_rank(String Graduation_rank) {
        this.Graduation_rank = Graduation_rank;
    }

    public String getGraduation() {
        return Graduation;
    }

    public void setGraduation(String Graduation) {
        this.Graduation = Graduation;
    }

    public Fresher() {
    }

    public Fresher(String Graduation_date, String Graduation_rank, String Graduation) {
        this.Graduation_date = Graduation_date;
        this.Graduation_rank = Graduation_rank;
        this.Graduation = Graduation;
    }

    public Fresher(String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
    }

    public Fresher(String Graduation_date, String Graduation_rank, String Graduation, String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
        this.Graduation_date = Graduation_date;
        this.Graduation_rank = Graduation_rank;
        this.Graduation = Graduation;
    }

    @Override
    public Candidate input(Candidate fresher) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Fresher Candidates ID: ");
        String candidateID = sc.nextLine();
        while (!Common.isNumber(candidateID)) {
            System.out.print("Please Re-enter!: ");
            candidateID = sc.nextLine();
        }
        System.out.print("Enter Fresher Candidates First Name: ");
        String FirstName = sc.nextLine();
        System.out.print("Enter Fresher Candidates Last Name: ");
        String LastName = sc.nextLine();
        System.out.print("Enter Fresher Candidates Birthdate: ");
        String BirthDate = sc.nextLine();
        while (!Common.isBirthdate(BirthDate)) {
            System.out.print("Please Re-enter!: ");
            BirthDate = sc.nextLine();
        }
        System.out.print("Enter Fresher Candidates Adress: ");
        String Address = sc.nextLine();
        System.out.print("Enter Fresher Candidates Phone: ");
        String Phone = sc.nextLine();
        while (!Common.isPhone(Phone)) {
            System.out.print("Please Re-enter!: ");
            Phone = sc.nextLine();
        }
        System.out.print("Enter Fresher Candidates Email: ");
        String Email = sc.nextLine();
        while (!Common.isValidEmailAddress(Email)) {
            System.out.print("Please Re-enter!: ");
            Email = sc.nextLine();
        }
        System.out.print("Enter Fresher Candidates Graduated Time: ");
        String GraduationDate = sc.nextLine();
        System.out.print("Enter Fresher Candidates Graduated Rank: ");
        String GraduationRank = sc.nextLine();
        while (!Common.isRanked(GraduationRank)) {
            System.out.print("Please Re-enter!: ");
            GraduationRank = sc.nextLine();
        }
        System.out.print("Enter Fresher Candidates Graduated School: ");
        String GraduationSchool = sc.nextLine();
        fresher = new Fresher(GraduationDate, GraduationRank, GraduationSchool, candidateID, FirstName, LastName, BirthDate, Address, Phone, Email, 1);
        return fresher;
    }
}
