/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.gui;

import com.anest.bo.Circle;
import com.anest.bo.Cube;
import com.anest.bo.Shape;
import com.anest.bo.Sphere;
import com.anest.bo.Square;
import com.anest.bo.Tetrahedron;
import com.anest.bo.Triangle;
import java.util.Scanner;

/**
 *
 * @author Nguyen Phong
 */
public class Test {

    public static void main(String[] args) {
        Shape shape;
        /**
         * shape = new Circle(); ((Circle) shape).setR(1); System.out.println("S
         * = " + shape.callArea()); System.out.println("P = " +
         * shape.callPri());
         *
         * shape = new Square(4); System.out.println("S = " + shape.callArea());
         * System.out.println("P = " + shape.callPri());
         */

        Shape[] shapes = new Shape[10];
        for (int i = 0; i < shapes.length; i++) {
            System.out.println("Index = " + i);
            if (i % 2 == 0) {
                shapes[i] = new Cube();

            } else if (i % 2 != 0) {
                shapes[i] = new Tetrahedron();

            }
            shapes[i].input();
            System.out.println("S = " + shapes[i].callArea());
            System.out.println("P = " + shapes[i].callPri());
        }
    }
}
