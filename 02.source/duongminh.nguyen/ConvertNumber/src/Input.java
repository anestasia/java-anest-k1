package util;

import java.util.Scanner;
public class Input {
    Scanner in = new Scanner(System.in);

    public int getNumberBase(String mes, String err) {
        while (true) {
            System.out.print(mes);
            String s = in.nextLine().trim();
            try {
                int num = Integer.parseInt(s);
                if (num == 2 || num == 10 || num == 16) {
                    return num;
                } else {
                    System.out.println(err);
                }
            } catch (NumberFormatException nfe) {
                System.out.println(err);
            }
        }
    }

    public boolean pressYN() {
        while (true) {
            System.out.print("Do you want to continue? (Y-yes, N-no)");
            String s = in.nextLine().trim();
            if (s.equalsIgnoreCase("y")) return true;
            if (s.equalsIgnoreCase("n")) return false;
            System.out.println("Invalid choice");
        }
    }
}