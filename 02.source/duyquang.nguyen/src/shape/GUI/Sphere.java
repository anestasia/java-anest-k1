/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;
import java.util.Scanner;
import shape.bo.ThreeDimensionShape;
/**
 *
 * @author Eo Mo
 */
public class Sphere extends ThreeDimensionShape {
    private double banKinh;

    public double getbanKinh() {
        return banKinh;
    }

    public void setbanKinh(double R) {
        this.banKinh = R;
    }
    

    @Override
    public double calArea() {
        return (( this.banKinh * this.banKinh *this.banKinh  *Math.PI)*4)/3;
    }

    @Override
    public double calPri() {
        return this.banKinh * 2 * Math.PI;
    }

    @Override
    public void input() {
        System.out.print("nhap vao R: "+this.banKinh);
        this.banKinh= new Scanner(System.in).nextDouble();
    }
    
}
