
package com.anest;


public interface Shape {
    public double calArea();
    public double calPri();
    public void input();
}
