package entire;

/**
 * Created by W10-PRO on 27-Jan-17.
 */
public class FruitOrder extends Fruit {
    private int quantity;
    private double amount;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return quantity*getFruitPrice();
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public FruitOrder(String fruitID, String fruitName, double fruitPrice, String origin, double amount) {
        super(fruitID, fruitName, fruitPrice, origin);
        this.quantity = quantity;
        this.amount = quantity*fruitPrice;
    }

    public FruitOrder() {

    }
}
