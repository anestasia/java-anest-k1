/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import bo.Product;
import com.edu.fpt.validate.Validator;
import dab.IOconnected;
import dab.Valid;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Menu {

    public void menu() {
        System.out.println("WELCOME TO PRODUCT MANAGEMENT");
        System.out.println("1. Create");
        System.out.println("2. Load from file");
        System.out.println("3. Sort (by price) and Find (by id)");
        System.out.println("4. Update / Delete");
        System.out.println("5. Report and save");
        System.out.println("0. exits");

    }

    public void run() {
        ArrayList<Product> product = new ArrayList<>();
        Valid i1 = new Valid();
        IOconnected io = new IOconnected();
        while (true) {
            menu();
            int choice = Validator.validateInt(0, 5, "Enter your choice :");
            switch (choice) {
                case 1:
                    i1.creatPro(product);
                    i1.show(product);
                    break;
                case 2:
                    product = io.read("products.txt");
                    break;
                case 3:
                    i1.sortAndFind(product);
                    break;
                case 4:
                    break;
                case 5:
                    if (io.write(product, "products.txt")) {
                        System.out.println("Save file sucessfully!");
                    } else {
                        System.out.println("Save file Error!!");
                    }
                    i1.show(product);
                    break;
                case 0:
                    System.out.println("goodbye :) ");
                    return;
                default:
                    System.out.println("Invalid input. Try again!");
                    break;
            }
        }
    }

}
