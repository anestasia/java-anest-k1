package gui;

import bo.Convert;
import bo.Validate;

import java.util.Scanner;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class Main {
    public static void main(String[] args) {
        Convert bo = new Convert();
        Validate val = new Validate();
        Scanner sc = new Scanner(System.in);
        System.out.println("Binary/Decimal/Hex Convertor");
        System.out.println("(Binary=1; Decimal=2; Hex=3)");
        do {
            int base = val.getInt("Enter base: ", "Please enter 1-3!!!", 1, 3);
            int target;
            switch (base) {
                case 1:
                    target = val.getInt("Enter target: ", "Please enter 2 or 3!!!", 2, 3);
                    bo.convBinary(target);
                    break;
                case 2:
                    do {
                        target = val.getInt("Enter target: ", "Please enter 1 or 3!!!", 1, 3);
                        if (target == 2) System.out.println("Please enter 1 or 3!!!");
                    } while (target == 2);
                    bo.convDecimal(target);
                    break;
                case 3:
                    target = val.getInt("Enter target: ", "Please enter 1 or 2!!!", 1, 3);
                    bo.convHex(target);
                    break;
            }
        } while (val.YNquestion());
    }
}
