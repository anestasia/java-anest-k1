package gui;

import bo.Circle;
import bo.Cube;
import bo.Sphere;
import bo.Square;
import bo.Tetrahedron;
import bo.Triangle;
import common.Common;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        do {
            Display.menu();
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            if (!Common.isNumber(input)) {
                System.out.println("Please enter a valid choice");
            } else {
                int choice = Integer.parseInt(input);
                switch (choice) {
                    case 1:
                        do {
                            Circle circle = new Circle();
                            circle.input();
                            circle.output();
                        } while (!Display.pressYN());
                        break;
                    case 2:
                        do{
                        Square square = new Square();
                        square.input();
                        square.output();
                        } while (!Display.pressYN());
                        break;
                    case 3:
                        do{
                        Triangle triangle = new Triangle();
                        triangle.input();
                        triangle.output();
                        } while (!Display.pressYN());
                        break;
                    case 4:
                        do{
                        Cube cube = new Cube();
                        cube.input();
                        cube.output();
                        } while (!Display.pressYN());
                        break;
                    case 5:
                        do{                                                
                        Tetrahedron tetrahedron = new Tetrahedron();
                        tetrahedron.input();
                        tetrahedron.output();
                        } while (!Display.pressYN());
                        break;
                    case 6:
                        do{
                        Sphere sphere = new Sphere();
                        sphere.input();
                        sphere.output();
                        } while (!Display.pressYN());
                        break;
                    default:
                        break;

                }
            }
        } while (true);

    }

}
