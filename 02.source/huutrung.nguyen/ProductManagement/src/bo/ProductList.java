/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Book;
import entity.Clothes;
import entity.Disc;
import entity.ECat;
import java.util.List;
import entity.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class ProductList {

    List<Product> productList;
    Utils val = new Utils();
    Scanner in = new Scanner(System.in);

    public ProductList() {
        productList = new ArrayList<>();
    }

    /**
     *
     * @param product
     * @return true if product added and otherwise
     */
    public boolean add(Product p) {
        if (!val.isID(p.getId())) {
            return false;
        }
        for (Product i : productList) {
            if (p.getId().equalsIgnoreCase(i.getId())) {
                return false;
            }
        }
        productList.add(p);
        return true;
    }

    /**
     * Sorting list with quick Sort algorithm
     */
    public void sort() {
        quickSort(productList, 0, getSize() - 1);
    }

    /**
     * @param arr, left, right
     * @return index of partition
     */
    public int partition(List<Product> arr, int left, int right) {
        int i = left, j = right;
        double pivot = arr.get((left + right) / 2).getPrice();
        while (i <= j) {
            while (arr.get(i).getPrice() < pivot) {
                i++;
            }
            while (arr.get(j).getPrice() > pivot) {
                j--;
            }
            if (i <= j) {
                Collections.swap(arr, i, j);
                i++;
                j--;
            }
        }
        return i;
    }

    /**
     * Quick Sort Algorithm
     */
    public void quickSort(List<Product> arr, int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1) {
            quickSort(arr, left, index - 1);
        }
        if (index < right) {
            quickSort(arr, index, right);
        }
    }

    /**
     * @return size of list
     */
    public int getSize() {
        return productList.size();
    }

    /**
     * @param id
     * @return product has this id
     */
    public Product search(String id) {
        for (Product p : productList) {
            if (p.getId().equalsIgnoreCase(id)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Delete product has this id
     *
     * @param id
     */
    public void delete(String id) {
        Product p = search(id);
        productList.remove(p);
    }

    /**
     * Update product has this id
     *
     * @param id
     */
    public void update(String id) {
        Product p = search(id);
        p.input();
    }

    /**
     * Load content from file to list
     *
     * @param fn : filename
     */
    public void loadFromFile(String fn) {
        FileIO f = new FileIO();
        try {
            String s[] = f.getLines(fn);
            for (String a : s) {
                String[] s1 = a.split("\\|");
                String id = s1[0];
                String category = s1[1];
                String pro1 = s1[2];
                String pro2 = s1[3];
                int price = Integer.parseInt(s1[4]);
                Product p = null;
                if (s1[1].equalsIgnoreCase("book")) {
                    p = new Book(id, ECat.BOOK, pro1, pro2, price);
                }
                if (s1[1].equalsIgnoreCase("clothes")) {
                    p = new Clothes(id, ECat.CLOTHES, pro1, pro2, price);
                }
                if (s1[1].equalsIgnoreCase("disc")) {
                    p = new Disc(id, ECat.DISC, pro1, pro2, price);
                }
                if (!add(p)) {
                    System.out.println("Items " + p.getId() + " existed or has an invalid id!");
                }
            }
        } catch (NumberFormatException ex) {
            System.out.println("Load failed!");
        } finally {
            f.closeReader();
        }

    }

    /**
     * Save content of list to file
     *
     * @param fn : filename
     */
    public void saveToFile(String fn) {
        FileIO f = new FileIO();
        try {
            for (Product p : productList) {
                f.write(p.toString(), fn);
            }
            System.out.println("Save successful!");
        } catch (Exception ex) {
            System.out.println("Save failed");
        } finally {
            f.closeWriter();
        }
    }

    /**
     * Display list
     */
    public void showList() {
        System.out.println("=====* Product List *======");
        for (Product p : productList) {
            System.out.println(p);
        }
    }
}
