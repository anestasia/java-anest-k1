/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Tetrahedron extends ThreeDimensionalShape{
 
    private double edge;

    public Tetrahedron() {
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    
    @Override
    public double calArea() {
        return Math.sqrt(3)*this.edge*this.edge;
    }

    @Override
    public double calPri() {
       return 0;
    }

    @Override
    public double calV() {
        return Math.pow(edge, 3)/(6*Math.sqrt(2));
    }

    @Override
    public void inPut() {
        System.out.println(" Tetrahedron:");
        System.out.println("enter e= ");
        this.edge= new Scanner(System.in).nextDouble();
    }
      
}
