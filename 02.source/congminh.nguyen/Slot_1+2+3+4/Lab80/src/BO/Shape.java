/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author Cong Minh
 */
public interface Shape {
    /**
     * the function to calculate area
     * @return 
     */
    
    public double calAre();
    /**
     * the function to calculate perimeter
     * @return 
     */
    public double calPri();
    
    public void input();
    
    public double volume();
}
