package bo;

public class Candidate {

    private int birthDate;
    private int id;
    private byte canType;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String phone;

    public Candidate() {
    }

    public Candidate(int birthDate, String phone, String email, String firstName, String lastName, String address) {
        this.birthDate = birthDate;
        this.phone = phone;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public short getCanType() {
        return canType;
    }

    public void setCanType(byte canType) {
        this.canType = canType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + " | " + firstName + " " + lastName + " | " + birthDate + " | "
                + address + " | " + phone + " | " + email + " | " + canType;

    }
}