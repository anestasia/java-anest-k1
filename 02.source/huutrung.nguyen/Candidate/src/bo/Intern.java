package bo;

public class Intern extends Candidate {

    private String major;
    private String semester;
    private String university;

    public Intern() {
    }

    public Intern(String major, String semester, String university, int birthDate, String phone, String email, String firstName, String lastName, String address) {
        super(birthDate, phone, email, firstName, lastName, address);
        super.setCanType((byte)2);
        this.major = major;
        this.semester = semester;
        this.university = university;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + major + " | " + semester + " | " + university;
    }
}