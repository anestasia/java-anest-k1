package gui;

import bo.Candidate;
import bo.Experience;
import bo.Fresher;
import bo.Intern;
import java.util.ArrayList;
import java.util.Scanner;

public class Display {

    public static void menu() {
        System.out.println("CANDIDATE MANAGEMENT SYSTEM");
        System.out.println("1. Experience");
        System.out.println("2. Fresher");
        System.out.println("3. Internship");
        System.out.println("4. Searching");
        System.out.println("5. exit.");
    }

    public static void displayCandidate(ArrayList<Candidate> candidate) {
        System.out.println("===========EXPERIENCE CANDIDATE============");
        for (Candidate candi : candidate) {
            if (candi instanceof Experience) {
                System.out.println(candidate.toString());
            }
        }
        System.out.println("==========FRESHER CANDIDATE==============");
        for (Candidate candi : candidate) {
            if (candi instanceof Fresher) {
                System.out.println(candidate.toString());
            }
        }
        System.out.println("===========INTERN CANDIDATE==============");
        for (Candidate candi : candidate) {
            if (candi instanceof Intern) {
                System.out.println(candidate.toString());
            }
        }
    }

    public static boolean pressYN() {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String s;
        while (true) {
            System.out.print("Do you want to continue? (Y-yes, N-no)");
            s = in.nextLine();
            s = s.trim();
            if (s.equalsIgnoreCase("y")) {
                return true;
            }
            if (s.equalsIgnoreCase("n")) {
                return false;
            }
            System.out.println("Invalid choice");
        }
    }
    
    public static int getInt(String mes, String err, int min, int max) {
        String s;
        int number;
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print(mes);
            s = in.nextLine().trim();
            try {
                number = Integer.parseInt(s);
                if (number < min || number > max)
                    System.out.println(err);
                else
                    return number;
            } catch (NumberFormatException nfe) {
                System.out.println(err);
            }
        }
    }
    
    
    public static void searchCandidate(ArrayList<Candidate> candidate) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input Candidate name (First Name or Last Name): ");
        String searchName = sc.nextLine();
        System.out.print("Input type of candidate: ");
        int searchType = Integer.parseInt(sc.nextLine());
        for (Candidate candi : candidate) {
            if ((candi.getCandidateType() == searchType) && (candi.getFirstName().contains(searchName)
                    || candi.getLastName().contains(searchName))) {
                System.out.println(candi.getFirstName() + " "
                        + candi.getLastName() + " | " + candi.getBirthdate() + " | "
                        + candi.getAddress() + " | " + candi.getPhone() + " | " + candi.getEmail() + " | "
                        + candi.getCandidateType());
            }
        }
    }

}
