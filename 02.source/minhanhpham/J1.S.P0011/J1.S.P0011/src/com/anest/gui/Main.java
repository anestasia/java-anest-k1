/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.gui;
import com.anest.bo.ChangBase;
import com.fptu.validator.Validator;
import java.util.Scanner;
/**
 *
 * @author MyPC
 */
public class Main {

    public static void main(String[] args) {

        int baseInput = 0, baseOutput = 0, value = 0, i = 0;
        String svalue, choice;
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Base number:");
            System.out.println("1- Binary");
            System.out.println("2- Decimal");
            System.out.println("3- Hexadecimal");
            while (true) {
                baseInput = Validator.validateInt("Choose the base number input:");
                if (baseInput < 1 || baseInput > 3) {
                    System.out.println("Invalid input! Enter again:");
                } else {
                    break;
                }
            }
            while (true) {
                baseOutput = Validator.validateInt("Choose the base number output:");
                if (baseOutput < 1 || baseOutput > 3 || baseOutput == baseInput) {
                    System.out.println("Invalid input! Enter again:");
                } else {
                    break;
                }
            }
            ChangBase.base(baseInput, baseOutput);

            System.out.println("Do you want to continue? (y / n)");
            while (true) {
                choice = in.nextLine();
                if (choice.equalsIgnoreCase("y") == false && choice.equalsIgnoreCase("n") == false) {
                    System.out.println("Invalid input! Enter again:");
                } else if (choice.equalsIgnoreCase("n")) {
                    return;
                } else if (choice.equalsIgnoreCase("y")) {
                    break;
                }
            }
        }

    }
}
