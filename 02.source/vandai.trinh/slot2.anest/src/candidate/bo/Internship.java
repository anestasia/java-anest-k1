package candidate.bo;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Trinh Dai
 */
public class Internship extends Deverloper {

    private String majors;
    private String Semester;
    private String universityName;

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        super.input();
        this.setCandidateTpye(3);

        System.out.print("Enter majors: ");
        this.majors = in.nextLine().trim();
        System.out.print("Enter Semester: ");
        this.Semester = in.nextLine().trim();
        System.out.print("Enter university name: ");
        this.universityName = in.nextLine().trim();
    }

    @Override
    public void showInfor() {
        super.showInfor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showName() {
        super.showName(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return Semester;
    }

    public void setSemester(String Semester) {
        this.Semester = Semester;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

}
