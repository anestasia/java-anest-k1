package bo;


import utir.Validate;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Cube extends ThreeDim {

    private double edge;

    public Cube() {
        edge = 0;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    @Override
    public double getArea() {
        return edge * edge * 6;
    }

    @Override
    public void input() {
        Validate validate = new Validate();
        setEdge(validate.getDouble("Enter the edge: ", "Please enter a float number!!!"));
        ;
    }

    @Override
    public double getVolumn() {
        return edge * edge * edge;
    }

    public double getPerimeter() {
        return 0;
    }
}
