/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package covertbase;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Input {

    static String base36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public Input() {
    }
    
    public static int charToNum(char c) {
        String base36b = base36.toLowerCase();
        for (int i = 0; i < 36; i++) {
            if (c == base36.charAt(i) || c == base36b.charAt(i)) {
                return i;
            }
        }
        return -1;
    }

    public static String inputNumber(int baseint) {
        String num;
        int k = 0;
        do {
            k = 1;
            System.out.print("Input number: ");
            num = new Scanner(System.in).nextLine();
            for (int i = 0; i < num.length(); i++) {
                if (charToNum(num.charAt(i)) >= baseint || charToNum(num.charAt(i)) == -1) {
                    k = 0;
                }
                if (k == 0) {
                    System.out.println("Wrong number in base");
                    break;
                }
            }
        } while (k == 0);
        return num;
    }
}
