package com.anest.util;

import java.util.Scanner;

/**
 * Created by Minh on 1/14/2017.
 */
public class Input {
    Scanner in = new Scanner(System.in);

    public double getDouble(String mes, String err, double min, double max) {
        while (true) {
            System.out.print(mes);
            String s = in.nextLine().trim();
            try {
                double num = Double.parseDouble(s);
                if (num > min && num < max)
                    return num;
                else
                    System.out.println(err);
            } catch (NumberFormatException nfe) {
                System.out.println(err);
            }
        }
    }

    public int getInt(String mes, String err, int min, int max) {
        while (true) {
            System.out.print(mes);
            String s = in.nextLine().trim();
            try {
                int num = Integer.parseInt(s);
                if (num > min && num < max)
                    return num;
                else
                    System.out.println(err);
            } catch (NumberFormatException nfe) {
                System.out.println(err);
            }
        }
    }
}
