/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.entity;

/**
 *
 * @author lean1
 */
public class Fresher extends Candidate{
    private String Graduation_date;
    private String Graduation_rank;//(Excellence, Good, Fair, Poor)
    private String Education;

    public Fresher() {
    }

    public Fresher(String id, String ltName, String ftName, int birthday, String address, String phone, String email, int type, String graduation_date, String graduation_rank, String education) {
        super(id, ltName, ftName, birthday, address, phone, email, type);
        Graduation_date = graduation_date;
        Graduation_rank = graduation_rank;
        Education = education;
    }

    public String getGraduation_date() {
        return Graduation_date;
    }

    public void setGraduation_date(String graduation_date) {
        Graduation_date = graduation_date;
    }

    public String getGraduation_rank() {
        return Graduation_rank;
    }

    public void setGraduation_rank(String graduation_rank) {
        Graduation_rank = graduation_rank;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }
}

