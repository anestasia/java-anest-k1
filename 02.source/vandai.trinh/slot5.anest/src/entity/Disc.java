/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Disc extends Product implements Serializable{
    private String genre;
    private String format;

    public Disc() {
    }

    public Disc(String category, int id, String price, String genre, String format) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    public Disc(String genre, String format) {
        this.genre = genre;
        this.format = format;
    }

    @Override
    public String toString() {
        return super.toString()+" | "+ genre +" | "+ format;
    }

    @Override
    public void input() {
        super.input();
        setCategory("Disc");
        System.out.print("Enter the genre: ");
        this.genre = new Scanner(System.in).nextLine().trim();
        System.out.print("Enter the format: ");
        this.format = new Scanner(System.in).nextLine().trim();
    }

    
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
    
    
}
