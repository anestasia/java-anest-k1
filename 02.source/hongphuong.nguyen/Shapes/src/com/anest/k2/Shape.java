
package com.anest.k2;

/**
 *
 * @author Hong Phuong
 */
public interface Shape {
    public double chuVi();
    public double dienTich();
    public void input();
}
