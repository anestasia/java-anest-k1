/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import static com.anest.bo.Valid.InputString;
import static com.anest.bo.Valid.getIntNumber;
import com.anest.entity.Candidate;
import com.anest.entity.Experience;
import com.anest.entity.Fresher;
import com.anest.entity.Intern;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class CandidateList {

    private List<Candidate> candidates;

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public CandidateList(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public CandidateList() {
        this.candidates = new ArrayList<>();
    }

    public void add(Candidate candidate) {
        this.candidates.add(candidate);
    }

    public void deleteCandidate() throws IOException {
        Scanner in = new Scanner(System.in);
        int flag = 0;
        System.out.println("Enter ID to delete");
        String id = in.nextLine();
        for (Candidate candidate : candidates) {
            if (id.equalsIgnoreCase(candidate.getCandidate_ID())) {
                candidates.remove(candidate);
                System.out.println("Delete successfully!");
                flag = 1;
            }
        }
        if (flag == 0) {
            System.out.println("No valid ID!");
        }
    }

    public void showList() {
        System.out.println("List of candidate:");
        if (this.candidates.isEmpty()) {
            System.out.println("Empty!");
        } else {
            for (Candidate candidate : this.candidates) {
                System.out.println(candidate.toString());
            }
        }
    }

    public void searchName(String name) {
        if (this.candidates.isEmpty()) {
            System.out.println("Empty!");
        } else {
            for (Candidate candidate : this.candidates) {
                if (candidate.getFirst_Name().contains(name) || candidate.getLast_Name().contains(name)) {
                    System.out.println(candidate.toString());
                }
            }
        }
    }

    public void searchType() throws IOException {
        Valid t = new Valid();
        String str;
        int type, flag = 0;
        while (true) {
            str = t.normalizeString(InputString("Enter name to search: "));
            if (!t.stringChecker(str)) {
                System.out.println("Invalid input!");
            } else {
                break;
            }
        }
        type = getIntNumber("Enter type: ", 3, 1);

        str = str.toLowerCase();
        for (Candidate candidate : this.candidates) {
            if (candidate.getType() == type) {
                if (candidate.getFirst_Name().toLowerCase().contains(str) || candidate.getLast_Name().toLowerCase().contains(str)) {
                    System.out.println(candidate.toString());
                    flag = 1;
                }
            }
        }
        if (flag == 0) {
            System.out.println("No valid name!");
        }
    }

    public void display(int type) {
        for (Candidate candidate : this.candidates) {
            if (candidate.getType() == type) {
                System.out.println(candidate.getCandidate_ID() + " - " + candidate.getFirst_Name() + " " + candidate.getLast_Name());
            }
        }
    }

    public void display1() {
        for (Candidate candidate : this.candidates) {
            System.out.println(candidate.getCandidate_ID() + " - " + candidate.getFirst_Name() + " " + candidate.getLast_Name());
        }
    }

}
