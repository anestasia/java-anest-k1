/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;

/**
 *
 * @author Cpt
 */
public class Sphere extends ThreeDimensionalShape {

    private double r;

    public Sphere() {
    }

    public Sphere(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calPri() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        return 4*Math.PI*this.r*this.r;
    }

    @Override
    public double calArea() {
        return 4 * Math.PI * this.r * this.r;
    }

    @Override
    public double volume() {
        return 4 / 3 * Math.PI * Math.pow(this.r, 3);
    }

    @Override
    public void input() {
        this.r = ValidDouble.getDoubleNumber("Enter sphere r: ",Double.MAX_VALUE,0);
    }

    @Override
    public String toString() {
        return "Sphere's volume: " + volume() + " Sphere's Area: " + calArea() +" Sphere's doesn't has Pri";
    }
    
    
}
