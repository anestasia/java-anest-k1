/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;

/**
 *
 * @author Cpt
 */
public class Cube extends ThreeDimensionalShape{
    private double a;

    public Cube() {
    }

    public Cube(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    @Override
    public double calPri() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 
    public double calAreaSide() {
        return this.a*this.a*4;
    }
    
    public double  calAreaAll(){
        return this.a*this.a*6;
    }
    
    public double calAreaTopAndBot(){
        return this.a*this.a;
    }

    @Override
    public double volume() {
        return Math.pow(this.a, 3);
    }

    @Override
    public void input() {
        this.a = ValidDouble.getDoubleNumber("Enter Cube a: ",Double.MAX_VALUE,0);
    }

    @Override
    public double calArea() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Cube's volume " + volume() + " Cube's Area bot and top: " 
                + calAreaTopAndBot() + " Cube's Area Side: " + calAreaSide() 
                + " Cube's Area all:  " + calAreaAll();
    }
    
    
    
}
