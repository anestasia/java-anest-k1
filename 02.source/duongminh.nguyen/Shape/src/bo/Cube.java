package bo;

import common.Common;
import java.util.Scanner;

public class Cube extends ThreeDimensionalShape {

    private float edge;
    private float area;
    private float volume;

    public Cube() {
    }

    public Cube(float edge, float area, float volume) {
        this.edge = edge;
        this.area = area;
        this.volume = volume;
    }

    public float getEdge() {
        return edge;
    }

    public void setEdge(float edge) {
        this.edge = edge;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public void input() {
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("Enter the edge for your cube: ");
            setEdge(in.nextFloat());
        } while (!Common.isNumber(in.nextLine()) || Common.isPositveNumber(in.nextFloat()));
    }

    @Override
    public float calArea() {
        return (6 * getEdge() * getEdge());
    }

    @Override
    public float calVolume() {
        return (getEdge() * getEdge() * getEdge());
    }

    @Override
    public void output() {
        System.out.println("Your cube has volume is: " + calVolume() + "\n");
        System.out.println("Your cube has area is:" + calArea());
    }

}
