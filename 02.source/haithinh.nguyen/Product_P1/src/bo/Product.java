/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Product implements Serializable, Comparable<Product>{

    /**
     * @param args the command line arguments
     */
    private static final long lll = 1L;
    private int id;
    private String category;
    private int price;

    public Product() {
    }

    public Product(int id, String category, int price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setInput() {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter ID :");
        this.id= s.nextInt();
    }

    @Override
    public String toString() {
        return  id + " | " + category ;
    }

    @Override
    public int compareTo(Product o) {
        return this.price;
    }

    
}
