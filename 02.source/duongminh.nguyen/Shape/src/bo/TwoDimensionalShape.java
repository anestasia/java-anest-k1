
package bo;

public abstract class TwoDimensionalShape implements Shape{
   public abstract float calVerimeter();
   public abstract float calArea();

    @Override
    public abstract void input();

    @Override
    public abstract void output();
}