package bo;

/**
 * Created by W10-PRO on 13-Jan-17.
 */
public interface Shape {
    double getPerimeter();

    double getArea();

    double getVolumn();

    void input();
}
