/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.threeDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import bo.Utils;

public class Sphere extends ThreeDimensionalShape {

    private double radius;

    public Sphere() {
        super();
        this.radius = 0;
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#calArea()
     */
    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#input()
     */
    @Override
    public void input() {
        // TODO Auto-generated method stub
        this.radius = Utils.getDoubleNumber("Enter radius : ", Double.MAX_VALUE, 0);
    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#output()
     */
    @Override
    public void output() {
        // TODO Auto-generated method stub
        System.out.println(" Sphere : \t Radius : (" + this.radius + ")");
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Vol  :" + this.calVol());

    }

    /* (non-Javadoc)
	 * @see com.anest.oop.k1.entity.threeDimensional.ThreeDimensionalShape#volume()
     */
    @Override
    public double calVol() {
        // TODO Auto-generated method stub
        return 4 / 3 * Math.PI * Math.pow(this.radius, 3);
    }
}
