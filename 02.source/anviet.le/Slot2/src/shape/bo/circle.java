/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.bo;



import com.edu.Validate.Validator;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class circle extends TwodementionalShape{
    private double r;

    public circle() {
    }

    public circle(double r) {
        this.r = r;
    }


    
    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    @Override
    public double calArea() {
        return this.r*this.r*Math.PI;
    }

    @Override
    public double calPri() {
       return 2*this.r*Math.PI;
    }

    @Override
    public void input() {
         System.out.print("Enter e=");
        this.r=Validator.validateDouble();
    }
    
}
