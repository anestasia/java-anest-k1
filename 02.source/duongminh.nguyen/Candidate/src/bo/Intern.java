package bo;

import common.Common;
import java.util.Scanner;

public class Intern extends Candidate {

    private String majors;
    private String semester;
    private String universityName;

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public Intern() {
    }

    public Intern(String majors, String semester, String universityName) {
        this.majors = majors;
        this.semester = semester;
        this.universityName = universityName;
    }

    public Intern(String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
    }

    public Intern(String majors, String semester, String universityName, String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
        this.majors = majors;
        this.semester = semester;
        this.universityName = universityName;
    }

    @Override
    public Candidate input(Candidate intern) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Intern Candidates ID: ");
        String candidateID = sc.nextLine();
        while (!Common.isNumber(candidateID)) {
            System.out.print("Please Re-enter!: ");
            candidateID = sc.nextLine();
        }
        System.out.print("Enter Intern Candidates First Name: ");
        String FirstName = sc.nextLine();
        System.out.print("Enter Intern Candidates Last Name: ");
        String LastName = sc.nextLine();
        System.out.print("Enter Intern Candidates Birthdate :");
        String BirthDate = sc.nextLine();
        while (!Common.isBirthdate(BirthDate)) {
            System.out.print("Please Re-enter!: ");
            BirthDate = sc.nextLine();
        }
        System.out.print("Enter Intern Candidates Adress: ");
        String Address = sc.nextLine();
        System.out.print("Enter Intern Candidates Phone: ");
        String Phone = sc.nextLine();
        while (!Common.isPhone(Phone)) {
            System.out.print("Please Re-enter!: ");
            BirthDate = sc.nextLine();
        }
        System.out.print("Enter Intern Candidates Email: ");
        String Email = sc.nextLine();
        while (!Common.isValidEmailAddress(Email)) {
            System.out.print("Please Re-enter!: ");
            Email = sc.nextLine();
        }
        System.out.print("Enter Intern Candidates Majors: ");
        String Majors = sc.nextLine();
        System.out.print("Enter Intern Candidates Graduated Semester: ");
        String Semester = sc.nextLine();
        System.out.print("Enter Intern Candidates University School: ");
        String UniversityName = sc.nextLine();
        intern = new Intern(Majors, Semester, UniversityName, candidateID, FirstName, LastName, BirthDate, Address, Phone, Email, 2);
        return intern;
    }
}
