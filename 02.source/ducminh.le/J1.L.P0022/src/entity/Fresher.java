package entity;

import util.Validate;

import java.util.Scanner;

/**
 * Created by Minh on 1/14/2017.
 */
public class Fresher extends Candidate {
    private int graduationDate;
    private String graduationRank, education;

    public Fresher() {
    }

    @Override
    public void input() {
        super.input();
        Scanner in = new Scanner(System.in);
        Validate validate = new Validate();
        setCandidateType(1);
        do {
            this.graduationDate = validate.getInt("Enter Graduation date: ", "Invalid year", 0, Integer.MAX_VALUE);
        } while (!validate.isYear(1900, this.graduationDate));
        do {
            System.out.println("Graduation rank: Excellence | Good | Fair | Poor");
            System.out.print("Enter Rank of graduation: ");
            this.graduationRank = in.nextLine();
        } while (!validate.isRankOfGraduation(this.graduationRank));
        System.out.print("Enter University name: ");
        this.education = in.nextLine();
    }

    public int getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(int graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getGraduationRank() {
        return graduationRank;
    }

    public void setGraduationRank(String graduationRank) {
        this.graduationRank = graduationRank;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
