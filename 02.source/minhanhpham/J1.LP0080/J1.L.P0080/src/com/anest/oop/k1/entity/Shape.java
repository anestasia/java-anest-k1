/**
 * 
 */
package com.anest.oop.k1.entity;

/**
 * @author admin A shape of universal
 */
public interface Shape {
	/**
	 * The function to calculate primater of the shape
	 * 
	 * @return area of the shape
	 */
	public double calArea();

	/**
	 * The function to calculate primater of the shape
	 * 
	 * @return perimater of shape
	 */
	public double calPri();

	/**
	 * The function to input properties of entity from the keyboard
	 */
	public void input();

	/**
	 * The function to show the perimater and area of shape.
	 */
	public void output();
}
