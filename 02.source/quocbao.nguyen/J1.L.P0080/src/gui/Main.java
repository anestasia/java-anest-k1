package gui;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Main {
    public static void main(String[] args) {
        Calculator cal = new Calculator();
        Menu Menu = new Menu(7);
        Menu.add("Circle");
        Menu.add("Square");
        Menu.add("Triangle");
        Menu.add("Sphere");
        Menu.add("Cube");
        Menu.add("Tetrahedron");
        Menu.add("Quit");
        int choice;
        double number = 0;
        do {
            System.out.println("\nCALCULATOR SHAPE PROGRAMMER");
            choice = Menu.getChoice();
            switch (choice) {
                case 1:
                    cal.calCircle();
                    break;
                case 2:
                    cal.calSquare();
                    break;
                case 3:
                    cal.calTriangle();
                    break;
                case 4:
                    cal.calSphere();
                    break;
                case 5:
                    cal.calCube();
                    break;
                case 6:
                    cal.calTetra();
                    break;
                case 7:
                    System.out.println("Exit!");
                    break;
                default:
                    break;
            }
        } while (choice != 7);

    }
}
