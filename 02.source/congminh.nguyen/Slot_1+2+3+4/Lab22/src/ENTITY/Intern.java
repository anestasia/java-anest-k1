/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

/**
 *
 * @author Cong Minh
 */
public class Intern extends Candidate{
    private String major;
    private String semster;
    private String universityName;

    public Intern() {
    }
    
       public Intern(String major, String semster, String universityName,
            String id, String firstName,String lastName,
            int birthDate,String address, String phone, String email,
            Type type) {
        super(id, firstName, lastName, birthDate, address, phone, email, type);
        this.major = major;
        this.semster = semster;
        this.universityName = universityName;
       }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSemster() {
        return semster;
    }

    public void setSemster(String semster) {
        this.semster = semster;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
    
    
    
}
