/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;
import java.util.Scanner;

/**
 *
 * @author Cpt
 */
public class Square extends TwoDimensionalShape{
    
    private double edge;

    public Square() {
    }

    public Square(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }
    
    
    @Override
    public double calArea() {
        return this.edge*this.edge;
    }

    @Override
    public double calPri() {
        return this.edge*4;
    }

    @Override
    public void input() {
        this.edge = ValidDouble.getDoubleNumber("Enter square e",Double.MAX_VALUE,0);
    }

    @Override
    public String toString() {
        return "Square's pri " + calPri() + " Square's Area: " + calArea();
    }
    
    
    
    
}
