/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.twoDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import entity.Shape;

public abstract class TwoDimensionalShape implements Shape {

    @Override
    public abstract double calArea();

    public abstract double calPri();

    @Override
    public abstract void input();

}
