/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

//import static BO.Input.inputInt;
import static BO.Input.*;
import BO.Search;
import BO.Validator;
import ENTITY.Candidate;
import ENTITY.Experience;
import ENTITY.Fresher;
import ENTITY.Intern;
import java.io.IOException;
import java.util.ArrayList;
import ENTITY.Type;
/**
 *
 * @author Cong Minh
 */
public class Main {

    public static void main(String[] args) throws IOException {

        ArrayList<Candidate> candidate = new ArrayList();
        
        System.out.println("1. Experience.");
        System.out.println("2. Fresher.");
        System.out.println("3. Internship.");
        System.out.println("4. Searching.");
        System.out.println("5. Exit.");
        int choice;
        while (true) {
            System.out.println("---------");
            choice = inputInt("Enter your choice: ", 5, 1);
            switch (choice) {
                case 1:
                    do {
                        candidate.add( creat(0, candidate));                 
                    } while (toBeContinue().equalsIgnoreCase("Y"));
                    break;
                case 2:
                    do {
                        candidate.add( creat(1,candidate));                    
                    } while (toBeContinue().equalsIgnoreCase("Y"));
                    break;
                case 3:
                    do {
                        candidate.add(creat(2,candidate));
                    } while (toBeContinue().equalsIgnoreCase("Y"));
                    break;
                case 4:
                    Search va = new Search();
                    Validator t = new Validator();
                    String name;
                    int type;
                    System.out.println("------EXPERIENCE-------");
                    va.displayAll(candidate,Type.Experience);
                    System.out.println("");
                    System.out.println("--------FRESHER--------");
                    va.displayAll(candidate,Type.Fresher);
                    System.out.println("");
                    System.out.println("--------INTERN--------");
                    va.displayAll(candidate,Type.Intern);
                    do {
                        name = InputString("Enter name to search: ");
                        if (t.stringChecker(name) == false) {
                            System.out.println("Invalid input!");
                        }
                    } while (t.stringChecker(name) == false);
//                    System.out.println(name);
                    type = inputInt("Enter type: ", 2, 0);
                    switch(type){
                        case 0:
                            va.display(va.search(candidate, name));
                            break;
                        case 1: 
                            va.display(va.search(candidate, name));
                            break;
                        case 2: 
                            va.display(va.search(candidate, name));
                            break;
                            
                    }

            }
        }

    }
}
