
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OPF_Lep
 */
public class Menu {

    ArrayList<String> menu = new ArrayList();

    public void addOption() {
        menu.add("1. Convert any base to base 10");
        menu.add("2. Convert base 10 to any base");
        menu.add("3. Exit");

    }

    public void showMenu() {

        System.out.println("==== Converting program ===");
        for (int i = 0; i < menu.size(); i++) {
            System.out.println(menu.get(i));
        }
    }

    public int getChoice(int i, int j, String input, String error) {
        Scanner sc = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.print(input);
            try {
                choice = Integer.parseInt(sc.nextLine());
                if (choice > i && choice < j) {
                    return choice;
                }
            } catch (Exception e) {
                System.out.println(error);
            }
        }
    }

    public void run() {
        int k = 0;
        int base;
        int choice = 0;
        int num10 = 0;
        String num;
        addOption();
        do {
            k = 1;
            showMenu();
            choice = getChoice(0, 4, "Input your choice: ", "Error");
            switch (choice) {
                case 1:
                    base = getChoice(0, 37, "Input base to covert to base 10 (1-36):", "Invalid.Reinput");
                    num = OthersToTen.inputNumber(base);
                    System.out.println("Result: " + OthersToTen.convert(num, base));
                    break;
                case 2:
                    int x = 1;
                    base = getChoice(0, 37, "Input base to covert from base 10 (1-36):", "Invalid.Reinput");
                    do {
                        System.out.print("Input base 10 number: ");
                        try {
                            x = 1;
                            num10 = Integer.parseInt(new Scanner(System.in).nextLine());
                        } catch (Exception e) {
                            x = 0;
                            System.out.println("Invalid base 10.");
                        }
                    } while (x == 0);
                    System.out.println("Result: " + TenToOthers.convert(num10, base).toString());
                    break;
                case 3:
                    System.out.println("Good bye");
                    k = 0;
            }
        } while (k == 1);
    }
}
