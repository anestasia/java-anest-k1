package entity;

import util.Validate;

import java.util.Scanner;

/**
 * Created by Minh on 1/14/2017.
 */
public class Experience extends Candidate {
    private int expInYear;
    private String proSkill;

    public Experience() {
    }

    @Override
    public void input() {
        super.input();
        setCandidateType(0);
        Scanner in = new Scanner(System.in);
        Validate validate = new Validate();
        this.expInYear = validate.getInt("Enter Year of experience: ", "Invalid year", 1, 100);
        System.out.print("Enter Professional kill: ");
        this.proSkill = in.nextLine();
    }

    public void test() {

    }

    public int getExpInYear() {
        return expInYear;
    }

    public void setExpInYear(int expInYear) {
        this.expInYear = expInYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }
}
