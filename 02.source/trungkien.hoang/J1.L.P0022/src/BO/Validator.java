/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Calendar;

/**
 *
 * @author KienKeng
 */
public class Validator {
    
    public boolean checkNumber(String a) {
        while (true) {
            try {
                int number = Integer.parseInt(a);
                return true;
            } catch (Exception e) {
               // System.out.println("Invalid input!Please Try Again");
                return false;
            }
        }

    }
    public boolean checkDOB(int DOB)
    {
        if( DOB > 1900 && DOB < Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR)))
        {
            return true;
        }
        else
        {
            System.out.println("Invalid Input");
            return false;
        }
            
    }
    
    public boolean checkPhone(String number)
    {
        if(number.length() >= 10)
        {
            return true;
        }
        else
        {
            System.out.println("Invalid Input! Try Again!");
             return false;
        }
        
    }
    
    public boolean checkMail(String email)
    {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
           java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
           java.util.regex.Matcher m = p.matcher(email);
           if(m.matches())
           {
               return true;
           }
           else
           {
               System.out.println("Invalid Input! Try Again!");
                return false;
           }
              
    }
    public boolean checkExpYear(int year)
    {
        if( 0 < year && year <=100)
        {
            return true;
        }
        else
        {
            System.out.println("Invalid Input! Try Again!");
            return false;
        }
            
    }
    public boolean checkRank(String a)
    {
         
        if(a.equals(Rank.excellence.toString()) || a.equals(Rank.fair.toString()) || a.equals(Rank.good.toString()) || a.equals(Rank.poor.toString()))
        {
            
            return true;
        }
        else
        {
            System.out.println("Invalid Input! Try Again!");
            return false;
        }
            
       
    }
    
     public boolean checkCanType(String a)
    {
         
        if(a.equals(CanType.Experience.toString()) || a.equals(CanType.Fresher.toString()) || a.equals(CanType.Intern.toString()))
        {
            
            return true;
        }
        else
        {
            System.out.println("Invalid Input! Try Again!");
            return false;
        }
            
       
    }

   
   

}
