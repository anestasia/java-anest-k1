package shape.gui;

import java.util.Scanner;
import shape.bo.Cube;
import shape.bo.Shapes;
import shape.bo.Sphere;
import shape.bo.Square;
import shape.bo.Tetrahedron;
import shape.bo.ThreeDimensionalShapes;

import shape.bo.Triangle;
import shape.bo.circle;

/**
 *
 * @author lean1
 */
public class test {

    public static void main(String[] args) {
        Shapes shape;
        
        int choice = 0;
        do {
            System.out.println("Enter your choice:");
            System.out.println("1:Circle\n2:Triangle\n3:Square\n4:Sphere\n5:Cube\n6:Tetrahdron\n");
            choice = new Scanner(System.in).nextInt();
            switch (choice) {
                case 0:
                    break;
                case 1: {
                    shape = new circle();
                    shape.input();
                    System.out.println("Area of circle:"+shape.calArea());
                    System.out.println("pri of circle"+shape.calPri());

                    break;
                }
                case 2: {
                   
                    shape = new Triangle();
                     double k;
                    shape.input();
                    k=((Triangle)(shape)).checkTriangle();
                    if(k==0){
                        System.out.println("It's not a triangle:");
                    }
                    else if(k==1){
                    System.out.println("Area of triangle:"+shape.calArea());
                    System.out.println("pri of Triangle"+shape.calPri());
                    }
                    break;
                }
                case 3: {
                    shape = new Square();
                    shape.input();
                    System.out.println("Area of Square:"+shape.calArea());
                    System.out.println("Pri of Square:"+shape.calPri());
                    break;
                }
                case 4: {
                    shape = new Sphere();
                    shape.input();
                    System.out.println("Area of Sphere:"+shape.calArea());
                    System.out.println("volume of sphere"+((ThreeDimensionalShapes)shape).volume());
                    break;
                }
                case 5: {
                    shape = new Cube();
                    shape.input();
                    System.out.println("Area of cube:"+shape.calArea());
                    System.out.println("volume of cube:"+((ThreeDimensionalShapes)shape).volume());
                    break;
                }
                case 6: {
                    shape = new Tetrahedron();
                    shape.input();
                    System.out.println("Area of tetrahedron:"+shape.calArea());
                    System.out.println("Volume of tetrahedron:"+((ThreeDimensionalShapes)shape).volume());

                    break;
                }

            }

        } while (choice != 0);
    }
}
