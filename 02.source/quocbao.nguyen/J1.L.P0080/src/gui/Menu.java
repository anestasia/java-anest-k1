package gui;

/**
 * Created by W10-PRO on 14-Jan-17.
 */

import utir.Validate;

public class Menu {

    String[] hints;
    int n = 0; //current number of hints
    //create a Menu with size elements

    public Menu(int size) {
        if (size < 1) {
            size = 10;
        }
        hints = new String[size];
    }

    // add a hint
    public void add(String aHint) {
        if (n < hints.length) {
            hints[n++] = aHint;
        }
    }
    // get user choice

    public int getChoice() {
        Validate Validate = new Validate();
        int result = 0;
        if (n > 0) {//print out hints
            for (int i = 0; i < n; i++) {
                System.out.println((i + 1) + "-" + hints[i]);
            }
            result = Validate.getInt("Please select an operation: ", 1, 7);
        }
        return result;
    }
}
