/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;

import java.util.Scanner;
import shape.bo.TwoDimensionShape;

/**
 *
 * @author Eo Mo
 */
public class Square extends TwoDimensionShape {

    private double h;

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public double calArea() {
        return this.h * this.h;
    }

    @Override
    public double calPri() {
        return this.h * 4;
    }

    @Override
    public void input() {
        System.out.print("Nhap vao h: " + this.h);
        this.h = new Scanner(System.in).nextDouble();
    }

}
