
package com.anest;


public abstract class TwoDimensionalShape implements Shape {

    @Override
    public abstract double calArea() ;

    @Override
    public abstract double calPri();

    @Override
    public abstract void input() ;
        
    
    
}
