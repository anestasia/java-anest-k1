/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k2;

import java.util.Scanner;

/**
 *
 * @author Hong Phuong
 */
public class Triangle extends TwoDimensionalShape {
    private double e1;
    private double e2;
    private double e3;

    public double getE1() {
        return e1;
    }

    public void setE1(double e1) {
        this.e1 = e1;
    }

    public double getE2() {
        return e2;
    }

    public void setE2(double e2) {
        this.e2 = e2;
    }

    public double getE3() {
        return e3;
    }

    public void setE3(double e3) {
        this.e3 = e3;
    }
    
    
    
    
    
    public Triangle () {
        
    }
    
    public Triangle (double e1, double e2, double e3) {
        this.e1 = e1;
        this.e2 = e2;
        this.e3 = e3;
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        System.out.println("e1 = ");
        this.e1 = s.nextDouble();
        System.out.println("e2 = ");
        
        this.e2 = s.nextDouble();
        
        System.out.println("e3 = ");
        this.e3 = s.nextDouble();
    }

    @Override
    public double chuVi() {
        return this.e1 + this.e2 + this.e3;
    }

    @Override
    public double dienTich() {
        return -1;
    }
}
