/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Cong Minh
 */
public class Validator {
    public static int inputInt(String mes){
       Scanner sc = new Scanner(System.in);
       int input;
        while(true){
            try{
                System.out.print(mes);
                input = sc.nextInt();
                return input;
            }catch(Exception e){
                System.out.println("Invalid input.");
                sc.next();
            }
        }
    }
    
    public static double inputDouble(String mes, double min){
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(mes);
        double input = 0;
        boolean flag = false;
        do {
            try {
                input = Double.parseDouble(br.readLine());
                if (input < min ) {
                    System.out.print("Re-Enter: ");
                    flag=false;
                } else flag=true;
            } catch (Exception e) {
                System.out.print("Re-Enter: ");
            }
        } while (flag==false);
        return input;
    }
}
