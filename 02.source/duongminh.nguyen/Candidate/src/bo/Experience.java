package bo;

import common.Common;
import java.util.Scanner;

public class Experience extends Candidate {

    private String expInYear;
    private String proSkill;

    public String getExpInYear() {
        return expInYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public Experience() {
    }

    public Experience(String expInYear, String proSkill) {
        this.expInYear = expInYear;
        this.proSkill = proSkill;
    }

    public Experience(String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
    }

    public Experience(String expInYear, String proSkill, String candidateID, String firstName, String lastName, String birthdate, String address, String phone, String email, int candidateType) {
        super(candidateID, firstName, lastName, birthdate, address, phone, email, candidateType);
        this.expInYear = expInYear;
        this.proSkill = proSkill;
    }

    @Override
    public Candidate input(Candidate exp) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Experience Candidates ID: ");
        String candidateID = sc.nextLine();
        while (!Common.isNumber(candidateID)) {
            System.out.print("Please Re-enter!: ");
            candidateID = sc.nextLine();
        }
        System.out.print("Enter Experience Candidates First Name: ");
        String FirstName = sc.nextLine();
        System.out.print("Enter Experience Candidates Last Name: ");
        String LastName = sc.nextLine();
        System.out.print("Enter Experience Candidates Birthdate: ");
        String BirthDate = sc.nextLine();
        while (!Common.isBirthdate(BirthDate)) {
            System.out.print("Please Re-enter!: ");
            BirthDate = sc.nextLine();
        }
        System.out.print("Enter Experience Candidates Adress: ");
        String Address = sc.nextLine();
        System.out.print("Enter Experience Candidates Phone: ");
        String Phone = sc.nextLine();
        while (!Common.isPhone(Phone)) {
            System.out.print("Please Re-enter!: ");
            Phone = sc.nextLine();
        }
        System.out.print("Enter Experience Candidates Email: ");
        String Email = sc.nextLine();
        while (!Common.isValidEmailAddress(Email)) {
            System.out.print("Please Re-enter!: ");
            Email = sc.nextLine();
        }
        System.out.print("Enter Experience Candidates Year of Experience: ");
        String ExpInYear = sc.nextLine();
        while (!Common.isYear(ExpInYear)) {
            System.out.print("Please Re-enter!: ");
            ExpInYear = sc.nextLine();
        }
        System.out.print("Enter Experience Candidates Professional Skill: ");
        String ProSkill = sc.nextLine();
        exp = new Experience(ExpInYear, ProSkill, candidateID, FirstName, LastName, BirthDate, Address, Phone, Email, 0);
        return exp;
    }

}
