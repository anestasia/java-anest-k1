package gui;

import bo.*;

import java.util.Scanner;

/**
 * Created by W10-PRO on 16-Jan-17.
 */
public class Calculator {
    Scanner sc = new Scanner(System.in);

    public void calCircle() {
        Shape circle = new Circle();
        System.out.println("CUBE");
        circle.input();
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());
    }

    public void calCube() {
        Shape cube = new Cube();
        System.out.println("CUBE");
        cube.input();
        System.out.println("Area: " + cube.getArea());
        System.out.println("Volumn: " + cube.getVolumn());
    }

    public void calSphere() {
        Shape sphere = new Sphere();
        System.out.println("SPHERE");
        sphere.input();

        System.out.println("Area: " + sphere.getArea());
        System.out.println("Volumn: " + sphere.getVolumn());
    }

    public void calSquare() {
        Shape square = new Square();
        System.out.println("SQUARE");
        square.input();
        System.out.println("Perimeter: " + square.getPerimeter());
        System.out.println("Area: " + square.getArea());
    }

    public void calTetra() {
        Shape tetra = new Tetrahedron();
        System.out.println("TETRAHEDRON");
        tetra.input();
        System.out.println("Area: " + tetra.getArea());
        System.out.println("Volumn: " + tetra.getVolumn());
    }

    public void calTriangle() {
        System.out.println("TRIANGLE");
        Shape triangle = new Triangle();
        triangle.input();
        System.out.println("Perimeter: " + triangle.getPerimeter());
        System.out.println("Area: " + triangle.getArea());
    }
}
