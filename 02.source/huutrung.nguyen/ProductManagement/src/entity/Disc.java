/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Disc extends Product {

    private String genre;
    private String format;

    public Disc() {
    }

    public Disc(String id, ECat category,String genre, String format, int price) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String toString() {
        return super.getId() + " | " + super.getCategory() + " | " + 
                genre + " | " + format + " | " + super.getPrice();
    }

    @Override
    public void input() {
        super.input();
        super.setCategory(ECat.DISC);
        System.out.print("Genre: ");
        genre = new Scanner(System.in).nextLine();
        System.out.print("Format: ");
        format = new Scanner(System.in).nextLine();
    }
    
    

}
