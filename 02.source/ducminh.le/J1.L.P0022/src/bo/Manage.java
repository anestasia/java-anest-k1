package bo;

import entity.Candidate;
import entity.Experience;
import entity.Fresher;
import entity.Intern;
import util.Validate;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Minh on 2/12/2017.
 */
public class Manage {
    public void searchCandidate(ArrayList<Candidate> candidates) {
        Validate validate = new Validate();
        Scanner in = new Scanner(System.in);
        System.out.println();
        System.out.print("Enter First or Last name: ");
        String name = in.nextLine().trim();
        int type = validate.getInt("Enter Candidate type: ", "Invalid type", 0, 2);
        for (Candidate candidate : candidates)
            if ((candidate.getFirstName().contains(name) || candidate.getLastName().contains(name))
                    && candidate.getCandidateType() == type)
                candidate.toString();
        System.out.println();
    }
}
