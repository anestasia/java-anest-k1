package entity;

import util.Validate;

import java.util.Scanner;

/**
 * Created by Minh on 1/14/2017.
 */
public class Candidate {
    private int candidateType, birthDate, candidateId;
    private String firstName, lastName, address, phone, email;

    public Candidate() {
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public void input() {
        Scanner in = new Scanner(System.in);
        Validate validate = new Validate();
        System.out.println();
        System.out.print("Enter Candidate ID: ");
        this.candidateId = validate.getInt("Enter Candidate ID: ", "Invalid ID", 0, Integer.MAX_VALUE);
        System.out.print("Enter First name: ");
        this.firstName = in.nextLine();
        System.out.print("Enter Last name: ");
        this.lastName = in.nextLine();
        do {
            this.birthDate = validate.getInt("Enter Birth date: ", "Invalid year", 0, Integer.MAX_VALUE);
        } while (!validate.isYear(1900, this.birthDate));
        System.out.print("Enter Address: ");
        this.address = in.nextLine();
        do {
            System.out.print("Enter Phone: ");
            this.phone = in.nextLine();
        } while (!validate.isPhone(this.phone));
        do {
            System.out.print("Enter Email: ");
            this.email = in.nextLine();
        } while (!validate.isEmail(this.email));
    }

    public int getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(int candidateType) {
        this.candidateType = candidateType;
    }

    public int getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}