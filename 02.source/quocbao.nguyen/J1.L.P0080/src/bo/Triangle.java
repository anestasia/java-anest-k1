package bo;

import utir.Validate;

import java.util.Scanner;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Triangle extends TwoDim {
    Scanner sc = new Scanner(System.in);
    private double firstEdge;
    private double secondEdge;
    private double thirdEdge;


    public Triangle() {
        firstEdge = 0;
        secondEdge = 0;
        thirdEdge = 0;
    }

    public void setFirstEdge(double firstEdge) {
        this.firstEdge = firstEdge;
    }

    public void setSecondEdge(double secondEdge) {
        this.secondEdge = secondEdge;
    }

    public void setThirdEdge(double thirdEdge) {
        this.thirdEdge = thirdEdge;
    }

    @Override
    public double getPerimeter() {
        return 0.5 * (firstEdge + secondEdge + thirdEdge);
    }

    @Override
    public double getArea() {
        double p = 0.5 * (firstEdge + secondEdge + thirdEdge);
        return Math.sqrt(p * (p - firstEdge) * (p - secondEdge) * (p - thirdEdge));
    }

    @Override
    public double getVolumn() {
        return 0;
    }

    @Override
    public void input() {
        System.out.println("Enter your triangle's edges");
        do {
            Validate validate = new Validate();
            double a, b, c = 0;
            a = validate.getDouble("The first: ", "Please enter a float number!!!");
            b = validate.getDouble("The second: ", "Please enter a float number!!!");
            c = validate.getDouble("The third: ", "Please enter a float number!!!");
            if ((a + b > c) && (a + c > b) && (b + c > a)) {
                setFirstEdge(a);
                setSecondEdge(b);
                setThirdEdge(c);
                break;
            } else System.out.println("This is not a triangle!!!! Please enter again");
        } while (true);
    }
}
