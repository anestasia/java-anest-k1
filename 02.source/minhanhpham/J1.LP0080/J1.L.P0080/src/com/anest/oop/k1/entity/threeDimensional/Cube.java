/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.oop.k1.entity.threeDimensional;

import com.anest.oop.k1.bo.Utils;

/**
 *
 * @author thienbd90@gmail.com
 */
public class Cube extends ThreeDimensionalShape {

    private double c;

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calArea() {
        return 6 * this.c * this.c;
    }

    @Override
    public double calPri() {
        return 6 * this.c;
    }

    @Override
    public void input() {
        c = Utils.getDoubleNumber("Enter edge: ", Double.MAX_VALUE, 0);
    }

    @Override
    public void output() {
        System.out.println(" Cube : \t Edge : (" + this.c + ")");
        System.out.println("\t+ Area : " + this.calArea());
        System.out.println("\t+ Pri  : " + this.calPri());
        System.out.println("\t+ Volume: " + this.volume());
    }

    @Override
    public double volume() {
        return this.c * this.c * this.c;
    }

}
