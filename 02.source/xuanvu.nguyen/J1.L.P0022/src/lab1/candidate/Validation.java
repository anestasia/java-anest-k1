/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.candidate;

import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.InternetHeaders;
import static java.lang.Character.isDigit;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author OPF_Lep
 */
public class Validation {

    public static String inputPhone() {
        String phone;
        int k = 1;
        do {
            k = 1;
            System.out.print("Input phone number: ");
            phone = new Scanner(System.in).nextLine();
            if (phone.length() < 10) {
                k = 0;
            }
            for (int i = 0; i < phone.length(); i++) {
                if (isDigit(phone.charAt(i)) == false) {
                    k = 0;
                }
            }
        } while (k == 0);
        return phone;
    }

    public static String inputEmail() {
        Matcher matcher;
        String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        String email;
        do {
            System.out.print("Input email: ");
            email = new Scanner(System.in).nextLine();
            matcher = pattern.matcher(email);
        } while (!matcher.matches());
        return email;
    }

    public static int inputExpYear() {
        int years;
        do {
            try {
                System.out.print("Input ExpYear: ");
                years = Integer.parseInt(new Scanner(System.in).nextLine());
            } catch (Exception e) {
                System.out.println("Wrong input");
                years = 101;
            }
        } while (!(years >= 0 || years <= 100));
        return years;

    }

    public static int inputBirthDate() {
        int birthDate;
        Date curr = new Date();
        do {
            System.out.print("Input birthdate: ");
            birthDate = Integer.parseInt(new Scanner(System.in).nextLine());
        } while (!(birthDate > 1900 && birthDate < 2017));
        return birthDate;
    }

    public static int inputGra_Date() {
        int graDate;
        Date curr = new Date();
        do {
            System.out.print("Input graduation date: ");
            graDate = Integer.parseInt(new Scanner(System.in).nextLine());
        } while (!(graDate > 1900 && graDate < curr.getYear()));
        return graDate;
    }

    public static String inputRank() {
        String[] arr = new String[4];
        arr[0] = "Poor";
        arr[1] = "Good";
        arr[2] = "Fair";
        arr[3] = "Excellence";
        String rank;
        int k;
        do {
            k = 0;
            System.out.print("Input graduation rank: ");
            rank = new Scanner(System.in).nextLine();
            for (int i = 0; i < 4; i++) {
                if (rank.equalsIgnoreCase(arr[i])) {
                    k = 1;
                }
            }
        } while (k == 0);
        return rank;
    }

}
