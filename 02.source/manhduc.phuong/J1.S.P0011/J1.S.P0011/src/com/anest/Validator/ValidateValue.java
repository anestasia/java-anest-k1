/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.Validator;

import com.anest.DB.InitializationThings;
import java.util.Scanner;

/**
 *
 * @author Taka
 */
public class ValidateValue {

    Scanner sc = new Scanner(System.in);

    public int baseInput;
    public int baseOutput;
    public String[] array;

    InitializationThings ini;

    public ValidateValue() {
        ini = new InitializationThings();
    }

    public InitializationThings getIni() {
        return ini;
    }

    public void setIni(InitializationThings ini) {
        this.ini = ini;
    }

    //base của số muốn chuyển
    public void inputBaseInputNumber() {
        System.out.println("Enter input's base number: ");
        baseInput = validInteger(2, 36);
        ini.setBaseInput(baseInput);
        createArrayToValidNumberInput();
    }

    //Lập một mảng chứa các phần tử là các số nằm trong giới hạn base của số bị chuyển đổi
    public void createArrayToValidNumberInput() {
        array = new String[ini.getBaseInput()];
        if (ini.getBaseInput() < 10) {
            for (int i = 0; i < ini.getBaseInput(); i++) {
                array[i] = Integer.toString(i);
            }
        } else if (ini.getBaseInput() >= 10) {
            char c = 65;
            for (int i = 0; i < 10; i++) {
                array[i] = Integer.toString(i);
            }
            for (int i = 10; i < ini.getBaseInput(); i++) {
                array[i] = Character.toString(c);
                c++;
            }
        }

    }
    
    //Lập một mảng chứa các phần tử là các số nằm trong giới hạn base của kết quả
    public void createArrayToValidNumberOutput() {
        array = new String[ini.getBaseOutput()];
        if (ini.getBaseOutput() < 10) {
            for (int i = 0; i < ini.getBaseOutput(); i++) {
                array[i] = Integer.toString(i);
            }
        } else if (ini.getBaseOutput() >= 10) {
            char c = 65;
            for (int i = 0; i < 10; i++) {
                array[i] = Integer.toString(i);
            }
            for (int i = 10; i < ini.getBaseOutput(); i++) {
                array[i] = Character.toString(c);
                c++;
            }
        }

    }

    //base của kết quả sau chuyển đổi:
    public void inputBaseOutputNumber() {
        System.out.println("Enter output's base number: ");
        baseOutput = validInteger(2, 36);
        ini.setBaseOutput(baseOutput);
    }

    //kiểm tra từng phần tử của số nhập vào có đủ điều kiện để chuyển hay không.
    public String validNumberToConvert() {
        while (true) {
            System.out.println("Enter number to convert: ");
            String number = sc.nextLine();
            int count = 0;
            //Xét từng phần tử của số nhập vào
            for (int i = 0; i < number.length(); i++) {
                boolean flag = false;
                //So sánh với từng phần tử trong mảng đã tạo
                for (int j = 0; j < this.array.length; j++) {
                    if (String.valueOf(number.charAt(i)).equals(this.array[j])) {
                        flag = true;//chữ số so sánh có tồn tại trong mảng, gán cờ = true và dừng kiểm tra
                        count++;//count để đếm xem đã xét được bao nhiêu số
                        break;
                    }
                }
                if (flag == false) {//cờ flag không hề bị thay đổi chứng tỏ số nhập vào chứa kí tự không nằm trong base input
                    System.out.println("Invalid number! Please input only number of base " + baseInput + "!");
                    break;
                }
            }
            if (count == number.length()) {//độ dài số bị chuyển bằng với số lần đã kiểm tra, thoả mãn điều kiện --> thoát & trả về số
                return number;
            }
        }
    }

    //lấy giá trị của số cần chuyển
    public void inputNumberToBeConverted() {
        String s = validNumberToConvert();
        ini.setNumber(s);
    }

    //kiem tra so integer nhap vao
    public int validInteger(int min, int max) {
        while (true) {
            try {
                int num = Integer.parseInt(sc.nextLine());
                if (num > max || num < min) {
                    System.out.println("Enter number from " + min + " to " + max);
                } else {
                    return num;
                }
            } catch (NumberFormatException e) {
                System.out.println("Please input number!");
            }
        }
    }

}
