/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edu.fpt.validate;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Validator {

    public static double validateDouble(double begin, double end, String str2) {
        while (true) {
            try {
                System.out.print(str2);
                String str = new Scanner(System.in).nextLine();
                double result = Double.parseDouble(str);
                if (result < begin || result > end) {
                    System.out.println("error, input again");
                } else {
                    return result;
                }
            } catch (Exception e) {
                System.out.println("error, input again");

            }
        }
    }

    public static int validateInt(int begin, int end,String str1) {
        while (true) {
            try {
                System.out.print(str1);
                String str = new Scanner(System.in).nextLine();
                int result = Integer.parseInt(str);
                if (result < begin || result > end) {
                    System.out.println("error, input again");
                } else {
                    return result;
                }
            } catch (Exception e) {
                System.out.println("error, input again");

            }
        }
    }
}
