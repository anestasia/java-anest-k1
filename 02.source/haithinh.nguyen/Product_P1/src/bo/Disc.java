/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Disc extends Product {

    private String genre;
    private String format;

    public Disc(String genre, String format) {
        this.genre = genre;
        this.format = format;
    }

    public Disc(int id, String category, String genre, String format, int price) {
        super(id, category, price);
        this.genre = genre;
        this.format = format;
    }

    public Disc() {
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void setInput() {
        Scanner s = new Scanner(System.in);
        super.setInput();
        System.out.print("Category : Disc");
        super.setCategory("Disc");
        System.out.print("Enter genre :");
        this.genre = s.nextLine().trim();
        System.out.print("Enter format : ");
        this.format = s.nextLine().trim();
        System.out.print("Enter price : ");
        super.setPrice(s.nextInt());
    }

    @Override
    public String toString() {
        return super.toString() + " | " + genre + " | " + format +" | "+ super.getPrice();
    }

}
