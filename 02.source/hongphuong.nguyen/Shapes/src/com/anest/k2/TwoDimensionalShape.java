/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k2;

/**
 *
 * @author Hong Phuong
 */
public  abstract class TwoDimensionalShape implements Shape{
    private double chuVi;
    private double dienTich;
    
    @Override
    public abstract void input();
     
    @Override
    public abstract double chuVi();
    
    @Override
    public abstract double dienTich();
    
}
