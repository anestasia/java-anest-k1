package com.anest.bo;

/**
 * Created by Minh on 1/14/2017.
 */
public abstract class ThreeDimensionalShape implements IntShape{
    @Override
    public void input() {
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getVolume() {
        return 0;
    }

    @Override
    public void printResult() {
    }
}
