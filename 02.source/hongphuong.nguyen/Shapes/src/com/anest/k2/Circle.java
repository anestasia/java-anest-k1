/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k2;

import java.util.Scanner;

/**
 *
 * @author Hong Phuong
 */
public class Circle extends TwoDimensionalShape {
    private double banKinh;

    public Circle()
    {
        
    }
    public Circle(double bk)
    {
        banKinh = bk;
    }    
    
    @Override
    public double chuVi() {
        return banKinh * 2 * 3.14159;
    }

    @Override
    public double dienTich() {
        return banKinh * banKinh * 3.14159;
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter the Redius: " );
        this.banKinh = s.nextDouble();
    }
    
}
