package com.anest.k1.product.entity;

import java.io.Serializable;
import java.util.Scanner;

public abstract class Product implements Serializable, Comparable<Product> {

    private int id;
    private ECat category;
    private int price;

    public Product() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Product(int id, ECat category, int price) {
        this.id = id;
        this.category = category;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ECat getCategory() {
        return category;
    }

    public void setCategory(ECat category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", category=" + category + "]";
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public int compareTo(Product other) {
        if (this.price < other.getPrice()) {        // Sap xep giam dan
            return 1;
        }
        if (this.price > other.getPrice()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        Product other = (Product) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }


    public void input() {
        System.out.print(" Id = ");
        this.id = new Scanner(System.in).nextInt();
        //System.err.println(this.id);
        System.out.print(" Price = ");
        this.price = new Scanner(System.in).nextInt();
    }

}
