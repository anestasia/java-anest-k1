package com.anest.gui;

import com.anest.bo.*;
import com.anest.util.Input;

/**
 * Created by Minh on 1/14/2017.
 */
public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        Input input = new Input();
        main.showMenu();
        int choice;
        do {
            choice = input.getInt("Enter your choice: ", "Invalid choice", 0, 8);
            switch (choice) {
                case 1:
                    Circle circle = new Circle();
                    circle.input();
                    circle.printResult();
                    break;
                case 2:
                    Square square = new Square();
                    square.input();
                    square.printResult();
                    break;
                case 3:
                    Triangle triangle = new Triangle();
                    triangle.input();
                    triangle.printResult();
                    break;
                case 4:
                    Sphere sphere = new Sphere();
                    sphere.input();
                    sphere.printResult();
                    break;
                case 5:
                    Cube cube = new Cube();
                    cube.input();
                    cube.printResult();
                    break;
                case 6:
                    Tetrahedron tetrahedron = new Tetrahedron();
                    tetrahedron.input();
                    tetrahedron.printResult();
                    break;
                case 7:
                    break;
            }
        } while (choice != 7);
    }

    public void showMenu() {
        System.out.println("1. Circle");
        System.out.println("2. Square");
        System.out.println("3. Triangle");
        System.out.println("4. Sphere");
        System.out.println("5. Cube");
        System.out.println("6. Tetrahedron");
        System.out.println("7. Exit");
    }
}
