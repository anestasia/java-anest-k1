/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Intern extends Candidate {

    private String majors;
    private String semester;
    private String universityName;
    Valid valid = new Valid();

    public Intern(String id, String firtsName, String lastName, int birthDate, String address, int phone, String email, String majors, String semester, String universityName) {
        super(id, firtsName, lastName, birthDate, address, phone, email);
        this.majors = majors;
        this.semester = semester;
        this.universityName = universityName;
        super.type = 2;
    }

    public Intern() {
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + majors + " | " + semester + " | " + universityName + " | " + type;
    }

    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        this.id = valid.setIdTest("Enter ID: ");
        System.out.println("Enter First Name: ");
        this.firtsName = s.nextLine();
        System.out.println("Enter Last Name: ");
        this.lastName = s.nextLine();
        this.birthDate = valid.setBirthDate("Enter birthdate: ");
        System.out.println("Enter address : ");
        this.address = s.nextLine();
        this.phone = valid.setPhoneTest("Enter your phone: ");
        this.email = valid.setEmail();
        System.out.println("Enter majors : ");
        this.majors = s.nextLine();
        System.out.println("Enter semester : ");
        this.semester = s.nextLine();
        System.out.println("Enter universityName : ");
        this.universityName = s.nextLine();
    }

    @Override
    public Object callDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callUpdate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object callCreat() {
        return new Intern(this.id, this.firtsName, this.lastName, this.birthDate, this.address, this.phone, this.email, this.majors, this.semester, this.universityName);
    }
}
