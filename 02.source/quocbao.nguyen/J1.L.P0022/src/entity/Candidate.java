package entity;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class Candidate {
    private String id;
    private String ltName;
    private String ftName;
    private int birthday;
    private String address;
    private String phone;
    private String email;
    private int type;

    public Candidate() {
    }

    public Candidate(String id, String ltName, String ftName, int birthday, String address, String phone, String email, int type) {
        this.id = id;
        this.ltName = ltName;
        this.ftName = ftName;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLtName() {
        return ltName;
    }

    public void setLtName(String ltName) {
        this.ltName = ltName;
    }

    public String getFtName() {
        return ftName;
    }

    public void setFtName(String ftName) {
        this.ftName = ftName;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "ID:'" + id + '\'' +
                ", Last name='" + ltName + '\'' +
                ", First Name: '" + ftName + '\'' +
                ", Birthday: " + birthday +
                ", Address: '" + address + '\'' +
                ", Phone: '" + phone + '\'' +
                ", Email: '" + email + '\'' +
                '}';
    }
}
