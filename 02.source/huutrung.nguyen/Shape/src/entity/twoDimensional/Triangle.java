/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.twoDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import bo.Utils;

public class Triangle extends TwoDimensionalShape {

    /*
	 * Three sides of a triangle
     */
    private double a;
    private double b;
    private double c;

    /**
     * Constructor without parameters
     */
    public Triangle() {
        super();
        this.a = 0;
        this.b = 0;
        this.c = 0;
    }

    /**
     * Constructor with three sides .
     *
     * @param a
     * @param b
     * @param c
     */
    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return the a
     */
    public double getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(double a) {
        this.a = a;
    }

    /**
     * @return the b
     */
    public double getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(double b) {
        this.b = b;
    }

    /**
     * @return the c
     */
    public double getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calArea() {
        double haftPri = calPri() / 2;
        return Math.sqrt(haftPri * (haftPri - a) * (haftPri - b) * (haftPri - c));
    }

    @Override
    public double calPri() {
        return this.a + this.b + this.c;
    }

    @Override
    public void input() {
        boolean check = true;
        do {
            this.a = Utils.getDoubleNumber("Enter Triangle a: ", Double.MAX_VALUE, 0);
            this.b = Utils.getDoubleNumber("Enter Triangle b: ", Double.MAX_VALUE, 0);
            this.c = Utils.getDoubleNumber("Enter Triangle c: ", Double.MAX_VALUE, 0);
            if ((this.a < this.b + this.c) && (this.b < this.a + this.c) && (this.c < this.a + this.b)) {
                check = false;
            } else {
                System.out.println("Invalid input! Enter again: ");
            }
        } while (check);
    }

    @Override
    public String toString() {
        return "Triangle's Area: " + calArea() + " Triangle's Pri: " + calPri();
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.Shape#output()
     */
    @Override
    public void output() {
        System.out.println(" Triangle : \t Three sides : (" + this.a + ", " + this.b + ", " + this.c + ")");
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Pri  :" + this.calPri());
    }

}
