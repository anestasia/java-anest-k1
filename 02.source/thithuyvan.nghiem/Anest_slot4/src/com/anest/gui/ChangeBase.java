package com.anest.gui;

import java.util.Scanner;

public class ChangeBase {

    public static void baseBinDecHex(int inBase, int outBase) {
        if (inBase == 1) {
            if (outBase == 2) {
                binToDec();
            }

            if (outBase == 3) {
                binToHex();
            }}
        if(inBase ==2 ) {
            if(outBase==1) decToBin();
            if(outBase==3) decToHex();
        }
        if(inBase ==3) {
            if(outBase==1) hexToBin();
            if(outBase==2) hexToDec();
        }
                   

    }

    public static int inputDec() {
        System.out.print("Enter a Integer > 0: ");
        Scanner in = new Scanner(System.in);
        int n;
        int sign;
        while (true) {
            try {sign=1;
                n = Integer.parseInt(in.nextLine());
                if (n < 0) {
                    System.out.print("Enter a Integer >0 :");
                    sign=0;
                }
               if(sign==1) return n;
            } catch (Exception e) {
                System.out.print("Enter a Integer > 0");
            }
        }
    }

    public static void decToHex() {
        int dec = inputDec();
        System.out.println("Convert " + dec + "(Dec)=" + Integer.toHexString(dec) + "(Hex)");
    }

    public static void decToBin() {
        int dec = inputDec();
        System.out.println("Convert: " + dec + "(Dec)=" + Integer.toBinaryString(dec) + "(Bin)");
    }

    public static String inputHex() {
        System.out.print("Enter a Hexadecimal: ");
        Scanner in = new Scanner(System.in);
        String hex = "";
        while (true) {
            try {
                hex = in.nextLine();
                int dec = Integer.parseInt(hex, 16);
                // System.out.println("***" + dec);
                return hex;
            } catch (Exception e) {
                System.out.print("Enter a Hexadecimal:");
            }
        }

    }

    public static void hexToBin() {

        String hex = inputHex();
        int dec = Integer.parseInt(hex, 16);
        System.out.println("Convert:  " + hex + "(Hex)=" + Integer.toBinaryString(dec) + "(Bin)");
    }

    public static void hexToDec() {
        String hex = inputHex();
        System.out.println("Convert:  " + hex + "(Hex)=" + Integer.parseInt(hex, 16) + "(Dec)");
    }

    public static String inputBin() {
        System.out.print("Enter a Binary: ");
        Scanner in = new Scanner(System.in);
        String bin = "";
        while (true) {
            try {
                bin = in.nextLine();
                int dec = Integer.parseInt(bin, 2);
                System.out.println("---" + dec);
                return bin;
            } catch (Exception e) {
                System.out.print("Enter a Binary:");
            }
        }

    }

    public static void binToHex() {

        String bin = inputBin();
        int dec = Integer.parseInt(bin, 2);
        System.out.println("Convert:  " + bin + "(Bin)=" + Integer.toHexString(dec) + "(Hex)");
    }

    public static void binToDec() {
        String bin = inputBin();
        System.out.println("Convert:  " + bin + "(Bin)=" + Integer.parseInt(bin, 2) + "(Dec)");
    }

//    public static void main(String[] args) {
//        binToHex();
//        binToDec();
//       hexToBin();
//        hexToDec();
//        decToBin();
//        decToHex();
//    }
}
