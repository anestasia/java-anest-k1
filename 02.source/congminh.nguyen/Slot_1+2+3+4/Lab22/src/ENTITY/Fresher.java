/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENTITY;

/**
 *
 * @author Cong Minh
 */
public class Fresher extends Candidate{
    
    private String graduationDate;
    private String graduationRank;
    private String university;
    
        public Fresher() {
    }

    public Fresher(String id, String firstName,String lastName,
            int birthDate,String address, String phone, String email,
            String graduationDate, String graduationRank, 
            String university, Type type) {
       super(id, firstName, lastName, birthDate, address, phone, email, type);
        this.graduationDate = graduationDate;
        this.graduationRank = graduationRank;
        this.university = university;
    }

    public String getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(String graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getGraduationRank() {
        return graduationRank;
    }

    public void setGraduationRank(String graduationRank) {
        this.graduationRank = graduationRank;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }
     
   
    
    
}
