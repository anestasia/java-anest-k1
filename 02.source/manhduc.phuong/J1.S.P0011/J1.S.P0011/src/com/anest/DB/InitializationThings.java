/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.DB;

/**
 *
 * @author Taka
 */
public class InitializationThings {//khởi tạo lung tung
    private int baseInput;
    private int baseOutput;
    private String number;
    private String result;


    public InitializationThings() {
    }    
    
    
    public int getBaseInput() {
        return baseInput;
    }

    public void setBaseInput(int baseInput) {
        this.baseInput = baseInput;
    }

    public int getBaseOutput() {
        return baseOutput;
    }

    public void setBaseOutput(int baseOutput) {
        this.baseOutput = baseOutput;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    
    
    @Override
    public String toString() {
        return "The result of converting number " + number + " from base " +baseInput+ " to  base " + baseOutput + " is: " + result;
    }
      
}
