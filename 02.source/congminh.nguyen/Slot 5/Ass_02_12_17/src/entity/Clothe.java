/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;
import entity.Category;
/**
 *
 * @author Cong Minh
 */
public class Clothe extends Product{
    private String department;
    private String brand;

    public Clothe() {
    }
    
    public Clothe(String department, String brand,  
            String id, Category category, double price) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
       
    }

    public String getDepartment() {
        return department;
    }

    public String getBrand() {
        return brand;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
}
