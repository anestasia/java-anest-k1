package bo;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public abstract class ThreeDim implements Shape {
    public abstract double getVolumn();

    public abstract double getArea();

    public abstract void input();
}
