/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Triagle extends TwoDimensionalShape {

    private double a;
    private double b;
    private double c;

    public Triagle() {
    }
    
    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }
 
    @Override
    public double calArea() {
        return a + b + c;
    }

    @Override
    public double calPri() {
        return Math.sqrt((a + b + c) * (a + b - c) * (a + c - b) * (b + c - a) / 16);
    }

    @Override
    public void inPut() {
        System.out.println(" Trigle:");
        System.out.println("Enter a= ");
        this.a = new Scanner(System.in).nextDouble();
        System.out.println("Enter b= ");
        this.b = new Scanner(System.in).nextDouble();
        System.out.println("Enter c= ");
        this.c = new Scanner(System.in).nextDouble();
        if ((a + b) <= c || ((b + c) <= a) || ((a + c) <= b)) {
            System.out.println("a b c don't exits Trigle");
            a = 0;
            b = 0;
            c = 0;
        }
    }

}
