package utir;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by W10-PRO on 14-Jan-17.
 */
public class Validate {
    public double getDouble(String mes, String error) {
        Scanner sc = new Scanner(System.in);
        double number = 0;
        while (true) {
            try {
                System.out.println(mes);
                number = sc.nextDouble();
                if (number <= 0) {
                    sc.nextLine();
                    System.out.println(error);
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }

    public int getInt(String mes, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        while (true) {
            try {
                System.out.println(mes);
                number = sc.nextInt();
                if (number < min || number > max) {
                    System.out.println("Please enter 1 - 7 !!!");
                    sc.nextLine();
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }
}
