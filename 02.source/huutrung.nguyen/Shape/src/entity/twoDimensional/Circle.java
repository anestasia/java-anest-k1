/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.twoDimensional;

/**
 *
 * @author Monkey D.Luffy
 */
import bo.Utils;

public class Circle extends TwoDimensionalShape {

    private double radius;

    /**
     * Constructor without parameters
     */
    public Circle() {
        this.radius = 0.0;
    }

    /**
     * @param radius
     */
    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.Shape#output()
     */
    @Override
    public void output() {
        // TODO Auto-generated method stub
        System.out.println(" Circle : \t Radius : (" + this.radius + ")");
        System.out.println("\t+ Area :" + this.calArea());
        System.out.println("\t+ Pri  :" + this.calPri());
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#calArea()
     */
    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        return Math.pow(this.radius, 2) * Math.PI;
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#calPri()
     */
    @Override
    public double calPri() {
        // TODO Auto-generated method stub
        return 2 * this.radius * Math.PI;
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.twoDimensional.TwoDimensionalShape#input()
     */
    @Override
    public void input() {
        // TODO Auto-generated method stub
        this.radius = Utils.getDoubleNumber("Enter radius : ", Double.MAX_VALUE, 0);
    }

}
