/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.bo;

import candidate.until.Input;
import candidate.until.Validate;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Fresher extends Deverloper {

    private int graduationDate;
    private String rankOfGraduation;
    private String university;

    public Fresher() {

    }

    @Override
    public void input() {
        super.input();
        this.setCandidateTpye(1);
        Scanner in = new Scanner(System.in);
        Input input = new Input();
        Validate validate = new Validate();
        do {
            this.graduationDate = (int) input.getNumber("Enter graduation date: ", "Invalid years", 0, Double.MAX_VALUE);
        } while (!validate.isYear(1980, this.graduationDate));
        do {
            System.out.print("Rank of Graduation: with one of 4 values (Excellence, Good, Fair, Poor): ");
            this.rankOfGraduation = in.nextLine().trim();
        } while (!validate.isRankOfGraduation(this.rankOfGraduation));
    }

    @Override
    public void showInfor() {
        super.showInfor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showName() {
        super.showName(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(int graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getRankOfGraduation() {
        return rankOfGraduation;
    }

    public void setRankOfGraduation(String rankOfGraduation) {
        this.rankOfGraduation = rankOfGraduation;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

}
