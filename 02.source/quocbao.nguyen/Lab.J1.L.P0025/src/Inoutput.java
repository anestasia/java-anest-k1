import java.io.*;

/**
 * Created by W10-PRO on 28-Jan-17.
 */
public class Inoutput {
    public String input() {
        String str = "";
        try {
            //Bước 1: Tạo đối tượng luồng và liên kết nguồn dữ liệu
            File f = new File("input.txt");
            FileReader fr = new FileReader(f);
            String temp;
            //Bước 2: Đọc dữ liệu
            BufferedReader br = new BufferedReader(fr);
            while ((temp = br.readLine()) != null) {
                str = str.concat(temp);
            }

            //Bước 3: Đóng luồng
            fr.close();
            br.close();
        } catch (Exception e) {
        }
        return str;
    }

    public void output(String str) {
        try {
            //Bước 1: Tạo đối tượng luồng và liên kết nguồn dữ liệu
            File f = new File("output.txt");
            FileWriter fw = new FileWriter(f);

            //Bước 2: Ghi dữ liệu
            fw.write(str);

            //Bước 3: Đóng luồng
            fw.close();
        } catch (IOException e) {
        }
    }
}
