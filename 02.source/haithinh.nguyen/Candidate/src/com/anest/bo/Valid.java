/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Thinh
 */
public class Valid {

    Scanner sc = new Scanner(System.in);

    public int setPhoneTest(String s) {
        System.out.print(s);
        int num;
        while (true) {
            String phoneNumber = sc.nextLine();
            if (phoneNumber.length() == 10) {
                try {
                    num = Integer.parseInt(phoneNumber);
                    return num;
                } catch (Exception e) {
                    System.out.println("Input number please...");
                }
            } else {
                System.out.println("Invalid input! Phone is number with minimum 10 characters");
            }
        }
    }

    public String convert(String str) {
        String tmp = "";
        tmp += String.valueOf(str.charAt(0)).toUpperCase();
        tmp += str.substring(1);
       return tmp;
    }

    public String setIdTest(String mess) {
        while (true) {
            System.out.println(mess);
            String str = sc.nextLine();
            str = str.trim();
            if (str.contains(" ")) {
                System.out.println("Invalid ID! Enter again: ");
            } else {
                str = convert(str);
                return str;
            }
        }
    }

    public int setBirthDate(String mess) {
        int birth;
        while (true) {
            System.out.println(mess);
            String birthDate = sc.nextLine();
            try {
                birth = Integer.parseInt(birthDate);
                if (birth < 1900 || birth > 2016) {
                    System.out.println("Invalid ID! Enter again: ");
                } else {
                    return birth;
                }
            } catch (Exception e) {
                System.out.println("Input number please...");
            }
        }
    }

    public int setExpInYear(String mess) {
        while (true) {
            int expinyear;
            while (true) {
                System.out.println(mess);
                String expIn = sc.nextLine();
                try {
                    expinyear = Integer.parseInt(expIn);
                    if (expinyear < 0 || expinyear > 100) {
                        System.out.println("Invalid ID! Enter again: ");
                    } else {
                        return expinyear;
                    }
                } catch (Exception e) {
                    System.out.println("Input number please...");
                }
            }
        }
    }

    public static String setEmail() {
        Matcher matcher;
        String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        String email;
        do {
            System.out.print("Enter Email : ");
            email = new Scanner(System.in).nextLine();
            matcher = pattern.matcher(email);
        } while (!matcher.matches());
        return email;
    }

    public static int setGraDate() {
        int graDate;
        Date curr = new Date();
        do {
            System.out.print("Input graduation date: ");
            graDate = Integer.parseInt(new Scanner(System.in).nextLine());
        } while (!(graDate > 1900 && graDate < curr.getYear()));
        return graDate;
    }

    public static String setRank() {
        String[] arr = new String[4];
        arr[0] = "Poor";
        arr[1] = "Good";
        arr[2] = "Fair";
        arr[3] = "Excellence";
        String rank;
        int k;
        do {
            k = 0;
            System.out.print("Input graduation rank: ");
            rank = new Scanner(System.in).nextLine();
            for (int i = 0; i < 4; i++) {
                if (rank.equalsIgnoreCase(arr[i])) {
                    k = 1;
                }
            }
        } while (k == 0);
        return rank;
    }
}
