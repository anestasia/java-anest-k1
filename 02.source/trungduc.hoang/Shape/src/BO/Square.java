/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Scanner;

/**
 *
 * @author Hoang Trung Duc
 */
public class Square extends TwoDimensons {

    private int a;

    public Square() {
    }

    public Square(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public void getArea() {
        System.out.println("DIen tich la: " + a * a);
    }

    @Override
    public void getParameter() {
        System.out.println("Chu vi la: " + 4 * a);
    }

    @Override
    public void input() {
        common.Input in = new common.Input();
        String s;
        do {
            do {
                System.out.println("Nhap canh:");
                Scanner sc = new Scanner(System.in);
                s = sc.nextLine();
            } while (!in.isNumber(s));
            setA(Integer.parseInt(s));
        } while (a <= 0);
    }

    @Override
    public void getV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
