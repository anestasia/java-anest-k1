/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.util.Scanner;

/**
 *
 * @author Hoang Trung Duc
 */
public class Cube extends ThreeDimensons {

    int a;

    public Cube(int a) {
        this.a = a;
    }

    public Cube() {
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public void getV() {
        System.out.println("V = " + a * a * a);
    }

    public void getParameter() {
        System.out.println("S = " + 6 * a * a);
    }

    @Override
    public void input() {
        common.Input in = new common.Input();
        String s;
        do {
            do {
                System.out.println("Nhap canh:");
                Scanner sc = new Scanner(System.in);
                s = sc.nextLine();
            } while (!in.isNumber(s));
            setA(Integer.parseInt(s));
        } while (a <= 0);
    }

}
