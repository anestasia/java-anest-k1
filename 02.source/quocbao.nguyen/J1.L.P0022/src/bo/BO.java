package bo;

import entity.Candidate;
import entity.Experience;
import entity.Fresher;
import entity.Intern;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class BO {
    ArrayList<Candidate> candi = new ArrayList<>();
    Validate val = new Validate();
    Scanner sc = new Scanner(System.in);

    public void getExperience() {
        do {
            Experience tp = new Experience();
            System.out.print("Enter ID: ");
            tp.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            tp.setFtName(sc.nextLine());
            System.out.print("Enter Last name: ");
            tp.setLtName(sc.nextLine());
            tp.setBirthday(val.getInt("Enter Birth year: ", "Please enter a year like 19xx!!! ", 1900, Integer.MAX_VALUE));
            System.out.print("Enter Address: ");
            tp.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (val.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers!!!");
                System.out.println("");
            } while (true);
            tp.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (val.isEmail(mail)) break;
                System.out.println("Please enter an email!!!");
            } while (true);
            tp.setEmail(mail);
            tp.setType(0);
            tp.setExpInYear(val.getInt("Enter Year of experience: ", "Error, enter 0-100", 0, 100));
            System.out.print("Rank of Graduation: ");
            tp.setProSkill(sc.nextLine());
            candi.add(tp);
        } while (val.YNquestion());
    }

    public void getFresher() {
        do {
            Fresher tp = new Fresher();
            System.out.print("Enter ID: ");
            tp.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            tp.setFtName(sc.nextLine());
            System.out.print("Enter Last name: ");
            tp.setLtName(sc.nextLine());
            tp.setBirthday(val.getInt("Enter Birth year: ", "Please enter a year like 19xx!!! ", 1900, Integer.MAX_VALUE));
            System.out.print("Enter Address: ");
            tp.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (val.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers!!!");
                System.out.println("");
            } while (true);
            tp.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (val.isEmail(mail)) break;
                System.out.println("Please enter an email!!!");
            } while (true);
            tp.setEmail(mail);
            tp.setType(1);
            System.out.print("Enter Graduation date: ");
            tp.setGraduation_date(sc.nextLine());
            String rankOfGraduation;
            do {
                System.out.print("Enter Rank of Graduation: ");
                rankOfGraduation = sc.nextLine();
            } while (!val.isRank(rankOfGraduation));
            tp.setGraduation_rank(rankOfGraduation);
            System.out.print("Enter Education: ");
            tp.setEducation(sc.nextLine());
            candi.add(tp);
        } while (val.YNquestion());
    }

    public void getIntern() {
        do {
            Intern tp = new Intern();
            System.out.print("Enter ID: ");
            tp.setId(sc.nextLine());
            System.out.print("Enter First name: ");
            tp.setFtName(sc.nextLine());
            System.out.print("Enter Last name: ");
            tp.setLtName(sc.nextLine());
            tp.setBirthday(val.getInt("Enter Birth year: ", "Please enter a year like 19xx!!! ", 1900, Integer.MAX_VALUE));
            System.out.print("Enter Address: ");
            tp.setAddress(sc.nextLine());
            String phone;
            do {
                System.out.print("Enter Phone: ");
                phone = sc.nextLine();
                if (val.isNumberic(phone)) break;
                System.out.print("Please enter a phone with at least 10 numbers!!!");
                System.out.println("");
            } while (true);
            tp.setPhone(phone);
            String mail;
            do {
                System.out.print("Enter Email: ");
                mail = sc.nextLine();
                if (val.isEmail(mail)) break;
                System.out.println("Please enter an email!!!");
            } while (true);
            tp.setEmail(mail);
            tp.setType(2);
            System.out.print("Enter Major: ");
            tp.setMajors(sc.nextLine());
            System.out.print("Enter Semester: ");
            tp.setSemester(sc.nextLine());
            System.out.print("Enter University: ");
            tp.setUniName(sc.nextLine());
            candi.add(tp);
        } while (val.YNquestion());
    }

    public void searching() {
        System.out.print("Enter First or Last Name: ");
        String searchingName = sc.nextLine();
        int type = val.getInt("Enter Type: ", "Error! Please enter 0-2", 0, 2);
        for (int i = 0; i < candi.size();i++){
            if(candi.get(i).getType()==type&&(candi.get(i).getFtName().contains(searchingName)||candi.get(i).getLtName().contains(searchingName)))
                System.out.println(candi.get(i).getFtName() + " " + candi.get(i).getLtName());
        }
    }
}

