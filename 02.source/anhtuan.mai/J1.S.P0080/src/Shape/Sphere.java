package Shape;

public class Sphere extends Shape {

    private double radius;

    public Sphere() {
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void calPer() {
        System.out.println("Tinh chu vi" + 2 * Math.PI * radius);
    }

    @Override
    public void calArea() {
        System.out.println("Em chiu :|");
    }

    @Override
    public void calV() {
        System.out.println("Em chiu :|");
    }
}
