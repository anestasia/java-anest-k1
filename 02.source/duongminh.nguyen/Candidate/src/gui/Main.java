package gui;

import bo.Candidate;
import bo.Experience;
import bo.Fresher;
import bo.Intern;
import common.Common;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Candidate> candidate = new ArrayList<>();
        int choice = 0;
        while (choice != 5) {
            Display.menu();
            choice = Display.getInt("Please enter your choice", "Invalid choice, Please re-enter", 1, 5);
                switch (choice) {
                    case 1:
                        do {
                            Experience e = new Experience();
                            e.input(e);
                            candidate.add(e);
                        } while (Display.pressYN());
                        break;
                    case 2:
                        do {
                            Fresher f = new Fresher();
                            f.input(f);
                            candidate.add(f);
                        } while (Display.pressYN());
                        break;
                    case 3:
                        do {
                            Intern i = new Intern();
                            i.input(i);
                            candidate.add(i);
                        } while (Display.pressYN());
                        break;
                    case 4:
                        Display.displayCandidate(candidate);
                        Display.searchCandidate(candidate);
                        break;
                    case 5:
                        break;
                }
            }
        }
}
