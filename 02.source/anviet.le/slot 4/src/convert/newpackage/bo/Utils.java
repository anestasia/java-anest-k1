/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convert.newpackage.bo;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class Utils {

    public static Scanner scanner = new Scanner(System.in);

    /**
     * Press anykey to continue
     *
     * @return <code>true</code> if user want to continue "Y/y",
     * <code>false</code>
     */
    //check continue
    public static boolean checkContinue() {
        boolean check;
        while (true) {
            System.out.print("Do you want to continue? Y/N?: ");
            String c = scanner.nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                check = true;
                return check;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                System.out.println("EXIT_SUCCESS");
                check = false;
                return check;
            } else {
                System.out.println("Invalid input!!! Enter again: ");
            }
        }
    }

    /**
     * The function to get an integer from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public static int getIntNumber(String mes, String error, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        while (true) {
            try {
                System.out.print(mes);
                number = sc.nextInt();
                if (number < min || number > max) {
                    System.out.println(error);
                    sc.nextLine();
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                sc.nextLine();
            }
        }
        return number;
    }

    /**
     * The function to get a double from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return a double from keyboard
     */
    public static double getDoubleNumber(String mess, double max, double min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                double result = Double.parseDouble(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
//validate hexnumber
  public static boolean isHex(String str) {
        boolean check;
        try {
            
            int t = Integer.parseInt(str, 16);
            
            check = true;
        } catch (NumberFormatException e) {
             System.out.println("Please enter hex number! Enter again: ");
            check = false;
        }
        return (check);
    }
  public static boolean isBinary(String str) {
        char[] arr = str.toCharArray();
        for (char checkBinary : arr) {
            if (!(checkBinary == '1' || checkBinary == '0')) {
                return false;
            }
        }
        return true;
    }

    public static boolean isDecimal(String str) {
        for (int i = 1; i < str.length() - 1; i++) {
            if (!(str.charAt(i) >= '0' && str.charAt(i) <= '9')) return false;
        }
        return true;
    }
}
