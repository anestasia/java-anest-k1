/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.bo;

import candidate.until.Input;
import java.util.Scanner;

/**
 *
 * @author Trinh Dai
 */
public class Experience extends Deverloper {

    private double expInYear;
    private String proSkill;

    public Experience() {
    }

    @Override
    public void showInfor() {
        super.showInfor(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showName() {
        super.showName(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void input() {
        super.input();
        this.setCandidateTpye(0);
        Scanner in = new Scanner(System.in);
        Input input = new Input();
        this.expInYear = input.getNumber("Enter years of experience: ", "Invalid experience", 0, 100);
        System.out.print("Enter professional skill: ");
        this.proSkill = in.nextLine().trim();
    }

    public double getExpInYear() {
        return expInYear;
    }

    public void setExpInYear(double expInYear) {
        this.expInYear = expInYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }
}
