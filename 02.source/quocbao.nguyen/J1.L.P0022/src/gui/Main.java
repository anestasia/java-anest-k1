package gui;

import bo.BO;

/**
 * Created by W10-PRO on 22-Jan-17.
 */
public class Main {
    public static void main(String[] args) {
        BO bo = new BO();
        Menu menu = new Menu(5);
        menu.add("Experience");
        menu.add("Fresher");
        menu.add("Internship");
        menu.add("Searching");
        menu.add("Exit");
        int choice;
        do {
            System.out.println("\nPERSON MANAGER");
            choice = menu.getChoice();
            switch (choice) {
                case 1:
                    bo.getExperience();
                    break;
                case 2:
                    bo.getFresher();
                    break;
                case 3:
                    bo.getIntern();
                    break;
                case 4:
                    bo.searching();
                    break;
            }
        } while (choice >= 1 && choice < 5);
    }
}
