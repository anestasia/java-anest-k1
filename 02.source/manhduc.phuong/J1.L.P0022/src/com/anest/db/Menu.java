package com.anest.db;

import com.anest.Validator.ValidValue;
import java.util.ArrayList;

public class Menu {
    ArrayList<Candidate> list;
    ValidValue vl = new ValidValue(list);

    public Menu(ArrayList<Candidate> list) {
        this.list = list;
    }
      
    public void displayComboMenu() {
        System.out.println("CANDIDATE MANAGEMENT SYSTEM\n"
                + "1.	Experience\n"
                + "2.	Fresher\n"
                + "3.	Internship\n"
                + "4.	Searching\n"
                + "5.	Exit");
    }

    public void loopEnterCandidateValue(int choice) {
        boolean check;
        do {
            switch (choice) {
                case 1:
                    Experience exp = new Experience(0);
                    exp.setExperienceInfo(list);
                    list.add(exp);
                    break;
                case 2:
                    Fresher fr = new Fresher(1);
                    fr.setFreasherInfo(list);
                    list.add(fr);
                    break;
                case 3:
                    Intern in = new Intern(2);
                    in.setInternInfo(list);
                    list.add(in);
                    break;
                default:
                    break;
            }
            check = vl.checkContinue();
        } while (check);
        if (check == false) {
            printOutList();
        }
    }

    public void chooseACase() {
        boolean check;
        do {
            
            displayComboMenu();
            System.out.println("Please enter your choice from 1 to 5: ");
            int choice = vl.setInt(5, 1);
            switch (choice) {
                case 1:
                    loopEnterCandidateValue(1);
                    break;
                case 2:
                    loopEnterCandidateValue(2);
                    break;
                case 3:
                    loopEnterCandidateValue(3);
                    break;
                case 4:
                    printOutList();
                    searchCandidate(list);
                    break;
                case 5:
                    System.out.println("EXIT_SUCCESS!!!");
                    System.exit(0);
                default:
                    return;
            }
            check =vl.checkContinue();
        } while (check == true);
        if (check == false) {
            printOutList();
        }
    }

    public void printOutList() {
        boolean checkE = false;
        boolean checkF = false;
        boolean checkI = false;
        if (list.isEmpty() == true) {
            System.out.println("The list is empty!");
        } else {
            System.out.println("The List: ");
            System.out.println("===========EXPERIENCE CANDIDATE============");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCandidatetype() == 0) {
                    System.out.println(list.get(i).getFirstName() + list.get(i).getLastName());
                    checkE = true;
                }
            }
            if (checkE == false) {
                System.out.println("EXPERIENCE CANDIDATE's list is Empty~!");
            }
            System.out.println("==========FRESHER CANDIDATE==============");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCandidatetype() == 1) {
                    System.out.println(list.get(i).getFirstName() + list.get(i).getLastName());
                    checkF = true;
                }
            }
            if (checkF == false) {
                System.out.println("FRESHER CANDIDATE's list is Empty~!");
            }
            System.out.println("===========INTERN CANDIDATE==============");
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCandidatetype() == 2) {
                    System.out.println(list.get(i).getFirstName() + list.get(i).getLastName());
                    checkI = true;
                }
            }
            if (checkI == false) {
                System.out.println("INTERN CANDIDATE's list is Empty~!");
            }
        }
    }

    public void searchCandidate(ArrayList<Candidate> list) {
        printOutList();
        System.out.println("Input Candidate name (First name or Last name): ");
        String name = vl.setString();
        System.out.println("Input type of candidate: ");
        int type = vl.setInt(2, 0);
        boolean check = false;
        for (int i = 0; i < list.size(); i++) {
            if ((list.get(i).getFirstName().contains(name) == true || list.get(i).getLastName().contains(name) == true) && list.get(i).getCandidatetype() == type) {
                System.out.println(list.get(i));
                check = true;
            }
        }
        if (check == false) {
            System.out.println("Can't find anythings");
        }
    }

}
