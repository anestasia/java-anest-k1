/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo;

import entity.Product;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Taka
 */
public class Validation {

    Scanner sc = new Scanner(System.in);

    public double setDouble(String mess, double min, double max) {
        while (true) {
            System.out.println(mess);
            try {
                double num = Double.parseDouble(sc.nextLine());
                if (num > max || num < min) {
                    System.err.println("Please enter number from " + min + " to " + max + "!!! ");
                    continue;
                }
                return num;
            } catch (NumberFormatException e) {
                System.err.println("Please input number!!! Enter again: ");
            }
        }
    }

    public int setInteger(String mess, int min, int max) {
        while (true) {
            System.out.println(mess);
            try {
                int num = Integer.parseInt(sc.nextLine());
                if (num > max || num < min) {
                    System.err.println("Please enter number from " + min + " to " + max + "!!! ");
                    continue;
                }
                return num;
            } catch (NumberFormatException e) {
                System.err.println("Please input number!!! Enter again: ");
            }
        }
    }

    public String setString(String mess) {
        System.out.println(mess);
        String string = sc.nextLine();
        string = string.replaceAll("\\s+", " ");
        return string;
    }

    //check xem item vừa tạo có tồn tại trong cửa hàng hay chưa
    public boolean checkDuplicateItem(ArrayList<Product> list_old, Product product) {
            if (list_old.contains(product)) {
                System.err.println("Duplicate item!!! Please create another item!");
                return false;
            }
        return true;
    }

    public boolean checkContinue(String mess) {
        while (true) {
            String c;
            System.out.println(mess);
            c = sc.nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                return true;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                return false;
            } else {
                System.err.println("Invalid input!!! Enter again: ");
            }
        }
    }
    
    public char checkUpdateOrDelete(String mess) {
        while (true) {
            String c;
            System.out.println(mess);
            c = sc.nextLine();
            if ((c.equals("U") == true) || (c.equals("u") == true)) {
                return 'U';
            } else if ((c.equals("D") == true) || (c.equals("d") == true)) {
                return 'D';
            } else {
                System.err.println("Invalid input!!! Enter again: ");
            }
        }
    }

}
