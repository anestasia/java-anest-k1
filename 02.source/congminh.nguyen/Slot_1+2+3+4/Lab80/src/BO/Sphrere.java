/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cong Minh
 */
public class Sphrere extends ThreeDimensionalShape{
    
    private double r;

    public Sphrere() {
    }

    public Sphrere(double r) {
        this.r = r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    
    
    @Override
    public double calAre() {
       return 4*Math.PI*this.r*this.r;
    }

    @Override
    public double calPri() {
       return 0;
    }

    @Override
    public double volume() {
       return 4/3*Math.PI*Math.pow(r, 3);
    }

    @Override
    public void input() {
        this.r = Validator.inputDouble("Enter r: ", 0);
    }
    
   
}
