/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.util.Scanner;

/**
 *
 * @author Thinh
 */
public class Circle extends TwoDimensionalShape {
    private double r;
    
    public Circle() {
        
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        return this.r * this.r * Math.PI;
    }

    @Override
    public double calPri() {
        return 2* this.r * Math.PI;
    }
    
    @Override
    public void inPut() {
        System.out.println(" Circle:");
        System.out.println("enter r= ");
        this.r = new Scanner(System.in).nextDouble();
    }

}
