/**
 * Created by W10-PRO on 28-Jan-17.
 */
public class Editor {
    public String trimSpace(String str) {
        str = str.replaceAll(",", " , ");
        str = str.replaceAll("\\.", " . ");
        str = str.replaceAll(";", " ; ");
        str = str.replaceAll(":", " : ");
        str = str.replaceAll("\"", " \" ");
        str = str.replaceAll("\\s+", " ");
        str = str.replaceAll("(^\\s+|\\s+$)", "");
        str = str.replaceAll(" ,", ",");
        str = str.replaceAll(" \\.", ".");
        str = str.replaceAll(" ;", ";");
        str = str.replaceAll(" :", ":");
        int count = 0;
        return str;
    }

    public String upperCaseAfterDot(String str) {
        StringBuilder sb = new StringBuilder(str);
        int count = 0;
        String nextChar = "";
        sb.replace(0, 1, Character.toString(Character.toUpperCase(sb.charAt(0))));
        if (Character.toString(sb.charAt(sb.length() - 1)).equals("."))
            sb.deleteCharAt(sb.length() - 1);
        for (int i = 1; i < sb.length(); i++) {
            //Dau ngoac kep
            if (Character.toString(sb.charAt(i)).equals("\"")) {
                count++;
                if (count % 2 == 1) sb.deleteCharAt(i + 1);
                else sb.deleteCharAt(i - 1);
            }
            //Dau .
            if (Character.toString(sb.charAt(i)).equals(".")) {
                sb.replace(i + 2, i + 3, Character.toString(Character.toUpperCase(sb.charAt(i + 2))));
            }
        }
        sb.append(".");
        return sb.toString();
    }
}
