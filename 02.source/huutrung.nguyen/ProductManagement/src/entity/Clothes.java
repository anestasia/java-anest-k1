/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Scanner;

/**
 *
 * @author Monkey D.Luffy
 */
public class Clothes extends Product {

    private String department;
    private String brand;

    public Clothes() {
    }

    public Clothes(String id, ECat category, String department, String brand, int price) {
        super(id, category, price);
        this.department = department;
        this.brand = brand;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return super.getId() + " | " + super.getCategory() + " | " + 
                department + " | " + brand + " | " + super.getPrice();
    }

    @Override
    public void input() {
        super.input();
        super.setCategory(ECat.CLOTHES);
        System.out.print("Department: ");
        department = new Scanner(System.in).nextLine();
        System.out.print("Brand: ");
        brand = new Scanner(System.in).nextLine();
    }
    
}
