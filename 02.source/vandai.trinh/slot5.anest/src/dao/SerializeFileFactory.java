/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template SerializeFileFactory, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Book;
import entity.Clothing;
import entity.Disc;
import entity.Product;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Trinh Dai
 */
public class SerializeFileFactory {

    public static boolean luuFile(ArrayList<Product> list, String path) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            BufferedWriter bw = new BufferedWriter(osw);
            int size = list.size();
            String s = String.valueOf(size);
            bw.write(s);
            bw.newLine();
            for (Product pr : list) {
                System.out.println(pr.toString());
                bw.write(pr.toString());
                bw.newLine();
            }
            bw.close();
            osw.close();
            fos.close();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static ArrayList<Product> docFile(String path) {
        ArrayList<Product> list = new ArrayList<Product>();
        try {
            FileInputStream fis = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            int size = Integer.parseInt(br.readLine());
            System.out.println(size);
            for (int i = 0; i < size; i++) {
                String line = br.readLine();
                String[] arr = line.split("|");
                if (isType(arr[0].trim()) == 1) {
                    Book book = new Book(arr[0].trim(), Integer.parseInt(arr[1].trim()), arr[2].trim(), arr[3].trim(), arr[4].trim());
                    list.add(book);
                }
                if (isType(arr[0].trim()) == 2) {
                    Clothing clothing = new Clothing(arr[0].trim(), Integer.parseInt(arr[1].trim()), arr[2].trim(), arr[3].trim(), arr[4].trim());
                    list.add(clothing);
                }
                if (isType(arr[0].trim()) == 3) {
                    Disc disc = new Disc(arr[0].trim(), Integer.parseInt(arr[1].trim()), arr[2].trim(), arr[3].trim(), arr[4].trim());
                    list.add(disc);
                }
            }
            br.close();
            isr.close();
            fis.close();
        } catch (Exception ex) {
            System.err.println("File not exit");;
        }
        return list;
    }

    public static boolean write(ArrayList<Product> list, String path){
        try {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.close();
            fos.close();
            return true;
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    public static ArrayList<Product> read(String path) {
        ArrayList<Product> list = new ArrayList<Product>();
        try {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object data = ois.readObject();
            list = (ArrayList<Product>)data;
            ois.close();
            fis.close();
            return list;
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

//    if book return 1;
//    if clothing return 2;
//    if disc return 3
    private static int isType(String string) {
        if (string.compareToIgnoreCase("Book") == 0) {
            return 1;
        }
        if (string.compareToIgnoreCase("Clothing") == 0) {
            return 2;
        }
        if (string.compareToIgnoreCase("Disc") == 0) {
            return 3;
        }
        return 0;
    }
}
