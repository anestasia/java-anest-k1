/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.dbo;

import com.anest.validator.ValidDouble;

/**
 *
 * @author Cpt
 */
public class Triangle implements Shape {

    double a;
    double b;
    double c;

    public Triangle() {
    }

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public double calArea() {
        return Math.sqrt(calPri() * (calPri() - this.a) * (calPri() - this.b) * (calPri() - this.c));
    }

    @Override
    public double calPri() {
        return this.a + this.b + this.c;
    }

    @Override
    public void input() {
        boolean check = true;
        do{
            this.a = ValidDouble.getDoubleNumber("Enter Triangle a: ", Double.MAX_VALUE, 0);
            this.b = ValidDouble.getDoubleNumber("Enter Triangle b: ", Double.MAX_VALUE, 0);
            this.c = ValidDouble.getDoubleNumber("Enter Triangle c: ", Double.MAX_VALUE, 0);
            if ((this.a < this.b + this.c) && (this.b < this.a + this.c) && (this.c < this.a + this.b)) {
                check = false;
            } else {
                System.out.println("Invalid input! Enter again: ");
            }
        }while(check);
    }

        @Override
        public String toString
        
            () {
        return "Triangle's Area: " + calArea() + " Triangle's Pri: " + calPri();
        }

    }
