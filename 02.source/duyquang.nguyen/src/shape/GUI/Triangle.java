/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.GUI;

import java.util.Scanner;
import shape.bo.TwoDimensionShape;

/**
 *
 * @author Eo Mo
 */
public class Triangle extends TwoDimensionShape {

    private double a;
    private double b;
    private double c;

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    @Override
    public double calArea() {
        double p = (this.a + this.b + this.c) / 2;
        return p * (p - a) * (p - b) * (p - c);
    }

    @Override
    public double calPri() {
        return this.a + this.b + this.c;
    }

    @Override
    public void input() {
        System.out.println("nhap vao cac canh:");
        System.out.println("a: " + this.a);
        System.out.println("b: " + this.b);
        System.out.println("c: " + this.c);
        this.a = new Scanner(System.in).nextDouble();
        this.b = new Scanner(System.in).nextDouble();
        this.c = new Scanner(System.in).nextDouble();
    }

}
