/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import BO.CanType;
import BO.Candidate;
import BO.Validator;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class Searching extends Candidate{
    
    public void SearchingCandidate(ArrayList<Candidate> a)
    {
        Input input = new Input();
        Scanner scan = new Scanner(System.in);
        Validator validate = new Validator();
        String s;
        int x;
        String sa = null;
        System.out.println("Input the First or Last name of Candidate:  ");
        s = scan.nextLine().trim();
        System.out.println(" The Candidate type : ");
        System.out.println("0. Experience");
        System.out.println("1. Fresher");
        System.out.println("2. Intern"); 
        x = (int)input.getNumber("Please choose the type(0-2): ", "Invalid Input! Please enter number only", 0, 2);
        switch(x)
        {
            case 0: 
                 sa = CanType.Experience.toString();
                break;
            case 1: 
                sa = CanType.Fresher.toString();
                break;
            case 2: 
                sa = CanType.Intern.toString();
                break;            
                
        }
        for (Candidate candidate : a) {
            System.out.println("The candidates found : ");
            if(candidate.getFirstName().contains(s) || candidate.getLastName().contains(s)|| validate.checkCanType(sa)==true)
            {
                System.out.println(candidate.getLastName()+""+candidate.getFirstName()+"|"+candidate.getDateOfBirth()+"|"+candidate.getAddress()+"|"+candidate.getEmail()+"|"+candidate.getPhone()+
                        "|"+sa);
            }
        }
    }
}
