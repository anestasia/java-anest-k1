package bo;

public class Experience extends Candidate {

    private int yearOfExp;
    private String proSkill;

    public Experience() {
    }

    public Experience(int yearOfExp, String proSkill, int birthDate, String phone,
            String email, String firstName, String lastName, String address) {
        super(birthDate, phone, email, firstName, lastName, address);
        super.setCanType((byte) 0);
        this.yearOfExp = yearOfExp;
        this.proSkill = proSkill;
    }

    public int getYearOfExp() {
        return yearOfExp;
    }

    public void setYearOfExp(int yearOfExp) {
        this.yearOfExp = yearOfExp;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + yearOfExp + " | " + proSkill;
    }
}