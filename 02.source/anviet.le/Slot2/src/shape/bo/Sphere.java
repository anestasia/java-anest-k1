
package shape.bo;


import com.edu.Validate.Validator;
import java.util.Scanner;

public class Sphere extends ThreeDimensionalShapes {

    private double r;

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public Sphere() {
    }
 
    public Sphere(double r) {
        this.r = r;
    }

    @Override
    public double calPri() {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double volume() {
       return 4/3*Math.PI*this.r*this.r*this.r;
    }

    @Override
    public double calArea() {
        return 4*Math.PI*this.r*this.r;
    }

    @Override
    public void input() {
        System.out.print("Enter r=");
        this.r = Validator.validateDouble();
    }

}
