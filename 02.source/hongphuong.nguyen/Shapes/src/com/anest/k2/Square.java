/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.k2;

import java.util.Scanner;

/**
 *
 * @author Hong Phuong
 */
public class Square extends TwoDimensionalShape {

    private double edges;
    
    public Square() {
        
    }
    
    public Square (double edges) {
        this.edges = edges;
    }
    
    @Override
    public double chuVi() {
        return this.edges * 4;
    }

    @Override
    public double dienTich() {
        return this.edges * edges;
    }
    @Override
    public void input() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter the Edges of Square: ");
        this.edges = s.nextDouble();
    }
    
}
