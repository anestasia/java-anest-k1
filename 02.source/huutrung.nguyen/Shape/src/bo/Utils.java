
package bo;

import java.util.Scanner;

public class Utils {

    private static Scanner scanner = new Scanner(System.in);

    /**
     * Press anykey to continue
     *
     * @return <code>true</code> if user want to continue "Y/y",
     * <code>false</code>
     */
    public static boolean checkContinue() {
        boolean check;
        while (true) {
            System.out.print("Do you want to continue? Y/N?: ");
            String c = scanner.nextLine();
            if ((c.equals("Y") == true) || (c.equals("y") == true)) {
                check = true;
                return check;
            } else if ((c.equals("N") == true) || (c.equals("n") == true)) {
                System.out.println("EXIT_SUCCESS");
                check = false;
                return check;
            } else {
                System.out.println("Invalid input!!! Enter again: ");
            }
        }
    }

    /**
     * The function to get an integer from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return an integer from keyboard
     */
    public static int getIntNumber(String mess, int max, int min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                int result = Integer.parseInt(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }

    /**
     * The function to get a double from the keyboard in console application
     *
     * @param mess : The <code>message</code>
     * @param max : maximum
     * @param min : minimum
     * @return a double from keyboard
     */
    public static double getDoubleNumber(String mess, double max, double min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                double result = Double.parseDouble(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }

}
