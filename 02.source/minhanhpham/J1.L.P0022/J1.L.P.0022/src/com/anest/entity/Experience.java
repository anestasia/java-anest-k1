/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.entity;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class Experience extends Candidate {

    private int expInYear;
    private String proSkill;
    Condition valid = new Condition();

    public Experience() {
        super.type = 1;
    }

    public Experience(String Candidate_ID, String First_Name, String Last_Name, int BirthDate, String Address, String Phone, String email, int expInYear, String proSkill) {
        super(Candidate_ID, First_Name, Last_Name, BirthDate, Address, Phone, email);
        this.expInYear = expInYear;
        this.proSkill = proSkill;
        super.type = 1;
    }

    public int getExpInYear() {
        return expInYear;
    }

    public void setExpInYear(int expInYear) {
        this.expInYear = expInYear;
    }

    public String getProSkill() {
        return proSkill;
    }

    public void setProSkill(String proSkill) {
        this.proSkill = proSkill;
    }
    
    @Override
    public String toString(){
        return super.toString() + " | " + expInYear + " | " + proSkill + " | " + type;
    }
    
    
    @Override
    public void Create(List<Candidate> can){
        super.Create(can);
        expInYear = valid.ExpInYear(super.getBirthDate());
        System.out.println("Enter your professional skill:");
        proSkill = in.nextLine();
    }


}
