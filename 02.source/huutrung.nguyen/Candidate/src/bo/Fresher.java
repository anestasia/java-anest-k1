package bo;


public class Fresher extends Candidate {

    private int gradTime;
    private String gradRank;
    private String education;

    public Fresher() {
    }

    public Fresher(int gradTime, String gradRank, String education, int birthDate,
            String phone, String email, String firstName, String lastName, String address) {
        super(birthDate, phone, email, firstName, lastName, address);
        super.setCanType((byte) 1);
        this.gradTime = gradTime;
        this.gradRank = gradRank;
        this.education = education;
    }

    public int getGradTime() {
        return gradTime;
    }

    public void setGradTime(int gradTime) {
        this.gradTime = gradTime;
    }

    public String getGradRank() {
        return gradRank;
    }

    public void setGradRank(String gradRank) {
        this.gradRank = gradRank;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + gradTime + " | " + gradRank + " | " + education;
    }
}
