package Shape;
public class Cube extends Shape {

    private double edge;

    public Cube() {
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    public void setEdge(double edge) {
        this.edge = edge;
    }

    public double getEdge() {
        return edge;
    }

    @Override
    public void calPer() {
        System.out.println("Tinh chu vi 1 mat cua hinh lap phuong" + edge * 4);
    }

    @Override
    public void calArea() {
        System.out.println("Em chiu :|");
    }

    @Override
    public void calV() {
        System.out.println("Em chiu :|");
    }

}
