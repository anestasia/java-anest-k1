/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity.threeDimensional;

import entity.Shape;

/**
 *
 * @author Cpt
 */
public abstract class ThreeDimensionalShape implements Shape {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.Shape#calArea()
	 */
	@Override
	public abstract double calArea();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.Shape#input()
	 */
	@Override
	public abstract void input();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.anest.oop.k1.entity.Shape#output()
	 */
	@Override
	public abstract void output();

	/**
	 * the function calculate volume of Three dimensional shape
	 * 
	 * @return volume
	 */
	public abstract double calVol();
}
