/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import util.Input;
import java.util.Scanner;

/**
 *
 * @author KienKeng
 */
public class getCandidateType {
    String s;

    public getCandidateType() {
    }

    public getCandidateType(String s) {
        this.s = s;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }
    
    public void getType()
    {
        String s = null;
        int x;
        Input input = new Input();
        Scanner scan = new Scanner(System.in); 
        System.out.println(" The Candidate type : ");
        System.out.println("0. Experience");
        System.out.println("1. Fresher");
        System.out.println("2. Intern"); 
        x = (int)input.getNumber("Please choose the type(0-2): ", "Invalid Input! Please enter number only", 0, 2);
        switch(x)
        {
            case 0: 
                s = CanType.Experience.toString();
             
                break;
            case 1: 
                s = CanType.Fresher.toString();
           
                break;
            case 2: 
                s = CanType.Intern.toString();
             
                break;                  
        }
        
        System.out.println(""+s);

    }
}
