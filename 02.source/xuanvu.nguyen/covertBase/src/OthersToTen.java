
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OPF_Lep
 */
public class OthersToTen {

    static int base;
    static String base36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static StringBuffer s = new StringBuffer("");

    public static int convert(String str, int base) {
        int n = 0;
        int j = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if (charToNum(str.charAt(i)) == -1) {
                return -1;
            }
            n += charToNum(str.charAt(i)) * Math.pow(base, j);
            j++;
        }
        return n;
    }

    public static int charToNum(char c) {
        String base36b = base36.toLowerCase();
        for (int i = 0; i < 36; i++) {
            if (c == base36.charAt(i) || c == base36b.charAt(i)) {
                return i;
            }
        }
        return -1;
    }

    public static String inputNumber(int base) {
        String num;
        int k = 0;
        do {
            k = 1;
            System.out.print("Input number: ");
            num = new Scanner(System.in).nextLine();
            for (int i = 0; i < num.length(); i++) {
                if (charToNum(num.charAt(i)) >= base || charToNum(num.charAt(i)) == -1) {
                    k = 0;
                }
                if (k == 0) {
                    System.out.println("Wrong number");
                }
            }
        } while (k == 0);
        return num;
    }
}
