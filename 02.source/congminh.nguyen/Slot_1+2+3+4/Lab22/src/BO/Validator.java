/////*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package BO;

import ENTITY.Candidate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ENTITY.Graduation;
import java.util.ArrayList;
/**
 *
 * @author Cong Minh
 */
public class Validator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public boolean birthChecker(int birthDate) {
        return (birthDate >= 1900 && birthDate <= 2017);
    }

    public boolean phoneChecker(String phone, ArrayList<Candidate> candidate) {
        for (int i = 0; i < phone.length(); i++) {
            if (!Character.isDigit(phone.charAt(i))) {
                return false;
            }
        }
        if (phone.length() < 10) {
            return false;
        }
        for(int i=0;i<candidate.size();i++){
            if(phone.equals(candidate.get(i).getId())) return false;
        }
        return true;
    }

    public boolean mailChecker(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean experienceYearChecker(int year, int birthDate) {
        if (year >= (2017 - birthDate)) {
            return false;
        }
        if (year < 0) {
            return false;
        }
        return true;
    }

    public boolean graduationChecker(String graduation) {
        Graduation[] x = Graduation.values();
        for(int i=0;i<x.length;i++){
            if(graduation.equalsIgnoreCase(x[i].toString())) return true;
        }
        return false;
    }

    public static String normalizeString(String str) {
        str = str.trim();
        str = str.toLowerCase();
        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

    public boolean stringChecker(String name) {
        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean dateChecker(String date,int birthDate) {
        SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");
        fm.setLenient(false);
        try {
            Date date1 = fm.parse(date);
            int year = Integer.parseInt( date.substring(6));
            if(year<=(birthDate+5)) return false;
        return true;
        } catch (Exception e) {
//            System.out.println("wrong");
        return false;
        }
    }
    public boolean idChecker(String id, ArrayList<Candidate> candidate){
        for(int i=0;i<candidate.size();i++){
            if(id.equals(candidate.get(i).getId())) return false;
        }
        return true;
    }
}


