/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dao.SerializeFileFactory;
import entity.Book;
import entity.Clothing;
import entity.Disc;
import entity.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import static javafx.application.Platform.exit;

/**
 *
 * @author Trinh Dai
 */
public class Test {

    static ArrayList<Product> list = new ArrayList<>();

    //create menu 
    public static void menu() {
        System.out.println("WELCOME TO PRODUCT MANAGEMENT");
        System.out.println("1. Create");
        System.out.println("2. Load from file");
        System.out.println("3. Sort (by price) and Find (by id)");
        System.out.println("4. Update / Delete");
        System.out.println("5. Report and save.");
        System.out.println("6. Exit and save");
        int choice;
        System.out.print("Enter your choice: ");
        choice = new Scanner(System.in).nextInt();
        System.out.println("\n\n");
        switch (choice) {
            case 1:
                createData();
                break;
            case 2:
                loadFile();
                break;
            case 3:
                sort();
                break;
            case 4:
                updateOrDelete();
                break;
            case 5:
                reportAndSave();
                break;
            case 6:
                exit();
            default:
                break;

        }
    }

    //create new book clothing disc
    private static void createData() {
        menu2();
    }

    //    support funtions createData()
    // create 1 book
    // create 2clothing
    // create 3 disc
    private static void menu2() {
        do {
            System.out.println("1. Add Book");
            System.out.println("2. Add Clothing");
            System.out.println("3. Add Disc");
            int choice;
            System.out.print("Enter your choice: ");

            choice = new Scanner(System.in).nextInt();
            System.out.println("\n\n");
            switch (choice) {
                case 1:
                    do {
                        System.out.println("1. Add Book");
                        Book book = new Book();
                        book.input();
                        list.add(book);
                        System.out.println("1 san pham da dc them vao");
                        for (Product ds : list) {
                            if (ds.getCategory().equalsIgnoreCase("book")) {
                                System.out.println(ds);
                            }
                        }
                    } while (pressYN());
                    break;
                case 2:
                    do {
                        System.out.println("2. Add Clothing");
                        Clothing clothing = new Clothing();
                        clothing.input();
                        list.add(clothing);
                        System.out.println("1 san pham da dc them vao");
                        for (Product ds : list) {
                            if (ds.getCategory().equalsIgnoreCase("clothing")) {
                                System.out.println(ds);
                            }
                        }
                    } while (pressYN());
                    break;
                case 3:
                    do {
                        System.out.println("3. Add Disc");
                        Disc disc = new Disc();
                        disc.input();
                        list.add(disc);
                        System.out.println("1 san pham da dc them vao");
                        for (Product ds : list) {
                            if (ds.getCategory().equalsIgnoreCase("disc")) {
                                System.out.println(ds);
                            }
                        }
                    } while (pressYN());
                    break;
                default:
                    break;
            }
            System.out.println("Do you want continute to add product");
            //new Scanner(System.in).next();
        } while (pressYN());
    }
//    ===>End of menu 2 =>
//     ==> end create data
// load file from text

    private static void loadFile() {
        list = SerializeFileFactory.read("f:\\test.txt");
        for (Product ds : list) {
            System.out.println(ds);
        }
    }

// sort arraylist by price
// by Interface Comperable 
    private static void sort() {
        System.out.println("1. sort(by price)");
        System.out.println("2. find(by id)");
        int choice;
        System.out.print("Enter your choice: ");
        choice = new Scanner(System.in).nextInt();
        System.out.println("\n\n");
        do {
            switch (choice) {
                case 1:
                    Collections.sort(list);
                    System.out.println("Sap xep thanh cong roi");
                    for (Product ds : list) {
                        System.out.println(ds);
                    }
                    break;
                case 2:
                    boolean count = false;
                    System.out.println("Enter the id of produt: ");
                    int id = new Scanner(System.in).nextInt();
                    for (Product pr : list) {
                        if (id == pr.getId()) {
                            System.out.println(pr);
                            count = true;
                        }
                    }
                    if (!count) {
                        System.out.println("No found!!!");
                    }
                default:
                    break;
            }
        } while (pressYN());
    }

// delete or update    
    private static void updateOrDelete() {
        do {
            System.out.println("1. Upđate");
            System.out.println("2. Delete");
            int choice;
            System.out.print("Enter your choice: ");
            choice = new Scanner(System.in).nextInt();
            System.out.println("\n");
            for (Product pro : list) {
                System.out.println(pro);
            }
            System.out.println("Enter the id of produt: ");
            int id = new Scanner(System.in).nextInt();
            switch (choice) {
                case 1:
                    for (Product pro : list) {
                        if (pro.getId() == id) {
                            pro.input();
                        }
                    }
                    for (Product pro : list) {
                        System.out.println(pro);
                    }
                    break;
                case 2:
                    for (Product pro : list) {
                        if (pro.getId() == id) {
                            list.remove(pro);
                        }
                    }
                    for (Product pro : list) {
                        System.out.println(pro);
                    }
                    break;
                default:
                    break;
            }
        } while (pressYN());
    }

// save file
    private static void reportAndSave() {
        if (SerializeFileFactory.write(list, "f:\\test.txt")) {
            System.out.println("Save file sucessfully!");
        } else {
            System.out.println("Save file Error!!");
        }
    }
// ask client
// if  press yes to continute    

    private static boolean pressYN() {
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String s;
        System.out.print("Do you want to continue?(press y to continue)");
        s = in.nextLine().trim();
        if (s.equalsIgnoreCase("y")) {
            return true;
        } else {
            return false;
        }
    }

    //===>End of menu
    public static void main(String[] args) {
        while (true) {
            menu();
        }
    }
}
