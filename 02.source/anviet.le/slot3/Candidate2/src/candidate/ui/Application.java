/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package candidate.ui;

import candidate.bo.*;
import java.util.Scanner;

/**
 *
 * @author lean1
 */
public class Application {

    public static void main(String[] args) {
        CandidateList can = new CandidateList();
        Scanner sc=new Scanner(System.in);
        Utils ut=new Utils();
        int choice;
        do {
          
            choice=ut.getIntNumber("Input your choice:", "Input your choice(1-5)", 1, 5);
            switch (choice) {
                case 1:
                    can.getExperience();
                    break;
                case 2:
                    can.getFresher();
                    break;
                case 3:
                    can.getIntern();
                    break;
                case 4:
                    can.searching();
                    break;
            }
        } while (choice >= 1 && choice < 5);
    }
}
