package com.anest.bo;

/**
 * Created by Minh on 1/13/2017.
 */
public interface IntShape {
    public void input();

    public double getPerimeter();

    public double getArea();

    public double getVolume();

    public void printResult();
}
