
import entity.Product;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListProduct implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int quantity;
	private List<Product> products;
	/**
	 * 
	 */
	public ListProduct() {
		super();
		this.quantity = 0;
		this.products = new ArrayList<>();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param quantity
	 * @param products
	 */
	public ListProduct(int quantity, List<Product> products) {
		super();
		this.quantity = quantity;
		this.products = products;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}
	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	/**
	 * 
	 * @param product
	 */
	public void addProduct(Product product) {
		if(products.contains(product)){
			System.out.println("Duplicate!");
		} else {
			this.products.add(product);
			this.quantity++;
		}
		
	}
	
	/**
	 * 
	 * @param fileName
	 */
	public void loadFromFile(String fileName) {
		
		ListProduct listProduct;
        try {   // dat try cacth de tranh ngoai le khi tao va doc File
            FileInputStream f = new FileInputStream("products.dat"); // tao file f tro den student.dat
            ObjectInputStream inStream = new ObjectInputStream(f);  // dung de doc theo Object vao file f
            // dung inStream doc theo Object, ep kieu tra ve la Student
            listProduct = (ListProduct) inStream.readObject();
            this.quantity = listProduct.quantity;
            this.products = listProduct.products;
            inStream.close();
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
            System.out.println("Class not found");
        } catch (IOException e) {
        	e.printStackTrace();
            System.out.println("Error Read file");
        }
	}
	
	/**
	 * 
	 * @param line
	 * @return
	 */
	public void getProductFromLine(String line) {
		ListProduct listProduct;
        try {   // dat try cacth de tranh ngoai le khi tao va doc File
            FileInputStream f = new FileInputStream("products.dat"); // tao file f tro den student.dat
            ObjectInputStream inStream = new ObjectInputStream(f);  // dung de doc theo Object vao file f
            // dung inStream doc theo Object, ep kieu tra ve la Student
            listProduct = (ListProduct) inStream.readObject();
            this.quantity = listProduct.quantity;
            this.products = listProduct.products;
            inStream.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
        } catch (IOException e) {
            System.out.println("Error Read file");
        }
		/*String[] tokens = line.split("\\|");
		String category = tokens[0].trim();
		//System.err.println(tokens[1].trim());
		int id = Integer.parseInt(tokens[1].trim());
		int price = Integer.parseInt(tokens[4].trim());
		String p1 = tokens[2].trim();
		String p2 = tokens[3].trim();
		
		if(category.equalsIgnoreCase("BOOK")) {
			return new Book(id, ECat.BOOK, price, p1, p2);
		} else if(category.equalsIgnoreCase("CLOTHING")) {
			return new Clothing(id, ECat.CLOTHING, price, p1, p2);
		}
		return new DISC(id, ECat.CD, price, p1, p2);*/
	}
	
	/**
	 * 
	 * @param fileName
	 */
	public void writeFile(String fileName) {
		try {   // dat try cacth de tranh ngoai le khi tao va ghi File
            FileOutputStream f = new FileOutputStream("products.dat");   // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(this);   // ghi student theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
        	e.printStackTrace();
            System.out.println("Error Write file");
        }
	}
	/**
	 * 
	 */
	public void show() {
		for(Product product : this.products) {
			System.out.println(product.toString());
		}
	}
}
