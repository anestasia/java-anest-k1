package com.anest.gui;

import com.anest.bo.CandidateList;
import com.anest.bo.Valid;
import com.anest.entity.Candidate;
import com.anest.entity.Experience;
import com.anest.entity.Fresher;
import com.anest.entity.Intern;
import com.fptu.validator.Validator;
import java.io.IOException;
import static java.lang.System.in;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class Main {

    public static void Display(CandidateList can) {
        System.out.println("List of candidate:");
        System.out.println("===========EXPERIENCE CANDIDATE============");
        can.display(1);
        System.out.println("===========FRESHER CANDIDATE==============");
        can.display(2);
        System.out.println("===========INTERN CANDIDATE==============");
        can.display(3);
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        CandidateList candidateList = new CandidateList();
        do {
            System.out.print("CANDIDATE MANAGEMENT SYSTEM \n\t 1. Create \n\t 2. Searching \n\t 3. Delete \n\t 4. Cancel \n Your choice: ");
            int choice = Valid.getIntNumber("", 4, 1);
            Candidate candidate = null;
            switch (choice) {
                case 1:
                    System.out.println("Enter your option: \n\t 1. Experience \n\t 2. Fresher \n\t 3. Intern");
                    int choice1 = Valid.getIntNumber("", 3, 1);
                    switch (choice1) {
                        case 1:
                            candidate = new Experience();
                            break;
                        case 2:
                            candidate = new Fresher();
                            break;
                        case 3:
                            candidate = new Intern();
                            break;
                    }
                    candidate.Create(candidateList.getCandidates());
                    candidateList.add(candidate);
                    break;
                case 2:
                    Display(candidateList);
                    candidateList.searchType();
                    break;
                case 3:
                    Display(candidateList);
                    candidateList.deleteCandidate();
                    break;
                case 4:
                    return;
            }
        } while (true);

    }
}
