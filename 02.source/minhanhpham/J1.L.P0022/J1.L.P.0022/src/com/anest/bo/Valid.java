/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anest.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author MyPC
 */
public class Valid {

    static Scanner scanner = new Scanner(System.in);

    public static int getIntNumber(String mess, int max, int min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                int result = Integer.parseInt(str);
                if (result > max || result < min) {
                    System.out.println("Please enter number from " + min + " to " + max + ": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
    
    public static String InputString(String mes) throws IOException {
        String input;
        Scanner in = new Scanner(System.in);
        System.out.println(mes);
        while (true) {
            input = in.nextLine();
            if (input.isEmpty()) {
                System.out.println("Enter again:");
            } else {
                return input;
            }
        }
    }

    public String normalizeString(String str) {
        str = str.trim();
        str = str.toLowerCase();
        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }

    public boolean stringChecker(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isAlphabetic(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static double getDoubleNumber(String mess,double max,double min) {
        while (true) {
            try {
                System.out.print(mess);
                String str = scanner.nextLine();
                double result = Double.parseDouble(str);
                if(result>max || result <min){
                    System.out.println("Please enter number from " +min + " to " + max +": ");
                    continue;
                }
                return result;
            } catch (Exception e) {
                System.out.println("Please enter number! Enter again: ");
            }
        }
    }
    

}
