package bo;

import entire.Fruit;
import entire.FruitOrder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

/**
 * Created by W10-PRO on 27-Jan-17.
 */
public class Bo {
    ArrayList<Fruit> fruit = new ArrayList<>();
    Hashtable customer = new Hashtable();
    Validate val = new Validate();

    public void getFruit() {
        do {
            Fruit tp = new Fruit();
            Scanner sc = new Scanner(System.in);
            tp.setFruitID(String.valueOf(val.getInt("Enter Fruit ID: ", "Please enter a number!!!", 0, Integer.MAX_VALUE)));
            String fruitName;
            do {
                System.out.print("Enter Fruit Name: ");
                fruitName = sc.nextLine();
            } while (getPriceSelected(fruitName) >= 0);
            tp.setFruitName(fruitName);
            tp.setFruitPrice(val.getDouble("Enter Price: ", "Invalid value!!!"));
            System.out.print("Enter Origin: ");
            tp.setOrigin(sc.nextLine());
            fruit.add(tp);
        } while (val.YNquestion("Do you want to continue (Y/N)?"));
        getListFruit();
    }

    public void getOrder() {
        if (!fruit.isEmpty()) {
            Scanner sc = new Scanner(System.in);
            ArrayList<FruitOrder> order = new ArrayList<>();
            getListFruit();
            do {
                FruitOrder tp = new FruitOrder();
                String selected;
                double priceOfSelected;
                do {
                    System.out.print("You selected: ");
                    selected = sc.nextLine();
                } while (getPriceSelected(selected) < 0);
                tp.setFruitPrice(getPriceSelected(selected));
                tp.setFruitName(getFruitName(selected));
                tp.setQuantity(val.getInt("Enter quantity: ", "Invalid value!!!", 1, Integer.MAX_VALUE));
                order.add(tp);
            } while (!val.YNquestion("Do you want to order now(Y/N)?"));
            System.out.print("Enter your name: ");
            customer.put(sc.nextLine(), order);
            getListOrder();
        } else System.out.println("Nothing in Fruit List. Please create!!!");
    }

    public double getPriceSelected(String string) {
        for (int i = 0; i < fruit.size(); i++) {
            if (string.equalsIgnoreCase(fruit.get(i).getFruitName())) return fruit.get(i).getFruitPrice();
            else if (string.equalsIgnoreCase(String.valueOf(fruit.get(i).getFruitID())))
                return fruit.get(i).getFruitPrice();
        }
        return -1;
    }

    public String getFruitName(String string) {
        for (int i = 0; i < fruit.size(); i++) {
            if (string.equalsIgnoreCase(fruit.get(i).getFruitName())) return fruit.get(i).getFruitName();
            else if (string.equalsIgnoreCase(String.valueOf(fruit.get(i).getFruitID())))
                return fruit.get(i).getFruitName();
        }
        return null;
    }

    public void getListFruit() {
        System.out.println("============================");
        System.out.print("List of Fruit:\n" +
                "| ++ Item ++ | ++ Fruit Name ++ | ++ Origin ++ | ++ Price ++ |\n");
        for (int i = 0; i < fruit.size(); i++) {
            System.out.print("      " + (i + 1) + "            " + String.format("%-17s", fruit.get(i).getFruitName())
                    + String.format("%-16s", fruit.get(i).getOrigin()) + String.format("%-15s", fruit.get(i).getFruitPrice() + "$"));
            System.out.println("");
        }
    }

    public void getListOrder() {
        Enumeration names = customer.keys();
        if (customer.isEmpty()) System.out.println("Nothing!");
        else
            while (names.hasMoreElements()) {
                String str = (String) names.nextElement();
                ArrayList<FruitOrder> order = (ArrayList<FruitOrder>) customer.get(str);
                System.out.println("Customer: " + str);
                System.out.println("Product | Quantity | Price | Amount");
                double total = 0;
                for (int i = 0; i < order.size(); i++) {
                    total += order.get(i).getAmount();
                    System.out.println(String.format("%2d.%-10s", (i + 1), order.get(i).getFruitName()) +
                            String.format("%-9d", order.get(i).getQuantity()) + String.format("%-9s", String.valueOf(order.get(i).getFruitPrice() + "$"))
                            + String.format("%-7s", String.valueOf(order.get(i).getAmount()) + "$"));
                }
                System.out.println("Total: " + total + "$");
                System.out.println("==============");
            }
    }
}
