package Shape;
public class Triangle extends Shape {
    
    private double a;
    private double b;
    private double c;
    
    public double getA() {
        return a;
    }
    
    public void setA(double a) {
        this.a = a;
    }
    
    public double getB() {
        return b;
    }
    
    public void setB(double b) {
        this.b = b;
    }
    
    public double getC() {
        return c;
    }
    
    public void setC(double c) {
        this.c = c;
    }

    public Triangle() {
    }
    
    @Override
    public void calPer() {
        System.out.println("Tinh chu vi" + (a + b + c));
    }
    
    @Override
    public void calArea() {
        float p = (float) ((a + b + c) / 2);
        System.out.println("Tinh dien tich" + Math.sqrt((p - a) * (p - b) * (p - c)));
    }
    
    @Override
    public void calV() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
