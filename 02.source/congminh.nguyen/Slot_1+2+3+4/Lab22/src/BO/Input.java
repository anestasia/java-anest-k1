/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import ENTITY.Candidate;
import ENTITY.Experience;
import ENTITY.Fresher;
import ENTITY.Intern;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import ENTITY.Type;
//import ENTITY.Graduation;
/**
 *
 * @author Cong Minh
 */
public class Input {

    public static int inputInt(String mes, int max, int min) {
        Scanner sc = new Scanner(System.in);
        int input;
        while (true) {
            try {
                System.out.print(mes);
                input = sc.nextInt();
//                System.out.println(input);
                if ((input > max || input < min)) {
//                    sc.next();    
                    System.out.println("Invalid input.");
                    input = inputInt(mes, max, min);
                }
                return input;
            } catch (Exception e) {
                System.out.println("Invalid input.");
                sc.next();
            }
        }
    }

    public static String InputString(String mes) throws IOException {
        String input;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(mes);
        while (true) {
            input = br.readLine();
            if (input.isEmpty()) {
                System.out.print("Re-Enter: ");
            } else {
                return input;
            }
        }
    }

    public static Candidate creat(int type, ArrayList<Candidate> candidate) throws IOException {
        Validator t = new Validator();
        String id;
        String firstName;
        String lastName;
        int birthDate;
        String address;
        String phone;
        String email;
        String graduationDate;
        String graduationRank;
        String university;
        String major;
        String semster;
        String universityName;
        Type type1;
//        int Type = 0;
        int year;
        
        do{
        id = InputString("Enter candidate ID: ").trim();
        if(! t.idChecker(id, candidate)) System.out.println("ID has been exist.");
        }while(! t.idChecker(id, candidate));
        
        
        
        do {
            firstName = t.normalizeString(InputString("Enter first name: "));
            if (t.stringChecker(firstName) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.stringChecker(firstName) == false);
        do {
            lastName = t.normalizeString(InputString("Enter last name: "));
            if (t.stringChecker(lastName) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.stringChecker(lastName) == false);

        birthDate = inputInt("Enter year of birth date: ", 2017, 1900);

        address = t.normalizeString(InputString("Enter address: "));

        do {
            phone = t.normalizeString(InputString("Enter phone number: "));
            if (t.phoneChecker(phone,candidate) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.phoneChecker(phone,candidate) == false);

        do {
            email = t.normalizeString(InputString("Enter email: "));
            if (t.mailChecker(email) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.mailChecker(email) == false);
        
        // Creat Experience Type
        if (type == 0) {
            type1 = Type.Experience;
            do {
                year = inputInt("Enter year of experience: ", 100, 0);
                if (t.experienceYearChecker(year, birthDate) == false) {
                    System.out.println("Invalid input!");
                }
            } while (t.experienceYearChecker(year, birthDate) == false);

            return (new Experience(id, firstName, lastName, birthDate, address, phone,
                    email, year, type1));
            
         // Creat Fresher Type
        } else if (type == 1) {
            type1 = Type.Fresher;
            do {
                graduationDate = t.normalizeString(InputString("Enter graduation date (dd/mm/yyyy): "));
//                System.out.println(graduationDate.substring(6));
                if (t.dateChecker(graduationDate,birthDate) == false) {
                    System.out.println("Invalid input!");
                }
            } while (t.dateChecker(graduationDate,birthDate) == false);

            do {
                graduationRank = t.normalizeString(InputString("Enter graduation rank: "));
                if (t.graduationChecker(graduationRank) == false) {
                    System.out.println("Invalid input!");
                }
            } while (t.graduationChecker(graduationRank) == false);

            do {
                university = t.normalizeString(InputString("Enter name of University: "));
                if (t.stringChecker(university) == false) {
                    System.out.println("Invalid input!");
                }
            } while (t.stringChecker(university) == false);

            return (new Fresher(id, firstName, lastName, birthDate, address, phone,
                    email, graduationDate, graduationRank, university, type1));
        }
        
        // Creat Intern Type
        else {
            type1 = Type.Intern;
        do {
            major = t.normalizeString(InputString("Enter major: "));
            if (t.stringChecker(major) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.stringChecker(major) == false);

        semster = t.normalizeString(InputString("Enter semester: "));

        do {
            universityName = t.normalizeString(InputString("Enter name of University: "));
            if (t.stringChecker(universityName) == false) {
                System.out.println("Invalid input!");
            }
        } while (t.stringChecker(universityName) == false);
        return (new Intern(major, semster, universityName, id, firstName, lastName,
                birthDate, address, phone, email, type1));
        }
    }

    public static String toBeContinue() throws IOException {
        String choice;
        do {
            choice = InputString("Do you want to continue?(Y/N): ");
            if (choice.equalsIgnoreCase("Y") == false && choice.equalsIgnoreCase("n") == false) {
                System.out.println("Invalid input");
            }
        } while (choice.equalsIgnoreCase("Y") == false && choice.equalsIgnoreCase("N") == false);
        return choice;
    }
}
